<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_accesscohort
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../../config.php');
require_once('../lib.php');
require_once('../form/sendingprofile_copy_form.php');
require_once($CFG->libdir . '/formslib.php');
global $DB,$USER;
require_login(0,false);
$companyid = $USER->company->id;
//edit params
$eid = optional_param('id',null, PARAM_INT);
$edit = optional_param('edit',null, PARAM_INT);
//api key initialization
//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$context = context_system::instance();
$contextid = $context->contextlevel;
$overall = has_capability('local/phishing:overall',$context);
$copysend = has_capability('local/phishing:stdprofile',$context);
$curl_handle = curl_init();
$PAGE->set_context(context_system::instance());
if($edit==1){
	$title = get_string('editsendingprofile', 'local_phishing');
}else{
	$title = get_string('sendingprofile', 'local_phishing');
}
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot.'/local/phishing/action/sendingprofile_copy.php?id='.$eid);
require_login();
$previewnode = $PAGE->navigation->add(get_string('pluginname','local_phishing'), new moodle_url($CFG->wwwroot.'/local/phishing/sendingprofilelist.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title, new moodle_url($CFG->wwwroot.'/local/phishing/action/sendingprofile_copy.php'));
$thingnode->make_active();

$mform = new sendingprofile_copy_form();
$temp = false;
$id = 1;
if ($mform->is_cancelled()){
	redirect(new moodle_url('/local/phishing/sendingprofilelist.php', array()));
} else if ($data = $mform->get_data()) {	
	$get_sending_profile = get_sending_profile($urlip,$apikey);
	if(!empty($get_sending_profile)){		
		end($get_sending_profile);      
		$key = key($get_sending_profile);
		$id = $key+1;
	}
	if($data->ignore_cert_errors==1){
		$val =true;
	}else{
		$val = false;
	}
	$arryval= [];
	$arryval[] = array('key'=>$data->key,'value'=>$data->value);
	$curl_handle = curl_init();
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);	
	$array = array(	
			'id'                 => $id,
			'name'               => $data->name,
			'username'           => $data->username,
			'password'            => $data->password,
			'host'                => $data->host,
			'interface_type'      => $data->interface_type,
			'from_address'        => $data->from_address,
			'ignore_cert_errors'  => $val,
			'headers'=>$arryval,
		);
	
	//curl initilaization
	$data_json = json_encode($array);
	$url = $protocol.$urlip.'/api/smtp/?api_key='.$apikey;
	curl_setopt($curl_handle, CURLOPT_URL, $url);	
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl_handle, CURLOPT_POST, 1);
	//curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$data_json);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	$response  = curl_exec($curl_handle);
	$msg = get_string('cresend','local_phishing');
	if($response) {
		$decodedResponse = json_decode($response);
		if(isset($decodedResponse->id) && $decodedResponse->id != ''){
			redirect(new moodle_url($CFG->wwwroot.'/local/phishing/sendingprofilelist.php'),$msg);
		}else{
			redirect(new moodle_url($CFG->wwwroot.'/local/phishing/action/sendingprofile_copy.php'),$decodedResponse->message, 0, 'error');
		}
	} else {
		echo "error";
	}	
	if (!curl_exec($curl_handle)) {
		die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
	}		
	curl_close($curl_handle);

}
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/phishing/sendingprofilelist.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback
if($copysend||$overall){
	$mform->display();
}else{
	echo html_writer::div(
		get_string('cap', 'local_phishing'),'alert alert-danger'
	);
}

echo $OUTPUT->footer();