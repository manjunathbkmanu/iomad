 <?php
define('CLI_SCRIPT', true);

require(__DIR__.'/../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->libdir.'/cronlib.php');
require_once('lib.php');
global $DB, $CFG, $USER;  

$allmusers=$DB->get_records('user',array('deleted'=>0));
add_users_to_all_user_groups($allmusers);
add_users_to_respective_departments($allmusers);
//add_users_to_all_departments($allmusers);
add_all_users_as_group($allmusers);
delete_groups_with_nousers();


