<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/enroll_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php'); 
require_once($CFG->dirroot.'/local/course_report/csslinks.php');
global $DB,$PAGE,$USER,$CFG; 
$companyid = optional_param('companyid','',PARAM_RAW);
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_report/index.php');
$title = get_string('title', 'local_course_report');
$title = '';
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();      
include_once('jslink.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/chart.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/custom.js'), true);
$mform = new local_enroll_form();
if ($mform->is_cancelled()) {

} else if ($formdata = $mform->get_data()) {
	$status = $formdata->userid;
	$courseid = $formdata->courseid;
	$updatesql = "UPDATE {enrol}
	SET status = $status WHERE courseid = $courseid AND enrol = 'self'";
	$updatestatus = $DB->execute($updatesql);
//also upade in custom field.
	$selffieldid=$DB->get_field('customfield_field', 'id', array('shortname'=>'self_enrolment'));
	if(!empty($selffieldid)){
		$fieldval=$DB->get_field('customfield_data', 'id', array('instanceid'=>$courseid,'fieldid'=>$selffieldid));
		if(!empty($fieldval)){
			$updatesql2 = "UPDATE {customfield_data}
			SET intvalue = $status , value = $status WHERE id=$fieldval";
			$DB->execute($updatesql2);
		}
	}
	if($updatestatus)
	{
		$url= $CFG->wwwroot.'/local/course_report/enroll.php';
		redirect($url, get_string('changesmadesuccessfully', 'local_course_report'), 5);

	}
}
echo $OUTPUT->header();
//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback

$html ='';
$html .='<div class="container-fluid pt-2 mb-3 formcontainer">';
$html .=html_writer::start_div('row');
$html .=html_writer::start_div('col-md-12');
$html .=html_writer::start_tag('h3');
$html .=get_string('enableenrolmentmethod','local_course_report');
$html .=html_writer::end_tag('h3');
$html .=html_writer::end_div();
$html .=html_writer::end_div();
$html .=html_writer::start_tag('hr');

//Manju: 13/11/2020. Creating normal dropdown instead of form.
$html .=html_writer::start_div('row');
$html .=html_writer::start_div('col-md-12');
$html .=html_writer::start_div('form-group row');
$html .=html_writer::start_tag('label',array('for'=>'courseid','class'=>'mr-5 ml-5'));
$html .= get_string('selectcourse','local_course_report');
$html .=html_writer::end_tag('label');
$html .=html_writer::start_tag('select',array('name'=>'courseid','id'=>'id_courseid','onchange'=>'courseSelected();','class'=>'custom-select'));

///manju: creating course dropdown with suggestion.30/06/2020
if(!is_siteadmin()){
    $coursesql ="SELECT c.* FROM {course} AS c
    JOIN {company_course} AS cc ON cc.courseid = c.id WHERE c.visible = 1 AND c.id != 1 AND cc.companyid =$companyid ORDER BY c.fullname ASC";
}else{
    $coursesql = 'SELECT * FROM {course} WHERE visible = 1 AND id !=1 ORDER BY fullname ASC';
}
$allcourses = $DB->get_records_sql($coursesql);
$course_options = [];
$selectcourse = get_string('pleaseselectcourse', 'local_course_report');
$course_options[]=$selectcourse;
foreach($allcourses as $course){
	$coursename=$course->fullname;
	if(current_language() == 'ar'){
		$coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));
		$coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$course->id));
		$coursename = $coursenamearabic;
		if(empty($coursename)){
			$coursename =$course->fullname;
		}
	}
	$course_options[$course->id] = $coursename;
}
foreach ($course_options as $coursekey => $coursevalue) {
	$html .=html_writer::start_tag('option',array('value'=>$coursekey));
	$html .=$coursevalue;
	$html .=html_writer::end_tag('option');
}
$html .=html_writer::end_tag('select');
$html .=html_writer::end_div();
$html .=html_writer::end_div();
$html .=html_writer::end_div();

$html .='<hr>';
$html .='<div class="container-fluid p-3 button-container">
<div class="row testclass">
<div class="col-md-3">
<div class="card ">
<a href="" id="bulkenrollink" class="btn btn-primary disabled">'.get_string('bulkenrollment', 'local_course_report').'</a>
</div>
</div>
<div class="col-md-3">
<div class="card">
<a href=""  id="manualenrollink"  class="btn btn-primary disabled">'.get_string('enrollmanually', 'local_course_report').'</a>
</div>
</div>
<!--	<div class="col-md-3">
<div class="card">
<a href="'.$coursereminderlink.'" id="coursereminderlink" class="btn btn-primary">'.get_string('coursereminder', 'local_course_report').'</a>
</div>
</div> -->
<div class="col-md-3">
</div>
</div>
</div>
</div>';
echo $html;
echo $OUTPUT->footer();
