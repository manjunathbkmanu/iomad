<?php 
$string['title'] = 'رخصة إبداع';
$string['licensesettings'] = 'إعدادات الترخيص';
$string['licensecode'] = 'الترخيص';
$string['clientname'] = 'اسم العميل';
$string['save'] = 'حفظ';
$string['licenseandclient'] = 'الرجاء إدخال الترخيص واسم العميل';
$string['verify'] = 'التحقق من الترخيص';
$string['verifylicence'] = 'تحقق';
$string['invalidmessage'] = 'انتهت مدة صلاحية الترخيص الخاص بك. اتصل بشركة إبداع لتجديد رخصتك. info@ebdaa.ae';
$string['validmessage'] = 'الترخيص الخاص بك صالح';
$string['saveandverify'] = 'الحفظ والتحقق';
$string['expirydate'] = 'تاريخ انتهاء الترخيص:';