<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 * @package    local_course_management
 * @copyright  Manjunath B K<manjunathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/autoenrol_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_management/index.php');
$title = get_string('addautoenrollmentrule', 'local_course_management');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_management/js/custom.js'), true);
//getting form data here.
$mform = new local_autoenrollment_form();	
if ($data = $mform->get_data()) {
	$savedata = save_new_rule($data);
	if($savedata){
		$homeurl = $CFG->wwwroot.'/local/course_management/autoenrollment.php';
		redirect($homeurl,get_string('savedsuccessfully','local_course_management'), 1);
	}else{
		$homeurl = $CFG->wwwroot.'/local/course_management/autoenrollment.php';
		redirect($homeurl,get_string('notsaved','local_course_management'), 1);
	}
}
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
$mhtml.='</br>';
echo $mhtml;
// end of goback

//for auto enrollment rules list.
$retuurnurl=$CFG->wwwroot.'/local/course_management/autoenrollist.php';
$html='';
$html.=html_writer::start_div('float-right');
$html.=html_writer::start_tag('a',array('href'=>$retuurnurl,'class'=>'font-weight-bold','style'=>'color:black'));
$html.='<i class="fa fa-list" aria-hidden="true"></i>&nbsp;
 ';
$html.=get_string('autoenrollmentlist','local_course_management');
$html.=html_writer::end_tag('a');
$html.=html_writer::end_div('');
echo $html;
//display form
$mform->display();
echo $OUTPUT->footer();
