<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 * @package    local_course_management
 * @copyright  Manjunath B K<manjunathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/default_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_management/default_setting.php');
$title = get_string('defaultsetting', 'local_course_management');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_management/js/custom.js'), true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/local/course_management/styles.css') , true);
//getting form data here.
$mform = new default_settings_form();
$langid=$DB->get_field('config', 'id', array('name'=>'lang'));
$langvalue=$DB->get_field('config', 'value', array('id'=>$langid));
$homeid=$DB->get_field('config', 'id', array('name'=>'defaulthomepage'));
$homevalue=$DB->get_field('config', 'value', array('id'=>$homeid));
$setobject  = new stdClass();
$setobject->defaultlanguage = $langvalue;
$setobject->defaulthome = $homevalue;
$mform->set_data($setobject);
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback
$html='';
$html.=html_writer::start_tag('span',array('class'=>'notifications text-success','id'=>'statusuccess'));
$html.=html_writer::end_tag('span');
$html.=html_writer::start_tag('span',array('class'=>'notifications text-danger','id'=>'statuserror'));
$html.=html_writer::end_tag('span');
$html .='<div class="container-fluid pt-2 mb-3 formcontainer">
<div class="row"><div class="col-md-12">';
$html .= '<h3>'.get_string('dlanganddhome', 'local_course_management').'</h3>'; 
$html .='<hr>';
$html .= $mform->render(); 
$html .='</div></div></div>';
echo $html;
echo $OUTPUT->footer();
