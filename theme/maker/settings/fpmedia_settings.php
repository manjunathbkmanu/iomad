<?php 
	
	
	defined('MOODLE_INTERNAL') || die();
	
    /* Frontpage Featured Blocks */	
	$page = new admin_settingpage('theme_maker_mediablocks', get_string('mediablocksheading', 'theme_maker'));
	$page->add(new admin_setting_heading('theme_maker_mediablocksheadingsub', get_string('mediablocksheadingsub', 'theme_maker'),
            format_text(get_string('mediablocksheadingsubdesc' , 'theme_maker'), FORMAT_MARKDOWN)));
            
            
	//Enable Featured Blocks.
    $name = 'theme_maker/usemediablocks';
    $title = get_string('usemediablocks', 'theme_maker');
    $description = get_string('usemediablocksdesc', 'theme_maker');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    // Featured Section Title
    $name = 'theme_maker/mediasectiontitle';
    $title = get_string('mediasectiontitle', 'theme_maker');
    $description = get_string('mediasectiontitledesc', 'theme_maker');
    $default = 'Digital Media';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    //media Block Height
    $name = 'theme_maker/mediablockheight';
	$title = get_string('mediablockheight', 'theme_maker');
	$description = get_string('mediablockheightdesc', 'theme_maker');;
	$default = 'auto';
	$choices = array(
		    'auto' => 'auto',
	        '300px' => '300px',
	        '320px' => '320px',
	        '340px' => '340px',
	        '360px' => '360px',
	        '380px' => '380px',
	        '400px' => '400px',
	        '420px' => '420px',
	        '440px' => '440px',
	        '460px' => '460px',
		    '480px' => '480px',
		    '500px' => '500px',
	    );
	$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
	$setting->set_updatedcallback('theme_reset_all_caches');
	$page->add($setting);

	/* media Block 1 */	
    $name = 'theme_maker/mediablock1info';
    $heading = get_string('mediablock1info', 'theme_maker');
    $information = get_string('mediablock1desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
	
	$name = 'theme_maker/mediablock1title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = 'Heading One';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock1image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock1content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = 'Block 1 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    
    $name = 'theme_maker/mediablock1url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '#link1';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock1label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    /* media Block 2 */    
    $name = 'theme_maker/mediablock2info';
    $heading = get_string('mediablock2info', 'theme_maker');
    $information = get_string('mediablock2desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);   
    
	$name = 'theme_maker/mediablock2title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = 'Heading Two';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock2image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock2content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = 'Block 2 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock2url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '#link2';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock2label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    /* media Block 3 */        
    $name = 'theme_maker/mediablock3info';
    $heading = get_string('mediablock3info', 'theme_maker');
    $information = get_string('mediablock3desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
    
	$name = 'theme_maker/mediablock3title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = 'Heading Three';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock3image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock3content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = 'Block 3 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock3url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '#link3';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock3label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    /* media Block 4 */    
    $name = 'theme_maker/mediablock4info';
    $heading = get_string('mediablock4info', 'theme_maker');
    $information = get_string('mediablock4desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
    
	$name = 'theme_maker/mediablock4title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = 'Heading Four';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock4image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock4content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = 'Block 4 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock4url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '#link4';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock4label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    
    /* media Block 5 */    
    $name = 'theme_maker/mediablock5info';
    $heading = get_string('mediablock5info', 'theme_maker');
    $information = get_string('mediablock5desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
    
	$name = 'theme_maker/mediablock5title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock5image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock5image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock5content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock5url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);    
    
    $name = 'theme_maker/mediablock5label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    /* media Block 6 */   
    $name = 'theme_maker/mediablock6info';
    $heading = get_string('mediablock6info', 'theme_maker');
    $information = get_string('mediablock6desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
     
	$name = 'theme_maker/mediablock6title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock6image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock6image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock6content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock6url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);    
    
    $name = 'theme_maker/mediablock6label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    /* media Block 7 */   
    $name = 'theme_maker/mediablock7info';
    $heading = get_string('mediablock7info', 'theme_maker');
    $information = get_string('mediablock7desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
     
	$name = 'theme_maker/mediablock7title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock7image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock7image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock7content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock7url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);    
    
    $name = 'theme_maker/mediablock7label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    /* media Block 8 */ 
    $name = 'theme_maker/mediablock8info';
    $heading = get_string('mediablock8info', 'theme_maker');
    $information = get_string('mediablock8desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
       
	$name = 'theme_maker/mediablock8title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock8image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock8image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock8content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock8url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);    
    
    $name = 'theme_maker/mediablock8label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    
     /* media Block 9 */ 
     $name = 'theme_maker/mediablock9info';
    $heading = get_string('mediablock9info', 'theme_maker');
    $information = get_string('mediablock9desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
       
	$name = 'theme_maker/mediablock9title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock9image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock9image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock9content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock9url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);    
    
    $name = 'theme_maker/mediablock9label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
     
     /* media Block 10 */ 
     $name = 'theme_maker/mediablock10info';
    $heading = get_string('mediablock10info', 'theme_maker');
    $information = get_string('mediablock10desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
       
	$name = 'theme_maker/mediablock10title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock10image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock10image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock10content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock10url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);    
    
    $name = 'theme_maker/mediablock10label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
     
     /* media Block 11 */
     $name = 'theme_maker/mediablock11info';
    $heading = get_string('mediablock11info', 'theme_maker');
    $information = get_string('mediablock11desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
       
	$name = 'theme_maker/mediablock11title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock11image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock11image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock11content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock11url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);    
    
    $name = 'theme_maker/mediablock11label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
     
     /* media Block 12 */
    $name = 'theme_maker/mediablock12info';
    $heading = get_string('mediablock12info', 'theme_maker');
    $information = get_string('mediablock12desc', 'theme_maker');
    $setting = new admin_setting_heading($name, $heading, $information);
    $page->add($setting);
       
	$name = 'theme_maker/mediablock12title';
    $title = get_string('mediablocktitle', 'theme_maker');
    $description = get_string('mediablocktitledesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock12image';
    $title = get_string('mediablockimage', 'theme_maker');
    $description = get_string('mediablockimagedesc', 'theme_maker');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'mediablock12image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock12content';
    $title = get_string('mediablockcontent', 'theme_maker');
    $description = get_string('mediablockcontentdesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    $name = 'theme_maker/mediablock12url';
    $title = get_string('mediablockurl', 'theme_maker');
    $description = get_string('mediablockbuttonurldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);    
    
    $name = 'theme_maker/mediablock12label';
    $title = get_string('mediablocklabel', 'theme_maker');
    $description = get_string('mediablocklabeldesc', 'theme_maker');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
    
    
    
    // Add the page
    $settings->add($page);