<?php
// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
* Handles uploading files
*
* @package    local_ebdaa_license
* @copyright  Manjunath B K<manjunath@elearn10.com>
* @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
* @license    http://www.lmsofindia.com 2017 or later
*/
require_once '../../config.php';
require_once 'forms/update_form.php';
require_once $CFG->libdir . '/formslib.php';
require_once $CFG->dirroot. '/local/ebdaa_license/includes/lb_helper.php';
// Create a new LicenseBoxAPI helper object.
$lbapi = new LicenseBoxAPI();
global $DB, $PAGE, $USER, $CFG;
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin_license');
$PAGE->set_url($CFG->wwwroot . '/local/local_ebdaa_update/index.php');
$title = get_string('title', 'local_ebdaa_update');
//$PAGE->navbar->ignore_active();
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$update_data = $lbapi->check_update();
$mform = new local_update_form(null, array(
	'update_id' => $update_data['update_id'], 
	'has_sql' => $update_data['has_sql'], 
	'version' => $update_data['version'])
);
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback
if($update_data['status']){
	$update_html='';
	$update_html.= html_writer::start_div('message');
	$update_html.='<h3>'.$update_data['message'].'</h3><br>';
	$update_html.='<h6>'.get_string('changelog', 'local_ebdaa_update').'</h6>';
	$update_html.='<p>'.$update_data['changelog'].'</p>';
	$update_html.= html_writer::end_div();
	echo $update_html;
	if ($formdata = $mform->get_data()){
		if(!empty($formdata)){
			if(!empty($formdata->submit)){
				$lbapi->download_update(
					$formdata->update_id, 
					$formdata->has_sql, 
					$formdata->version);
			}
		}
	}else{
		$mform->display();
	}
}else{
	$html='';
	$html.= html_writer::start_div('message');
	$html.='<h4>'.get_string('noupdate', 'local_ebdaa_update').'</h4>';
	$html.='<a href="'.$CFG->wwwroot.'/local/ebdaa_update/index.php" class="btn btn-primary"><i class="fa fa-refresh"></i> '.get_string('refresh', 'local_ebdaa_update').'</a>';
	$html.= html_writer::end_div();
	echo $html;
}
echo $OUTPUT->footer();