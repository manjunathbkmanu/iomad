<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_cs_reminder
 * @copyright  Manjunath B K<manjunath@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/reminder_form.php');
require_once($CFG->libdir . '/formslib.php');
global $DB, $CFG, $USER;  
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/cs_reminder/reminder_settings.php');
$title = get_string('emailsettings', 'local_cs_reminder');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$mform = new reminder_setting_form();

//Form processing and displaying is done here
if ($mform->is_cancelled()) {

} else if ($data = $mform->get_data()) {
    $companyid = $USER->company->id;
    set_config('subjectfornewuseraccountmail',$data->subjectfornewuseraccountmail, 'emailcontent'.$companyid);

    set_config('newusermailbody', $data->newusermailbody['text'], 'emailcontent'.$companyid);

    set_config('subjectfornewuserenrollment', $data->subjectfornewuserenrollment, 'emailcontent'.$companyid);
    set_config('userenrollmentmailbody', $data->userenrollmentmailbody['text'], 'emailcontent'.$companyid);


    set_config('subjectforuserremindermail', $data->enterremindermailsubject, 'emailcontent'.$companyid);
    set_config('userremindermailbody', $data->enterreminderemailbody['text'], 'emailcontent'.$companyid);


    set_config('subjectforuserremindermailwithcron', $data->enterremindermailsubject2, 'emailcontent'.$companyid);
    set_config('userremindermailbodywithcron', $data->enterreminderemailbody2['text'], 'emailcontent'.$companyid);

    set_config('subjectforuserremindermailwithcrononeday', $data->enterremindermailsubject3, 'emailcontent'.$companyid);
    set_config('userremindermailbodywithcrononeday', $data->enterreminderemailbody3['text'], 'emailcontent'.$companyid);

    set_config('subjectfornewuseraccountmail', $data->subjectfornewuseraccountmail, ' emailcontent'.$companyid);

} else {
    $companyid = $USER->company->id;
    $setdata = get_config('emailcontent'.$companyid);
    $set = new stdClass();
    $set->subjectfornewuseraccountmail = $setdata->subjectfornewuseraccountmail;
    $set->newusermailbody['text'] = $setdata->newusermailbody;

    $set->subjectfornewuserenrollment = $setdata->subjectfornewuserenrollment;
    $set->userenrollmentmailbody['text'] = $setdata->userenrollmentmailbody;

    $set->enterremindermailsubject = $setdata->subjectforuserremindermail;
    $set->enterreminderemailbody['text'] = $setdata->userremindermailbody;

    $set->enterremindermailsubject2 = $setdata->subjectforuserremindermailwithcron;
    $set->enterreminderemailbody2['text'] = $setdata->userremindermailbodywithcron;

    $set->enterremindermailsubject3 = $setdata->subjectforuserremindermailwithcrononeday;
    $set->enterreminderemailbody3['text'] = $setdata->userremindermailbodywithcrononeday;
    $mform->set_data($set);

}
echo $OUTPUT->header();
$mform->display();
echo $OUTPUT->footer();