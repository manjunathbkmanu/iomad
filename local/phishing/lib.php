<?php
function local_phishing_extend_navigation(global_navigation $nav) {

	global $CFG,$USER;
	$context = context_system::instance();
	$overall = has_capability('local/phishing:overall',$context);
	$createtemp = has_capability('local/phishing:createtemp',$context);
	$landingpage = has_capability('local/phishing:landingpage',$context);
	//$sendingpage = has_capability('local/phishing:landingpage',$context);
	$nav->showinflatnavigation = true;
	//template
	if($overall || $createtemp){
		$abc = $nav->add(get_string('templatehrd','local_phishing'),
			$CFG->wwwroot.'/local/phishing/action/template_create.php'); 
		$abc->showinflatnavigation = true;

		$xx = $nav->add(get_string('listoftemp','local_phishing'),
			$CFG->wwwroot.'/local/phishing/templatelist.php'); 
		$xx->showinflatnavigation = true;
	}
	//landing page	
	if($overall || $landingpage){
		$abc = $nav->add(get_string('createladingpage','local_phishing'),
			$CFG->wwwroot.'/local/phishing/action/landingpage_create.php'); 
		$abc->showinflatnavigation = true;

		$xx = $nav->add(get_string('listoflandingpage','local_phishing'),
			$CFG->wwwroot.'/local/phishing/landingpagelist.php'); 
		$xx->showinflatnavigation = true;
	}
	//sending profile 
	if($overall){
		//sending profile 
		$abc = $nav->add(get_string('newsendingprofile','local_phishing'),
			$CFG->wwwroot.'/local/phishing/action/sendingprofile_create.php'); 
		$abc->showinflatnavigation = true;

		$xx = $nav->add(get_string('sendingprofilelist','local_phishing'),
			$CFG->wwwroot.'/local/phishing/sendingprofilelist.php'); 
		$xx->showinflatnavigation = true;

		//campaign
		$abc = $nav->add(get_string('createcamp','local_phishing'),
			$CFG->wwwroot.'/local/phishing/action/campaign_create.php'); 
		$abc->showinflatnavigation = true;

		$xx = $nav->add(get_string('recentcamp','local_phishing'),
			$CFG->wwwroot.'/local/phishing/campaignsumlist.php'); 
		$xx->showinflatnavigation = true;

		//user create 
		$abc = $nav->add(get_string('newusergroup','local_phishing'),
			$CFG->wwwroot.'/local/phishing/action/user_group_create.php'); 
		$abc->showinflatnavigation = true;

		$xx = $nav->add(get_string('listofuser','local_phishing'),
			$CFG->wwwroot.'/local/phishing/usergrouplist.php'); 
		$xx->showinflatnavigation = true;

	}

}
//new progress chart 
function progress_chart_val($key,$value,$i,$total,$class){
	$data = '';
	$idd= 'chart'.$i;
	$per = $total/$total*100;
	if($value==0){
		$per1 = 0;
		$value =0;
	}else{
		$per1 = $value/$total*100;
	}

	$color = '';
	switch ($class) {
		case 'emailsent':
		$color='#1abc9c';
		break;
		case 'emailopen':
		$color='#f9bf3b';
		break;
		case 'linkclick':
		$color='#f39c12';
		break;
		case 'submitteddata':
		$color='#f05b4f';
		break;

		default:
		$color='#45d6ef';
		break;
	}
	

	$data .=' 
	<div class="col-md-2 col-xs-12 col-sm-6 col-lg-2 text-center">
	<h5 class="">'.$key.'</h5>
	<div id="'.$idd.'" class="pie-title-center" data-percent="'.$per1.'"> 
	<span class="pie-value"><h3>'.$value.'</h3></span>
	</div>
	</div>';



	$data .="<script>  
	$(document).ready(function() {
		$('#".$idd."').pieChart({
			barColor: '".$color."',
			trackColor: '#eee',
			lineCap: 'round',
			lineWidth: 8,
			});
			});
			</script>";

			$data .='<style>
			.pie-title-center {
				display: inline-block;
				position: relative;
				text-align: center;
			}

			.pie-value {
				display: block;
				position: absolute;
				font-size: 14px;
				height: 40px;
				top: 50%;
				left: 0;
				right: 0;
				margin-top: -20px;
				line-height: 40px;
			}
			
			</style>';

			return $data;
		}
//get templates 
		function get_templates($urlip,$apikey){
			global $USER;
			$companyid = $USER->company->id;
			$protocol = get_config('local_phishing'.$companyid, 'protocol');
			$curl_handle = curl_init();
			$url = $protocol.$urlip.'/api/templates/?api_key='.$apikey;
			curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
// Set the curl URL option
			curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($curl_handle, CURLOPT_URL, $url);
// This option will return data as a string instead of direct output
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
// Execute curl & store data in a variable
			$curl_data = curl_exec($curl_handle);
			if (!curl_exec($curl_handle)) {
				die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
			}
			$template_datas = json_decode($curl_data);
			$template = [];
			if(!empty($template_datas)){
		foreach ($template_datas as $key => $template_data) {				# code...
			$template[$template_data->id]=$template_data->name;
		}
	}
	return $template;
}

//get landing page 

function get_landing_pages($urlip,$apikey){
	global $USER;
	$companyid = $USER->company->id;
	$protocol = get_config('local_phishing'.$companyid, 'protocol');
	$curl_handle = curl_init();
	$url = $protocol.$urlip.'/api/pages/?api_key='.$apikey;
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// Set the curl URL option
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl_handle, CURLOPT_URL, $url);
	// This option will return data as a string instead of direct output
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	// Execute curl & store data in a variable
	$curl_data = curl_exec($curl_handle);
	if (!curl_exec($curl_handle)) {
		die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
	}
	$page_datas = json_decode($curl_data);
	$pagedata = [];
	if(!empty($page_datas)){
			foreach ($page_datas as $key => $page_data) {				# code...
				$pagedata[$page_data->id]=$page_data->name;
			}
		}
		return $pagedata;
	}
//sending profile 
	function get_sending_profile($urlip,$apikey){
	global $USER;
	$companyid = $USER->company->id;	
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$curl_handle = curl_init();
		$url = $protocol.$urlip.'/api/smtp/?api_key='.$apikey;
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
	// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		if (!curl_exec($curl_handle)) {
			die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
		}
		$smtp_datas = json_decode($curl_data);
	//print_object($smtp_datas);
		$smtpdata = [];
		if(!empty($smtp_datas)){
			foreach ($smtp_datas as $key => $smtp_data) {
				$smtpdata[$smtp_data->id]=$smtp_data->name;
			}
		}
		return $smtpdata;
	}

//get groups 

	function get_groups($urlip,$apikey){
		global $DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$curl_handle = curl_init();
		$url = $protocol.$urlip.'/api/groups/?api_key='.$apikey;
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
	// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		if (!curl_exec($curl_handle)) {
			die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
		}
		$groups_datas = json_decode($curl_data);
	//print_object($smtp_datas);
		$groups_dataa = [];
		if(!empty($groups_datas)){
			foreach ($groups_datas as $key => $groups_data) {
				//check fo the groups name as email id and pic user fullname.
				$user = $DB->get_record('user',array('email'=>$groups_data->name));
				if(!empty($user)){
					$groups_data->name = fullname($user);
				}
				$groups_dataa[$groups_data->id] = $groups_data->name;
			}
		}
		asort($groups_dataa);
		return $groups_dataa;
	}
	
	//get template single 
	//here we will get single template object 
	function get_template_value($urlip,$apikey,$id){
		global $DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$curl_handle = curl_init();
		$url = $protocol.$urlip.'/api/templates/'.$id.'?api_key='.$apikey;
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		$temp_data = json_decode($curl_data);
		if(!empty($temp_data)){
			return $temp_data;
		}
		
	}
	
	//get single landing page 
	//here we will get single landing page object 
	function get_page_value($urlip,$apikey,$id){
		global $DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$curl_handle = curl_init();
		$url = $protocol.$urlip.'/api/pages/'.$id.'?api_key='.$apikey;
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		$page_data = json_decode($curl_data);
		if(!empty($page_data)){
			return $page_data;
		}
		
	}
	//get single sendingprofile
	//here we will get single sendingprofile object 
	function get_sendingprofile_value($urlip,$apikey,$id){
		global $DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$curl_handle = curl_init();
		$url = $protocol.$urlip.'/api/smtp/'.$id.'?api_key='.$apikey;
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		$smtp_data = json_decode($curl_data);
		if(!empty($smtp_data)){
			return $smtp_data;
		}		
	}
	
	//
	//get single usergroup
	//here we will get single sendingprofile object 
	function get_usergroup_value($urlip,$apikey,$id){
		global $DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$curl_handle = curl_init();
		if(!empty($id)){
			$url = $protocol.$urlip.'/api/groups/'.$id.'?api_key='.$apikey;
		}else{
			$url = $protocol.$urlip.'/api/groups/?api_key='.$apikey;
		}		
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		$groups_data = json_decode($curl_data);
		if(!empty($groups_data)){
			return $groups_data;
		}		
	}
	//get campaign summary 
	function get_campaign_summary($urlip,$apikey,$id){
		global $DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$curl_handle = curl_init();
		if(!empty($id)){
			$url = $protocol.$urlip.'/api/campaigns/'.$id.'/summary?api_key='.$apikey;
		}else{
			$url = $protocol.$urlip.'/api/campaigns/?api_key='.$apikey;
		}		
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		$campaign_data = json_decode($curl_data);
		if(!empty($campaign_data)){
			return $campaign_data;
		}
		
	}
	//get_moodle_users

	function get_moodle_users(){
		global $DB,$CFG;
		$users = [];
		$activeusers = $DB->get_records('user',array('deleted'=>0,'suspended'=>0,'confirmed'=>1));
		unset($activeusers[1]);
		unset($activeusers[2]);
		if(!empty($activeusers)){
			foreach($activeusers as $key =>  $activeuser){
				$users[$activeuser->id] = $activeuser->firstname.'-'.$activeuser->lastname.'-'.$activeuser->email;
			}
			return $users;
		}	
	}

//get campaign results 
	function get_campaign_result($urlip,$apikey,$id){
		global $DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$curl_handle = curl_init();
		if(!empty($id)){
			$url = $protocol.$urlip.'/api/campaigns/'.$id.'/results?api_key='.$apikey;
		}else{
			$url = $protocol.$urlip.'/api/campaigns/?api_key='.$apikey;
		}		
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
	// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		$campaign_data = json_decode($curl_data);
		if(!empty($campaign_data)){
			return $campaign_data;
		}

	}

//get campaign summary 
	function get_campaign($urlip,$apikey,$id){
		global $DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$curl_handle = curl_init();
		$url = $protocol.$urlip.'/api/campaigns/'.$id.'?api_key='.$apikey;		
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
	// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		$campaign_data = json_decode($curl_data);
		if(!empty($campaign_data)){
			return $campaign_data;
		}else{
			return [];
		}

	}

/*Adde by dilip
	$user : Array of users detail
	function: create groups called AllUsers and add new users if registered to this group.
*/
	function add_all_users_as_group($users){
		$usergroupdatas = get_user_groups();
		$groups = array();
		if(!empty($usergroupdatas)){
			foreach($usergroupdatas as $index => $group){
				$groups[$index] = $group->name;
			}
		}

		if(!empty($users)){
			foreach($users as $key => $userobject){
				if(!empty($userobject)){
					if(strpos($userobject->email, '@') !== false ){
						$userarray = array();
						$cerareobject = new stdClass();
						$cerareobject->email = $userobject->email;
						$cerareobject->first_name =  $userobject->firstname;
						$cerareobject->last_name =  $userobject->lastname;
						$cerareobject->position = '';
						$userarray[] = $cerareobject;
						if(!in_array($userobject->email, $groups)){
							$array = array('name'=>$userobject->email,'targets'=>$userarray);
							$jsonarray = json_encode($array);
							add_group_and_users($jsonarray);
						}
					}
				}			
			}
		}
	}

	function add_users_to_all_user_groups($users){
		$usergroupdatas = get_user_groups();
		$groups = array();
		if(!empty($usergroupdatas)){
			foreach($usergroupdatas as $index => $group){
				$groups[$index] = $group->name;
			}
		}

		if(!empty($users)){
			$userarray = [];
			foreach($users as $key => $userobject){
				if(!empty($userobject)){
					if(strpos($userobject->email, '@') !== false ){
						$cerareobject = new stdClass();
						$cerareobject->email = $userobject->email;
						$cerareobject->first_name =  $userobject->firstname;
						$cerareobject->last_name =  $userobject->lastname;
						$cerareobject->position = '';
						$userarray[] = $cerareobject;
					}
				}			
			}
		}
		if(!in_array('@All users', $groups)){
			$array = array('name'=>'@All users','targets'=>$userarray);
			$jsonarray = json_encode($array);
			add_group_and_users($jsonarray);
		}else{
			$id = '';
			if(!empty($usergroupdatas)){
				foreach($usergroupdatas as $index => $group){
					if($group->name == '@All users'){
						$id = $group->id;
					}
				}
			}

			if($id != ''){
				$array = array('id' => $id, 'name'=>'@All users','targets'=>$userarray);
				$jsonarray = json_encode($array);
				update_group_and_users($jsonarray, $id);
			}
		}

	}

/*Adde by dilip
	$user : Array of users detail
	function: create groups called user department. each departments must me added as a single group and add new users if registered to this respective department group.
*/
	function add_users_to_all_departments($users){
		global $DB,$CFG;
		$usergroupdatas = get_user_groups();
		$groups = array();
		if(!empty($usergroupdatas)){
			foreach($usergroupdatas as $index => $group){
				$groups[$index] = $group->name;
			}
		}
		$config = get_config('coursemanagement');
		$departmentsarray=$config->userdepartment;
		$exploadedarray = explode(",",$departmentsarray);
		foreach ($exploadedarray as $arraykey => $arrayvalue) {
			if($arrayvalue != ""){
				if(!empty($users)){
					$userarray = [];
					foreach($users as $key => $userobject){
						if(!empty($userobject) && $userobject->department != ''){
							if(strpos($userobject->email, '@') !== false ){
								$cerareobject = new stdClass();
								$cerareobject->email = $userobject->email;
								$cerareobject->first_name =  $userobject->firstname;
								$cerareobject->last_name =  $userobject->lastname;
								$cerareobject->position = $userobject->position;
								$userarray[] = $cerareobject;
							}
						}			
					}
				}
				if(!in_array('@All Departments', $groups)){
					$array = array('name'=>'@All Departments' ,'targets'=>$userarray);
					$jsonarray = json_encode($array);
					add_group_and_users($jsonarray);
				}else{
					$id = '';
					if(!empty($usergroupdatas)){
						foreach($usergroupdatas as $index => $group){
							if($group->name == $arrayvalue){
								$id = $group->id;
							}
						}
					}

					if($id != ''){
						$array = array('id' => $id, 'name'=>'@All Departments','targets'=>$userarray);
						$jsonarray = json_encode($array);
						update_group_and_users($jsonarray, $id);
					}
				}
			}
		}
	}
	function add_users_to_respective_departments($users){
		global $DB,$CFG;
		$usergroupdatas = get_user_groups();
		$groups = array();
		if(!empty($usergroupdatas)){
			foreach($usergroupdatas as $index => $group){
				$groups[$index] = $group->name;
			}
		}
		$config = get_config('coursemanagement');
		$departmentsarray=$config->userdepartment;
		$exploadedarray = explode(",",$departmentsarray);
		foreach ($exploadedarray as $arraykey => $arrayvalue) {
			if($arrayvalue != ""){
				if(!empty($users)){
					$userarray = [];
					foreach($users as $key => $userobject){
						if(!empty($userobject) && $userobject->department == $arrayvalue){
							if(strpos($userobject->email, '@') !== false ){
								$cerareobject = new stdClass();
								$cerareobject->email = $userobject->email;
								$cerareobject->first_name =  $userobject->firstname;
								$cerareobject->last_name =  $userobject->lastname;
								$cerareobject->position = $userobject->position;
								$userarray[] = $cerareobject;
							}
						}			
					}
				}
				if(!in_array('@'.$arrayvalue, $groups)){
					$array = array('name'=>'@'.$arrayvalue ,'targets'=>$userarray);
					$jsonarray = json_encode($array);
					add_group_and_users($jsonarray);
				}else{
					$id = '';
					if(!empty($usergroupdatas)){
						foreach($usergroupdatas as $index => $group){
							if($group->name == $arrayvalue){
								$id = $group->id;
							}
						}
					}

					if($id != ''){
						$array = array('id' => $id, 'name'=>$arrayvalue,'targets'=>$userarray);
						$jsonarray = json_encode($array);
						update_group_and_users($jsonarray, $id);
					}
				}
			}
		}
	}

	function get_user_groups(){
	//Manju: changed the plugin name from local_gophish to local_phishing.08/02/2021.
		global $DB,$USER;
		$companyid = $USER->company->id;

		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$apikey = get_config('local_phishing'.$companyid, 'api');
		$urlip = get_config('local_phishing'.$companyid, 'urlip');
		$curl_handle = curl_init();
		$url = $protocol.$urlip.'/api/groups/?api_key='.$apikey;
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		$curl_data = curl_exec($curl_handle);
		if (!curl_exec($curl_handle)) {
			die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
		}
		curl_close($curl_handle);
		$usergroupdatas = json_decode($curl_data);

		return $usergroupdatas;
	}

	function add_group_and_users($jsonarray){
	//Manju: changed the plugin name from local_gophish to local_phishing.08/02/2021.
		global $DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$apikey = get_config('local_phishing'.$companyid, 'api');
		$urlip = get_config('local_phishing'.$companyid, 'urlip');
		$url = $protocol.$urlip.'/api/groups/?api_key='.$apikey;
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl_handle, CURLOPT_URL, $url);	
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_POST, 1);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$jsonarray);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($curl_handle);

		if (!curl_exec($curl_handle)) {
			die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
		}
		curl_close($curl_handle);
	}

	function update_group_and_users($jsonarray, $id){
	//Manju: changed the plugin name from local_gophish to local_phishing.08/02/2021.
		global $DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$apikey = get_config('local_phishing'.$companyid, 'api');
		$urlip = get_config('local_phishing'.$companyid, 'urlip');
		$url = $protocol.$urlip.'/api/groups/'.$id.'?api_key='.$apikey;				
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);	
		curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$jsonarray);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($curl_handle);
		if (!curl_exec($curl_handle)) {
			die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
		}
		curl_close($curl_handle);
	}

//Manju: function to delete the groups which dont have the users.11/02/2021.
	function delete_groups_with_nousers(){
		global $DB,$USER;
		$companyid = $USER->company->id;
	//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$apikey = get_config('local_phishing'.$companyid, 'api');
		$urlip = get_config('local_phishing'.$companyid, 'urlip');
	//getting all the groups from the gophish.
		$group = get_groups($urlip,$apikey);
	//the character or string you want to search.   
		$startWithChar = '@';
	//get all items of the array starting with the specified character or string.  
		$newArray = array_filter($group, function($v) use ($startWithChar) {
			return strpos($v, $startWithChar) === 0;
		});
		$config = get_config('coursemanagement');
		$departmentsarray = $config->userdepartment;
		$exploadedarray = explode(",",$departmentsarray);
		$lmsgroups = [];
		$lmsgroups[] = "@All users";
		foreach ($exploadedarray as $arraykey => $arrayvalue) {
			$users = $DB->get_records('user',array('department'=>$arrayvalue));
			if(!empty($users)){
				$lmsgroups[] ='@'.$arrayvalue;
			}
		}
		$notpresent = [];
		foreach ($newArray as $newkey => $newvalue) {
			if (in_array($newvalue, $lmsgroups)) {
			}else{
				$notpresent[] = $newkey;
			}
		}
	//Delete groups which are not present in LMS.
		if(!empty($notpresent)){
			foreach ($notpresent as $notkey => $id) {
				$curl_handle = curl_init();
				$url = $protocol.$urlip.'/api/groups/'.$id.'?api_key='.$apikey;
				curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl_handle, CURLOPT_URL, $url);
				curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
				$curl_data = json_encode($curl_handle);
				if(!empty($id)){
					$flag =1;
					curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST,"DELETE");
					curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $curl_data);
					curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
					$response  = curl_exec($curl_handle);
					curl_close($curl_handle);
				}
			}
		}
	}