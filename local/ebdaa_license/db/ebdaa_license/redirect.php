<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_ebdaa_license
 * @copyright  Manjunath B K<manjunath@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('forms/license_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once 'includes/lb_helper.php';


// Create a new LicenseBoxAPI helper class.
$lbapi = new LicenseBoxAPI();

// Performs background license check, pass true as 1st parameter to perform periodic verifications only.
// $lcode = '2AB6-51FD-91C2-09FE';
// $clientname = 'test';
// $lb_verify_res = $lbapi->verify_license(FALSE,$lcode,$clientname);

global $DB,$PAGE,$USER,$CFG; 
require_login(true);

echo "<h1>Your license is invalid, please contact support</h1>";


