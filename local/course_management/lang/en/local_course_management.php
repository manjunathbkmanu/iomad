<?php 
$string['title'] = 'Profile';
$string['coursemanagement'] = 'Manage LMS';
$string['addcourse'] = 'Add Course';
$string['managecourses'] = 'Manage Courses and Categories'; 
$string['managecategories'] = 'Manage Categories';
$string['adduser'] = 'Add Users';
$string['uploaduser'] = 'Add Users via CSV';
$string['browseusers'] = 'Browse Users'; 
$string['pages'] = 'Pages'; 
$string['SMTP_Settings'] = 'SMTP Settings'; 
$string['LDAP_Settings'] = 'LDAP Settings'; 
$string['color_settings'] = 'Colors and Logo Settings'; 
$string['coursedashboard'] = 'Course Enrollment'; 
$string['reminder_Settings'] = 'Reminder Email Settings';
$string['adminreports'] = 'Reports';  
$string['userreport'] = 'User Reports';  
$string['coursereport'] = 'Course Reports';  
$string['license'] = 'Verify License';  
$string['addadmin'] = 'Add Admins';  
$string['update'] = 'Check for Updates';  
$string['productmanagement'] = 'Product Management';

$string['coursemanagementsettings'] = 'User Department Settings';  
$string['userdepartment'] = 'User Departments';  
$string['userdepartmentdescription'] = 'Enter User Departments';
$string['enrolmultiplecourse'] = 'Enroll User in Multiple Courses';
//manju: 
$string['addautoenrollmentrule'] = 'Auto Enrollment Rules';
$string['rulename'] = 'Rule Name';
$string['ruledescription'] = 'Rule Description';
$string['selectkey'] = 'Rule Type';
$string['selectvalue'] = 'Rule Value';
$string['selectallcourse'] = 'Select all courses';
$string['selectcourse'] = 'Select courses';
$string['submit'] = 'Submit';
$string['rulekey'] = 'Rule Key';
$string['rulevalue'] = 'Rule Value';
$string['courses'] = 'Courses';
$string['manager'] = 'Manager';
$string['all_users'] = 'All Users';
$string['department'] = 'Department';
$string['delete'] = 'Delete';
$string['deleterule'] = 'Are you sure you want to delete the autoenrollment rule ?';
$string['deleteheadding'] = 'Delete autoenrollment rule';
$string['delsuccessmsg'] = 'Deleted Successfully!';
$string['delerrormsg'] = 'Unable to delete!';
$string['matchingusers'] = 'Matching Users';
$string['firstname'] = 'First Name';
$string['lastname'] = 'Last Name';
$string['email'] = 'Email';
$string['autoenrollmentlist'] = 'Autoenrollment Rules List';
$string['back'] = 'Back';
$string['savedsuccessfully'] = 'Saved Successfully';
$string['notsaved'] = 'Saved Successfully';
$string['noaccess'] = '<h4 class="text-center">You do not have permission to access this page</h4>';
$string['gophish'] = 'Gophish';

$string['selectdefaulthome'] = 'Use case';
$string['seldefaultlanguage'] = 'Default language';
$string['defaultsetting'] = 'Default Settings';
$string['success'] = 'Successfully Saved';
$string['dlanganddhome'] = 'Default Use Case and Language Settings';
$string['reportsandreminders'] = 'Reports and Reminders';
$string['sendreminders'] = 'Send Reminders';
$string['phishing'] = 'Phishing Management';

//for certificate setting.
$string['certificatesetting'] = 'Certificate Setting';
$string['backgroundarabic'] = 'Upload arabic background';
$string['backgroundenglish'] = 'Upload english background';
$string['certificates'] = 'Certificates';
$string['downloadcertificate'] = 'Download Certificates';
$string['coursename'] = 'Course Name';
$string['username'] = 'Username';
$string['downloadcert'] = 'Download';
$string['courses'] = 'Courses';
$string['users'] = 'Users';
$string['othersettings'] = 'Other Settings';
$string['assignrole'] = 'Assign Roles';
$string['helpdescription'] = 'For multiple selection press CTRL and then select courses';

$string['defaultcase'] = 'Default';
$string['lmshomepage'] = 'LMS Homepage';
$string['lmsonly'] = 'LMS Only';
$string['SAML2_Settings'] = 'SAML2 Settings';
$string['progress_dashboard'] = 'Progress Dashboard';

$string['iomadcompanies'] = 'Iomad Companies';
$string['createcompany'] = 'Create Company';
$string['editcompany'] = 'Edit Company';
$string['managecompany'] = 'Manage Companies';
$string['managedepartments'] = 'Manage Departments';
$string['optionalprofiles'] = 'Optional Profiles';
$string['restrictcapabilities'] = 'Restrict Capabilities';
$string['emailtemplates'] = 'Email Templates';


$string['iomadusers'] = 'Iomad Users';
$string['createuser'] = 'Create User';
$string['editusers'] = 'Edit Users';
$string['departmentusersmanager'] = 'Department users and Managers';
$string['assigntocompany'] = 'Assign to Company';
$string['uploadusers'] = 'Upload Users';
$string['userbulkdownload'] = 'User bulk download';
$string['approvetrainingevents'] = 'Approve training events';
$string['mergeusers'] = 'Merge user accounts';


$string['iomadlicenses'] = 'Iomad Licenses';
$string['licensemanagement'] = 'License Management';
$string['userlicenseallocation'] = 'User License Allocation';


$string['iomadecommerce'] = 'Iomad Ecommerce';
$string['ecommercecourses'] = 'Courses';
$string['ecommerceorders'] = 'Orders';

$string['iomadmicrolearning'] = 'Iomad Microlearning Threads';
$string['microlearningthreads'] = 'Microlearning Threads';
$string['managethreadusers'] = 'Manage Thread Users';

$string['iomadcourses'] = 'Iomad Courses';
$string['createcourse'] = 'Create Course';
$string['userenrollments'] = 'User Enrolments';
$string['manageiomadcoursesettings'] = 'Manage Iomad Course Settings';
$string['managecompanygroups'] = 'Manage Company Groups';
$string['assigncoursegroups'] = 'Assign Course Groups';
$string['teachinglocations'] = 'Teaching Locations';
$string['learningpaths'] = 'Learning Paths';

$string['iomadcompetencies'] = 'Iomad Competencies';
$string['manageiomadframeworksettings'] = 'Manage Iomad Framework settings';
$string['manageiomadtemplatesettings'] = 'Manage iomad template settings';
$string['assignframeworkcompany'] = 'Assign frameworks to company';
$string['assignlearningplantemplates'] = 'Assign learning plan templates to company';
$string['competencyframeworks'] = 'Competency frameworks';
$string['learningplantemplates'] = 'Learning plan templates';




$string['iomadreports'] = 'Iomad Reports';

$string['attendancereportbycourse'] = 'Attendance report by course';
$string['companyoverviewreport'] = 'Iomad company overview report';
$string['completionreportbycourse'] = 'Completion report by course';

$string['completionreportbymonth'] = 'Completion report by month';
$string['outgoingemailreport'] = 'Outgoing email report';
$string['licenseallocationreport'] = 'License allocation report';

$string['userlicenseallocationreport'] = 'User license allocation report';

$string['userloginreport'] = 'User login report';
$string['userreport'] = 'User report';
$string['selectcompany'] = 'Select Company';

$string['openidconnect_settings'] = 'OpenID Connect Settings';
$string['brandingsettings'] = 'Branding Settings';



$string['certificatesadded'] = 'Certificates Added Successfully';
$string['certificatesupdated'] = 'Certificates Updated Successfully';

