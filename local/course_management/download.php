<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Simple Certificate block outline view.
 *
 * @package local_course_management.
 * @author Manjunath B K
 * @copyright Mihir jana (lmsofindia.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or late
 */
require_once('../../config.php');
require_once($CFG->libdir.'/pdflib.php');
require_once('lib.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
global $DB, $COURSE, $OUTPUT, $PAGE, $USER,$CFG;
$context = context_system::instance();
$PAGE->set_context($context);
$contextid = $context->contextlevel;
$userid = required_param('username',PARAM_RAW);
$courseid = required_param('coursename',PARAM_RAW);
$complete = required_param('complete',PARAM_RAW);
$lang = required_param('lg',PARAM_RAW);
//get the user fullname.
$userobj=$DB->get_record('user',array('id'=>$userid));
$username=fullname($userobj);
//get the coursename based on the language choosen.
$courseobj=$DB->get_record('course',array('id'=>$courseid));
$coursename=$courseobj->fullname;
if(current_language() == 'ar'){
	
	$coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));
	$coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$courseid));
	$coursename = $coursenamearabic;
	if(empty($coursename)){
		$coursename =$courseobj->fullname;
	}
}
//to save logo from site to pix folder
$logo = $PAGE->theme->setting_file_url('logo', 'logo');
$url_to_image = "http:".$logo;
$my_save_dir = 'pix/';
$filename = basename($url_to_image);
$complete_save_loc = $my_save_dir.$filename;

file_put_contents($complete_save_loc,file_get_contents($url_to_image));
$exarray=explode("/",$logo);
//*******************************************************
if($lang == 'ar'){
	class MYPDF extends TCPDF {
    //Page header
		public function Header() {
			global $DB, $COURSE, $OUTPUT, $PAGE, $USER,$CFG;
			require_once($CFG->dirroot . '/local/iomad/lib/company.php');
			$companyid = iomad::get_my_companyid(context_system::instance(), false);

			$context = context_system::instance();

			$fs = get_file_storage();

			$files = $fs->get_area_files($context->id, 'theme_iomad', 'arabicback', $companyid );
			if ($files) {
				foreach ($files as $file) {
					$filename = $file->get_filename();
					$filepath = ((int) $maxwidth . 'x' . (int) $maxheight) . '/';
					if ($filename != '.') {
						$setting =$CFG->wwwroot."/pluginfile.php/1/theme_iomad/arabicback/".$filepath.$companyid."/".$filename;
					}
				}
			}
        // get the current page break margin
			$bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
			$auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
			$this->SetAutoPageBreak(false, 0);
               // set bacground image
			$this->Image($setting, 0, 0, 300, 210, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
			$this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
			$this->setPageMark();
		}
	}

	// create new PDF document
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');

// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(0);
	$pdf->SetFooterMargin(0);

// remove default footer
	$pdf->setPrintFooter(false);

// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);

	}

// set font
	$pdf->SetFont('times', '', 20);

// add a page
	$pdf->AddPage('L', 'A4');
	$ext_val = strtoupper(pathinfo($exarray[8], PATHINFO_EXTENSION));
	$pdf->Image($my_save_dir.$exarray[8], 40, 15, 50, 50, $ext_val, '', '', true, 300, '', false, false, 0, false, false, false);
	// set font
	$pdf->SetFont('dejavusans', '', 35);

	$pdf->Text(185, 77, '');
	$pdf->Cell(0, 50, $username.' ', 0, false, 'R', 0, '', 0, false, 'T', 'M' );

	$pdf->SetFont('aealarabiya', '', 25);

	//$pdf->Text(210, 125, $coursename);
	$pdf->Cell(0, 103, $coursename.'  ', 0, false, 'R', 0, '', 0, false, 'T', 'M' );
	$pdf->SetFont('times', '', 16);
	$pdf->Text(220, 176, $complete);

	//Close and output PDF document
	$pdf->Output('Certificate.pdf', 'D');

}else if($lang == 'en'){
	class MYPDF extends TCPDF {
		public function Header() {
			global $DB, $COURSE, $OUTPUT, $PAGE, $USER,$CFG;
			require_once($CFG->dirroot . '/local/iomad/lib/company.php');
			$companyid = iomad::get_my_companyid(context_system::instance(), false);
			$context = context_system::instance();

			$fs = get_file_storage();

			$files = $fs->get_area_files($context->id, 'theme_iomad', 'englishback', $companyid );
			if ($files) {
				foreach ($files as $file) {
					$filename = $file->get_filename();
					$filepath = ((int) $maxwidth . 'x' . (int) $maxheight) . '/';
					if ($filename != '.') {
						$setting =$CFG->wwwroot."/pluginfile.php/1/theme_iomad/englishback/".$filepath.$companyid."/".$filename;
					}
				}
			}
        		// get the current page break margin
			$bMargin = $this->getBreakMargin();
        		// get current auto-page-break mode
			$auto_page_break = $this->AutoPageBreak;
        		// disable auto-page-break
			$this->SetAutoPageBreak(false, 0);
               		// set bacground image
			$this->Image($setting, 0, 0, 300, 210, '', '', '', false, 300, '', false, false, 0);
			
        		// restore auto-page-break status
			$this->SetAutoPageBreak($auto_page_break, $bMargin);
        		// set the starting point for the page content
			$this->setPageMark();
		}
	}

// create new PDF document
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');

// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(0);
	$pdf->SetFooterMargin(0);

// remove default footer
	$pdf->setPrintFooter(false);

// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}
// set font
	$pdf->SetFont('times', '', 20);
// add a page
	$pdf->AddPage('L', 'A4');
	$pdf->setImageScale(1.53);
	$ext_val = strtoupper(pathinfo($exarray[8], PATHINFO_EXTENSION));
	
	$pdf->Image($my_save_dir.$exarray[8], 220, 15, 50, 50, $ext_val, '', '', true, 300, '', false, false, 0, false, false, false);
	// set font
	$pdf->SetFont('times', '', 35);
	$pdf->Text(23, 95, $username);
		// set font
	//$pdf->SetFont('aealarabiya', '', 25);
	$pdf->Text(23, 125, $coursename);
	$pdf->SetFont('times', '', 15);
	$pdf->Text(50, 177, $complete);
//Close and output PDF document
	$pdf->Output('Certificate.pdf', 'D');
}

