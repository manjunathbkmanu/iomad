<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phishing
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../lib.php');
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
require_once($CFG->libdir.'/formslib.php');
class campaign_edit_form extends moodleform {
	function definition() {
		global $CFG,$USER;
		$id = optional_param('id',null, PARAM_INT);
		$edit = optional_param('edit',null, PARAM_INT);
		$mform = $this->_form; 
		//orgaziation name field
		$companyid = $USER->company->id;
		$apikey = get_config('local_gophish'.$companyid, 'api');
		$urlip = get_config('local_gophish'.$companyid, 'urlip');
		$campaign = get_campaign($urlip,$apikey,$id);
		$launchTime = new DateTime($campaign->launch_date, core_date::get_user_timezone_object());

		$sendTime = new DateTime($campaign->send_by_date, core_date::get_user_timezone_object());
		$launchDate = $launchTime->getTimestamp();
		$sendByDate = $sendTime->getTimestamp();

		$context = context_system::instance();
		$mform->addElement('static','campaignhrd',get_string('copycampaign','local_phishing'),'');
		$mform->addElement('html', '<hr>');
		// Add some extra hidden fields.
		$mform->addElement('hidden', 'id',$id);
        //$mform->setType('id', core_user::get_property_type('id'));
		$mform->setType('id', PARAM_INT);

		$mform->addElement('hidden', 'edit',$edit);
        //$mform->setType('status', core_user::get_property_type('status'));
		$mform->setType('edit', PARAM_INT);
		//name of the template
		$mform->addElement('text', 'name',
         get_string('temp_name','local_phishing')); // Add elements to your form
		$mform->addRule('name', get_string('required'), 'required', null, 'client');
		// $mform->addElement('static','name1','',get_string('cam_help','local_phishing'));
		$mform->setType('name', PARAM_TEXT);
		$mform->setDefault('name', 'Copy of '.$campaign->name);
		//template
		$template = get_templates($urlip,$apikey);
		$select = $mform->addElement('searchableselector', 'template',
			get_string('emailtemp','local_phishing'),$template);
		$mform->addRule('template', get_string('required'), 'required', null, 'client');
		// $mform->addElement('static','template1','',get_string('temp_help','local_phishing'),'');
		$select->setMultiple(false);
		$mform->setDefault('template', $campaign->template->id);

		//landing page
		$page = get_landing_pages($urlip,$apikey);
		$select = $mform->addElement('searchableselector', 'page',
			get_string('landingpage','local_phishing'),$page);
		$mform->addRule('page', get_string('required'), 'required', null, 'client');
		// $mform->addElement('static','page1','',get_string('land_help','local_phishing'),'');
		$select->setMultiple(false);
		$mform->setDefault('page', $campaign->page->id);

		//url
		$mform->addElement('text', 'url',
         get_string('url','local_phishing')); // Add elements to your form
		// $mform->addElement('static','url_help','',get_string('url_help','local_phishing'),'');
		$mform->setType('url', PARAM_TEXT);
		$mform->setDefault('url', $campaign->url);


		//launch_date
		$mform->addElement('date_time_selector', 'launch_date',
         get_string('launchdate','local_phishing')); // Add elements to your form
		// $mform->addElement('static','launch_help','',get_string('launch_help','local_phishing'),'');
		$mform->setType('launch_date', PARAM_TEXT);
		$mform->setDefault('launch_date', $launchDate);
		//send_by_date
		$mform->addElement('date_time_selector', 'send_by_date',
         get_string('sendemail','local_phishing')); // Add elements to your form
		// $mform->addElement('static','email_help','',get_string('email_help','local_phishing'),'');
		$mform->setType('send_by_date', PARAM_TEXT);
		$mform->setDefault('send_by_date', $sendByDate);

		//profile
		$smpt = get_sending_profile($urlip,$apikey);
		$select = $mform->addElement('select', 'smtp',
         get_string('sendprofile','local_phishing'),$smpt); // Add elements to your form
		$mform->addRule('smtp', get_string('required'), 'required', null, 'client');
		// $mform->addElement('static','send_help','',get_string('send_help','local_phishing'),'');
		$select->setMultiple(false);
		$mform->setDefault('smtp', $campaign->smtp->id);
		//html editor

		//groups
		$group = get_groups($urlip,$apikey);

		$select = $mform->addElement('searchableselector', 'groupss',
         get_string('groups','local_phishing'),[]); // Add elements to your form
		$mform->addRule('groups', get_string('required'), 'required', null, 'client');
		// $mform->addElement('static','group_help','',get_string('group_help','local_phishing'),'');
		$select->setMultiple(true);

		$this->add_action_buttons();
	}
}
