<?php
///////////////////////////////////////////////////////////////////////////
//                                                                       //
// This file is part of Moodle - http://moodle.org/                      //
// Moodle - Modular Object-Oriented Dynamic Learning Environment         //
//                                                                       //
// Moodle is free software: you can redistribute it and/or modify        //
// it under the terms of the GNU General Public License as published by  //
// the Free Software Foundation, either version 3 of the License, or     //
// (at your option) any later version.                                   //
//                                                                       //
// Moodle is distributed in the hope that it will be useful,             //
// but WITHOUT ANY WARRANTY; without even the implied warranty of        //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         //
// GNU General Public License for more details.                          //
//                                                                       //
// You should have received a copy of the GNU General Public License     //
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.       //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

/**
 * Form for community search
 *
 * @package    block_community
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 1999 onwards Martin Dougiamas  http://dougiamas.com
 */
global $DB;
require_once($CFG->libdir . '/formslib.php');

class local_profile_form extends moodleform {

    public function definition() {
        global $CFG,$DB,$USER,$OUTPUT;
        $mform = & $this->_form;
        $id = $this->_customdata['eid'];

        //for firstname
        $mform->addElement('text', 'firstname', get_string('firstname', 'local_profile'));
        $mform->addRule('firstname', get_string('firstnameerror','local_profile'), 'required', 'extraruledata', 'client', false, false);
        $mform->setType('firstname', PARAM_TEXT);


        //For Lastanme
        $mform->addElement('text', 'lastname', get_string('lastname', 'local_profile'));
        $mform->addRule('lastname', get_string('lastnameerror','local_profile'), 'required', 'extraruledata', 'client', false, false);
        $mform->setType('lastname', PARAM_TEXT);

        //For Email
        $mform->addElement('text', 'email', get_string('email', 'local_profile'),'');
        $mform->setType('email', PARAM_RAW);

        //For Employeeid
         $mform->addElement('hidden', 'id', get_string('employeeid', 'local_profile'));
        $mform->setType('id', PARAM_INT);

        // For Department
        $mform->addElement('text', 'department', get_string('department', 'local_profile'), ' rows="20" cols="50"');
        $mform->setType('department', PARAM_TEXT);


        // For Department
        $mform->addElement('text', 'alternatename', get_string('alternatename', 'local_profile'), ' rows="20" cols="50"');
        $mform->setType('alternatename', PARAM_TEXT);
                //filepicker
        //$mform->addElement('filepicker', 'userpic', get_string('insert_file', 'local_profile'), null, null);  
        $mform->addElement('filepicker', 'userpic', get_string('insert_file', 'local_profile'), null, array('accepted_types' => array('.png', '.jpg')));  
		
		//$this->set_upload_manager(new upload_manager('userpic', false, false, null, false, 0, true, true, false));
        //$mform->addElement('file', 'userpic', get_string('insert_file', 'local_profile'));
		
        $mform->setDefault('userpic', null);
        
        $mform->addElement('static', 'currentpicture', get_string('currentpicture'));

        $mform->addElement('submit', 'submitbutton', get_string('submit', 'local_profile'));
   
            $mform->addElement('hidden', 'userid',$USER->id);
            $mform->setType('userid', PARAM_INT);
            $mform->setType('eid', PARAM_INT);
        
    }
    /**
     * Extend the form definition after data has been parsed.
     */
    public function definition_after_data() {
	global $USER, $CFG, $DB, $OUTPUT;
        $user = $USER;
        $userid = $USER->id;
        $mform = $this->_form;

        // User can not change own auth method.
        if ($userid == $USER->id) {
            $mform->hardFreeze('email');
        }

        // User can not change own auth method.
        if ($userid == $USER->id) {
            $mform->hardFreeze('id');
        }

        // User can not change own auth method.
        if ($userid == $USER->id) {
            $mform->hardFreeze('department');
        }        // User can not change own auth method.
        if ($userid == $USER->id) {
            $mform->hardFreeze('alternatename');
        }

        // Print picture.
        // if (empty($USER->newadminuser)) {
            if ($user) {
                $context = context_user::instance($user->id, MUST_EXIST);
                $fs = get_file_storage();
                $hasuploadedpicture = ($fs->file_exists($context->id, 'user', 'icon', 0, '/', 'f2.png') || $fs->file_exists($context->id, 'user', 'icon', 0, '/', 'f2.jpg'));
                if (!empty($user->picture) && $hasuploadedpicture) {
                    $imagevalue = $OUTPUT->user_picture($user, array('courseid' => SITEID, 'size' => 64));
                    $imageelement = $mform->getElement('currentpicture');
                    $imageelement->setValue($imagevalue);
                } 
            }

                    // Next the customisable profile fields.
        profile_definition_after_data($mform, $userid); 

    }
    


}
