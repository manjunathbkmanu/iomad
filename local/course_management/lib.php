<?php
//Manju:
function local_course_management_image($itemid,$filearea){
	global $DB,$CFG,$USER;

	if(!empty($itemid)){
		$context = context_coursecat::instance($USER->company->id);
		$contextid = $context->id;

		$component = 'local_course_management';
		$fs = get_file_storage();
		$files = $fs->get_area_files($contextid, $component, $filearea, $itemid);
		
		if(!empty($files)){
			$url2 ='';
			foreach($files as $file) {
				$file->get_filename();
				$url2 = moodle_url::make_pluginfile_url(
					$file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename()
				);
				// $url2=$CFG->wwwroot.'/pluginfile.php/'.$file->get_contextid().'/'.$file->get_component().'/'.$file->get_filearea().'/'.$file->get_itemid().'/'.$file->get_filepath().$file->get_filename();
			}
			return $file->get_filename();
		}
	}
}


function local_course_management_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {


// Make sure the filearea is one of those used by the plugin.
	// if ($filearea !== 'rhbgimage') {
	// 	return false;
	// }

// Make sure the user is logged in and has access to the module (plugins that are not course modules should leave out the 'cm' part).
	require_login();


// Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
$itemid = array_shift($args); // The first item in the $args array.

// Use the itemid to retrieve any relevant data records and perform any security checks to see if the
// user really does have access to the file in question.

// Extract the filename / filepath from the $args array.
$filename = array_pop($args); // The last item in the $args array.
if (!$args) {
$filepath = '/'; // $args is empty => the path is '/'
} else {
$filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
}

// Retrieve the file from the Files API.
$fs = get_file_storage();
$file = $fs->get_file($context->id, 'local_course_management', $filearea, $itemid, $filepath, $filename);
if (!$file) {
return false; // The file does not exist.
}


// We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering.
// From Moodle 2.3, use send_stored_file instead.
//send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!

$forcedownload = true;

send_file($file, $file->get_filename(), true, $forcedownload, $options);

}

// function get_filepath_pdf($itemid,$filearea){
// 	global $DB,$CFG;
// 	$context = context_system::instance();
// 	$contextid = $context->contextlevel;
// 	$fs = get_file_storage();

// // Prepare file record object
// 	$fileinfo = array(
//     'component' => 'local_course_management',     // usually = table name
//     'filearea' => $filearea,     // usually = table name
//     'itemid' => $itemid,               // usually = ID of row in table
//     'contextid' => $context->id, // ID of context
//     'filepath' => '/',           // any path beginning and ending in /
//     'filename' => 'urdu.png'); // any filename


// // Get file
// 	$file = $fs->get_file($fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'],
// 		$fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename']);
	
// 	print_object($file);die;

// // Read contents
// 	if ($file) {
// 		$contents = $file->get_content();
// 	} else {
//     // file doesn't exist - do something
// 	}
// 	return $contents;
// }

//commanhtml
function setting_card($link,$icon,$name){
	$html='';
	$html.='<div class="col-md-4 text-center">
	<div class="tile is-parent">
		<article class="tile is-child   box has-shadow2 grow">
			<a href="'.$link.'" class="text-center  text-dark" >
				<div class="t-icon right">
					'.$icon.'
				</div>
				<div class="t-content pt-4">
					<p class="subtitle is-size-5-mobile is-size-6-5-tablet is-size-5-desktop   shrink-text">'.get_string($name, 'local_course_management').'</p>
				</div>
			</a>
		</article>
	</div>
</div>';
return $html;
}