<?php

$string['pluginname'] = 'تسجيل سريع';
$string['notallowed'] = 'لا يسمح لك برؤية هذه الصفحة';
$string['courses'] = 'الدورات';
$string['roles'] = 'الأدوار';
$string['users'] = 'المستخدمين';
$string['enrollusers'] = 'تسجيل المستخدمين';
$string['manager'] = 'المدير';
$string['teacher'] = 'المدرس';
$string['student'] = 'التلميذ';
$string['student'] = 'التلميذ';
$string['enrollment:addinstance'] = 'إضافة تسجيل سريع';

$string['enddate'] = 'تاريخ الانتهاء';
$string['startdate'] = 'تاريخ البدء';
$string['validate'] = 'التحقق';
$string['selectuser'] = 'اختر المستخدم';
$string['selectcourse'] = 'قم باختيار الدورة';