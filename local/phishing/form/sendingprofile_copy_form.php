<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phishing
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../lib.php');

/*if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}*/
require_once($CFG->libdir.'/formslib.php');
class sendingprofile_copy_form extends moodleform {
	function definition() {
		global $CFG,$DB,$USER,$PAGE,$OUTPUT;
		$id = optional_param('id',null, PARAM_INT);
		$mform = $this->_form; 
		//Manju: changed the plugin name from local_gophish to local_phishing.08/02/2021.
		$companyid = $USER->company->id;
		$apikey = get_config('local_phishing'.$companyid, 'api');
		$urlip = get_config('local_phishing'.$companyid, 'urlip');
		$profile = get_sendingprofile_value($urlip,$apikey,$id);
		//orgaziation name field	
		$context = context_system::instance();
		$mform->addElement('static','sendingprofilehrd',get_string('sendingprofile','local_phishing'),'');
		$mform->addElement('html', '<hr>');
		// Add some extra hidden fields.
		$mform->addElement('hidden', 'id',$id);
        //$mform->setType('id', core_user::get_property_type('id'));
		$mform->setType('id', PARAM_INT);
        //$mform->setType('status', core_user::get_property_type('status'));
		$mform->setType('edit', PARAM_INT);
		//name 
		$mform->addElement('text', 'name',
         get_string('temp_name','local_phishing')); // Add elements to your form
		$mform->addRule('name', get_string('required'), 'required', null, 'client');
		// $mform->addElement('static','name1','',get_string('name_help','local_phishing'),'');
		$mform->setType('name', PARAM_RAW);
		$mform->setDefault('name', 'Copy of '.$profile->name);

		//name of the Interface Type:
		$mform->addElement('text', 'interface_type',
         get_string('interface_type','local_phishing'), 'readonly'); // Add elements to your form
		// $mform->addElement('static','interface_type1','',get_string('interface_help','local_phishing'),'');
		$mform->setType('interface_type', PARAM_RAW);
		$mform->setDefault('interface_type', 'SMTP');

		//name of the Interface Type:
		$mform->addElement('text', 'from_address',
         get_string('from','local_phishing')); // Add elements to your form	
        $mform->addRule('from_address', get_string('required'), 'required', null, 'client');
        // $mform->addElement('static','from_address1','',get_string('from_address_help','local_phishing'),'');
		$mform->setType('from_address', PARAM_RAW);
		$mform->setDefault('from_address', $profile->from_address);

		//name of the Interface Type:
		$mform->addElement('text', 'host',
         get_string('host','local_phishing')); // Add elements to your form	
          // $mform->addElement('static','host1','',get_string('host_help','local_phishing'),'');
		$mform->addRule('host', get_string('required'), 'required', null, 'client');	
		$mform->setType('host', PARAM_RAW);
		$mform->setDefault('host', $profile->host);

		//name of the Interface Type:
		$mform->addElement('text', 'username',
         get_string('username','local_phishing')); // Add elements to your form
		// $mform->addElement('static','username1','',get_string('username_help','local_phishing'),'');	
		$mform->setType('username', PARAM_RAW);

		//name of the Interface Type:
		$mform->addElement('text', 'password',
         get_string('password','local_phishing')); // Add elements to your form
		// $mform->addElement('static','password1','',get_string('password_help','local_phishing'),'');	
		$mform->setType('password', PARAM_RAW);

		$mform->addElement('static','sendingprofilehrd2',get_string('emailhdr','local_phishing'),'');
		$mform->addElement('html', '<hr>');
		$hrfno = array('1'=>1,
			'2'=>2,
			'3'=>3,
			'4'=>4,
			'5'=>5
		);
		$mform->addElement('advcheckbox', 'ignore_cert_errors', '', get_string('ignorecer', 'local_phishing'), '', array(0,1));
		$mform->setDefault('ignore_cert_errors', 1);
		$mform->addElement('text', 'key',
         get_string('xcheader','local_phishing')); // Add elements to your form
		// $mform->addElement('static','key1','',get_string('header_help','local_phishing'),'');	
		$mform->setType('key', PARAM_RAW);

		//name of the Interface Type:
		$mform->addElement('text', 'value',
         get_string('urlphish','local_phishing')); // Add elements to your form
		// $mform->addElement('static','value1','',get_string('header1_help','local_phishing'),'');	
		$mform->setType('value', PARAM_RAW);		

		$this->add_action_buttons();
	}
}
