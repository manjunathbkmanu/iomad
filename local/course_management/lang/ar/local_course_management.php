<?php 
$string['title'] = 'الملف الشخصي';
$string['coursemanagement'] = 'إدارة نظام إدارة التعليم';
$string['addcourse'] = 'إضافة دورة';
$string['managecourses'] = 'إدارة الدورات والفئات'; 
$string['managecategories'] = 'إدارة الفئات';
$string['adduser'] = 'إضافة مستخدمين';
$string['uploaduser'] = 'إضافة المستخدمين باستخدام ملف csv';
$string['browseusers'] = 'تصفح قائمة المستخدمين'; 
$string['pages'] = 'الصفحات'; 
$string['SMTP_Settings'] = 'إعدادات SMTP'; 
$string['LDAP_Settings'] = 'إعدادات LDAP'; 
$string['color_settings'] = 'إعدادات الألوان والأشعار'; 
$string['coursedashboard'] = 'لوحة قيادة الدورة'; 
$string['reminder_Settings'] = 'إعدادات بريد إلكتروني للتذكير';
$string['adminreports'] = 'التقارير';  
$string['userreport'] = 'تقارير المستخدم';  
$string['coursereport'] = 'تقارير الدورة';  
$string['license'] = 'التحقق من الرخصة';  
$string['addadmin'] = 'إضافة مدير للنظام';  
$string['update'] = 'التحقق من التحديثات';  
$string['productmanagement'] = 'إدارة المنتج'; 
$string['noaccess'] = 'ليس لديك الصلاحية لدخول هذه الصفحة';
$string['selectdefaulthome'] = 'حالة الاستخدام'; 

$string['certificates'] = 'الشهادات';
$string['downloadcertificate'] = 'تنزيل الشهادات';
$string['coursename'] = 'اسم الدورة التدريبية';
$string['username'] = 'اسم المستخدم';
$string['downloadcert'] = 'تحميل';
$string['seldefaultlanguage'] = 'اللغة التلقائية';
$string['helpdescription'] = 'للاختيار المتعدد ، اضغط على CTRL ثم حدد الدورات التدريبية';
$string['selectallcourse'] = 'قم باختيار جميع الدورات';
$string['rulename'] = 'اسم الإعداد';
$string['ruledescription'] = 'وصف الإعداد';
$string['selectkey'] = 'نوع الإعداد';
$string['selectvalue'] = 'قيمة الإعداد ';
$string['selectallcourse'] = 'قم باختيار جميع الدورات';
$string['selectcourse'] = 'قم باختيار الدورات';
$string['autoenrollmentlist'] = 'قائمة إعدادات التسجيل التلقائي';
$string['delete'] = 'الحذف';
$string['cancel'] = 'إلغاء';

//for certificate setting.
$string['certificatesetting'] = 'إعدادات الشهادات';
$string['backgroundarabic'] = 'قم تحميل خلفية اللغة العربية';
$string['backgroundenglish'] = 'قم بتحميل خلفية اللغة الإنجليزية';
$string['submit'] = 'التسجيل';
$string['dlanganddhome'] = 'الإعدادات التلقائية للنظام وإعدادات اللغة';
$string['addautoenrollmentrule'] = 'إعدادات التسجيل التلقائي';
$string['assignrole'] = 'تحديد الأدوار';
$string['sendreminders'] = 'رسال اشعارات التذكير';
$string['enrolmultiplecourse'] = 'تسجيل المستخدم في دورات متعددة';
$string['reportsandreminders'] = 'التقارير وإشعارات التذكير';
$string['coursemanagementsettings'] = 'إعدادات مستخدمي الإدارات';
$string['courses'] = 'الدورات';
$string['users'] = 'المستخدمين';
$string['othersettings'] = 'الإعدادات الأخرى';
$string['userdepartment'] = 'قم باختيار الإدارة';
$string['all_users'] = 'الجميع';
$string['department'] = 'قسم';
$string['manager'] = 'مدير';
$string['deleterule'] = 'هل تريد حذف قائمة التسجيل التلقائي؟';
$string['defaultcase'] = 'التلقائي';
$string['lmshomepage'] = 'الصفحة الرئيسية للنظام';
$string['lmsonly'] = 'نظام التعليم الإلكتروني فقط';
$string['SAML2_Settings'] = 'إعدادات SAML2';

$string['progress_dashboard'] = 'لوحة التقدم';
$string['rulekey'] = 'نوع القاعدة';
$string['rulevalue'] = 'القاعدة';
