<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the Activity modules block.
 *
 * @package    block_activity_modules
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->libdir . '/filelib.php');
require_once("$CFG->libdir/gradelib.php");
require_once("$CFG->dirroot/lib/completionlib.php");
require_once("$CFG->dirroot/grade/querylib.php");
use core_completion\progress;

class block_myreport_pass extends block_base {

   function init() {
    $this->title = get_string('pluginname', 'block_myreport_pass');
}

function get_content() {
    global $DB, $OUTPUT,$USER;


    if($this->content !== NULL) {
        return $this->content;
    }

    $this->content = new stdClass;
    $this->content->items = array();
    $this->content->icons = array();
	$this->content->text = '';
    $sortorder = 'visible DESC, sortorder ASC';
    $courses = enrol_get_users_courses($USER->id);
    $dateformat = '%d-%b-%Y';
    //print_object($courses);
    $coursesprogress = [];
    $statusfailed = [];
    $statuscompleted = [];
    $statusnotcompleted = [];
    $statusprogress = [];
	$pass = 0;
	$fail = 0;
	$notcompl = 0;
	$inprogress = 0;
    if(!empty($courses)){
        foreach($courses as $course) {
            $courseobj = $DB->get_record('course',array('id'=>$course->id));
             		
			$cminfo = get_fast_modinfo($course, $USER->id);
                foreach($cminfo->get_cms() as $cm) {
					if($cm->modname == 'scorm')
					 {
						 $scormstatus = $this->get_scorm_status_ebdaa_blockpass($cm->instance);
						 if ($scormstatus == 'completed' OR $scormstatus == 'complete' OR $scormstatus == 'passed'){
								$statuscompleted[] = $courseobj->id;
							} else if ($scormstatus == 'failed') {
								$statusfailed[] = $courseobj->id;
							} else if ($scormstatus == 'incomplete') {
								$statusprogress[] = $courseobj->id;
							}else if ($scormstatus == 'notstarted') {
								$statusnotcompleted[] =$courseobj->id; // this is actually not started
							}
					 }
				}
				
        }
    
    $pass = count($statuscompleted);
    $fail = count($statusfailed);
    $notcompl = count($statusnotcompleted);
    $inprogress = count($statusprogress);

    $passar = array($pass); 
    $failar = array($fail); 
    $labels = array(get_string('pass','block_myreport_pass'),get_string('fail','block_myreport_pass'));   

	
	$sales = new \core\chart_series(get_string('pass','block_myreport_pass'), $passar);
$expenses = new \core\chart_series(get_string('fail','block_myreport_pass'), $failar);
$labels = [get_string('totalcount','block_myreport_pass')];
$chart7 = new \core\chart_bar();
$chart7->add_series($sales);
$chart7->add_series($expenses);
$chart7->set_labels($labels);
	global $CFG;
	
	
//green - #28a944
//red - #dc3546
//yellow - #ffc107

	$CFG->chart_colorset = ['#dc3546','#28a944' ];
    //for y axis 

    $yy =  $OUTPUT->render($chart7);
    $this->content->text .= $yy;
    
	
	}
	
    $this->content->footer = '';
    return $this->content;
}

function get_scorm_status_ebdaa_blockpass($scormid) {
    global $DB, $USER;
    $status = '-';
	$userid = $USER->id;
    /* $sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
    $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid)); */
	
	$sql = "SELECT attempt
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
               ";
	
    $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));
	
    if (!empty($lastattempt)) {
        $sql = "SELECT value
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
        $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));
		
    } else {
        //mihir for no score pass/fail

        $sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
            $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
            if (!empty($lastattempt)) {
                $sql = "SELECT value
                      FROM {scorm_scoes_track}
                     WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
                $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));
            } else {
                $status = 'notstarted';
            }
	}
    return $status;
}

}


