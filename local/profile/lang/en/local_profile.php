<?php 
$string['title'] = 'Profile';
$string['firstname'] = 'First Name';
$string['lastname'] = 'Last Name';
$string['email'] = 'Email';
$string['employeeid'] = 'Employee ID';
$string['department'] = 'Department';
$string['search'] = 'Search';
$string['firstnameerror'] = 'Please enter Firstname';
$string['lastnameerror'] = 'Please enter Lastname';
$string['insert_file'] = 'Profile Picture';
$string['submit'] = 'Submit';
$string['changepassword'] = 'Change Password';
$string['alternatename'] = 'Manager';
$string['lang'] = 'Language';