<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phishing
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
//defined('MOODLE_INTERNAL') || die();

require('../../../config.php');
require('../lib.php');
global $DB,$USER;
require_login(0 , FALSE);
$companyid = $USER->company->id;
$sid = optional_param('id',null, PARAM_INT);
$context = context_system::instance();
$overall = has_capability('local/phishing:overall',$context);
$campaigns = has_capability('local/phishing:campaigns',$context);
$PAGE->set_context(context_system::instance());
$title = get_string('copy', 'local_phishing');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/action/campaignsinglelist.php');
//api key initialization
//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$PAGE->navbar->ignore_active();
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/local/phishing/css/kendo.default-v2.min.css'), true);
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/phishing/js/pie-chart.js'), true);
$PAGE->requires->css(new 
    moodle_url($CFG->wwwroot.'/local/course_report/css/dataTables.bootstrap4.min.css'));
$PAGE->requires->css(new 
    moodle_url($CFG->wwwroot.'/local/course_report/css/buttons.bootstrap4.min.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/newjs/jquery.dataTables.min.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/newjs/dataTables.bootstrap4.min.js'), true);
$previewnode = $PAGE->navbar->add(get_string('pluginname','local_phishing'),'');
$previewnode = $previewnode->add(get_string('dashboard','local_phishing'),$CFG->wwwroot.'/local/phishing/campaignlist.php');
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/phishing/campaignslist.php';
$deleteUrl= $CFG->wwwroot . '/local/phishing/action/campaign_delete.php?id='.$sid;
$completeUrl= $CFG->wwwroot . '/local/phishing/action/campaign_complete.php?id='.$sid;
$refreshUrl= $CFG->wwwroot . '/local/phishing/action/campaignsinglelist.php?id='.$sid;
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback

//api curl initialization
if($overall || $campaigns){
    //

    $campaignResult = get_campaign_result($urlip,$apikey,$sid);
    // print_object($campaignResult);

	$resulttable  = new \html_table();
	$resulttable->id = 'result';
	$resulttable->head = array('id',
		'status',
		'ip',
		'latitude',
		'longitude',
		'send_date',
		'reported',
		'modified_date',
		'email',
		'first_name',
		'last_name',
		'position',
	);

	$eventtable  = new \html_table();
	$eventtable->id = 'rawevents';
	$eventtable->head = array('campaign_id',
		'email',
		'time',
		'message',
		'details'
	);
	if(!empty($campaignResult->timeline)){
        foreach ($campaignResult->timeline as $t) {
			$eventtable->data[] = array($t->campaign_id, $t->email, $t->time, $t->message, $t->details);
        }
    }
    echo html_writer::table($eventtable);

	$i= 1;
    $sent = 0;
    $open = 0;
    $click = 0;
    $sdata = 0;
    $ereport = 0;
	$camsummary = get_campaign_summary($urlip,$apikey,$sid);
	// print_object($camsummary);die;
	$header = '';
	$header .= '<div class="text-center">';
	$header .= '<h2>'.get_string('copy','local_phishing').$camsummary->name.'</h2>';
	$header .= '</div>';
	$header .= '<div class="action-groups text-center">';
	$header .= '<a href="'.$retuurnurl.'" class="btn btn-default"><i class="fa fa-arrow-circle-o-left fa-lg"></i>'.get_string('back_button','local_compliance_dashboard').'</a>';
	$header .= '<div class="btn-group">';
    $header .= '<button type="button" id="exportButton" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-file-excel-o"></i> Export CSV</button><ul class="dropdown-menu" aria-labelledby="exportButton"><li><a href="javascript:void(0);" id="exportresult">Results</a></li><li><a href="javascript:void(0);" id="exportevent">Raw Events</a></li></ul>';
	$header .= '</div>';
	if($campaignResult->status == 'Completed'){

		$header .= '<button type="button" class="btn btn-default" data-toggle="tooltip" data-original-title="" title="">Completed!</button>';
	}else{

		$header .= '<button id="complete_button" data-href="'.$completeUrl.'" type="button" class="btn btn-blue completeCampaign" data-toggle="tooltip" data-original-title="" title=""><i class="fa fa-flag-checkered"></i> Complete</button>';
	}
	$header .= '<button type="button" data-href="'.$deleteUrl.'" class="btn btn-danger deleteCampaign" data-toggle="tooltip" data-original-title="" title=""><i class="fa fa-trash-o fa-lg"></i> Delete</button>';
	$header .= '<button id="refresh_btn" type="button" data-href="'.$refreshUrl.'" class="btn btn-blue refreshCampaign" data-toggle="tooltip" data-original-title="" title="" style="display: inline-block;"><i class="fa fa-refresh fa-lg"></i> Refresh</button>';
	$header .= '</div>';
	$header .= '<hr>';
	echo $header;
	$sent += $camsummary->stats->sent;
	$open += $camsummary->stats->opened;
	$click += $camsummary->stats->clicked;
	$sdata += $camsummary->stats->submitted_data;
	$ereport += $camsummary->stats->email_reported;
	//prepare chart and table here 
	$array = array('Email Sent'=>$sent,'Email Opened'=>$open,'Clicked Link'=>$click,'Submitted Data'=>$sdata,'Email Reported'=>ereport);
	$data = '';
	$data .= html_writer::start_div('container');
	$data .= html_writer::start_div('row');
	//for chart preparation 
	$total = $sent;
	$data.='<div class="col-md-1"></div>';
	foreach ($array as $key => $value) {
		$class = '';
		switch ($key) {
			case 'Email Sent':
				$class='emailsent';
				break;
			case 'Email Opened':
				$class='emailopen';
				break;
			case 'Clicked Link':
				$class='linkclick';
				break;
			case 'Submitted Data':
				$class='submitteddata';
				break;
			
			default:
				$class='emailreport';
				break;
		}
    $data .= progress_chart_val($key,$value,$i,$total,$class);
    $i++;
	}
	$data.='<div class="col-md-1"></div>';

    $data .= html_writer::end_div();//card end
    $data .= html_writer::end_div();//card end
    echo $data;
    //table data here 
    echo '<br><br>';
    echo '<h1>'.get_string('details','local_phishing').'</h1>';
    echo '<br><br>';
    $html .= '<table id="result-table" class="generaltable">';
    $html .= '<thead>';     
    $html .= '<tr>';    
    $html .= '<th></th>';
    $html .= '<th>'.get_string('fname', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('lname', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('email', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('position', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('status', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('reported', 'local_phishing').'</th>';
    $html .= '</tr>';
    $html .= '</thead>';
    $html .= '<tbody>';
    if(!empty($campaignResult->results)){
        foreach ($campaignResult->results as $result) {
        	$resulttable->data[] = array($result->id, $result->status, $result->ip, $result->latitude, $result->longitude, $result->send_date, $result->reported, $result->modified_date, $result->email, $result->first_name, $result->last_name, $result->position);
            $class = '';
			switch ($result->status) {
				case 'Email Sent':
					$class='emailsent';
					break;
				case 'Email Opened':
					$class='emailopen';
					break;
				case 'Clicked Link':
					$class='linkclick';
					break;
				case 'Submitted Data':
					$class='submitteddata';
					break;
				
				default:
					$class='emailreport';
					break;
			} 
            //table creation here
            
            $html .= '<tr>';
            $html .= '<td class="details-control" data-campaignid="'.$campaignResult->id.'" data-email="'.$result->email.'" data-id="'.$result->id.'"><i id="caret" class="fa fa-caret-right"></i></td>';
            $html .= '<td class=" text-center">'.$result->first_name.'</td>';
            $html .= '<td class=" text-center">'.$result->last_name.'</td>';
            $html .= '<td class=" text-center">'.$result->email.'</td>';
            $html .= '<td class=" text-center">'.$result->position.'</td>';
            $html .= '<td class=" text-center"><p class="badge badge-'.$class.' text-wrap" style="width: 6rem;">'.$result->status.'</p></td>';
            $html .= '<td class=" text-center"><i role="button" class="fa fa-times-circle text-center text-muted"></i></td>';
            $html .= '</tr>';
        }   
    }else{

        echo html_writer::div(
            get_string('cap', 'local_phishing'),'alert alert-danger'
        );
    }
    $html .= '</tbody>';
    $html .= '</table>';
    echo $html;
    echo html_writer::table($resulttable);
    
}else{
    echo html_writer::div(
        get_string('cap', 'local_phishing'),'alert alert-danger'
    );
}
echo $OUTPUT->footer();
?>


<style type="text/css">
	.emailsent{
	    font-weight: 700;
	    color: #1abc9c;
	}
	.emailopen{
	    font-weight: 700;
	    color: #f9bf3b;
	}
	.linkclick{
	    font-weight: 700;
	    color: #f39c12;
	}
	.submitteddata{
	    font-weight: 700;
	    color: #f05b4f;
	}
	.emailreport{
	    font-weight: 700;
	    color: #45d6ef;
	}
	.btn-default {
	    color: #fff !important;
	    background-color: #bdc3c7 !important;
	}
	.btn {
	    border: none !important;
	    font-size: 15px !important;
	    font-weight: 400 !important;
	    line-height: 1.4 !important;
	    border-radius: 4px !important;
	    padding: 10px 15px !important;
		display: inline-block;
	    margin-bottom: 0;
	    text-align: center;
	    white-space: nowrap;
	    vertical-align: middle;
	    touch-action: manipulation;
	    cursor: pointer;
	    user-select: none;
	    background-image: none;
	    -webkit-font-smoothing: subpixel-antialiased;
	    -webkit-transition: border .25s linear,color .25s linear,background-color .25s linear;
	    transition: border .25s linear,color .25s linear,background-color .25s linear;
	}
	.btn-secondary{
	    color: #fff;
	    background-color: #48c9b0;
	    border-color: #48c9b0;
	}

	.btn-secondary:hover{
	    color: #fff;
	    background-color: #48c9b0;
	    border-color: #48c9b0;
	}

	.btn-blue {
	    color: #fff;
	    background-color: #428bca;
	    border-color: #428bca;
	}

	.btn-blue:hover {
	    background-color: #64a1d6;
	    color: #fff;
	}
	.action-groups .btn {
	    margin: 3px;
	}
	.badge-emailsent{
	    color: #fff;
	    font-size: 12px !important;
	    background-color: #1abc9c;
	}
	.badge-emailopen{
	    color: #fff;
	    font-size: 12px !important;
	    background-color: #f9bf3b;
	}
	.badge-linkclick{
	    color: #fff;
	    font-size: 12px !important;
	    background-color: #f39c12;
	}
	.badge-submitteddata{
	    color: #fff;
	    font-size: 12px !important;
	    background-color: #f05b4f;
	}
	.badge-emailreport{
	    color: #fff;
	    font-size: 12px !important;
	    background-color: #45d6ef;
	}
	td.details-control {
	    cursor: pointer;
	}
	/*Gophish timeline css*/
	.timeline {
	    text-align: left;
	    background-color: #fff;
	}
	.well-lg {
	    padding: 24px;
	    border-radius: 6px;
	}
	.well {
	    min-height: 20px;
	    padding: 19px;
	    margin-bottom: 20px;
	    background-color: #ffffff;
	    border: 1px solid #e3e3e3;
	    border-radius: 4px;
	    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
	    box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
	}
	.timeline>.subtitle {
	    color: #999;
	    font-style: italic;
	    margin-bottom: 15px;
	    display: block;
	}
	.timeline-graph {
	    margin-left: 30px;
	}
	.timeline-entry {
	    position: relative;
	    padding-bottom: 36px;
	}
	.timeline-bar {
	    display: block;
	    content: "";
	    position: absolute;
	    top: 5px;
	    bottom: 0;
	    width: 1px;
	    left: -6px;
	    background: #aaa;
	}
	.timeline-icon {
	    position: relative;
	    float: left;
	    margin-left: -24px;
	    width: 36px;
	    top: -5px;
	    height: 36px;
	    text-align: center;
	    border-radius: 50%;
	}
	.timeline-message {
	    padding-left: 30px;
	}
	.timeline-date {
	    float: right;
	    color: #999;
	    font-style: italic;
	}
	.label-clicked {
	    background-color: #f39c12!important;
	}
	.timeline-device-details {
	    font-size: .8em;
	    margin-top: 10px;
	}
	.timeline-device-browser>span.fa-stack {
	    font-size: .8em;
	}
	.label-danger {
	    background-color: #d9534f;
	}
	.label-success {
	    background-color: #5cb85c;
	}
	.timeline-icon>i {
	    margin-top: 10px;
	    color: #fff;
	}
	#result{
		display: none;
	}
	#rawevents{
		display: none;
	}
	ul.dropdown-menu{
		padding-left: 15px !important;
	}
</style>
<script type="text/javascript">
	var campaignName = "<?=$camsummary->name?>";
	var resultTable;
	$(document).ready(function() {
        resultTable = $('#result-table').DataTable({});
        $('#result-table tbody').on('click', 'td.details-control', function (e) {
        	e.preventDefault();
        	var campaignId = $(this).attr('data-campaignid');
        	var email = $(this).attr('data-email');
        	var userId = $(this).attr('data-id');
	        var tr = $(this).closest('tr');
	        var row = resultTable.row( tr );
	 
	        if ( row.child.isShown() ) {
	            // This row is already open - close it
	            row.child.hide();
	            tr.removeClass('shown');
	        	$(this).find('.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-right');
	        }
	        else {
	        $(this).find('.fa-caret-right').removeClass('fa-caret-right').addClass('fa-caret-down');
	            // Open this row
	            row.child( format(campaignId, email, userId) ).show();
	            tr.addClass('shown');
	        }
	    });
	});

	$('.deleteCampaign').click(function(e){
		e.preventDefault();
		var url = $(this).attr('data-href');
		window.location.href = url;
	});	

	$('.completeCampaign').click(function(e){
		e.preventDefault();
		var url = $(this).attr('data-href');
		window.location.href = url;
	});	

	$('.refreshCampaign').click(function(e){
		e.preventDefault();
		var url = $(this).attr('data-href');
		window.location.href = url;
	});

	/* Formatting function for row details - modify as you need */
	function format (campaignId, useremail, userId) {
	    // `campaignId` is passed to get results according to row
	    var formatHtml;
	    $.ajax({
            type: "POST",
            url: 'result_ajax.php',
  			async: false,  
            data: {
            	campaign_id: campaignId,
            	email: useremail,
            	user_id: userId,
            },
            success: function(response){
            	formatHtml = response;
            },
            error: function(error){

            }

        });

	    return formatHtml;
	}

	$('body').delegate('.timeline-event-details', 'click', function(e){
		e.preventDefault();
		if($(this).hasClass('hiddenn')){
			$(this).removeClass('hiddenn').addClass('shown');
			$('.timeline-event-results').show();
		}else{
			$(this).removeClass('shown').addClass('hiddenn');
			$('.timeline-event-results').hide();
		}
	})

	$(document).ready(function(){
		  function exportTableToCSV($table, filename) {

		    var $rows = $table.find('tr'),

		      // Temporary delimiter characters unlikely to be typed by keyboard
		      // This is to avoid accidentally splitting the actual contents
		      tmpColDelim = String.fromCharCode(11), // vertical tab character
		      tmpRowDelim = String.fromCharCode(0), // null character

		      // actual delimiter characters for CSV format
		      colDelim = '","',
		      rowDelim = '"\r\n"',

		      // Grab text from table into CSV formatted string
		      csv = '"' + $rows.map(function(i, row) {
		        var $row = $(row),
		          $cols = $row.find('th, td');

		        return $cols.map(function(j, col) {
		          var $col = $(col),
		            text = $col.text();

		          return text.replace(/"/g, '""'); // escape double quotes

		        }).get().join(tmpColDelim);

		      }).get().join(tmpRowDelim)
		      .split(tmpRowDelim).join(rowDelim)
		      .split(tmpColDelim).join(colDelim) + '"';

		    // Deliberate 'false', see comment below
		    if (false && window.navigator.msSaveBlob) {

		      var blob = new Blob([decodeURIComponent(csv)], {
		        type: 'text/csv;charset=utf-8'
		      });

		      // Crashes in IE 10, IE 11 and Microsoft Edge
		      // See MS Edge Issue #10396033
		      // Hence, the deliberate 'false'
		      // This is here just for completeness
		      // Remove the 'false' at your own risk
		      window.navigator.msSaveBlob(blob, filename);

		    } else if (window.Blob && window.URL) {
			/** Created by Dilip */
			// Added bom universal bom character to export csv arabic string
			  var universalBOM = "\uFEFF";
		      // HTML5 Blob        
		      var blob = new Blob([universalBOM+csv], {
		        type: 'text/csv;charset=utf-8'
		      });
		      var csvUrl = URL.createObjectURL(blob);

		      $(this)
		        .attr({
		          'download': filename,
		          'href': csvUrl
		        });
		    } else {
		      // Data URI
				var universalBOM = "\uFEFF";
		      var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(universalBOM+csv);

		      $(this)
		        .attr({
		          'download': filename,
		          'href': csvData,
		          'target': '_blank'
		        });
		    }
		  }

		  // This must be a hyperlink
		  $("#exportresult").on('click', function(event) {
		    // CSV
		    var args = [$('table#result'), campaignName+'.csv'];

		    exportTableToCSV.apply(this, args);

		    // If CSV, don't do event.preventDefault() or return false
		    // We actually need this to be a typical hyperlink
		  });
		    // This must be a hyperlink
		  $("#exportevent").on('click', function(event) {
		    // CSV
		    var args = [$('table#rawevents'), campaignName+'.csv'];

		    exportTableToCSV.apply(this, args);

		    // If CSV, don't do event.preventDefault() or return false
		    // We actually need this to be a typical hyperlink
		  });
	});
</script>
