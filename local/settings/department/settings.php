<?php

require_once('../../../config.php');
require_once('lib.php');
global $DB, $CFG, $USER, $PAGE;

/// no guest autologin
require_login(true);
$PAGE->set_context(context_system::instance());
$PAGE->set_url($CFG->wwwroot . '/local/settings/department/settings.php');
$PAGE->set_pagetype('admin-setting-coursemanagement');
$PAGE->set_pagelayout('admin');
$PAGE->navigation->clear_cache();
navigation_node::require_admin_tree();

$adminroot = admin_get_root(); // need all settings
$settingspage = $adminroot->locate('coursemanagement', true);

if (empty($settingspage) or !($settingspage instanceof admin_settingpage)) {
    if (moodle_needs_upgrading()) {
        redirect(new moodle_url('/admin/index.php'));
    } else {
        print_error('sectionerror', 'admin', "$CFG->wwwroot/$CFG->admin/");
    }
    die;
}

if (!($settingspage->check_access())) {
    print_error('accessdenied', 'admin');
    die;
}

/// WRITING SUBMITTED DATA (IF ANY) -------------------------------------------------------------------------------

$statusmsg = '';
$errormsg  = '';

// Form is submitted with changed settings. Do not want to execute when modifying a block.
if ($data = data_submitted() and confirm_sesskey() and isset($data->action) and $data->action == 'save-settings') {

    $count = admin_write_settings($data);
    $sql = "SELECT * FROM {user} WHERE deleted=0 AND department != ' '";
    $userLists = $DB->get_records_sql($sql);
    $departments=[];
    $config = get_config('coursemanagement');
    $departmentsarray=$config->userdepartment;
    $exploadedarray = explode(",",$departmentsarray);
    foreach ($exploadedarray as $arraykey => $arrayvalue) {
        $departments[]=$arrayvalue;
    }
    foreach($userLists as $user){
        if(!in_array($user->department, $departments)){
            $dataobject = new stdClass();
            $dataobject->id=$user->id;
            $dataobject->department = ' ';
            $DB->update_record('user', $dataobject, $bulk=false);
        }
    }
    // Regardless of whether any setting change was written (a positive count), check validation errors for those that didn't.
    if (empty($adminroot->errors)) {
        $url = "{$CFG->wwwroot}/local/settings/department/settings.php";
        // No errors. Did we change any setting? If so, then redirect with success.
        if ($count) {
            redirect($url, get_string('changessaved'), null, \core\output\notification::NOTIFY_SUCCESS);
        }
        redirect($url, get_string('changessaved'), null, \core\output\notification::NOTIFY_SUCCESS);
    } else {
        $errormsg = get_string('errorwithsettings', 'admin');
        $firsterror = reset($adminroot->errors);
    }
    $settingspage = $adminroot->locate('coursemanagement', true);
}

/// print header stuff ------------------------------------------------------------
if ($PAGE->user_allowed_editing()) {
    $url = clone($PAGE->url);
    if ($PAGE->user_is_editing()) {
        $caption = get_string('blockseditoff');
        $url->param('adminedit', 'off');
    } else {
        $caption = get_string('blocksediton');
        $url->param('adminedit', 'on');
    }
    $buttons = $OUTPUT->single_button($url, $caption, 'get');
    $PAGE->set_button($buttons);
}

$visiblepathtosection = array_reverse($settingspage->visiblepath);

$PAGE->set_title("$SITE->shortname: " . implode(": ",$visiblepathtosection));
$PAGE->set_heading($SITE->fullname);
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback

if ($errormsg !== '') {
    echo $OUTPUT->notification($errormsg);

} else if ($statusmsg !== '') {
    echo $OUTPUT->notification($statusmsg, 'notifysuccess');
}

// ---------------------------------------------------------------------------------------------------------------

$pageparams = $PAGE->url->params();
$context = [
    'actionurl' => $PAGE->url->out(false),
    'params' => [
            'name' => 'section',
            'value' => 'coursemanagement'
        ],
    'sesskey' => sesskey(),
    'title' => $settingspage->visiblename,
    'settings' => $settingspage->output_html(),
    'showsave' => $settingspage->show_save()
];

echo $OUTPUT->render_from_template('core_admin/settings', $context);

$PAGE->requires->yui_module('moodle-core-formchangechecker',
        'M.core_formchangechecker.init',
        array(array(
            'formid' => 'adminsettings'
        ))
);
$PAGE->requires->string_for_js('changesmadereallygoaway', 'moodle');

if ($settingspage->has_dependencies()) {
    $opts = [
        'dependencies' => $settingspage->get_dependencies_for_javascript()
    ];
    $PAGE->requires->js_call_amd('core/showhidesettings', 'init', [$opts]);
}

echo $OUTPUT->footer();
