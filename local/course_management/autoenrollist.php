<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 * @package    local_course_management
 * @copyright  Manjunath B K<manjunathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');

global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$did = optional_param('did','',PARAM_INT);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
if(!empty($did)){
	$PAGE->set_url($CFG->wwwroot . '/local/course_management/autoenrollist.php?did='.$did);
}else{
	$PAGE->set_url($CFG->wwwroot . '/local/course_management/autoenrollist.php');
}
$title = get_string('autoenrollmentlist', 'local_course_management');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_management/js/custom.js'), true);
echo $OUTPUT->header();
$html='';
$html.=html_writer::start_div('pt-5 pb-3');
$html.=html_writer::start_tag('h2');
$html.=get_string('autoenrollmentlist','local_course_management');
$html.=html_writer::end_tag('h2');
$html.=html_writer::end_div('');
echo $html;
$retuurnurl=$CFG->wwwroot.'/local/course_management/autoenrollment.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
if(!empty($did)){
	//unenrolling users.
	//unenroll_users_enrolled_in_rule($did);
	$response = $DB->delete_records('local_course_management', array('id'=>$did));
	if($response){
		$sucssmsg = get_string('delsuccessmsg','local_course_management');
		echo $OUTPUT->notification($sucssmsg, 'notifysuccess');
	}else{
		$delerrormsg = get_string('delerrormsg','local_course_management');
		echo $OUTPUT->notification($delerrormsg);
	}
}
//creating a table to display already created autoenrollment rules.
$rulestable  = new \html_table();
$rulestable->head = array(get_string('rulename', 'local_course_management'),
	get_string('ruledescription', 'local_course_management'),
	get_string('rulekey', 'local_course_management'),
	get_string('rulevalue', 'local_course_management'),
	get_string('courses', 'local_course_management'),
	get_string('delete', 'local_course_management')
);
$getallrules = $DB->get_records('local_course_management',array('companyid'=>$USER->company->id));
foreach ($getallrules as $rule) {
	if($rule->rulekey == 0){
		$rulekey = get_string('department','local_course_management');
		$alldepartments = get_alldepartments_ebdaa();
		if (array_key_exists($rule->rulevalue, $alldepartments)) {
			$rulevalue = $alldepartments[$rule->rulevalue];
		}
		$matchingurl = new moodle_url($CFG->wwwroot.'/local/course_management/matchingusers.php',array('ruleval'=>$rulevalue,'ruletype'=>$rule->rulekey));
		$matchinglink = '<a href ='.$matchingurl.'>'.get_string('matchingusers', 'local_course_management').'</a>';
	}elseif($rule->rulekey == 1){
		$rulekey = get_string('manager','local_course_management');
		$allmanagers = get_all_managers_ebdaa();
		if (array_key_exists($rule->rulevalue, $allmanagers)) {
			$rulevalue = $allmanagers[$rule->rulevalue];
		}
		$matchingurl = new moodle_url($CFG->wwwroot.'/local/course_management/matchingusers.php',array('ruleval'=>$rule->rulevalue,'ruletype'=>$rule->rulekey));
		$matchinglink = '<a href ='.$matchingurl.'>'.get_string('matchingusers', 'local_course_management').'</a>';
	}
	elseif($rule->rulekey == 2){
		//$rulekey = 'All Users';
		$rulekey = get_string('all_users','local_course_management');
		$allmanagers = get_all_users_ebdaa();
		$matchingurl = new moodle_url($CFG->wwwroot.'/local/course_management/matchingusers.php',array('ruleval'=>'','ruletype'=>$rule->rulekey));
		$matchinglink = '<a href ='.$matchingurl.'>'.get_string('matchingusers', 'local_course_management').'</a>';
	}
	$courses = explode(",", $rule->courses);
	$selectedcourse = '';
	$i=0;
	foreach($courses as $course){
		$coursename = $DB->get_field('course', 'fullname',array('id'=>$course));
		if($i==0){
			$selectedcourse=$coursename;
		}else{
			$selectedcourse.=', <br>'.$coursename;
		}
		$i++;
	}
	$delete = new moodle_url($CFG->wwwroot.'/local/course_management/delete.php?roleid='.$rule->id);
	$deletebutton = '<a href ='.$delete.'>'.'<i class="fa fa-trash-o" aria-hidden="true"></i>'.'</a>';
	$rulestable->data[] = array($rule->rulename,
		$rule->ruledescription,
		$rulekey,
		$rulevalue.'<br>'.$matchinglink,
		$selectedcourse,
		$deletebutton);
}
echo html_writer::table($rulestable);
echo $OUTPUT->footer();
