 <?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Skillaccess cap local
 *
 * @package    local_access_level_org_report
 */

$capabilities = array(
    //view overall
	'local/phishing:overall' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'manager' => CAP_ALLOW
		)
	),
    //for phishing admin.
	'local/phishing:phishingadmin' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_USER,
		'archetypes' => array(
			'manager' => CAP_ALLOW
		)
	),
    //template creation 
	'local/phishing:createtemp' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'manager' => CAP_ALLOW
		)
	),
     //landing page 
	'local/phishing:landingpage' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'manager' => CAP_ALLOW
		)
	),
      //sending profile 
	'local/phishing:stdprofile' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'manager' => CAP_ALLOW
		)
	),
      //Campaigns
	'local/phishing:campaigns' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'manager' => CAP_ALLOW
		)
	),
	//usergroup
	'local/phishing:usergroup' => array(
		'captype' => 'write',
		'contextlevel' => CONTEXT_SYSTEM,
		'archetypes' => array(
			'manager' => CAP_ALLOW
		)
	)
);
