<?php 
$string['pluginame'] = 'تذكير بالدورة';
$string['pleaseselectcourse'] = 'الرجاء اختيار الدورة';
$string['selectcourse'] = 'اختار الدورة';
$string['submit'] = 'الموافقة';
$string['course'] = 'الدورة';
$string['inprogress'] = 'في تقدم';
$string['notstarted'] = 'لم يتم البدأ';
$string['sendmail'] = 'إرسال بريد';
$string['previousmailsent'] = 'عرض آخر بريد تم ارساله';
$string['emailsettings'] = 'إعدادات البريد الإلكتروني للتذكير';
$string['mailbody'] = 'محتوى البريد';
$string['mailsubject'] = 'الموضوع';
$string['send'] = 'ارسال';
$string['resetpass'] = 'إعادة تعيين كلمة السر';
$string['newaccountmail'] = 'بريد حساب جديد';
$string['enrollmentmail'] = 'بريد التسجيل';
$string['courseremindermail'] = 'بريد تذكير عن الدورة';
$string['username'] = 'الاسم';
$string['sentdate'] = 'تاريخ الارسال';
$string['coursename'] = 'اسم الدورة';
$string['mailtype'] = 'نوع البريد';
$string['mailreport'] = 'تقرير البريد';
$string['headingforuserremindermailwithcron'] = 'لرسائل التذكير التلقائية عبر البريد الإلكتروني - أسبوع واحد';
$string['headingforuserremindermailwithcrononeday'] = 'لرسائل التذكير التلقائية عبر البريد الإلكتروني - يوم واحد';
$string['not_applicable'] = 'غير متاح';







//For New User Account Mail 
$string['subjectheadingfornewuseremail'] = 'لحساب بريد جديد';
$string['newuseraccountmail'] = 'الموضوع والنص لبريد حساب المستخدم الجديد';
$string['enternewusermailsubject'] = 'الرجاء إدخال موضوع البريد لحساب المستخدم الجديد';
$string['enternewusermailbody'] = 'الرجاء إدخال نص البريد لحساب المستخدم الجديد';
//For New User Enrollment Mail
$string['headingforuserenrolmentmail'] = 'للحصول على نص البريد لتسجيل مستخدم جديد';
$string['enterenrollmailsubject'] = 'الرجاء إدخال موضوع البريد لتسجيل المستخدم';
$string['userenrollmentmail'] = 'الموضوع والمحتوى لتسجيل مسنخدم';
$string['enterenrollmailbody'] = 'يرجى إدخال محتوى البريد الإلكتروني';
//For User Reminder Mail 
$string['headingforuserremindermail'] = 'لبريد تذكير المستخدم';
$string['enterremindermailsubject'] = 'الرجاء إدخال موضوع البريد لتذكير المستخدم';
$string['enterreminderemailbody'] = 'يرجى إدخال محتوى البريد الإلكتروني للتذكير';
$string['userremindermail'] = ' برجى إدخال البريد الإلكتروني للتذكير';

$string['enteremailsubject'] = 'الرجاء إدخال موضوع البريد';
$string['emailbodydescription'] = 'الرجاء إدخال البريد الإلكتروني';

