<?php
//get templates 
function get_templates1($urlip,$apikey){
	global $CFG,$DB,$USER;
	$companyid = $USER->company->id;
	$protocol = get_config('local_phishing'.$companyid, 'protocol');
	$curl_handle = curl_init();
	$url = $protocol.$urlip.'/api/templates/?api_key='.$apikey;
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
// Set the curl URL option
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl_handle, CURLOPT_URL, $url);
// This option will return data as a string instead of direct output
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
// Execute curl & store data in a variable
	$curl_data = curl_exec($curl_handle);
	if (!curl_exec($curl_handle)) {
		die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
	}
	$template_datas = json_decode($curl_data);
	$template = [];
	if(!empty($template_datas)){
		foreach ($template_datas as $key => $template_data) {				# code...
			$template[$template_data->id]=$template_data->name;
		}
	}
	return $template;
}

//get landing page 

function get_landing_pages1($urlip,$apikey){
	global $CFG,$DB,$USER;
	$companyid = $USER->company->id;
	$protocol = get_config('local_phishing'.$companyid, 'protocol');
	$curl_handle = curl_init();
	$url = $protocol.$urlip.'/api/pages/?api_key='.$apikey;
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// Set the curl URL option
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl_handle, CURLOPT_URL, $url);
	// This option will return data as a string instead of direct output
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	// Execute curl & store data in a variable
	$curl_data = curl_exec($curl_handle);
	if (!curl_exec($curl_handle)) {
		die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
	}
	$page_datas = json_decode($curl_data);
	$pagedata = [];
	if(!empty($page_datas)){
			foreach ($page_datas as $key => $page_data) {				# code...
				$pagedata[$page_data->id]=$page_data->name;
			}
		}
		return $pagedata;
	}
//sending profile 
	function get_sending_profile1($urlip,$apikey){
		global $CFG,$DB,$USER;
		$companyid = $USER->company->id;
		$protocol = get_config('local_phishing'.$companyid, 'protocol');
		$curl_handle = curl_init();
		$url = $protocol.$urlip.'/api/smtp/?api_key='.$apikey;
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
	// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		if (!curl_exec($curl_handle)) {
			die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
		}
		$smtp_datas = json_decode($curl_data);
	//print_object($smtp_datas);
		$smtpdata = [];
		if(!empty($smtp_datas)){
			foreach ($smtp_datas as $key => $smtp_data) {
				$smtpdata[$smtp_data->id]=$smtp_data->name;
			}
		}
		return $smtpdata;
	}





