<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_accesscohort
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../../config.php');
require_once('../form/user_group_create_form.php');
require_once($CFG->libdir . '/formslib.php');
global $DB,$USER;
require_login(0,false);
$companyid = $USER->company->id;
//edit params
$eid = optional_param('id',null, PARAM_INT);
$edit = optional_param('edit',null, PARAM_INT);
//api key initialization
//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$context = context_system::instance();
$overall = has_capability('local/phishing:overall',$context);
//$createusergroup = has_capability('local/phishing:usergroup',$context);
//curl initialization
$curl_handle = curl_init();
$PAGE->set_context(context_system::instance());
if($edit==1){
	$title = get_string('editusergroup', 'local_phishing');
}else{
	$title = get_string('usergroup', 'local_phishing');
}
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/action/user_group_create.php');
require_login();
$previewnode = $PAGE->navigation->add(get_string('pluginname','local_phishing'), new moodle_url($CFG->wwwroot.'/local/phishing/usergrouplist.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title, new moodle_url($CFG->wwwroot.'/local/phishing/action/user_group_create.php'));
$thingnode->make_active();
$mform = new user_group_create_form();
$temp = false;
if ($mform->is_cancelled()){
	redirect(new moodle_url('/local/phishing/usergrouplist.php', array()));
} else if ($data = $mform->get_data()) {
	//take user records here 
	if(!empty($data->targets)){
		$userarray = [];
		foreach($data->targets as $key => $target){
			$userobject = $DB->get_record('user',array('id'=>$target));
			if(!empty($userobject)){
				$cerareobject = new stdClass();
				$cerareobject->email = $userobject->email;
				$cerareobject->first_name =  $userobject->firstname;
				$cerareobject->last_name =  $userobject->lastname;
				$cerareobject->position = '';
				$userarray[] = $cerareobject;
			}			
		}
	}
	//textarea values here 
	if(!empty($data->emailarea)){
		$useremails =explode(',',$data->emailarea);
		$userarray = [];
		$eamilarray ='';
		foreach($useremails as $key1 => $useremail){
			$userobject = $DB->get_record('user',array('email'=>$useremail));
			if(!empty($userobject)){
				$cerareobject = new stdClass();
				$cerareobject->email = $userobject->email;
				$cerareobject->first_name =  $userobject->firstname;
				$cerareobject->last_name =  $userobject->lastname;
				$cerareobject->position = '';
				$userarray[] = $cerareobject;
			}else{
				$eamilarray .= $useremail.'-';
			}			
		}		
	}
	
	//insert values array preparation
	
	//print_object($userarray);die;
	//Create array here 
	if($edit==1 and !empty($eid)){
		$array = array('id'=>$eid,'name'=>$data->name,'targets'=>$userarray);
	}else{
		$array = array('name'=>$data->name,'targets'=>$userarray);
	}
	
	$jsonarray = json_encode($array);
	//print_object($jsonarray);die;
	//curl initialization
	if(!empty($jsonarray)){
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		if($edit==1 and !empty($eid)){
			$url = $protocol.$urlip.'/api/groups/'.$eid.'?api_key='.$apikey;				
			curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($curl_handle, CURLOPT_URL, $url);	
			curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$jsonarray);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
			$response  = curl_exec($curl_handle);
			$msg = get_string('groupedit','local_phishing');		
		}else{
			$url = $protocol.$urlip.'/api/groups/?api_key='.$apikey;
			curl_setopt($curl_handle, CURLOPT_URL, $url);	
			curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($curl_handle, CURLOPT_POST, 1);
			curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$jsonarray);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
			$response  = curl_exec($curl_handle);
			$msg = get_string('groupcreate','local_phishing');
		}
		if (!curl_exec($curl_handle)) {
			die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
		}
		curl_close($curl_handle);	
	}
	if(empty($userarray)){
		$msg = 'Please contact to admin for group and user creation';
	}
		//REDIRECT PAGE 
		if($response) {
			if(!empty($eamilarray)){
				redirect(new moodle_url($CFG->wwwroot.'/local/phishing/usergrouplist.php'),$msg.'<br>'.$eamilarray.get_string('emailnot','local_phishing'));
			}else{
				redirect(new moodle_url($CFG->wwwroot.'/local/phishing/usergrouplist.php'),$msg);
			}
			
		} else {
			echo "error";
		}	
				
			
	
	
}else{
	$url = $protocol.$urlip.'/api/groups/'.$eid.'?api_key='.$apikey;
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		// Set the curl URL option
			//curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl_handle, CURLOPT_URL, $url);
		// This option will return data as a string instead of direct output
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		// Execute curl & store data in a variable
	$curl_data = json_decode(curl_exec($curl_handle));	
	if(!empty($curl_data)){
		//users data array 
		$userarray =[];
		if(!empty($curl_data->targets)){
			foreach($curl_data->targets as $key => $target){
				$userobject = $DB->get_record('user',array('email'=>$target->email,'firstname'=>$target->first_name,'lastname'=>$target->last_name));
				if(!empty($userobject)){
					$userarray[] = $userobject->id;
				}					
			}
		}		
	//edit value set here 		
		if($edit==1 and !empty($eid)){
			$setdata = new stdClass();
			$setdata->name  = $curl_data->name;
			$setdata->targets  = $userarray;
			$mform->set_data($setdata);
		}
	}	
}
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/phishing/usergrouplist.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback
if($overall){
	$mform->display();
}else{
	echo html_writer::div(
		get_string('cap', 'local_phishing'),'alert alert-danger'
	);
}

echo $OUTPUT->footer();
?>






