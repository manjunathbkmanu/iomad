<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Pages main view page.
 *
 * @package         local_pages
 * @author          Kevin Dibble <kevin.dibble@learningworks.co.nz>.
 * @copyright       2017 LearningWorks Ltd.
 * @license         http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later.
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');

// Get the id of the page to be displayed.
$pageid = optional_param('id', 0, PARAM_INT);
require_login(true);
// Setup the page.
$PAGE->set_context(\context_system::instance());
$PAGE->set_url("{$CFG->wwwroot}/local/pages/index.php", ['id' => $pageid]);

require_once("{$CFG->dirroot}/local/pages/lib.php");
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/local/pages/js/lightbox/css/style.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/local/pages/js/lightbox/css/effect.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/local/pages/js/lightbox/css/flashy.css'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/responsive.css'));

// $PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/pages/js/custom.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/pages/js/lightbox/jquery-1.12.4.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/pages/js/lightbox/jquery.flashy.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/pages/js/lightbox/main.js'),true);
// Set the page layout.
$custompage     = \local_pages\custompage::load($pageid);

// Check if the page has an access level requirement.
$accesslevel    = $custompage->accesslevel;
if ($accesslevel != '') {
    require_login();
}

$templatename   = trim($custompage->pagelayout) != '' ? $custompage->pagelayout : 'standard';
$templatename = 'media'; //Mihir 
$PAGE->set_pagelayout($templatename);

// Now, get the page renderer.
$renderer = $PAGE->get_renderer('local_pages');

// More page setup.
$PAGE->set_title($custompage->pagename);
$PAGE->set_heading($custompage->pagename);

// Add a class to the body that identifies this page.
if ($pageid) {
    if ($pagedata = $DB->get_record('local_pages', ['id' => $pageid])) {
        // Make the page name lowercase.
        $pagedata->pagename = \core_text::strtolower($pagedata->pagename);

        // Now join the page name if we need to.
        $pagedata->pagename = implode('-', explode(' ', $pagedata->pagename));

        // Generate the class name with the following naming convention {pagetype}-local-pages-{pagename}-{pageid}.
        $classname = "{$pagedata->pagetype}-local-pages-{$pagedata->pagename}-{$pageid}";

        // Now add that class name to the body of this page :).
        $PAGE->add_body_class($classname);
    }
}

$previewnode = $PAGE->navigation->add(get_string('preview'), new moodle_url('/a/link/if/you/want/one.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add(get_string('home'), new moodle_url('/a/link/if/you/want/one.php'));
$thingnode->make_active();

// Output the header.
echo $OUTPUT->header();

// Output the page content.
echo $renderer->showpage($custompage);

//get current language and based on that we will make the tab active
$enactive = '';
$aractive = '';
$lang = current_language();
if ($lang == 'en') {
	$enactive = 'active';
} else {
	$aractive = 'active';
}

//now we will show the child pages in grid
//check if page is a parent page and is having children
$records = $DB->get_records_sql("SELECT * FROM {local_pages} WHERE deleted=0 AND " .
            "pageparent=? ORDER BY pageorder", array($pageid));
if (!empty($records)) {
	$pagedetails = '';	
	$pagedetails .= '<ul class="nav nav-tabs" id="myTab" role="tablist">
	  <li class="nav-item">
		<a class="nav-link '.$enactive.'" id="english-tab" data-toggle="tab" href="#english" role="tab" aria-controls="english"
		  aria-selected="true">English</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link '.$aractive.'" id="arabic-tab" data-toggle="tab" href="#arabic" role="tab" aria-controls="arabic"
		  aria-selected="false">Arabic</a>
	  </li>
	</ul>';	
	$pagedetails .= '<div class="tab-content" id="myTabContent">';
		//englis tab
		$pagedetails .= '<div class="tab-pane fade show '.$enactive.'" id="english" role="tabpanel" aria-labelledby="english-tab">';
		$pagedetails .= '<div class="container-fluid">';
		$pagedetails .= '<div class="row mt-4 mb-4">';
		if (!empty($records)) {
			foreach($records as $key =>$child) {
				if($child->pagelang == 'en') {
				$custompagechild     = \local_pages\custompage::load($child->id);
				$pagedetails .= $renderer->showpage_child($custompagechild);
				}
			}
		}
		$pagedetails .= '</div>';
		$pagedetails .= '</div>';
		$pagedetails .= '</div>';
		
		//arabic tab
		$pagedetails .= '<div class="tab-pane fade show '.$aractive.'" id="arabic" role="tabpanel" aria-labelledby="arabic-tab">';
		$pagedetails .= '<div class="container-fluid">';
		$pagedetails .= '<div class="row mt-4 mb-4">';
		if (!empty($records)) {
			foreach($records as $key =>$child) {
				if($child->pagelang == 'ar') {
				$custompagechild     = \local_pages\custompage::load($child->id);
				$pagedetails .= $renderer->showpage_child($custompagechild);
				}
			}
		}
		$pagedetails .= '</div>';
		$pagedetails .= '</div>';
		$pagedetails .= '</div>';
	$pagedetails .= '</div>'; //end of tab-content
	
	
  $pagedetails .= '<div class="modal fade" id="myModal">
    <div class="modal-dialog" style="max-width:100%;height:100%;">
      <div class="modal-content" style="height:100%;background-color:transparent;">
      
        
        <!-- Modal body -->
        <div class="modal-body">
          <iframe id="lightboxiframe" style="width:100%;height:100%;"></iframe>
        </div>
        
      </div>
    </div>
  </div>
  
  <style>
  	iframe > html > body {
  		background-color: yellow;
	}
  </style>
  ';

	echo $pagedetails;
}


// Now output the footer.
echo $OUTPUT->footer();