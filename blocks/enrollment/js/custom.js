$(document).ready(function() {
	$('#course_selection_auto').multiselect({
      includeSelectAllOption: true,
      allSelectedText: 'All',
      maxHeight: 200,
      enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
      selectAllText:'All Courses',
      nonSelectedText:'Select Courses'
	});
});


$('#user_auto_selection').on('change', function() {
	var user = $('#user_auto_selection').val();
	$.ajax({
		type:"post",
		url:M.cfg.wwwroot+"/blocks/enrollment/refresh.php",
		data:{
			submit:'submit',
			user:user,
		},
		success: function( newdata ) {
			/*Changed By Dilip*/
			// $(".multiselect-container").html(newdata);
			$('#course_selection_auto').val(JSON.parse(newdata));
		},
	});
});

$('.enroll_submit_btn').click(function(e){
	e.preventDefault();
	var user = $('#user_auto_selection').val();
	var array1 = []; 
	$("input:checked").each(function() { 
		array1.push($(this).val()); 
	});
	var sendarray = array1.toString();
	$.ajax({
		type:"GET",
		url:M.cfg.wwwroot+"/blocks/enrollment/ajax.php",
		data:{enrol:'enrol',user:user,sendarray:sendarray},
		success: function( ndata ) {
				$('#success').html(ndata);
		},
	});
})
