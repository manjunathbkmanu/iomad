<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'newiomad';
$CFG->dbuser    = 'iomad';
$CFG->dbpass    = 'IomadDemo123!@#';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
  'dbcollation' => 'utf8mb4_unicode_ci',
);

$CFG->wwwroot   = 'https://dof.ebdaalms.com';
$CFG->dataroot  = '/var/www/dof/iomad';
$CFG->admin     = 'admin';
$CFG->theme     = 'maker';

$CFG->directorypermissions = 0777;



require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!