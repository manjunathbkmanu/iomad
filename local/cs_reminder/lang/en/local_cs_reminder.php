<?php 
$string['pluginame'] = 'Course Reminder';
$string['pleaseselectcourse'] = 'Please Select Course';
$string['selectcourse'] = 'Select Course';
$string['submit'] = 'Submit';
$string['course'] = 'Course';
$string['inprogress'] = 'In Progress';
$string['notstarted'] = 'Not Started';
$string['sendmail'] = 'Send Mail';
$string['previousmailsent'] = 'View Previous Mail Sent';
$string['emailsettings'] = 'Reminder Email Settings';
$string['mailbody'] = 'Body';
$string['mailsubject'] = 'Subject';
$string['send'] = 'Send';
$string['resetpass'] = 'Reset Password';
$string['newaccountmail'] = 'New Account Mail';
$string['enrollmentmail'] = 'Enrollment Mail';
$string['courseremindermail'] = 'Course Reminder Mail';
$string['username'] = 'Name';
$string['sentdate'] = 'Sent Date';
$string['coursename'] = 'Course Name';
$string['mailtype'] = 'Mail Type';
$string['mailreport'] = 'Mail Report';



//For New User Account Mail 
$string['subjectheadingfornewuseremail'] = 'For New User Account Mail';
$string['newuseraccountmail'] = 'Subject and body for new user account mail';
$string['enternewusermailsubject'] = 'Please enter mail subject for new user account (For password please mention [[your_password]])';
$string['enternewusermailbody'] = 'Please enter mail body for new user account';
//For New User Enrollment Mail
$string['headingforuserenrolmentmail'] = 'For New User Enrollment Mail';
$string['enterenrollmailsubject'] = 'Please enter mail subject for user enrollment';
$string['userenrollmentmail'] = 'Subject and Body For User Enrollment Mail';
$string['enterenrollmailbody'] = 'Please enter mail body for enrollment';


//For User Reminder Mail 
$string['headingforuserremindermail'] = 'For User Reminder Mail';
$string['enterremindermailsubject'] = 'Please enter mail subject for user reminder';
$string['enterreminderemailbody'] = 'Please enter mail body for reminder mail';
$string['userremindermail'] = 'Subject and Body For User Reminder Mail';

$string['enteremailsubject'] = 'Please enter mail subject';
$string['emailbodydescription'] = 'Please enter mail body';

//For User reminder mail with cron 
$string['headingforuserremindermailwithcron'] = 'For Automatic Reminder Emails - 1 week';
$string['headingforuserremindermailwithcrononeday'] = 'For Automatic Reminder Emails - 1 day';
$string['not_applicable'] = 'N/A';
