<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * You may have settings in your plugin
 *
 * @copyright  Manjunath<manjunath@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
global $OUTPUT, $CFG;
require_once($CFG->libdir.'/formslib.php');
require_login();

class reminder_setting_form extends moodleform {
    function definition() {
        $mform =& $this->_form;
        //1.
        $mform->addElement('header', 'newuseraccountheader', get_string('subjectheadingfornewuseremail', 'local_cs_reminder'));

        $mform->addElement('static', 'newuseraccountheadertext', get_string('newuseraccountmail', 'local_cs_reminder'));

        $mform->addElement('text', 'subjectfornewuseraccountmail', get_string('enternewusermailsubject', 'local_cs_reminder'));
        $mform->setType('subjectfornewuseraccountmail', PARAM_TEXT);

        $mform->addElement('editor', 'newusermailbody', get_string('enternewusermailbody', 'local_cs_reminder'));
        $mform->setType('newusermailbody', PARAM_RAW);

        //2.
        $mform->addElement('header', 'headingforuserenrolmentmail', get_string('headingforuserenrolmentmail', 'local_cs_reminder'));

        $mform->addElement('static', 'userenrollmentmail', get_string('userenrollmentmail', 'local_cs_reminder'));

        $mform->addElement('text', 'subjectfornewuserenrollment', get_string('enterenrollmailsubject', 'local_cs_reminder'));
        $mform->setType('subjectfornewuseraccountmail', PARAM_TEXT);

        $mform->addElement('editor', 'userenrollmentmailbody', get_string('enterenrollmailbody', 'local_cs_reminder'));
        $mform->setType('newusermailbody', PARAM_RAW);

        //3.
        $mform->addElement('header', 'headingforuserremindermail', get_string('headingforuserremindermail', 'local_cs_reminder'));

        $mform->addElement('static', 'userremindermail', get_string('userremindermail', 'local_cs_reminder'));

        $mform->addElement('text', 'enterremindermailsubject', get_string('enterremindermailsubject', 'local_cs_reminder'));
        $mform->setType('enterremindermailsubject', PARAM_TEXT);

        $mform->addElement('editor', 'enterreminderemailbody', get_string('enterreminderemailbody', 'local_cs_reminder'));
        $mform->setType('enterreminderemailbody', PARAM_RAW);

        //4.
        $mform->addElement('header', 'headingforuserremindermailwithcron', get_string('headingforuserremindermailwithcron', 'local_cs_reminder'));

        $mform->addElement('static', 'userremindermail2', get_string('userremindermail', 'local_cs_reminder'));

        $mform->addElement('text', 'enterremindermailsubject2', get_string('enterremindermailsubject', 'local_cs_reminder'));
        $mform->setType('enterremindermailsubject2', PARAM_TEXT);

        $mform->addElement('editor', 'enterreminderemailbody2', get_string('enterreminderemailbody', 'local_cs_reminder'));
        $mform->setType('enterreminderemailbody2', PARAM_RAW);

        //5.
        $mform->addElement('header', 'headingforuserremindermailwithcrononeday', get_string('headingforuserremindermailwithcrononeday', 'local_cs_reminder'));

        $mform->addElement('static', 'userremindermail3', get_string('userremindermail', 'local_cs_reminder'));

        $mform->addElement('text', 'enterremindermailsubject3', get_string('enterremindermailsubject', 'local_cs_reminder'));
        $mform->setType('enterremindermailsubject3', PARAM_TEXT);

        $mform->addElement('editor', 'enterreminderemailbody3', get_string('enterreminderemailbody', 'local_cs_reminder'));
        $mform->setType('enterreminderemailbody3', PARAM_RAW);


        $this->add_action_buttons();




    }
}