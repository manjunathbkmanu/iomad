<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_ebdaa_license
 * @copyright  Manjunath B K<manjunath@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('forms/license_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once 'includes/lb_helper.php';


// Create a new LicenseBoxAPI helper class.
$lbapi = new LicenseBoxAPI();
global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin_license');
$PAGE->set_url($CFG->wwwroot . '/local/ebdaa_license/index.php');
$title = get_string('title', 'local_ebdaa_license');
//$PAGE->navbar->ignore_active();
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();

$lb_verify_res = $lbapi->verify_license(false);

echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback

$mform = new local_license_form();
if($formdata = $mform->get_data()){
	if(!empty($formdata)){
		if (!empty($formdata->submit)) {
			$lb_activate_res = $lbapi->activate_license($formdata->licensecode, $formdata->clientname);
			$lb_verify_res = $lbapi->verify_license(false);
		}	
	}
}

$html='';
$html.= html_writer::start_div('message');
if($lb_verify_res['status']!=true){
	$html.='<div class="alert alert-danger alert-block fade in" role="alert" data-aria-autofocus="true" tabindex="0">'.((isset($lb_activate_res['message']))?$lb_activate_res['message']:(get_string('invalidmessage', 'local_ebdaa_license'))).'</h6></div>';
	$html.='<h4 style="padding-bottom: 4px;">'.get_string('licenseandclient', 'local_ebdaa_license').'</h4>';
}else{
	$html.='<div class="alert alert-success alert-block fade in" role="alert" data-aria-autofocus="true" tabindex="0">'.get_string('licenseactivated', 'local_ebdaa_license').get_string('expirydate', 'local_ebdaa_license').(($lb_verify_res['data'])?(date("d-m-Y H:i:s A", strtotime($lb_verify_res['data']))):'-').'</h6></div>';
	$html.='<h4 style="padding-bottom: 4px;">'.get_string('licenseandclientupdate', 'local_ebdaa_license').'</h4>';
}
$html.= html_writer::end_div();
echo $html;

$mform->display();
echo $OUTPUT->footer();

