<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the Activity modules block.
 *
 * @package    block_activity_modules
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/filelib.php');


class block_enrolled_courses extends block_base {

   function init() {
    $this->title = get_string('pluginname', 'block_enrolled_courses');
}

function get_content() {

    global $CFG, $DB, $OUTPUT,$USER;
    require_once($CFG->dirroot. "/lib/enrollib.php");
    require_once($CFG->dirroot . "/course/lib.php");
    require_once("$CFG->libdir/gradelib.php");
    require_once("$CFG->dirroot/grade/querylib.php");

    if($this->content !== NULL) {
        return $this->content;
    }

    $this->content = new stdClass;
    $this->content->items = array();
    $this->content->icons = array();
    $sortorder = 'visible DESC, sortorder ASC';
    $dateformat = '%d-%b-%Y';
    $duration = '';
    $table = new html_table();
    $table->head = (array) get_strings(array( 'cname','enddate','duration','vgrade','status'), 'block_enrolled_courses');
    //Fetch all the courses enrolled by logged in user.
    $courses = enrol_get_all_users_courses($USER->id);
    //check wether the courses are empty or not.
    if(!empty($courses)){
        //fetch single courses  using foreach loop.
        foreach($courses as $course) {
            //get all the course module info.
            $cminfo = get_fast_modinfo($course, $USER->id);
            foreach($cminfo->get_cms() as $cm) {
                if($cm->modname == 'scorm'){
                    //This will provide link button.
                    $scormlink = $this->get_scorm_status_ebdaa_blocktable($cm->instance,$course);
                    //This will provide pass or fail.
                    $passfail = $this->get_scorm_status_ebdaa_passfail_blockenrolled($cm->instance,$course);
                    //get the enrolled date.
                    $instance1 = $DB->get_record('enrol', array('courseid' => $course->id, 'enrol' => 'manual'), '*', MUST_EXIST);
                    $enrolid = $DB->get_record('user_enrolments',array('enrolid'=>$instance1->id,'userid'=>$USER->id));
                    if(!empty($enrolid)){
                        $enroldate = userdate($enrolid->timecreated,$dateformat);
                    }else{
                        $enroldate = '-';
                    }
                    //Get the end date of the course.
                    $completecdetails = $DB->get_record('course', array('id'=>$course->id));
                    $enddate = $completecdetails->enddate;
                    if(!empty($enddate)){
                        $enddate = date('d-m-Y',$enddate);
                    }else{
                        $enddate = '-';
                    }
                    //Now get duration from custom field table.
                    $fieldidsql = $DB->get_record('customfield_field', array('shortname'=>'courseduration'));
                    if(!empty($fieldidsql))
                    {
                        $fieldid = $fieldidsql->id;
                //get value
                        $fielddurationsql = $DB->get_record('customfield_data', array('fieldid'=>$fieldid, 'instanceid' =>$course->id));
                        if(!empty($fielddurationsql))
                        {
                         $duration = $fielddurationsql->value;
                     }
                 }
                 //If pass/fail returns pass then show default score as 100% 9 Jan 2020.
                 if ($passfail == 'PASS' or $passfail == 'pass') {
                    $gradeRoundvalue= '100';
                } else {
                    $grades = grade_get_course_grades($course->id, $USER->id);
                    $grademax = $grades->grademax;
                    $gradeValue =(((int) $grades->grades[$USER->id]->str_grade));
                    if(!$gradeValue==null){
                        $gradeRoundvalue = (($gradeValue * 100)/$grademax);
                    }else{
                        $gradeRoundvalue='-';
                    }
                }
                //Manju: arabic name and english name based on language selection.
                $coursedurationar = $duration; 
                $coursename =$course->fullname;       
                if(current_language() == 'ar'){
                    $coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));

                    $coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$course->id));
                    $coursename = $coursenamearabic;
                    if(empty($coursename)){
                        $coursename =$course->fullname;
                    }

                    $courseduration = $DB->get_field('customfield_field', 'id', array('shortname'=>'durationinarabic'));

                    $coursedurationar = $DB->get_field('customfield_data', 'value', array('fieldid'=>$courseduration,'instanceid'=>$course->id));
                    if(empty($coursedurationar)){
                        $coursedurationar = $duration;
                    }
                }
                $table->data[] = array($coursename,$enddate,$coursedurationar,$gradeRoundvalue,$scormlink);
            }
        }
    }
    $tabeled = html_writer::table($table);
    $this->content->text = html_writer::div($tabeled,null,array('id'=>'table12'));
    $this->content->footer = '';
    return $this->content;
}
}
/**
 *
 * @param type $scormid
 * @param type $userid
 */
function get_scorm_status_ebdaa_passfail_blockenrolled($scormid,$course) {
    global $DB, $USER,$CFG;
    $scormstatus = '-';
    $userid = $USER->id;
    /*
    $sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
    $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
*/
    $sql = "SELECT attempt
    FROM {scorm_scoes_track}
    WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
    ";
    
    $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));
    if (!empty($lastattempt)) {
        $sql = "SELECT value
        FROM {scorm_scoes_track}
        WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
        $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

        if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
            if($status == 'passed') { $stringshow = 'pass';}
            if($status == 'failed') { $stringshow = 'fail';}
            if($status == 'complete') { $stringshow = 'pass';}
            if($status == 'completed') { $stringshow = 'pass';}
            $scormstatus = strtoupper($stringshow);

        } else if ($status == 'incomplete') {
            $scormstatus = '-'; 
        }
    } else {

        //non scored scorm only returns lesson status
        $sql = "SELECT MAX(attempt)
        FROM {scorm_scoes_track}
        WHERE userid = ? AND scormid = ?
        ";
        $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
        if (!empty($lastattempt)) {
            $sql = "SELECT value
            FROM {scorm_scoes_track}
            WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
            $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

            if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
                if($status == 'passed') { $stringshow = 'pass';}
                if($status == 'failed') { $stringshow = 'fail';}
                if($status == 'complete') { $stringshow = 'pass';}
                if($status == 'completed') { $stringshow = 'pass';}
                $scormstatus = strtoupper($stringshow);

            } else if ($status == 'incomplete') {
                $scormstatus = '-'; 
            }
        } else {
            $scormstatus = '-';
        }

    }
    return $scormstatus;
}

	/**
 *
 * @param type $scormid
 * @param type $userid
 */
    function get_scorm_status_ebdaa_blocktable($scormid,$course) {
        global $DB, $USER,$CFG;
        $status = '-';
        $userid = $USER->id;
        $sql = "SELECT attempt
        FROM {scorm_scoes_track}
        WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
        ";

        $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));

        if (!empty($lastattempt)) {
            $sql = "SELECT value
            FROM {scorm_scoes_track}
            WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
            $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

            if(!empty($status)) {
                if ($status == 'completed' OR $status == 'complete' OR $status == 'passed' OR $status == 'failed'){
                	$scormstatus = get_string('completed','block_enrolled_courses');
                      $scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
                    <button class="btn btn-sm bg-green w-125" style="cursor:pointer;">'.$scormstatus.'</button>
                    </a>';
                    
                } else if ($status == 'incomplete') {
                  	$scormstatus = get_string('inprogress','block_enrolled_courses');
                   $scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
                    <button class="btn btn-sm bg-yellow w-125" style="cursor:pointer;">'.$scormstatus.'</button>
                    </a>';
                }
            }
        } else {

            $sql = "SELECT MAX(attempt)
            FROM {scorm_scoes_track}
            WHERE userid = ? AND scormid = ?
            ";
            $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
            if (!empty($lastattempt)) {
                $sql = "SELECT value
                FROM {scorm_scoes_track}
                WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
                $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

                if ($status == 'completed' OR $status == 'complete' OR $status == 'passed' OR $status == 'failed'){
                 	$scormstatus = get_string('complete','block_enrolled_courses');
                    $scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
                    <button class="btn btn-sm bg-green w-125" style="cursor:pointer;">'.$scormstatus.'</button>
                    </a>';

                } else if ($status == 'incomplete') {
					$scormstatus = get_string('inprogress','block_enrolled_courses');
                    $scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
                    <button class="btn btn-sm bg-yellow w-125" style="cursor:pointer;">'.$scormstatus.'</button>
                    </a>';
                }
            } else {
                $scormstatus = get_string('notstarted','block_enrolled_courses');
                $scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
                <button class="btn btn-sm bg-red w-125" style="cursor:pointer;">'.$scormstatus.'</button>
                </a>';
            }
        }
        return $scormlink;
    }

}
