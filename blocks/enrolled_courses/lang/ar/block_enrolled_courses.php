<?php 

$string['pluginname'] = 'الدورات المسجلة';
$string['enrolleddate'] = 'تاريخ التسجيل';
$string['cname'] = 'اسم الدورة';
$string['vgrade'] = 'النتيجة';
$string['lcourse'] = 'ابدأ الدورة';
$string['enddate'] = 'تاريخ الانتهاء';
$string['duration'] = 'المدة';
$string['status'] = 'الحالة';

$string['complete'] = 'تم الانتهاء';
$string['inprogress'] = 'في تقدم';
$string['notstarted'] = 'لم يتم البدء';
$string['completed'] = 'تم الانتهاء';