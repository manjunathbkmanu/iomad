<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php');  
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_management/phishing.php');
$title = get_string('phishing', 'local_course_management');

//$PAGE->navbar->ignore_active();
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
echo $OUTPUT->header();

	$html ='';

//gophish menus added here by prashant on march 23-2020
		$html .='<div class="container-fluid"> 
			<div class="row">
				<div class="col-md-12 mt-3 mb-3 text-center">
				<h3>'.get_string('phishing', 'local_course_management').'</h3>
				</div>
			</div>
			<div class="row">
			
			<div class="col-md-4  " id="cbody">
			<div class="tile is-parent">
    			<article class="tile is-child box has-shadow2 grow"><a href="'.$CFG->wwwroot. "/local/gophish/templatelist.php".'" class="text-center  text-dark">
         			<div class="t-icon right"><i class="fa fa-list" aria-hidden="true"></i></div>
        				<div class="t-content pt-4">
        				<p class="subtitle is-size-5-mobile is-size-6-5-tablet is-size-5-desktop shrink-text">'.get_string('listoftemp', 'local_gophish').'</p>
        			</div></a>
    			</article>
			</div>
			</div>


			<div class="col-md-4 " id="cbody">
				<div class="tile is-parent">
	    			<article class="tile is-child box has-shadow2 grow"><a href="'.$CFG->wwwroot. "/local/gophish/landingpagelist.php".'" class="text-center  text-dark">
	         			<div class="t-icon right"><i class="fa fa-list-alt" aria-hidden="true"></i></div>
	        				<div class="t-content pt-4">
	        				<p class="subtitle is-size-5-mobile is-size-6-5-tablet is-size-5-desktop shrink-text">'.get_string('listoflandingpage', 'local_gophish').'</p>
	        			</div></a>
	    			</article>
				</div>
			</div>

			<div class="col-md-4 " id="cbody">
				<div class="tile is-parent">
	    			<article class="tile is-child box has-shadow2 grow"><a href="'.$CFG->wwwroot. "/local/gophish/sendingprofilelist.php".'" class="text-center  text-dark">
	         			<div class="t-icon right"><i class="fa fa-user" aria-hidden="true"></i></div>
	        				<div class="t-content pt-4">
	        				<p class="subtitle is-size-5-mobile is-size-6-5-tablet is-size-5-desktop shrink-text">'.get_string('sendingprofilelist', 'local_gophish').'</p>
	        			</div></a>
	    			</article>
				</div>
			</div>

			<div class="col-md-4 " id="cbody">
				<div class="tile is-parent">
	    			<article class="tile is-child box has-shadow2 grow"><a href="'.$CFG->wwwroot. "/local/gophish/campaignsumlist.php".'" class="text-center  text-dark">
	         			<div class="t-icon right"><i class="fa fa-spinner" aria-hidden="true"></i></div>
	        				<div class="t-content pt-4">
	        				<p class="subtitle is-size-5-mobile is-size-6-5-tablet is-size-5-desktop shrink-text">'.get_string('recentcamp', 'local_gophish').'</p>
	        			</div></a>
	    			</article>
				</div>
			</div>

			<div class="col-md-4 " id="cbody">
				<div class="tile is-parent">
	    			<article class="tile is-child box has-shadow2 grow"><a href="'.$CFG->wwwroot. "/local/gophish/action/gophish_campaign_create.php".'" class="text-center  text-dark">
	         			<div class="t-icon right"><i class="fa fa-plus-square-o" aria-hidden="true"></i></div>
	        				<div class="t-content pt-4">
	        				<p class="subtitle is-size-5-mobile is-size-6-5-tablet is-size-5-desktop shrink-text">'.get_string('createcamp', 'local_gophish').'</p>
	        			</div></a>
	    			</article>
				</div>
			</div>

			<div class="col-md-4" id="cbody">
				<div class="tile is-parent">
	    			<article class="tile is-child box has-shadow2 grow"><a href="'.$CFG->wwwroot. "/local/gophish/usergrouplist.php".'" class="text-center  text-dark">
	         			<div class="t-icon right"><i class="fa fa-users" aria-hidden="true"></i></div>
	        				<div class="t-content pt-4">
	        				<p class="subtitle is-size-5-mobile is-size-6-5-tablet is-size-5-desktop shrink-text">'.get_string('listofuser', 'local_gophish').'</p>
	        			</div></a>
	    			</article>
				</div>
			</div>

			</div>
		</div>';
echo $html;
echo $OUTPUT->footer();
