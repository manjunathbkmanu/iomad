<?php
include "../../config.php";
global $DB,$CFG;
if(isset($_POST['insert_data']))
{
	$rulekey=$_POST['rulekey'];
	//0 - Department and 1 - Manager.
	if($rulekey == 0){
		    //Manju: for department dropdown in user create table.[13/02/2020]
		$departmentoptions=[];
		$config = get_config('coursemanagement');
		$departmentsarray=$config->userdepartment;
		$exploadedarray = explode(",",$departmentsarray);
		foreach ($exploadedarray as $arraykey => $arrayvalue) {
			if($arrayvalue != ""){
                $departmentoptions[]=$arrayvalue;
            }
		}
		$html='';
		echo json_encode($departmentoptions);

	}elseif($rulekey == 1){
		$managersarray=[];
		$rethtml='';
		$managerquery="SELECT id,alternatename as managermail FROM {user} WHERE alternatename != 0 OR alternatename IS NOT NULL";
		$managersmail=$DB->get_records_sql($managerquery);
		foreach ($managersmail as $mail) {
			if(!empty($mail->managermail)){
				$manager=$DB->get_record('user',array('email'=>$mail->managermail));
				if(!empty($manager)){
                    $managersarray[$manager->id] = fullname($manager);
                }
			}
		}
		echo json_encode($managersarray);

	}
}


