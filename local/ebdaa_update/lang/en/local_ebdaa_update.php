<?php 
$string['title'] = 'EBDAA Updater';
$string['refresh'] = 'Refresh';
$string['changelog'] = 'Changelog';
$string['downloadupdate'] = 'Download & Install Update';
$string['noupdate'] = 'It looks like there are no new updates available at the moment.';