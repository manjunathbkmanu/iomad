<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_cs_reminder
 * @copyright  Manjunath B K<manjunath@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/user_mail.php');
require_once('classes/message.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
global $DB, $CFG, $USER;  
require_login(true);
$courseid = optional_param('courseid','',PARAM_INT);
$status = optional_param('status','',PARAM_INT);
$userid = optional_param('userid','',PARAM_INT);
$department = optional_param('department','',PARAM_TEXT);

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/cs_reminder/send_mail.php');
$title = get_string('pluginame', 'local_cs_reminder');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$mform = new local_course_form();
// Dilip: Filtered User on the basis of department
if ($formdata = $mform->get_data()) {
	$decodedDepartments = json_decode($formdata->department)[0];
	$selecteddepartments = json_decode($decodedDepartments);

	//for managers to send reminder mails to users.
	if($formdata->status == 0 && $formdata->userid !== 0){
		$userobject = $DB->get_record('user', array('id' => $formdata->userid));
		if($userobject){
			$depart = (strlen(trim($userobject->department))>0) ? $userobject->department : 'N/A';
		    if(in_array($depart, $selecteddepartments)){
				$sendmailtousers = send_mail_to_teammember($formdata->userid);
		    }
		}
		//after mail sent successfully, page will redirect to myteam page.
		$homeurl = $CFG->wwwroot.'/local/course_report/myteam.php';
		redirect($homeurl, "Mail sent Successfully", 1);
	}else{
		if(empty($formdata->status) OR $formdata->status == 0){
			$homeurl = $CFG->wwwroot.'/local/cs_reminder/index.php';
			redirect($homeurl, "Unable to send mail. Please select users!", 1);
		}else{
			$sendmailtousers = send_mail_to_users($formdata->courseid,$formdata->status,$selecteddepartments);
			$homeurl = $CFG->wwwroot.'/local/cs_reminder/sentmail.php';
			redirect($homeurl, "Mail sent Successfully", 1);			
		}

	}
}
echo $OUTPUT->header();
$companyid = $USER->company->id;
$welcome = new \local_cs_reminder\message(null,null);
$config = get_config('emailcontent'.$companyid);
$subject = $config->subjectforuserremindermail;
$body =  $config->userremindermailbody;
$maildata = new \stdClass();
$maildata->mailsubject = $subject;
$maildata->mailbody['text'] = $body;
$maildata->courseid = $courseid;
$maildata->status = $status;
$maildata->userid = $userid;
$maildata->department = json_encode($department);
$mform->set_data($maildata);

$mform->display();

echo $OUTPUT->footer();
