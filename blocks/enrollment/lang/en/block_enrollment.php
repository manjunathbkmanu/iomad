<?php

$string['pluginname'] = 'Quick enrollment';
$string['notallowed'] = 'You are not allowed to see that page';
$string['courses'] = 'Courses';
$string['roles'] = 'Roles';
$string['users'] = 'User';
$string['enrollusers'] = 'Enroll users';
$string['manager'] = 'Manager';
$string['teacher'] = 'Teacher';
$string['student'] = 'Student';
$string['student'] = 'Student';
$string['enrollment:addinstance'] = 'Add an instance of Quick enrollment';

$string['enddate'] = 'End Date';
$string['startdate'] = 'Start Date';
$string['validate'] = 'Enroll';

$string['selectuser'] = 'Select User';
$string['selectcourse'] = 'Select Course(s)';