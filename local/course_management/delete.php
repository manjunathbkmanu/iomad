<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 * @package    local_course_management
 * @copyright  Manjunath B K<manjunathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

require_once('../../config.php');
require_once('lib.php');
global $CFG;

$roleid = optional_param('roleid','',PARAM_INT); 
$context = context_system::instance();
$contextid = $context->contextlevel;
require_login();
$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');
$PAGE->set_url($CFG->wwwroot . '/local/course_management/delete.php?roleid='.$roleid);

global $DB,$CFG;

//for delete and cancel url//
$deleteurl = new moodle_url('/local/course_management/autoenrollist.php', array('did' => $roleid,'flag'=>2));
$pageheadding = get_string("deleteheadding",'local_course_management');
$message = get_string("deleterule",'local_course_management');

$continuebutton = new single_button($deleteurl, get_string('delete','local_course_management'), 'post');
$cancelurl = new moodle_url('/local/course_management/autoenrollist.php',array('did' => 0,'flag'=>0));
$PAGE->navbar->add($pageheadding);
$PAGE->set_heading($pageheadding);
echo $OUTPUT->header();
echo $OUTPUT->confirm($message, $continuebutton,$cancelurl);
echo $OUTPUT->footer();
exit;
