<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 * @package    local_course_management
 * @copyright  Manjunath B K<manjunathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$ruleval = optional_param('ruleval','',PARAM_RAW);
$ruletype = optional_param('ruletype','',PARAM_INT);

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_management/autoenrollist.php');
$title = get_string('matchingusers', 'local_course_management');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_management/js/custom.js'), true);
echo $OUTPUT->header();
$thtml='';
$thtml.=html_writer::start_div('pt-3 pb-1');
$thtml.=html_writer::start_tag('h2');
$thtml.=get_string('matchingusers','local_course_management');
$thtml.=html_writer::end_tag('h2');
$thtml.=html_writer::end_div('');
echo $thtml;
$retuurnurl=$CFG->wwwroot.'/local/course_management/autoenrollist.php';
$html='';
$html.=html_writer::start_div('float-right');
$html.=html_writer::start_tag('a',array('href'=>$retuurnurl));
$html.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$html.=get_string('back','local_course_management');
$html.=html_writer::end_tag('a');
$html.=html_writer::end_div('');
echo $html;
//creating a table to display already created autoenrollment rules.
$matchingusers  = new \html_table();
$matchingusers->head = array(get_string('firstname', 'local_course_management'),
	get_string('lastname', 'local_course_management'),
	get_string('email', 'local_course_management')
);
if($ruletype === 0){
    if(!is_siteadmin()){
        $companyid = $DB->get_field('company_users','companyid',array('userid'=>$USER->id));
        $usersql ="SELECT u.* FROM {user} AS u
        JOIN {company_users} AS cu ON cu.userid = u.id WHERE u.deleted = 0 AND u.suspended = 0 AND u.id > 2 AND cu.companyid =$companyid AND u.department = $ruleval  ORDER BY u.firstname ASC";
    }else{
        $usersql ="SELECT * FROM {user} WHERE deleted = 0 AND suspended = 0 AND id > 2 ORDER BY firstname ASC";
    }
    $getallusers = $DB->get_records_sql($usersql);
    // $getallusers = $DB->get_records('user',array('department'=>$ruleval,'deleted'=>0));

}elseif($ruletype === 1){
    $usermail = $DB->get_field('user', 'email',array('id'=>$ruleval));
    if(!is_siteadmin()){
        $companyid = $DB->get_field('company_users','companyid',array('userid'=>$USER->id));
        $usersql ="SELECT u.* FROM {user} AS u
        JOIN {company_users} AS cu ON cu.userid = u.id WHERE u.deleted = 0 AND u.suspended = 0 AND u.id > 2 AND cu.companyid =$companyid AND u.alternatename = $usermail  ORDER BY u.firstname ASC";
    }else{
        $usersql ="SELECT * FROM {user} WHERE deleted = 0 AND suspended = 0 AND id > 2 ORDER BY firstname ASC";
    }
    $getallusers = $DB->get_records_sql($usersql);
    // $getallusers = $DB->get_records('user',array('alternatename'=>$usermail,'deleted'=>0));
}
elseif($ruletype === 2){
    //$getallusers = $DB->get_records('user',array('deleted'=>0));
    if(!is_siteadmin()){
        $companyid = $DB->get_field('company_users','companyid',array('userid'=>$USER->id));
        $usersql ="SELECT u.* FROM {user} AS u
        JOIN {company_users} AS cu ON cu.userid = u.id WHERE u.deleted = 0 AND u.suspended = 0 AND u.id > 2 AND cu.companyid =$companyid ORDER BY u.firstname ASC";
    }else{
        $usersql ="SELECT * FROM {user} WHERE deleted = 0 AND suspended = 0 AND id > 2 ORDER BY firstname ASC";
    }
    $getallusers = $DB->get_records_sql($usersql);
}
foreach ($getallusers as $users) {
    $matchingusers->data[] = array($users->firstname,
        $users->lastname,
        $users->email);
}
echo html_writer::table($matchingusers);
echo $OUTPUT->footer();
