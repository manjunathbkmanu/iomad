<?php
// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
global $DB, $CFG, $USER;
require_once($CFG->dirroot. "/lib/enrollib.php");
require_once($CFG->dirroot . "/course/lib.php");
require_once($CFG->libdir . "/gradelib.php");
require_once($CFG->dirroot . "/grade/querylib.php");
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');	
$user = $USER;
$enrolledcourseid =[];
$enrolledcourses = enrol_get_users_courses($user->id);
foreach ($enrolledcourses as $enrolledcourse) {
	$enrolledcourseid []=$enrolledcourse->id;
}

/**
param '$searchtext' is seach input text
param '$cdropdownvalue' is category dropdown value
param 'sortval' is sorting dropdownvalue
**/
$sql ="SELECT id,fullname,category,enddate FROM {course}
WHERE visible = 1
AND id != 1";
if(isset($_POST['inputtex'])){
	$searchtext = $_POST['inputtex'];
}
if(isset($_POST['selectedvalue'])){
	$categoryid = $_POST['selectedvalue'];
}
if(isset($_POST['sortval'])){
	$sorttext = $_POST['sortval'];
}
if(!empty($searchtext) OR !empty($categoryid) OR  !empty($sorttext)){
	if(!empty($searchtext)){
		$sql .= " AND fullname LIKE '%" .$searchtext."%'";
	}
	if (!empty($categoryid)) {
		$sql .= " AND category =".$categoryid;
	}
	if (!empty($sorttext)) {
		$sql .= " ORDER BY fullname ".$sorttext;
	}
	$allcourses = $DB->get_records_sql($sql);
	//Manju:creating table here.
	$table  = new \html_table();
	$table->id = 'first-table';
	$table->head = array(get_string('coursename','local_student'),get_string('categoryname','local_student'),get_string('duration','local_student'),get_string('duedate','local_student'),get_string('status','local_student'),get_string('selfenrollment','local_student'));
	$name ='';
	$date ='';
	$status ='';
	$selfenrollmentstatus ='';

	foreach ($allcourses as $key => $scourse) {
//Manju:check for course visibility and self enrollment.[24/02/2020]
		$coursevisibility = $DB->get_field('customfield_field', 'id', array('shortname'=>'course_visibility'));
		$visibilityvalue = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursevisibility,'instanceid'=>$scourse->id));
		$coursecontext = context_course::instance($scourse->id);

		if($visibilityvalue == 1 || $visibilityvalue == 2 && is_enrolled($coursecontext, $user->id)){
		//1 = all users, 2 = enrolled users only.
			
			$enrollsql = $DB->get_record('enrol',array('courseid'=>$scourse->id,'enrol'=>'self'));
			if($enrollsql->status == 0){
				$categoryname = $DB->get_record('course_categories',array('id'=>$scourse->category));
				if(!empty($categoryname)){
					$name = $categoryname->name;
				}
				if($scourse->enddate != 0){
					$date = gmdate("Y-m-d", $scourse->enddate);
				}
				if (is_enrolled($coursecontext, $user->id)) 
				{
					$statusstring = get_string('enrolled', 'local_student');
				//get the status for this user for this course via scorm Mihir 03 Oct 2019
					$cminfo = get_fast_modinfo($scourse, $USER->id);
					foreach($cminfo->get_cms() as $cm) {
						if($cm->modname == 'scorm')
						{
							$status = get_scorm_status_ebdaa_blocktable_link($cm->instance,$scourse);
						}
					}
				}else{
					$statusstring = strtoupper(get_string('notenrlled', 'local_student'));
					$enrollink = new moodle_url('/enrol/index.php?id='.$scourse->id);
					$status = '<a class="btn bg-grey btn-sm w-125 p8px" href="'.$enrollink.'">'.$statusstring.'</a>';
				} 
			//now get duration from custom field table
				$duration = '';
				$fieldidsql = $DB->get_record('customfield_field', array('shortname'=>'courseduration'));
				if(!empty($fieldidsql))
				{
					$fieldid = $fieldidsql->id;
				//get value
					$fielddurationsql = $DB->get_record('customfield_data', array('fieldid'=>$fieldid, 'instanceid' =>$scourse->id));
					if(!empty($fielddurationsql))
					{
						$duration = $fielddurationsql->value;
					}
				}
//manju: added for self enrollment status column in user course view table.on 21/11/2019
				$sql = $DB->get_record('enrol',array('courseid'=>$scourse->id,'enrol'=>'self'));
				if(!empty($sql)){
				if($sql->status == 0){ //Mihir check if active or not
					$selfenrollmentstatus = get_string('yes','local_student');
				}
				else
				{
					$selfenrollmentstatus = get_string('no','local_student');
				}
			}
//Manju:16/03/2020.
			$coursename =$scourse->fullname;
			$coursedurationar = $duration;
			if(current_language() == 'ar'){
				$coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));

				$coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$scourse->id));
				$coursename = $coursenamearabic;
				if(empty($coursename)){
					$coursename =$scourse->fullname;
				}

				$courseduration = $DB->get_field('customfield_field', 'id', array('shortname'=>'durationinarabic'));

				$coursedurationar = $DB->get_field('customfield_data', 'value', array('fieldid'=>$courseduration,'instanceid'=>$scourse->id));
				if(empty($coursedurationar)){
					$coursedurationar = $duration;
				}
			}
			$table->data[] = array($coursename,$name,$coursedurationar,$date,$status,$selfenrollmentstatus);
		}
	}
}
echo html_writer::table($table);
}
