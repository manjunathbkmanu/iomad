<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_accesscohort
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../../config.php');
require_once('../lib.php');
require_once('../form/campaign_create_form.php');
require_once($CFG->libdir . '/formslib.php');
global $DB,$USER;
require_login(0,false);
//edit params
$eid = optional_param('id',null, PARAM_INT);
$edit = optional_param('edit',null, PARAM_INT);
//api key initialization
//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
$companyid = $USER->company->id;
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$context = context_system::instance();
$contextid = $context->contextlevel;
$overall = has_capability('local/gophish:overall',$context);
$campaign = has_capability('local/gophish:campaigns',$context);
//$createorgcap = has_capability('local/accesscohort:addorganization',$context);
//curl initialization;
$PAGE->set_context(context_system::instance());
$title = get_string('createcamp', 'local_phishing');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/action/campaign_create.php');
require_login();
$previewnode = $PAGE->navigation->add(get_string('pluginname','local_phishing'), new moodle_url($CFG->wwwroot.'/local/phishing/campaignsumlist.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title, new moodle_url($CFG->wwwroot.'/local/phishing/action/campaign_create.php'));
$thingnode->make_active();
$mform = new campaign_create_form();
$temp = false;
if ($mform->is_cancelled()){
	redirect(new moodle_url('/local/phishing/campaignslist.php', array()));
} else if ($data = $mform->get_data()) {
	//curl intialization
	//single template data here 
	$templateobj = get_template_value($urlip,$apikey,$data->template);
	//single page data 
	$pageobj = get_page_value($urlip,$apikey,$data->page);
	//single sending progile 
	$smtpobj = get_sendingprofile_value($urlip,$apikey,$data->smtp);
	$group_store = [];
	if(!empty($data->groupss)){
	//single groups
		foreach($data->groupss as $val){
			$groupobj = get_usergroup_value($urlip,$apikey,$val);
			if(!empty($groupobj)){
				$group_store[] = array('name'=>$groupobj->name);
			}
		}
	}
	//campain array creation here
	$array = array(
	'name'=>$data->name,
    'template'=> array('name'=>$templateobj->name),
    'url'=>$data->url,
    'page' =>array('name'=>$pageobj->name),
    'smtp'=> array('name'=>$smtpobj->name),
    'launch_date' => gmdate("Y-m-d\TH:i:s\Z", $data->launch_date),
    //'send_by_date' =>gmdate("Y-m-d\TH:i:s\Z", $data->send_by_date),
    'send_by_date' =>null,
    'groups'=>$group_store,
	);
	$data_json = json_encode($array);
	if(!empty($data_json)){
		//insert values
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);	
		$url = $protocol.$urlip.'/api/campaigns/?api_key='.$apikey;
		curl_setopt($curl_handle, CURLOPT_URL, $url);	
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_POST, 1);
		//curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($curl_handle);
		$msg = get_string('newcamp','local_phishing');
		//REDIRECT PAGE 
		if($response) {
			redirect(new moodle_url($CFG->wwwroot.'/local/phishing/campaignslist.php'),$msg);
		} else {
			echo "error";
		}
	}
	if (!curl_exec($curl_handle)) {
		die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
	}
	
		
	curl_close($curl_handle);
}
echo $OUTPUT->header();
if($overall || $campaign){
	$mform->display();
}else{
	echo html_writer::div(
		get_string('cap', 'local_phishing'),'alert alert-danger'
	);
}
echo $OUTPUT->footer();
?>

<style type="text/css">
	#fitem_id_send_by_date{
		display: none;
	}
	#fitem_id_send_by_date .felement{
		margin-left: auto;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('#id_sendbydate').change(function(){
			if($(this).prop("checked")){
				$('#fitem_id_send_by_date').show(500);
			}else{
				$('#fitem_id_send_by_date').hide(500);
			}
		});

		$.get('group-ajax.php', function(data){
			var groups = JSON.parse(data);
			var groups = Object.values(groups)
			  .sort();
			var html = '<option value="">No Selection</option>';
			$.each(groups, function(index, group){
				html += '<option value="'+index+'">'+group+'</option>';
			});
			$('#fitem_id_groups').find('#id_groups').html(html);
		});
	});
</script>






