<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phish
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
//defined('MOODLE_INTERNAL') || die();

require('../../config.php');
require('lib.php');
require_login(0 , FALSE);
global $CFG,$DB,$USER;
$context = context_system::instance();
$overall = has_capability('local/phishing:overall',$context);
$campaigns = has_capability('local/phishing:campaigns',$context);
$PAGE->set_context(context_system::instance());
$title = get_string('dashboard', 'local_phishing');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/campaignlist.php');
//api key initialization
//Manju: changed the plugin name from local_gophish to local_phishing.08/02/2021.
$companyid = $USER->company->id;
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$PAGE->navbar->ignore_active();
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/local/phishing/css/kendo.default-v2.min.css'), true);
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/phishing/js/pie-chart.js'), true);
$previewnode = $PAGE->navbar->add(get_string('pluginname','local_phishing'),'');
$previewnode = $previewnode->add(get_string('dashboard','local_phishing'),$CFG->wwwroot.'/local/phishing/campaignlist.php');
echo $OUTPUT->header();
echo '<h2>'.get_string('dashboard','local_phishing').'</h2>';
echo '<br>';
//api curl initialization
if($overall || $campaigns){
    $curl_handle = curl_init();
    $url = $protocol.$urlip.'/api/campaigns/?api_key='.$apikey;
    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
// Set the curl URL option
    curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl_handle, CURLOPT_URL, $url);
// This option will return data as a string instead of direct output
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
// Execute curl & store data in a variable
    $curl_data = curl_exec($curl_handle);

    if (!curl_exec($curl_handle)) {
        die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
    }
    curl_close($curl_handle);
// Decode JSON into PHP array
    $campaigns = json_decode($curl_data);
    //print_object($campaigns);
    //print_r($campaigns);
    $emailsent = [];
    $emailopen = [];
    $clickedlink =[];
    $submitteddata =[];
    $emailreport = [];
    $array = [];
    $table = new html_table();
    $i= 1;
    $sent = 0;
    $open = 0;
    $clink = 0;
    $sdata = 0;
    $ereport = 0;
    $table->head = (array) get_strings(array('sno','name', 'cdate','emailsent','emailopen','linkclick','submitteddata','emailreport','status','action'), 'local_phishing');
    if(!empty($campaigns)){
        foreach ($campaigns as $key => $campaign) {

            //table creation here 

           foreach ($campaign->timeline as $key11 => $timelineval) {
            if($timelineval->message =='Email Sent'){
               $sent =1 ;
           }
           if($timelineval->message =='Email Opened'){
            $open =1;
        }
        if($timelineval->message =='Clicked Link'){
            $clink=1;
        }
        if($timelineval->message =='Submitted Data'){
            $sdata =1;
        }
        if($timelineval->message =='Email Reported'){
            $ereport =1;
        }
        $array[$timelineval->message][] =  $timelineval->message;
    }
    $table->data[] = array(
        $i++,
        $campaign->name,
        $campaign->created_date,
        $sent,
        $open,
        $clink,
        $sdata,
        $ereport,
        $campaign->status,
        html_writer::link(
            new moodle_url(
                $CFG->wwwroot.'/local/phishing/action/campaignsinglelist.php',
                array('sid' => $campaign->id)),'Chart',array('class' =>'btn btn-primary btn-xs' )),
        html_writer::link(
            new moodle_url(
                $CFG->wwwroot.'/local/phishing/action/campaign_delete.php',
                array('id' => $campaign->id)),'Delete',array('class' =>'btn btn-danger btn-xs' ))
    );
}
}
//echo $sent ;
$data = '';
$data .= html_writer::start_div('container');
$data .= html_writer::start_div('row');
$i=1;
$total = count($array['Email Sent']);
foreach ($array as $key => $value) {
    $data .= progress_chart_val($key,$value,$i,$total);
    $i++;
}
    $data .= html_writer::end_div();//card end
    $data .= html_writer::end_div();//card end
    echo $data;
    //table data here 
    echo '<br><br>';
    echo '<h1>campaign report</h1>';
    echo '<br><br>';

    echo html_writer::table($table);
}else{
    echo html_writer::div(
                'No Data','alert alert-danger'
    );
}
echo $OUTPUT->footer();
