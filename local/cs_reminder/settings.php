<?php

defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {

    $moderator = get_admin();
    $site = get_site();

    $settings = new admin_settingpage('emailcontent', get_string('emailsettings','local_cs_reminder'));
    $ADMIN->add('localplugins', $settings);

    // settings for new user account email
    $name = 'emailcontent/subjectfornewuseraccount';
    $information = get_string('newuseraccountmail', 'local_cs_reminder');
    $heading = get_string('subjectheadingfornewuseremail', 'local_cs_reminder');
    $setting = new admin_setting_heading($name,$heading, $information);
    $settings->add($setting);

    $name = 'emailcontent/subjectfornewuseraccountmail';
    $default = '';
    $title = get_string('enternewusermailsubject', 'local_cs_reminder');
    $description = get_string('enteremailsubject', 'local_cs_reminder');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $settings->add($setting);

    $name = 'emailcontent/newusermailbody';
    $title = get_string('enternewusermailbody', 'local_cs_reminder');
    $description = get_string('emailbodydescription', 'local_cs_reminder');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);    
    $settings->add($setting);





    //settings for user enrollment mail 
    $name = 'emailcontent/headingforuserenrolmentmail';
    $information = get_string('userenrollmentmail', 'local_cs_reminder');
    $heading = get_string('headingforuserenrolmentmail', 'local_cs_reminder');
    $setting = new admin_setting_heading($name,$heading, $information);
    $settings->add($setting);

    $name = 'emailcontent/subjectfornewuserenrollment';
    $default = '';
    $title = get_string('enterenrollmailsubject', 'local_cs_reminder');
    $description = get_string('enteremailsubject', 'local_cs_reminder');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $settings->add($setting);

    //mail body for new user mail
    $name = 'emailcontent/userenrollmentmailbody';
    $title = get_string('enterenrollmailbody', 'local_cs_reminder');
    $description = get_string('emailbodydescription', 'local_cs_reminder');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);    
    $settings->add($setting);
   


    //settings for user reminder mail
    $name = 'emailcontent/headingforuserremindermail';
    $information = get_string('userremindermail', 'local_cs_reminder');
    $heading = get_string('headingforuserremindermail', 'local_cs_reminder');
    $setting = new admin_setting_heading($name,$heading, $information);
    $settings->add($setting);

    $name = 'emailcontent/subjectforuserremindermail';
    $default = '';
    $title = get_string('enterremindermailsubject', 'local_cs_reminder');
    $description = get_string('enteremailsubject', 'local_cs_reminder');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $settings->add($setting);

    //mail body for new user mail
    $name = 'emailcontent/userremindermailbody';
    $title = get_string('enterreminderemailbody', 'local_cs_reminder');
    $description = get_string('emailbodydescription', 'local_cs_reminder');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);    
    $settings->add($setting);

    
	  //settings for user reminder mail with cron 7 days before due date
     $name = 'emailcontent/headingforuserremindermailwithcron';
	$information = get_string('userremindermail', 'local_cs_reminder');
    $heading = get_string('headingforuserremindermailwithcron', 'local_cs_reminder');
    $setting = new admin_setting_heading($name,$heading, $information);
    $settings->add($setting);

     $name = 'emailcontent/subjectforuserremindermailwithcron';
    $default = '';
    $title = get_string('enterremindermailsubject', 'local_cs_reminder');
    $description = get_string('enteremailsubject', 'local_cs_reminder');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $settings->add($setting);
	
	
    //mail body for new user mail
    $name = 'emailcontent/userremindermailbodywithcron';
    $title = get_string('enterreminderemailbody', 'local_cs_reminder');
    $description = get_string('emailbodydescription', 'local_cs_reminder');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);    
    $settings->add($setting);


/// one day befor

  $name = 'emailcontent/headingforuserremindermailwithcrononeday';
	$information = get_string('userremindermail', 'local_cs_reminder');
    $heading = get_string('headingforuserremindermailwithcrononeday', 'local_cs_reminder');
    $setting = new admin_setting_heading($name,$heading, $information);
    $settings->add($setting);

     $name = 'emailcontent/subjectforuserremindermailwithcrononeday';
    $default = '';
    $title = get_string('enterremindermailsubject', 'local_cs_reminder');
    $description = get_string('enteremailsubject', 'local_cs_reminder');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $settings->add($setting);
	
	
    //mail body for new user mail
    $name = 'emailcontent/userremindermailbodywithcrononeday';
    $title = get_string('enterreminderemailbody', 'local_cs_reminder');
    $description = get_string('emailbodydescription', 'local_cs_reminder');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);    
    $settings->add($setting);




   } 