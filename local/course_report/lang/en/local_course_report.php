<?php 
$string['title'] = 'Course Report';
$string['pleaseselectcourse'] = 'Please Select Course';
$string['selectuser'] = 'Select User';
$string['submit'] = 'Submit';
$string['completionstatus'] = 'Completion Status';
$string['averageassessmentscore'] = 'Average Assessment Score %';
$string['notstarted'] = 'Not Started';
$string['complete'] = 'Completed';
$string['inprogress'] = 'In Progress';
$string['averagesore'] = 'Average Score %';
$string['totalscount'] = 'Total Count';
$string['course'] = 'Course';
$string['fullname'] = 'Full Name';
$string['email'] = 'Email';
$string['status'] = 'Status';
$string['score'] = 'Score';
$string['nousersavailable'] = '<div class="text-center"><h1>No Data found</h1></div>';
$string['selectcourse'] = 'Select Course';
$string['pleaseselectuser'] = 'Please Select User';
$string['userreport'] = 'User Report';
$string['allusers'] = 'All';
$string['completioninpercentage'] = 'Completion (in %)';
$string['coursename'] = 'Course Name';
$string['chooseusercategory'] = 'Choose User Category';
$string['selectuserby'] = 'Select User By';
$string['adminreports'] = 'Reports';
$string['enrollmentm'] = 'Course Enrollment';
$string['selectenrolmethod'] = 'Enable Self Enrolment';
$string['bulkenrollment'] = 'Click to do Bulk Enrolment';
$string['enrollmanually'] = 'Click to do Quick Enrolment';
$string['coursereminder'] = 'Send Reminder';
$string['yes'] = 'Yes';
$string['no'] = 'No';
$string['changesmadesuccessfully'] = '<p class="text-success">Changes done Successfully</p>';

//added for pdf report on 16-10-19
$string['downloadpdf'] = 'Download PDF';
$string['downloadpdfarabic'] = 'Download PDF in Arabic';
$string['space'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
$string['action'] = 'Action';
$string['notstarted_ar'] = 'لم يبدأ';
$string['complete_ar'] = 'منجز';
$string['inprogress_ar'] = 'في تقدم';
$string['course_ar'] = 'الدورة';
$string['averageassessmentscore_ar'] = 'متوسط درجة التقييم %';
$string['totalscount_ar'] = 'العدد الإجمالي';
$string['fullname_ar'] = 'الاسم الكامل';
$string['email_ar'] = 'البريد الإلكتروني';
$string['status_ar'] = 'الحالة';
$string['score_ar'] = 'النتيجة';
$string['teamstatus'] = 'Team Status';
$string['teamreport'] = 'Team Report';

$string['enrolcount'] = 'Courses Enrolled : ';
$string['completecount'] = 'Completed Courses : ';
$string['inprogresccount'] = 'In-Progress Courses : ';
$string['notstartedcount'] = 'Not Started Courses : ';
$string['teamsize'] = 'Team Size : ';
$string['coursesenrolled'] = 'Courses Enrolled';
$string['completionperc'] = 'Completion % : ';
$string['completionperc_ar'] = 'منجز % : ';
$string['enrolled'] = 'Enrolled';
//for arabic pdf.
$string['complestats'] = 'حالة إكمال';
$string['legends'] = '<ul id="legend">
		<li><span class="notstarted"></span> لم يبدأ</li>
		<li><span class="completed"></span> منجز</li>
		<li><span class="inprogess"></span> في التقدم</li>
		</ul>';
$string['enableenrolmentmethod'] = 'Enable Enrolment Method';
$string['department'] = 'Department';
$string['department_ar'] = 'قسم';
$string['not_applicable_ar'] = 'غير متاح';
$string['not_applicable'] = 'N/A';

$string['encsvpdf'] = 'Download CSV';
$string['arcsvpdf'] = 'Download CSV in AR';
$string['exportinar'] = 'Export in Arabic';
$string['export'] = 'Export in English';
$string['overdue'] = 'Overdue';
$string['notcompleted'] = 'Not Completed';
$string['pleaseselectdepartment'] = 'Please Select Department';
$string['selectdepartment'] = 'Select Department(s)';
$string['nouser'] = 'No users';