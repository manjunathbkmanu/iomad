<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * You may have settings in your plugin
 *
 * @package    local_hpanalytics
 * @copyright  2014 Daniel Neis
 * @license    http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); //It must be included from a Moodle page
}
global $OUTPUT, $CFG;
require_once($CFG->libdir.'/formslib.php');
require_login();
class local_user_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        $id = $USER->id;
        $mform =& $this->_form;
        if(!is_siteadmin()){
            $companyid = $DB->get_field('company_users','companyid',array('userid'=>$USER->id));
            $usersql ="SELECT u.* FROM {user} AS u
            JOIN {company_users} AS cu ON cu.userid = u.id WHERE u.deleted = 0 AND u.suspended = 0 AND u.id > 2 AND cu.companyid =$companyid ORDER BY u.firstname ASC";
        }else{
            $usersql ="SELECT * FROM {user} WHERE deleted = 0 AND suspended = 0 AND id > 2 ORDER BY firstname ASC";
        }

        //Manju: 11/12/2020. changed the sql query.
        // $usersql ="SELECT * FROM {user} WHERE deleted = 0 AND suspended = 0 AND id > 2 ORDER BY firstname ASC";
        $user_options =[];
        $selectuser = get_string('allusers', 'local_course_report');
        $user_options[] = $selectuser;
        $userdetails = $DB->get_records_sql($usersql, array(1));
        foreach($userdetails as $uid => $users){
            $user_options[$users->id] = $users->firstname.' '.$users->lastname;
        }
        $options = array(                                                                                                           
            'multiple' => false,                                                                                                     
            'noselectionstring' => get_string('selectuser', 'local_course_report'),
            'placeholder' => get_string('selectuser', 'local_course_report'),                                                              
        );
        if(!empty($options)) {
            $select = $mform->addElement('autocomplete', 'userid', get_string('selectuser','local_course_report'), $user_options,$options);
        }
        $mform->addElement('submit', 'submitbutton', get_string('submit', 'local_course_report'));
    }
}