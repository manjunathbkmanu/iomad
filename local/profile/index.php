<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form.php');
require_once($CFG->libdir . '/formslib.php');
require_once("$CFG->libdir/externallib.php");
require_once($CFG->dirroot . '/user/externallib.php');
require_once($CFG->dirroot . '/files/externallib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once('lib.php');
global $DB, $USER;  
$editid = optional_param('eid','',PARAM_INT);
$uid = optional_param('uid','',PARAM_INT);
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/profile/index.php');
$title = get_string('title', 'local_profile');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
//inserting the form.
if(!empty($editid)){
	$mform = new local_profile_form($CFG->wwwroot . '/local/profile/index.php',array('eid'=>$editid));
}else{
	$mform = new local_profile_form();

}
if ($mform->is_cancelled()) {
    //Handle form cancel operation, if cancel button is present on form
} else if ($data = $mform->get_data()) {

	$usercontext = context_user::instance($data->userid);
	
	$editoroptions = array(
		'maxfiles' => 0,
		'maxbytes' => 0,
		'trusttext' => false,
		'forcehttps' => false,
		'user' => $usercontext
	);
	$draftitemid = $data->userpic;
	$filemanagercontext = $editoroptions['user'];
	$filemanageroptions = array('maxbytes'       => $CFG->maxbytes,
		'subdirs'        => 0,
		'maxfiles'       => 1,
		'accepted_types' => 'web_image');
	file_prepare_draft_area($draftitemid, $filemanagercontext->id, 'user', 'newicon', 0, $filemanageroptions);
	//update code here 
	$editdata = new \stdClass();
	$editdata->id = $data->userid;
	$editdata->firstname = $data->firstname;
	$editdata->lastname = $data->lastname;	
	$editdata->picture = $data->userpic;
	//manju:updated code for profile update. 06/12/2019.
	$user2 = $USER;
	$user2->imagefile = $draftitemid;
	$updateuserimage = core_user::update_picture($user2, $filemanageroptions);
    // Load custom profile fields data.
	user_update_user($editdata, false, false);
	$user = $DB->get_record('user', array('id' => $editdata->id));
	if ($user->id == $USER->id) {
        // Override old $USER session variable.
        foreach ((array)$editdata as $variable => $value) {
            if ($variable === 'description' or $variable === 'password') {
                // These are not set for security nad perf reasons.
                continue;
            }
            $USER->$variable = $value;
        }
    }
    redirect("{$CFG->wwwroot}/local/profile/index.php");
	// $updatedresult = $DB->update_record('user', $editdata);
	// $user = $USER;
	// $user->imagefile = $draftitemid;
	// $updateuserimage = core_user::update_picture($user, $filemanageroptions);
	// if($updateuserimage){
	// 	$url = new moodle_url('/login/logout.php', array('sesskey' => $user->sesskey));
	// 	redirect($url, 'Successfully Updated, please login again.', 5); 
	// }
	// else if($updatedresult){
	// 	$url = new moodle_url('/my');
	// 	redirect($url, 'Your Profile is Successfully Updated', 5);
	// }
} else {
	if (!empty($uid)) {
		if (is_siteadmin()) {
			$id = $uid;
		} else {
			$id = $USER->id;
		}
	} else {
		$id = $USER->id;
	}
	if($id){
		//setting form data
		$editdatainfo = $DB->get_record('user', array('id'=>$id));
		$mform->set_data($editdatainfo);
	}
}
echo $OUTPUT->header(); 

$forgotpassword  = ''; 
$url = new moodle_url('/login/change_password.php');
//$forgotpassword = html_writer::link($url, get_string('changepassword','local_profile'), array('class'=>' btn btn-primary float-right'));
$forgotpassword .=html_writer::start_div('container');
$forgotpassword .=html_writer::start_div('row');
$forgotpassword .=html_writer::start_div('col-md-12');
$forgotpassword .= html_writer::link($url, get_string('changepassword','local_profile'), array('class'=>' btn btn-primary float-right'));
$forgotpassword .= html_writer::end_div();
$forgotpassword .= html_writer::end_div();
$forgotpassword .= html_writer::end_div();
echo $forgotpassword;
echo '<br><br>';

$mform->display();
echo $OUTPUT->footer();
/*$user_list = $DB->get_record('user',array('id'=>$USER->id));
echo '<pre>';
print_r($user_list);*/	
?>