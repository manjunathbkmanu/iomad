<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * You may have settings in your plugin
 *
 * @copyright  Manjunath<manjunath@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
global $OUTPUT, $CFG;
require_once($CFG->libdir.'/formslib.php');
require_login();
class local_course_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        $id = $USER->id;
        $mform =& $this->_form;
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT); 

        $mform->addElement('hidden', 'courseid');
        $mform->setType('courseid', PARAM_INT); 

        $mform->addElement('hidden', 'status');
        $mform->setType('status', PARAM_INT);

        $mform->addElement('hidden', 'userid');
        $mform->setType('userid', PARAM_INT); 

        $mform->addElement('hidden', 'department');
        $mform->setType('department', PARAM_TEXT); 

        $mform->addElement('text', 'mailsubject', get_string('mailsubject', 'local_cs_reminder'));
        $mform->setType('mailsubject', PARAM_TEXT);
        $mform->addElement('editor','mailbody',get_string('mailbody','local_cs_reminder'),'wrap="virtual" rows="5" cols="40" ');
        $mform->addElement('submit', 'submitbutton', get_string('send', 'local_cs_reminder'));
    }
}