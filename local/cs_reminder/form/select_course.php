<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * You may have settings in your plugin
 *
 * @copyright  Manjunath<manjunath@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
global $OUTPUT, $CFG;
require_once($CFG->libdir.'/formslib.php');
require_login();
class local_course_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        $id = $USER->id;
        $mform =& $this->_form;
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT); 
        if(!is_siteadmin()){
            $companyid = $DB->get_field('company_users','companyid',array('userid'=>$USER->id));
            $coursesql ="SELECT c.* FROM {course} AS c
            JOIN {company_course} AS cc ON cc.courseid = c.id WHERE c.visible = 1 AND c.id != 1 AND cc.companyid =$companyid ORDER BY c.fullname ASC";
        }else{
            $coursesql = 'SELECT * FROM {course} WHERE visible = 1 AND id !=1';
        }
        $allcourses = $DB->get_records_sql($coursesql);
        $course_options = [];
        $selectcourse = get_string('pleaseselectcourse', 'local_cs_reminder');
        $course_options[''] = $selectcourse;
        foreach($allcourses as $course){
           $coursename=$course->fullname;
           if(current_language() == 'ar'){
            $coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));

            $coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$course->id));
            $coursename = $coursenamearabic;
            if(empty($coursename)){
                $coursename =$course->fullname;
            }

        }
        $course_options[$course->id] = $coursename;

    }
    /* Created by Manish */
    $options = array(                                     
        'multiple' => false,                                      
        'noselectionstring' => get_string('selectcourse', 'local_cs_reminder'),
        'placeholder' => get_string('selectcourse', 'local_cs_reminder'),
        'id'=>'course_selection_auto',                                                               
    );
    if(!empty($options)) {
        $select = $mform->addElement('select','courseid', get_string('selectcourse','local_cs_reminder'), $course_options,$options);
    }
    $departmenyOptions = array(                                                                                                           
        'multiple' => "multiple",
        'noselectionstring' => get_string('selectdepartment', 'local_course_report'),
        'placeholder' => get_string('selectdepartment', 'local_course_report'),
        'id'=>'department_selection_cs_reminder',                                                            
    );
    $departments = get_alldepartments_ebdaa(true);
    $checkenrollments = check_availability_enrolled_user_courses();
    if($checkenrollments > 0){
        $departments["N/A"] = "N/A";
    }
    if(!empty($departmenyOptions)) {
        $select = $mform->addElement('select','departmentId[]', get_string('selectdepartment','local_course_report'), $departments,$departmenyOptions);
    }
    $mform->addElement('submit', 'submitbutton', get_string('submit', 'local_cs_reminder'),['class' => 'cs_reminder_submit_btn']);


}
}