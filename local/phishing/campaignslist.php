<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phishing
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
//defined('MOODLE_INTERNAL') || die();

require('../../config.php');
require('lib.php');
require_login(0 , FALSE);
global $CFG,$DB,$USER;
$context = context_system::instance();
$overall = has_capability('local/phishing:overall',$context);
$createtemp = has_capability('local/phishing:campaigns',$context);
$PAGE->set_context(context_system::instance());
$title = get_string('campaigns', 'local_phishing');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/campaignslist.php');
//Manju: changed the plugin name from local_gophish to local_phishing.08/02/2021.
$companyid = $USER->company->id;
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$PAGE->navbar->ignore_active();
$PAGE->requires->jquery();
$PAGE->requires->css(new 
    moodle_url($CFG->wwwroot.'/local/course_report/css/dataTables.bootstrap4.min.css'));
$PAGE->requires->css(new 
    moodle_url($CFG->wwwroot.'/local/course_report/css/buttons.bootstrap4.min.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/newjs/jquery.dataTables.min.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/newjs/dataTables.bootstrap4.min.js'), true);
// $PAGE->requires->js(new moodle_url('https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js'), true);
// $PAGE->requires->js(new moodle_url('https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js'), true);
$previewnode = $PAGE->navbar->add(get_string('pluginname','local_phishing'),'');
$previewnode = $previewnode->add(get_string('campaigns','local_phishing'),$CFG->wwwroot.'/local/phishing/campaignslist.php');
echo $OUTPUT->header();
echo '<h2>'.get_string('campaigns','theme_maker').'</h2>';
echo '<br>';
//api curl initialization
if($overall || $createtemp){
    echo html_writer::link(
        new moodle_url(
            $CFG->wwwroot.'/local/phishing/action/campaign_create.php'
        ),
        get_string('newcampaign','local_phishing'),
        array(
          'class' => 'btn btn-primary'
      )
    );
    echo '<br><br>';  

    $curl_handle = curl_init();
    // $url = $protocol.$urlip.'/api/campaigns/?api_key='.$apikey;
    $url = $protocol.$urlip.'/api/campaigns/?api_key='.$apikey;
    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
// Set the curl URL option
    curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl_handle, CURLOPT_URL, $url);

// This option will return data as a string instead of direct output
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

// Execute curl & store data in a variable
    $curl_data = curl_exec($curl_handle);

    if (!curl_exec($curl_handle)) {
        die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
    }

    curl_close($curl_handle);

// Decode JSON into PHP array
    $campaigns = json_decode($curl_data);

    $html = '';
    $html .= '<ul class="nav nav-tabs" id="myTab" role="tablist">';
    $html .= '<li class="nav-item"><a class="nav-link active" id="active-campaign-tab" data-toggle="tab" href="#active-campaign" role="tab" aria-controls="active-campaign" aria-selected="true">'.get_string('activecampaign','local_phishing').'</a></li>';
    $html .= '<li class="nav-item"><a class="nav-link" id="archieved-tab" data-toggle="tab" href="#archieved" role="tab" aria-controls="archieved" aria-selected="false">'.get_string('archivecampaign','local_phishing').'</a></li></ul>';
    $html .= '<div class="tab-content" id="myTabContent">';
    $html .='<div class="tab-pane fade show active" id="active-campaign" role="tabpanel" aria-labelledby="active-campaign-tab">';
    $html .= '<br><br>';
    $html .= '<table id="active-campaigns" class="generaltable">';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .= '<th>'.get_string('name', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('cdate', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('status', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('action', 'local_phishing').'</th>';
    $html .= '</tr>';
    $html .= '</thead>';
    $html .= '<tbody>';
    if(!empty($campaigns)){
        foreach ($campaigns as $key => $campaign) {
            if($campaign->status != 'Completed'){
                //table creation here
                $camsummary = get_campaign_summary($urlip,$apikey,$campaign->id);
                // print_object($camsummary);
                $sent += $camsummary->stats->sent;
                $open += $camsummary->stats->opened;
                $click += $camsummary->stats->clicked;
                $sdata += $camsummary->stats->submitted_data;
                $ereport += $camsummary->stats->email_reported;
                $error += $camsummary->stats->error;

                $launchTime = new DateTime($campaign->launch_date, core_date::get_user_timezone_object());
		$launchDate = date('Y-m-d H:i:s', $launchTime->getTimestamp());
                $launchDateNum = strtotime($launchDate);

                if($campaign->status == 'Completed'){
                    $statusClass = 'success';
                }else{
                    $statusClass = 'primary';
                }
                //$dataOriginalTitle = "Launch Date: ".$launchDate."<br><br>Number of recipients: ".$sent."<br><br>Emails opened: ".$open."<br><br>Emails clicked: ".$click."<br><br>Submitted Credentials: ".$sdata."<br><br>Errors : ".$error."<br><br>Reported : ".$ereport."";

                $html .= '<tr>';
                $html .= '<td>'.$campaign->name.'</td>';
                $html .= '<td>'.$launchDateNum.'</td>';
                $html .= '<td><p class="badge badge-'.$statusClass.' text-wrap" style="width: 6rem;"  data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="'.$dataOriginalTitle.'">'.$campaign->status.'</p></td>';    
                $html .= '<td>'.html_writer::link(
                        new moodle_url(
                            $CFG->wwwroot.'/local/phishing/action/campaignsinglelist.php',
                            array('id' => $campaign->id)),'Chart',array('class' =>'btn btn-primary btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'View Results')).' '.html_writer::link(
                        new moodle_url(
                            $CFG->wwwroot.'/local/phishing/action/campaign_edit.php',
                            array('id' => $campaign->id)),'',array('class' =>'btn btn-campaign btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Copy Campaign')).' '.html_writer::link(
                        new moodle_url(
                            $CFG->wwwroot.'/local/phishing/action/campaign_delete.php',
                            array('id' => $campaign->id)),'Delete',array('class' =>'btn btn-danger btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Delete Campaign')).'</td>'; 
                $html .= '</tr>';
            }
        }
    }else{

        echo html_writer::div(
		'No Data','alert alert-danger'
        );
    }
    $html .= '</tbody>';
    $html .= '</table>';
    $html .= '</div>';
    $html .='<div class="tab-pane fade" id="archieved" role="tabpanel" aria-labelledby="archieved-tab">';
    $html .= '<br><br>';
    $html .= '<table id="archieved-campaigns" class="generaltable">';
    $html .= '<thead>';     
    $html .= '<tr>';    
    $html .= '<th>'.get_string('name', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('cdate', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('status', 'local_phishing').'</th>';
    $html .= '<th>'.get_string('action', 'local_phishing').'</th>';
    $html .= '</tr>';
    $html .= '</thead>';
    $html .= '<tbody>';
    if(!empty($campaigns)){
        foreach ($campaigns as $key => $campaign) {
            if($campaign->status == 'Completed'){
                //table creation here
                $camsummary = get_campaign_summary($urlip,$apikey,$campaign->id);
                //print_object($camsummary);
                $sent += $camsummary->stats->sent;
                $open += $camsummary->stats->opened;
                $click += $camsummary->stats->clicked;
                $sdata += $camsummary->stats->submitted_data;
                $ereport += $camsummary->stats->email_reported;
                $error += $camsummary->stats->error;

		$launchTime = new DateTime($campaign->launch_date, core_date::get_user_timezone_object());
                $launchDate = date('Y-m-d H:i:s', $launchTime->getTimestamp());
                $launchDateNum = strtotime($launchDate);

                if($campaign->status == 'Completed'){
                    $statusClass = 'success';
                }else{
                    $statusClass = 'primary';
                }

                //$dataOriginalTitle = "Launch Date: ".$launchDate."<br><br>Number of recipients: ".$sent."<br><br>Emails opened: ".$open."<br><br>Emails clicked: ".$click."<br><br>Submitted Credentials: ".$sdata."<br><br>Errors : ".$error."<br><br>Reported : ".$ereport."";

                $html .= '<tr>';
                $html .= '<td>'.$campaign->name.'</td>';
                $html .= '<td>'.$launchDateNum.'</td>';
                $html .= '<td><p class="badge badge-'.$statusClass.' text-wrap" style="width: 6rem;"  data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="'.$dataOriginalTitle.'">'.$campaign->status.'</p></td>';      
                $html .= '<td>'.html_writer::link(
                        new moodle_url(
                            $CFG->wwwroot.'/local/phishing/action/campaignsinglelist.php',
                            array('id' => $campaign->id)),'Chart',array('class' =>'btn btn-primary btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'View Results')).' '.html_writer::link(
                        new moodle_url(
                            $CFG->wwwroot.'/local/phishing/action/campaign_edit.php',
                            array('id' => $campaign->id)),'',array('class' =>'btn btn-campaign btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Copy Campaign')).' '.html_writer::link(
                        new moodle_url(
                            $CFG->wwwroot.'/local/phishing/action/campaign_delete.php',
                            array('id' => $campaign->id)),'Delete',array('class' =>'btn btn-danger btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Delete Campaign')).'</td>';  
                $html .= '</tr>';
            }   
        }   
    }else{

    }
    $html .= '</tbody>';
    $html .= '</table>';
    $html .= '</div></div>';
    echo $html;
}
echo $OUTPUT->footer();

?>

<style type="text/css">
    .path-local-phishing .grade-contents .btn-campaign {
        font-size: 15px;
        padding: 2px 5px;
    }
    .path-local-phishing .grade-contents .btn-campaign:before{
        content: "\f0c5";
        font-family: 'FontAwesome';
        font-size: 14px;
        font-weight: 400;
        margin-right:5px;
    }
    a.btn-campaign, .btn-campaign {
        background: #ff56da;
        border: 2px solid #ff56da;
        color: #fff;
    }
    a.btn-campaign:hover{
        background: #220087;
        border: 2px solid #220087;
        color: #fff;
        outline: none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
    }
</style>
<script type="text/javascript">
    $(function(){
        $('#active-campaigns').dataTable({
            "order": [[ 1, "asc" ]],
            "columnDefs" : [
                {
                "render": function ( data, type, row ) {
                    const d = new Date( parseInt(row[1])*1000 );
                    var date = getDateFormat(d);
                    return '<span style="display:none;">'+(parseInt(row[1]))+'</span>'+ date;
                },
                "targets": 1
            },
            { "visible": true,  "targets": [ 1 ] }
            ],
        });
        $('#archieved-campaigns').dataTable({
            "order": [[ 1, "asc" ]],
            "columnDefs" : [
                {
                "render": function ( data, type, row ) {
                    const d = new Date( parseInt(row[1])*1000 );
                    var date = getDateFormat(d);
                    return '<span style="display:none;">'+(parseInt(row[1]))+'</span>'+ date;
                },
                "targets": 1
            },
            { "visible": true,  "targets": [ 1 ] }
            ],
        });
        function getDateFormat(dt){
            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            var hours = dt.getHours();
          var minutes = dt.getMinutes();
          var sec = dt.getSeconds();
          var ampm = hours >= 12 ? 'pm' : 'am';
          hours = hours % 12;
          hours = hours ? hours : 12; // the hour '0' should be '12'
          minutes = minutes < 10 ? '0'+minutes : minutes;
          sec = sec < 10 ? '0'+sec : sec;
          var strTime = hours + ':' + minutes + ':'+sec+' ' + ampm;

            return month[dt.getMonth()] +' '+dt.getDate()+(dt.getDate() % 10 == 1 && dt.getDate() != 11 ? 'st' : (dt.getDate() % 10 == 2 && dt.getDate() != 12 ? 'nd' : (dt.getDate() % 10 == 3 && dt.getDate() != 13 ? 'rd' : 'th')))+ ' '+dt.getFullYear()+', '+strTime;
        }

        $('[data-toggle="tooltip"]').tooltip();
    })

</script>
