<?php 
$string['title'] = 'EBDAA License';
$string['licensesettings'] = 'EBDAA License Settings';
$string['licensecode'] = 'License Key';
$string['clientname'] = 'Client Name';
$string['save'] = 'Save';
$string['licenseandclient'] = 'Enter your license key and client name';
$string['licenseandclientupdate'] = 'Enter your license key and client name below to update';
$string['verify'] = 'Verify License';
$string['verifylicence'] = 'Verify';
$string['invalidmessage'] = 'Your license has expired. Contact EBDAA to renew your license. info@ebdaa.ae';
$string['validmessage'] = 'Your License is Valid';
$string['saveandverify'] = 'Save & Verify';
$string['licenseactivated'] = 'Activated! ';
$string['expirydate'] = 'Your license expires on ';
$string['updatelicensehere'] = 'Go to Update License';