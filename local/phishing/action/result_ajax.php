<?php

require_once('../../../config.php');
require_once('../lib.php');
require_login(0 , FALSE);
global $DB,$USER;
$companyid = $USER->company->id;
$context = context_system::instance();
$overall = has_capability('local/phishing:overall',$context);
$campaigns = has_capability('local/phishing:campaigns',$context);
//api key initialization
//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');

$campaignId=$_POST['campaign_id'];        // Collecting data from query string
$email = $_POST['email'];
$userId = $_POST['user_id'];
if($campaignId == '' && $email == '' && $userId == ''){ // Checking data it is a number or not
  echo "Data Error";    
  exit;
}
$campaignResult = get_campaign_result($urlip,$apikey,$campaignId);
$html = '';
$html .= '<div class="timeline col-sm-12 well well-lg">';
$html .= '<h6>Timeline for '.$campaignResult->name.'</h6>';
$html .= '<span class="subtitle">Email: '.$email.'<br>Result ID: '.$userId.'</span>';
$html .= '<div class="timeline-graph col-sm-6">';
if(!empty($campaignResult->timeline)){
    foreach($campaignResult->timeline as $timeline){
        if($timeline->message == 'Campaign Created'){
            $time = new DateTime(substr($timeline->time, 0, strlen($timeline->time)-2), core_date::get_user_timezone_object());
            $date = date('F jS Y H:i:s a',$time->getTimestamp());
            $html .= '<div class="timeline-entry">';    
            $html .= '<div class="timeline-bar"></div>';    
            $html .= '<div class="timeline-icon label-success">';    
            $html .= '<i class="fa fa-rocket"></i>';
            $html .= '</div>';
            $html .= '<div class="timeline-message">Campaign Created';    
            $html .= '<span class="timeline-date">'.$date.'</span>';
            $html .= '</div>';
            $html .= '</div>';
        }

        if($email == $timeline->email){
            if($timeline->message == 'Email Sent'){
                $time = new DateTime(substr($timeline->time, 0, strlen($timeline->time)-2), core_date::get_user_timezone_object());
                $date = date('F jS Y H:i:s a',$time->getTimestamp());
                $html .= '<div class="timeline-entry">';    
                $html .= '<div class="timeline-bar"></div>';    
                $html .= '<div class="timeline-icon label-success">';    
                $html .= '<i class="fa fa-envelope"></i>';
                $html .= '</div>';
                $html .= '<div class="timeline-message">Email Sent';    
                $html .= '<span class="timeline-date">'.$date.'</span>';
                $html .= '</div>';
                $html .= '</div>';
            }elseif($timeline->message == 'Clicked Link'){
                $time = new DateTime(substr($timeline->time, 0, strlen($timeline->time)-2), core_date::get_user_timezone_object());
                $date = date('F jS Y H:i:s a',$time->getTimestamp());
                $html .= '<div class="timeline-entry">';
                $html .= '<div class="timeline-bar"></div>';
                $html .= '<div class="timeline-icon label-clicked">';
                $html .= '<i class="fa fa-mouse-pointer"></i></div>';
                $html .= '<div class="timeline-message">Clicked Link';
                $html .= '<span class="timeline-date">'.$date.'</span>';
                $details = json_decode($timeline->details);
                if(!empty($details)){
                    $html .= '<div class="timeline-device-details"><div class="timeline-device-os">';
                    $html .= '<span class="fa fa-stack"><i class="fa fa-laptop fa-stack-2x"></i><i class="fa fa-vendor-icon fa-windows fa-stack-1x"></i></span>Windows (OS Version: 10)</div>';
                    $html .= '<div class="timeline-device-browser">';
                    $html .= '<span class="fa fa-stack"><i class="fa fa-chrome fa-stack-1x"></i></span> Chrome (Version: 77.0.3865.75)</div>';

                }
                // $html .= '<div class="timeline-event-results" style="display: none;"><table class="table table-condensed table-bordered table-striped"><thead><tr><th>Parameter</th><th>Value(s)</th></tr></thead><tbody></tbody>';
                //     $html .= '<tr><td>email</td><td>'.$details->email.'</td></tr><tr><td>pass</td><td>'.$details->pass.'</td></tr>';
                //     $html .='</tbody></table></div>';
                $html .= '</div></div>';
            }elseif($timeline->message == 'Submitted Data'){
                $time = new DateTime(substr($timeline->time, 0, strlen($timeline->time)-2), core_date::get_user_timezone_object());
                $date = date('F jS Y H:i:s a',$time->getTimestamp());
                $html .= '<div class="timeline-entry">';
                $html .= '<div class="timeline-bar"></div>';
                $html .= '<div class="timeline-icon label-danger">';
                $html .= '<i class="fa fa-exclamation"></i></div>';
                $html .= '<div class="timeline-message">Submitted Data';
                $html .= '<span class="timeline-date">'.$date.'</span>';
                $details = json_decode($timeline->details);
                
                // print_object($details->payload->password[0]);
                // if(!empty($details)){
                    $html .= '<div class="timeline-device-details"><div class="timeline-device-os">';
                    $html .= '<span class="fa fa-stack"><i class="fa fa-laptop fa-stack-2x"></i><i class="fa fa-vendor-icon fa-windows fa-stack-1x"></i></span>Windows (OS Version: 10)</div>';
                    $html .= '<div class="timeline-device-browser">';
                    $html .= '<span class="fa fa-stack"><i class="fa fa-chrome fa-stack-1x"></i></span> Chrome (Version: 77.0.3865.75)</div>';

                    $html .= '<div class="timeline-event-details hiddenn"><i class="fa fa-caret-down"></i> View Details</div>';

                    $html .= '<div class="timeline-event-results" style="display: none;"><table class="table table-condensed table-bordered table-striped" id="submittedTable"><thead><tr><th>Parameter</th><th>Value(s)</th></tr></thead><tbody>';
                    

                    foreach ($details->payload as $akey => $avalue) {
                        if($akey == "rid"){
                           
                        }else{
                            $html.='<tr><td>'.$akey.'</td><td>'.$avalue[0].'</td></tr>'; 
                        }
                    }


                    // $html .= '<tr><td>email</td><td>'.$details->payload->email_address[0].'</td></tr><tr><td>pass</td><td>'.$details->payload->password[0].'</td></tr>';
                    
                    $html .='</tbody></table></div>';
                // }
                $html .= '</div></div>';
            }
        }
    }
}
$html .= '</div>';
$html .= '</div>';

echo $html;
