<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/enroll_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php'); 
require_once($CFG->dirroot.'/local/course_report/csslinks.php');
global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$id = optional_param('id','',PARAM_INT);
// print_object($id);die();
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_report/index.php');
$title = get_string('enrollmentm', 'local_course_report');
//$PAGE->navbar->ignore_active();
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();      
include_once('jslink.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/chart.js'), true);
$mform = new local_enroll_form($CFG->wwwroot . '/local/course_report/enroll.php',array('id'=>$id));
//Form processing and displaying is done here
if(!empty($id)){
	$coursename = $DB->get_record('course',array('id'=>$id));
	$editdata = new \stdClass();
	$editdata->fullname = $coursename->fullname;
	$mform->set_data($editdata);
}

echo $OUTPUT->header();

if ($mform->is_cancelled()) {

} else if ($formdata = $mform->get_data()) {
	$enrollid = $formdata->userid;
	$updatesql = "UPDATE {enrol}
SET status = 0 WHERE id = $enrollid";
$updatestatus = $DB->execute($updatesql);
if($updatestatus)
{
echo "Updated Successful";

}
}

$mform->display(); 

echo $OUTPUT->footer();
?>