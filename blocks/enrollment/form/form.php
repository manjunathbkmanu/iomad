<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * You may have settings in your plugin
 *
 * @package    local_hpanalytics
 * @copyright  2014 Daniel Neis
 * @license    http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
global $OUTPUT, $CFG;
require_once($CFG->libdir.'/formslib.php');
require_once("./lib.php");
require_once($CFG->dirroot . '/enrol/manual/locallib.php');
$userid = optional_param('userid', null, PARAM_INT);
require_login();
class Enrollment_form extends moodleform {
    function definition() {
        global $CFG,$DB,$PAGE,$OUTPUT,$USER;
        $mform =& $this->_form;
        if(!empty($userid)){
            $useremail = $DB->get_field('user', 'email', array('id'=>$userid));
            $users = $DB->get_records('user',array('deleted'=>0,'suspended'=>0,'alternatename'=>$useremail));
        }else{
            $users = enrollment_get_users_list();
        }
        $userLists = array();
        $userLists[] = get_string('selectuser', 'block_enrollment');
        if(!empty($users)){
            foreach($users as $u){
                $userLists[$u->id] =  $u->lastname . ' ' . $u->firstname;
            }
        }
        // get users dropdown
        $userOptions = array(                                                                                                           
            'multiple' => false,
            'noselectionstring' => get_string('selectuser', 'block_enrollment'),
            'placeholder' => get_string('selectuser', 'block_enrollment'),
            'id'=>'user_auto_selection',                                                            
        );
        if(!empty($userOptions)) {
            $select = $mform->addElement('select','users', get_string('users','block_enrollment'), $userLists,$userOptions);
        }

        if(!is_siteadmin()){
            $companyid = $DB->get_field('company_users','companyid',array('userid'=>$USER->id));
            $coursesql ="SELECT c.* FROM {course} AS c
            JOIN {company_course} AS cc ON cc.courseid = c.id WHERE c.visible = 1 AND c.id != 1 AND cc.companyid =$companyid ORDER BY c.fullname ASC";
        }else{
            $coursesql = 'SELECT * FROM {course} WHERE visible = 1 AND id !=1';
        }

        $allcourses = $DB->get_records_sql($coursesql);
        $course_options = [];
        foreach($allcourses as $course){
         $coursename=$course->fullname;
         if(current_language() == 'ar'){
             $coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));

             $coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$course->id));
             $coursename = $coursenamearabic;
             if(empty($coursename)){
                $coursename =$course->fullname;
            }

        }
        $course_options[$course->id] = $coursename;

    }
    $options = array(                                                                                                           
        'multiple' => "multiple",
        'noselectionstring' => get_string('selectcourse', 'local_course_report'),
        'placeholder' => get_string('selectcourse', 'local_course_report'),
        'id'=>'course_selection_auto',                                                            
    );
    if(!empty($options)) {
        $select = $mform->addElement('select','courses[]', get_string('selectcourse','block_enrollment'), $course_options,$options);
    }


    $mform->addElement('submit', 'submitbutton', get_string('validate', 'block_enrollment'),['class' => 'enroll_submit_btn']);

}
}