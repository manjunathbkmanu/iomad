<?php 

$string['pluginname'] = 'Enrolled Courses';
$string['enrolleddate'] = 'Enrolled Date';
$string['cname'] = 'Course Name';
$string['vgrade'] = 'Score';
$string['lcourse'] = 'Launch Course';
$string['enddate'] = 'End Date';
$string['duration'] = 'Duration';
$string['status'] = 'Status';

$string['completed'] = 'COMPLETE';
$string['inprogress'] = 'IN PROGRESS';
$string['notstarted'] = 'NOT STARTED';
$string['complete'] = 'COMPLETE';