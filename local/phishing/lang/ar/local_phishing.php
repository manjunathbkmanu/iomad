<?php
$string['pluginname'] = 'برنامج التصيد';
$string['settings'] = '.الإعدادات الرئيسية لبرنامج التصيد';
$string['enable'] = '.قم باختيار خانة الإعدادات لبرنامج التصيد ';
$string['api'] = '.ادخل واجهة برمجة تطبيق التصيد';
$string['username'] = '.ادخل اسم المستخدم لواجهة برمجة تطبيق التصيد';
$string['templatehrd'] = '<h3>انشاء النموذج</h3>';
$string['templatecopy'] = '<h3>نسخ النموذج</h3>';
$string['templatehrdedit'] = '<h3>تعديل النموذج</h3>';
$string['temp_name'] = ':الإسم';
$string['temp_name_help'] = ':ادخل الإسم ';
$string['subname'] = ':الموضوع ';
$string['text'] = ':نص خام من البريد الإلكتروني المرسل إلى المستخدمين ';
$string['html'] = ' html أضف كود';
$string['attachments'] = '.إضافة ملفات';
$string['modified_date'] = '.التاريخ المعدل';
$string['checkboxtext'] = 'إضافة صورة تتبع ';
//templates list 
$string['listoftemp'] = 'قائمة النماذج';
$string['sno'] = 'SNO';
$string['name'] = 'الإسم';
$string['mdate'] = 'التاريخ المعدل';
$string['action'] = 'عمل';
$string['newtemp'] ='نموذج جديد';
$string['tempnotfound'] = 'لا يوجد نموذج';
//delete template 
$string['delete_temp'] = 'حذف نموذج';
$string['temppostmsg'] = 'تم انشاء النموذج بنجاح';
$string['tempeditmsg'] = 'تم تحديث سجل النموذج بنجاح';
$string['deletemsg'] = 'تم حذف النموذج بنجاح';
$string['cap'] = 'الدخول محظور';
//help button 
$string['tempname_name'] = 'ادخل اسم النموذج';
$string['subject_name'] = 'ادخل اسم الموضوع';
$string['text_help']= 'ادخل النص العادي';
$string['html_help'] = 'hmtl ادخل كود ';
$string['addfile_help'] = 'أضف الملف هنا';

//Landing Pages
$string['createladingpage'] = '<h3>انشاء صفحة الموقع</h3>';
$string['copyladingpage'] = '<h3>نسخ صفحة الموقع</h3>';
$string['editladingpage'] = '<h3>تعديل صفحة الموقع</h3>';
$string['listoflandingpage'] = 'قائمة صفحات المواقع';
$string['newlandingpage'] ='صفحة موقع جديدة';
$string['update'] ='.!تم تحديث السجل بنجاح';
$string['success'] ='.!تم انشاء السجل بنجاح';
$string['delete_page'] ='.حذف صفحة الموقع';
$string['deletepage'] = 'تم حذف الصفحة بنجاح';

//landing page help
$string['landing_name'] = 'ادخل اسم صفحة الموقع';


//campaigns
$string['dashboard'] = 'لوحة المعلومات';
$string['campaigns'] = 'قائمة الحملات';
$string['emailsent'] = 'تم ارسال البريد الإلكتروني';
$string['emailopen'] = 'تم فتح البريد الإلكرتروني';
$string['linkclick'] = 'تم الضغط على الرابط';
$string['submitteddata'] = 'تم ادخال البيانات';
$string['emailreport'] = 'تم اللإبلاغ عن البريد';
$string['recentcamp'] = 'الحملات الحديثة';
$string['cdate'] = 'تاريخ الإنشاء';
$string['status'] = 'الحالة';
$string['urlip'] = 'ادخل عنوان الإنترنت لعنوان الموقع';
$string['ccampaign'] = '<h3>انشاء حملة</h3>';
$string['ecampaign'] = '<h3>تعديل الحملة</h3>';
$string['dcampaign'] = 'حذف الحملة';
$string['cocampaign'] = 'انهاء الحملة';
$string['details'] = 'التفاصيل';
$string['copy'] ='نتائج لنسخة ';
$string['rcamp'] ='الحملات الحديثة';
$string['newcamp'] = '.!تم انشاء حملة جديدة بنجاح';
$string['deletecamp'] = 'تم حذف الحملة بنجاح!';
//create camp
$string['createcamp']= 'حملة جديدة';
$string['copycampaign']= 'نسخ الحملة';
$string['editcamp']= 'تعديل الحملة';
$string['emailtemp'] = 'نموذج البريد الإلكتروني';
$string['landingpage'] = 'صفحة الموقع';
$string['url'] = 'بروتوكول';
$string['all'] = 'الكل';
$string['launchdate'] = 'تاريخ الإطلاق';
$string['sendemail'] = ' ارسال رسائل البريد الإلكتروني عن طريق (اختياري) ';
$string['sendprofile'] = 'ملف الإرسال';
$string['groups'] = 'المجموعات ';
$string['sendemail'] = 'ارسال رسائل البريد الإلكتروني عن طريق (اختياري)';
//static words here 
$string['cam_help'] = 'ادخل اسم الحملة';
$string['temp_help'] = 'اختر النموذج';
$string['land_help'] = 'اختر صفحة الموقع';
$string['url_help'] = 'ادخل العنوان';
$string['launch_help'] = 'ادخل تاريخ الإطلاق';
$string['email_help'] = 'ادخل تاريخ ارسال البريد الإلكتروني';
$string['send_help'] = 'اختر ملف الإرسال';
$string['group_help'] = '.عن طريق تحديد مجموعات متعددة تمت إضافة المستخدمين إلى مجموعات';

//sending profile langfile 
$string['sendingprofile'] = '<h3>ملفات الإرسال</h3>';
$string['name_help'] = '.أدخل اسم ملف تعريف الإرسال';
$string['sendingprofilelist'] = 'قائمة ملفات الإرسال';
$string['newsendingprofile'] = 'انشاء ملف الإرسال';
$string['editsendingprofile'] = 'تعديل ملفات الإرسال';
$string['interface_type'] = 'نوع الواجهة ';
$string['from'] = 'من ';
$string['host'] = 'المضيف ';
$string['host_help'] = 'مساعدة المضيف ';
$string['username'] = 'اسم المستخدم ';
$string['password'] = 'كلمة المرور ';
$string['emailhdr'] = '<h3>رؤوس البريد الإلكتروني </h3>';
$string['xcheader'] = 'رأس مخصص';
$string['urlphish'] = '{{العنوان}} - التصيد';
$string['ignorecer'] = 'تجاهل أخطاء الشهادة';
$string['delete_sendp'] = 'حذف قائمة الإرسال';
$string['cresend'] = '.!تم انشاء ملف الإرسال بنجاح';
$string['editresend'] = '.!تم تحديث سجل الإرسال بنجاح';
$string['deletesend'] = 'تم حذف ملف الإرسال بنجاح!';
//static words here
$string['interface_help'] = 'interface_type: SMTP :أدخل نوع الواجهة: مثال';
$string['from_address_help'] = 'from_address: John Doe <john@example.com> :أدخل من العنوان: مثال';
$string['host_help'] = 'smtp.example.com: 25أدخل اسم المضيف: مثال';
$string['username_help'] = 'أدخل اسم المستخدم: {اختياري}';
$string['password_help'] = 'أدخل كلمة المرور: {اختياري}';
$string['header_help'] = 'X-Header :أدخل المفتاح: مثال: المفتاح';
$string['header1_help'] = 'Foo Bar :أدخل القيمة: مثال: القيمة';


//user and grops
$string['usergroup'] ='<h3>المستخدمين والمجموعات</h3>';
$string['delusergroup'] ='حذف المستخدمين والمجموعات';
$string['newusergroup'] ='مستخدمين ومجموعات جدد';
$string['editusergroup'] ='<h3>تعديل المستخدمين والمجموعات</h3>';
$string['groupcreate'] = '.تم انشاء مستخدمين ومجموعات جديدة بنجاح';
$string['groupedit'] = '.!تن تحديث المستخدمين والمجموعات بنجاح';
$string['listofuser'] = 'قائمة المستخدمين والمجموعات';
$string['noofmembes'] = '# الأعضاء';
$string['position'] = 'الموقع';
$string['add'] = 'إضافة';
$string['bulk user'] = 'إضافة مجموعة من المستخدمين';
$string['adduser'] = '<h3>إضافة مستخدم</h3>';
$string['edituser'] = '<h3>تعديل مستخدم</h3>';
$string['targets'] = 'اختيارالمستخدمين';
$string['emailtext'] ='أدخل عنوان البريد الإلكتروني مع مسافة بواسطة الفاصلة';
$string['textnote'] = '<h3>.اختر القائمة المنسدلة أو عدا ذلك أدخل عنوان البريد الإلكتروني للمستخدم مع تقسيمه بفاصلة</h3>';
$string['note'] = '<h3>ملاحظة </h3>';
$string['emailnot'] = '.عنوان البريد الإلكتروني هذا غير موجود في النظام. لذا يرجى إنشاء مستخدم أولاً ثم إنشاء مجموعة';

$string['import'] = 'استيراد';
$string['import_content'] = 'استيراد محتوى البريد الإلكتروني';
$string['checkboxtemp'] = 'تغيير الروابط للإشارة إلى صفحة  الموقع';
$string['addtrackingimage'] = 'إضافة صورة التتبع';
$string['url_static'] = '.أدخل عنوان الموقع ، تتم إضافة جميع محتويات موقع الويب إلى النموذج أدناه القابل للتحرير';
$string['capture_credentials'] = 'التقاط البيانات المقدمة';
$string['capture_passwords'] = 'التقاط كلمات المرور';
$string['redirect_url'] = 'إعادة توجيه إلى';
$string['passnote'] = ' !تحذير: بيانات الاعتماد غير مشفرة حاليًا. هذا يعني أنه يتم تخزين كلمات المرور الملتقطة في قاعدة البيانات كنص واضح. كن حذرا مع هذا ';
$string['sure'] = 'هل أنت متأكد بأنك تريد أن تخذف؟';
$string['sure_complete'] = 'هل أنت متأكد بأنك تريد الإكمال؟';
$string['list1'] = 'العودة إلى صفحة القائمة';
$string['imagedesc'] = 'png. وjpg. حجم الصورة الموصى به للصورة هو 800 × 800 بكسل. يجب أن يكون نوع الملف بتنسيق ';
$string['fname'] = 'الإسم الأول';
$string['lname'] = 'اسم العائلة';
$string['email'] = 'البريد الإلكتروني';
$string['reported'] = 'Reported';
$string['activecampaign'] = 'حملة فعالة';
$string['archivecampaign'] = 'حملة تم أرشفتها';
$string['newcampaign'] = 'حملة جديدة';


$string['launch_date'] = 'تاريخ الإطلاق';