<?php
require_once('../../../config.php');
global $DB,$USER;
$companyid = $USER->company->id;
//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');	
$curl_handle = curl_init();
$url = $protocol.$urlip.'/api/groups/?api_key='.$apikey;
curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
// Set the curl URL option
curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($curl_handle, CURLOPT_URL, $url);
// This option will return data as a string instead of direct output
curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
// Execute curl & store data in a variable
$curl_data = curl_exec($curl_handle);
if (!curl_exec($curl_handle)) {
	die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
}
$groups_datas = json_decode($curl_data);
//print_object($smtp_datas);
$groups_dataa = [];
if(!empty($groups_datas)){
	foreach ($groups_datas as $key => $groups_data) {
		$groups_dataa[$groups_data->id]=$groups_data->name;
	}
}
asort($groups_dataa);
echo json_encode($groups_dataa);