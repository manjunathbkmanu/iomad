<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_accesscohort
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../../config.php');
require_once('../form/landingpage_copy_form.php');
require_once($CFG->libdir . '/formslib.php');

global $DB,$USER;

require_login(0,false);
$eid = optional_param('id',null, PARAM_INT);
//api key initialization
//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
$companyid = $USER->company->id;
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$context = context_system::instance();
//capability 
$overall = has_capability('local/phishing:overall',$context);
$createtemp = has_capability('local/phishing:landingpage',$context);
$PAGE->set_context(context_system::instance());
//header changes on basis of edit and create form
	$title = get_string('copyladingpage', 'local_phishing');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/action/landingpage_copy.php?id='.$eid);
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/phishing/js/import.js'), true);
require_login();
$previewnode = $PAGE->navigation->add(get_string('pluginname','local_phishing'), new moodle_url($CFG->wwwroot.'/local/phishing/landingpagelist.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title, new moodle_url($CFG->wwwroot.'/local/phishing/action/landingpage_copy.php'));
$thingnode->make_active();
$mform = new landingpage_copy_form();
$curl_handle = curl_init();
if ($mform->is_cancelled()){
	redirect(new moodle_url('/local/phishing/landingpagelist.php', array()));
} else if($data = $mform->get_data()) {
	$x=false;
	$xx = false; 
	$red = '';
	if($data->capture_credentials==1){
			$x=true;
	}
	if($data->capture_credentials==1){
		$xx = true; 
	}
	if(!empty($data->redirect_url)){
		$red = $data->redirect_url;
	}
	$url = $protocol.$urlip.'/api/pages/?api_key='.$apikey;
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		// Set the curl URL option
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl_handle, CURLOPT_URL, $url);
		// This option will return data as a string instead of direct output
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	//create array for creating template

	$dataarray = array('id'=>$eid,'name'=>$data->name,'html'=>$data->editorvalue,'capture_credentials'=>$x,'capture_passwords'=>$xx,'redirect_url'=>$red);
	$data_json = json_encode($dataarray);
	$msg ='';
	if(!empty($data_json)){		
		if($edit==1 and !empty($eid)){
			$url1 = $protocol.$urlip.'/api/pages/'.$eid.'?api_key='.$apikey;
			curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($curl_handle, CURLOPT_URL, $url1);	
			curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
			$response  = curl_exec($curl_handle);
			$msg = get_string('update','local_phishing');
		}else{
			curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
			$response  = curl_exec($curl_handle);
			$msg = get_string('success','local_phishing');		
		}
		//display message code 
		///print_r($response);die();
		if($response) {
			redirect(new moodle_url($CFG->wwwroot.'/local/phishing/landingpagelist.php'),$msg);
		} else {
			echo "error";
		}
		//curl error will display on page	
		if (!curl_exec($curl_handle)) {
			die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
		}
	}
	curl_close($curl_handle);
}
echo $OUTPUT->header();
if($overall || $createtemp){
	$mform->display();
}else{
	echo html_writer::div(
		get_string('cap', 'local_phishing'),'alert alert-danger'
	);
}

echo $OUTPUT->footer();
?>






