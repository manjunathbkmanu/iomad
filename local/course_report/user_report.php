<style>
#region-main #encsvtable {
	display: none !important;
}
#region-main #arcsvtable{
	display: none !important;
}
.ard {
transform: translate3d(-205px, 54px, 0px) !important;
margin: 0px auto;
left: auto !important;
/*width: 20%;*/
}
.ard2 {
transform: translate3d(-205px, 54px, 0px) !important;
margin: 0 auto;
left: auto !important;
/*width: 20%;*/
}
</style>
<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/user_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot.'/local/course_report/csslinks.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_report/user_report.php');
$title = get_string('userreport', 'local_course_report');
//$PAGE->navbar->ignore_active();
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();		
include_once('jslink.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/chart.js'), true);
$ard = '';
if(current_language() == 'ar'){
	$ard = 'ard';
	echo '<style>div#language-drop {margin-right:4%;}</style>';	
}
$mform = new local_user_form();	
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot.'/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback


$html .='<div class="container-fluid pt-2 mb-3 formcontainer">
<div class="row"><div class="col-md-12">';
$html .= '<h3>'.get_string('userreport', 'local_course_management').'</h3>'; 
$html .='<hr>';
$html .= $mform->render(); 
$html .='</div></div></div>';
echo $html;
$not_applicable = get_string('not_applicable', 'local_course_report');
if ($mform->is_cancelled()) {
	
} else if ($data = $mform->get_data()) {
	$userid = $data->userid;
	if($userid == 0){
		$table  = new \html_table();
		$table->id = 'usertable';
		$table->head = array(get_string('fullname', 'local_course_report'),
			get_string('complete', 'local_course_report'),
			get_string('inprogress', 'local_course_report'),
			get_string('notstarted', 'local_course_report'),
			get_string('completioninpercentage', 'local_course_report')
		);
		$getallusersid = $DB->get_records('user',array('deleted'=>0,'suspended'=>0));
		//creating csv for en.
		$encsvtable  = new \html_table();
		$encsvtable->id = 'encsvtable';
		$encsvtable->data[]= array('Full Name','Completed','In Progress','Not Started','Completion (in %)');

		//creating csv for ar.
		$arcsvtable  = new \html_table();
		$arcsvtable->id = 'arcsvtable';
		$arcsvtable->data[] = array(get_string('fullname_ar', 'local_course_report'),
				get_string('complete_ar', 'local_course_report'),
				get_string('inprogress_ar', 'local_course_report'),
				get_string('notstarted_ar', 'local_course_report'),
				get_string('completionperc_ar', 'local_course_report')
			);
		

		foreach ($getallusersid as $values) {
			if(!empty($values->id) && !is_siteadmin($values->id) && $values->id != 1)
			{
				$alluserdetails = track_course_status($values->id);
				$link = new moodle_url($CFG->wwwroot.'/local/course_report/user_details.php?id='.$alluserdetails['uid']);
				$username = '<a href="'.$link.'">'.$alluserdetails['username'].'</a>';
				$table->data[] = array($username, $alluserdetails['completed'], $alluserdetails['inprogress'],$alluserdetails['notcompleted'],$alluserdetails['completionpercentage']);

				$encsvtable->data[] = array($alluserdetails['username'],$alluserdetails['completed'],$alluserdetails['inprogress'],$alluserdetails['notcompleted'],$alluserdetails['completionpercentage']);
				$arcsvtable->data[] = array($alluserdetails['username'],$alluserdetails['completed'],$alluserdetails['inprogress'],$alluserdetails['notcompleted'],$alluserdetails['completionpercentage']);
			}
		}

	}else{
				//creating csv for en.
		$encsvtable  = new \html_table();
		$encsvtable->id = 'encsvtable';
		$encsvtable->data[]= array('Full Name','Completed','In Progress','Not Started','Completion (in %)');

				//creating csv for ar.
		$arcsvtable  = new \html_table();
		$arcsvtable->id = 'arcsvtable';
		$arcsvtable->data[] = array(get_string('fullname_ar', 'local_course_report'),
				get_string('complete_ar', 'local_course_report'),
				get_string('inprogress_ar', 'local_course_report'),
				get_string('notstarted_ar', 'local_course_report'),
				get_string('completionperc_ar', 'local_course_report')
			);

		$usercoursedata = track_course_status($userid);
		$table  = new \html_table();
		$table->id = 'usertable';
		$table->head = array(get_string('fullname', 'local_course_report'),
			get_string('department', 'local_course_report'),
			get_string('complete', 'local_course_report'),
			get_string('inprogress', 'local_course_report'),
			get_string('notstarted', 'local_course_report'),
			get_string('completioninpercentage', 'local_course_report')
		);
		$user_data = $DB->get_record('user',array('id'=>$userid));



		$link = new moodle_url($CFG->wwwroot.'/local/course_report/user_details.php?id='.$usercoursedata['uid']);

		$username = '<a href="'.$link.'">'.$usercoursedata['username'].'</a>';
		$table->data[] = array($username, strlen(trim($user_data->department))==0?$not_applicable:$user_data->department,$usercoursedata['completed'], $usercoursedata['inprogress'],$usercoursedata['notcompleted'],$usercoursedata['completionpercentage']);

		$encsvtable->data[] = array($username,$usercoursedata['completed'], $usercoursedata['inprogress'],$usercoursedata['notcompleted'],$usercoursedata['completionpercentage']);
		$arcsvtable->data[] = array($username,$usercoursedata['completed'], $usercoursedata['inprogress'],$usercoursedata['notcompleted'],$usercoursedata['completionpercentage']);


	}
	echo html_writer::table($encsvtable);
	echo html_writer::table($arcsvtable);
	$data = html_writer::start_div('container-fluid');
	$data .= html_writer::start_div('row pb-3', array('id'=>'reportPage'));
	$data .= html_writer::start_div('col-md-12 text-center');
	$data .= '<div class="dropdown">
	<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">'.get_string('export','local_course_report').'
	<span class="caret"></span></button>
	<ul class="dropdown-menu '.$ard.'" style="text-align: center;">
	<!--<a href="#" id="downloadPdf"><li>PDF</li></a> -->
	<a href="#" id="enexport"><li>CSV</li></a>
	</ul>
	</div>
	<div class="dropdown">
	<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">'.get_string('exportinar','local_course_report').'
	<span class="caret"></span></button>
	<ul class="dropdown-menu '.$ard.'2" style="text-align: center;">
	<!--<a href="#" id="downloadPdfarabic"><li>PDF</li></a>-->
	<a href="#" id="arexport"><li>CSV</li></a>
	</ul>
	</div>';

	$data .= html_writer::end_div();
	$data .= html_writer::end_div();
	$data .= html_writer::end_div();
	echo $data;
	echo html_writer::table($table);
	
}
echo $OUTPUT->footer();
?>

<script type="text/javascript">
	$(document).ready(function() {

		function exportTableToCSV($table, filename) {

			var $rows = $table.find('tr:has(td)'),

      // Temporary delimiter characters unlikely to be typed by keyboard
      // This is to avoid accidentally splitting the actual contents
      tmpColDelim = String.fromCharCode(11), // vertical tab character
      tmpRowDelim = String.fromCharCode(0), // null character

      // actual delimiter characters for CSV format
      colDelim = '","',
      rowDelim = '"\r\n"',

      // Grab text from table into CSV formatted string
      csv = '"' + $rows.map(function(i, row) {
      	var $row = $(row),
      	$cols = $row.find('td');

      	return $cols.map(function(j, col) {
      		var $col = $(col),
      		text = $col.text();

          return text.replace(/"/g, '""'); // escape double quotes

      }).get().join(tmpColDelim);

      }).get().join(tmpRowDelim)
      .split(tmpRowDelim).join(rowDelim)
      .split(tmpColDelim).join(colDelim) + '"';

    // Deliberate 'false', see comment below
    if (false && window.navigator.msSaveBlob) {

    	var blob = new Blob([decodeURIComponent(csv)], {
    		type: 'text/csv;charset=utf-8'
    	});

      // Crashes in IE 10, IE 11 and Microsoft Edge
      // See MS Edge Issue #10396033
      // Hence, the deliberate 'false'
      // This is here just for completeness
      // Remove the 'false' at your own risk
      window.navigator.msSaveBlob(blob, filename);

  } else if (window.Blob && window.URL) {
  	var universalBOM = "\uFEFF";
      // HTML5 Blob        
      var blob = new Blob([universalBOM+csv], {
      	type: 'text/csv;charset=utf-8'
      });
      var csvUrl = URL.createObjectURL(blob);

      $(this)
      .attr({
      	'download': filename,
      	'href': csvUrl
      });
  } else {
      // Data URI
      var universalBOM = "\uFEFF";
      var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(universalBOM+csv);

      $(this)
      .attr({
      	'download': filename,
      	'href': csvData,
      	'target': '_blank'
      });
  }
}

  // This must be a hyperlink
  $("#enexport").on('click', function(event) {
    // CSV
    var args = [$('table#encsvtable'), 'export.csv'];

    exportTableToCSV.apply(this, args);

    // If CSV, don't do event.preventDefault() or return false
    // We actually need this to be a typical hyperlink
});
    // This must be a hyperlink
    $("#arexport").on('click', function(event) {
    // CSV
    var args = [$('table#arcsvtable'), 'export.csv'];

    exportTableToCSV.apply(this, args);

    // If CSV, don't do event.preventDefault() or return false
    // We actually need this to be a typical hyperlink
});
});

</script>
<style>
a.export,
a.export:visited {
  display: inline-block;
  text-decoration: none;
  color: #000;
  background-color: #ddd;
  border: 1px solid #ccc;
  padding: 8px;
}

.input-group-btn{
	/*display:none*/
}

button.multiselect.dropdown-toggle.btn.btn-default {
    color: #e2e2e2;
    background-color: white;
    border: 1px solid #e2e2e2;
}

.form-inline .multiselect-container label.checkbox, .form-inline .multiselect-container label.radio{
	justify-content:inherit !important;
}

.form-inline .multiselect-container li a label.checkbox input[type=checkbox], .form-inline .multiselect-container li a label.radio input[type=radio]{
margin-left: 10px !important;
}

.multiselect-container>li>a>label {
    padding: 3px 20px 3px 40px!important;
}

.btn-group, .btn-group-vertical{
	width: 234px;
}


span.multiselect-selected-text{
	text-align: left;
}

.col-md-3{
	display:flex;
}

.col-form-label{
	margin-top: auto;
    padding: 9px 0;
}
.custom-select{
    overflow: hidden !important;
    text-overflow: ellipsis;
    white-space: nowrap;
    padding-right: 36px;
}

.form-autocomplete-downarrow{
	color: #e2e2e2;
    top: .5rem;
    right: .5rem;
    cursor: pointer;
    font-size: 10px;
}

.mform .form-inline .form-control, .mform .form-inline .custom-select{
    width: 230px;
    font-size: 14px;
}

button.multiselect.dropdown-toggle.btn.btn-default{
    color: #4f5256;
	font-size:14px;
	width:230px !important;
    font-weight:normal;
	display:flex;
	justify-content:space-between;   
}
.caret::after { 
	margin: auto 0;
    margin-right: -3px;
}

button.multiselect.dropdown-toggle.dropdown-toggle::after{
	margin-left: 26px;
    border-right: .3em solid transparent !important;
    margin-bottom: 12px;
    <?php if(current_language() == 'ar') { ?>

    position: absolute;
    right: 15px;
    top: 15px;
<?php } ?>
}

select#course_selection_auto{
    color: #4f5256;
    background-image: linear-gradient(45deg, transparent 50%, #4f5256 60%), linear-gradient(135deg, #4f5256 40%, transparent 50%) !important;
    background-position: calc(100% - 25px) 10px, calc(100% - 15px) 10px, 100% 0;
    background-size: 4px 4px, 10px 4px;
    background-repeat: no-repeat;
    -webkit-appearance: none;
    -moz-appearance: none;
    font-size: 14px;
}



</style>
