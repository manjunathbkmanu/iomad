<?php

defined('MOODLE_INTERNAL') || die();
include_once('licensecheck.php');
user_preference_allow_ajax_update('drawer-open-nav', PARAM_ALPHA);
require_once($CFG->libdir . '/behat/lib.php');

if (isloggedin()) {
    $navdraweropen = (get_user_preferences('drawer-open-nav', 'true') == 'true');
} else {
    $navdraweropen = false;
}

global $USER;
/* if (is_siteadmin()) {
	$navdraweropen = true; // MIhir for ebdaa
} else {
	$navdraweropen = false; // MIhir for ebdaa
} */

$navdraweropen = false; // MIhir for ebdaa
$extraclasses = [];
if ($navdraweropen) {
    $extraclasses[] = 'drawer-open-left';
}
$bodyattributes = $OUTPUT->body_attributes($extraclasses);
$blockshtml = $OUTPUT->blocks('side-pre');
$hasblocks = strpos($blockshtml, 'data-block=') !== false;
$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();
$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'sidepreblocks' => $blockshtml,
    'hasblocks' => $hasblocks,
    'bodyattributes' => $bodyattributes,
    'navdraweropen' => $navdraweropen,
    'regionmainsettingsmenu' => $regionmainsettingsmenu,
    'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu)
];

$PAGE->requires->jquery ();
$PAGE->requires->js('/theme/maker/plugins/back-to-top.js');
$PAGE->requires->js('/theme/maker/plugins/flexslider/jquery.flexslider-min.js');
$PAGE->requires->js('/theme/maker/plugins/owlcarousel/owl.carousel.min.js');
$PAGE->requires->js('/theme/maker/plugins/drop-hover.js',true);
$PAGE->requires->css('/theme/maker/plugins/flexslider/flexslider.css');
$PAGE->requires->css('/theme/maker/plugins/owlcarousel/assets/owl.carousel.min.css');
$PAGE->requires->css('/theme/maker/plugins/owlcarousel/assets/owl.theme.default.min.css');
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/responsive.css') , true);

$templatecontext['flatnavigation'] = $PAGE->flatnav;
echo $OUTPUT->render_from_template('theme_maker/dashboard', $templatecontext);

