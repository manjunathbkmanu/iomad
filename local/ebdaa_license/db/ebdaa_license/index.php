<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_ebdaa_license
 * @copyright  Manjunath B K<manjunath@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('forms/license_form.php');
require_once($CFG->libdir . '/formslib.php');
require_once 'includes/lb_helper.php';


// Create a new LicenseBoxAPI helper class.
$lbapi = new LicenseBoxAPI();

// Performs background license check, pass true as 1st parameter to perform periodic verifications only.
// $lcode = '2AB6-51FD-91C2-09FE';
// $clientname = 'test';
// $lb_verify_res = $lbapi->verify_license(FALSE,$lcode,$clientname);

global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin_license');
$PAGE->set_url($CFG->wwwroot . '/local/local_ebdaa_license/index.php');
$title = get_string('title', 'local_ebdaa_license');
//$PAGE->navbar->ignore_active();
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$sql ="SELECT * FROM {local_ebdaa_license}";
$licensedata = $DB->get_record_sql($sql);
$mform = new local_license_form();
echo $OUTPUT->header();
if ($formdata = $mform->get_data()) {
	if(!empty($formdata)){
		if ($formdata->submit ==='Save') {
			if(empty($licensedata)){
				$insert  = new stdClass();
				$insert->licensekey = $formdata->licensecode;
				$insert->clientname = $formdata->clientname;
				$DB->insert_record('local_ebdaa_license',$insert);
			}else{
				$update  = new stdClass();
				$update->id = $licensedata->id;
				$update->licensekey = $formdata->licensecode;
				$update->clientname = $formdata->clientname;
				$DB->update_record('local_ebdaa_license',$update);
			}
		}else if ($formdata->submit ==='Verify') {
			if(!empty($licensedata)){
				$lkey = $licensedata->licensekey;
				$cname = $licensedata->clientname;
				$lb_verify_res = $lbapi->verify_license(FALSE,$lkey,$cname);
				if($lb_verify_res['status']!=true){
					$url = $CFG->wwwroot.'/local/ebdaa_license/redirect.php';
					redirect($url, 'Your license is invalid, please contact support', 5);
				}else{
					echo "license is valid";
				}

			}
			
		}
	}

}
else{
	if(!empty($licensedata)){
		$sdata  = new stdClass();
		$sdata->licensecode = $licensedata->licensekey;
		$sdata->clientname = $licensedata->clientname;
		$mform->set_data($sdata);
	}

}
$mform->display();
echo $OUTPUT->footer();

