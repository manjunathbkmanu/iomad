<?php 

$string['pluginname'] = 'مخطط النجاح والرسوب';
$string['completed'] = 'تم الانتهاء';
$string['inprogress'] = 'في تقدم';
$string['notstarted'] = 'لم يتم البدأ';
$string['pass'] = 'ناجح';
$string['fail'] = 'راسب';
$string['totalcount'] = 'العدد الإجمالي';
