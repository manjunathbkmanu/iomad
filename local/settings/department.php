<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_healthcompany
 * @copyright  Manjunath B K<manjunathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/department_form.php');
require_once($CFG->libdir . '/formslib.php');
global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/settings/department.php');
$title = get_string('title', 'local_settings');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();


$mform = new local_department_form();
//Form processing and displaying is done here
if ($mform->is_cancelled()) {
    //Handle form cancel operation, if cancel button is present on form
} else if ($data = $mform->get_data()) {
	$companyid = $USER->company->id;
	set_config('userdepartment', $data->department, 'coursemanagement'.$companyid);
}else{
	$companyid = $USER->company->id;
	$config = get_config('coursemanagement'.$companyid);
	$set = new stdClass();
	$set->department = $config->userdepartment;
	$mform->set_data($set);
}
echo $OUTPUT->header();
$mform->display();
echo $OUTPUT->footer();