<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phish
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
//defined('MOODLE_INTERNAL') || die();

require('../../config.php');
require_login(0 , FALSE);
global $CFG,$DB,$USER;
$context = context_system::instance();
$overall = has_capability('local/phish:overall',$context);
$landingpage = has_capability('local/phish:landingpage',$context);
$PAGE->set_context(context_system::instance());
$title = get_string('listofuser', 'local_phish');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/usergrouplist.php');
//api key initialization
//Manju: changed the plugin name from local_gophish to local_phishing.08/02/2021.
$companyid = $USER->company->id;
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$overall = has_capability('local/phish:overall',$context);
$PAGE->navbar->ignore_active();
$PAGE->requires->jquery();
$PAGE->requires->css(new 
    moodle_url($CFG->wwwroot.'/local/course_report/css/dataTables.bootstrap4.min.css'));
$PAGE->requires->css(new 
    moodle_url($CFG->wwwroot.'/local/course_report/css/buttons.bootstrap4.min.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/newjs/jquery.dataTables.min.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/newjs/dataTables.bootstrap4.min.js'), true);
$previewnode = $PAGE->navbar->add(get_string('pluginname','local_phish'),'');
$previewnode = $previewnode->add(get_string('listofuser','local_phish'),$CFG->wwwroot.'/local/phishing/usergrouplist.php');
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback
echo '<h2>'.get_string('listofuser','local_phish').'</h2>';
echo '<br>';
//api curl initialization
if($overall || $landingpage){
    echo html_writer::link(
        new moodle_url(
            $CFG->wwwroot.'/local/phishing/action/user_group_create.php'
        ),
        get_string('newusergroup','local_phish'),
        array(
          'class' => 'btn btn-primary'
      )
    );
    echo '<br><br>';  

    $curl_handle = curl_init();
    $url = $protocol.$urlip.'/api/groups/?api_key='.$apikey;
    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// Set the curl URL option
    curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl_handle, CURLOPT_URL, $url);
	// This option will return data as a string instead of direct output
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	// Execute curl & store data in a variable
    $curl_data = curl_exec($curl_handle);
    if (!curl_exec($curl_handle)) {
        die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
    }
    curl_close($curl_handle);
	// Decode JSON into PHP array
    $usergroupdatas = json_decode($curl_data);
    $table = new html_table();
    $table->head = (array) get_strings(array('name','noofmembes', 'mdate','action'), 'local_phish');
    $i=1;
    if(!empty($usergroupdatas)){
    foreach ($usergroupdatas as $key => $usergroupdata) {
          # code...
        $date = new DateTime(substr($usergroupdata->modified_date, 0, 19) . 'Z');
        $modifiedDate = $date->format('F jS Y H:i:s a');
       $table->data[] = array(
        $usergroupdata->name,
		count($usergroupdata->targets),
        $modifiedDate,
        html_writer::link(
            new moodle_url(
                $CFG->wwwroot.'/local/phishing/action/user_group_create.php',
                array(
                    'id' => $usergroupdata->id,
                    'edit'=>1
                )
            ),
            'Edit',
            array(
                'class' => 'btn btn-small btn-primary'
            )
        ).' '.
        html_writer::link(
            new moodle_url(
                $CFG->wwwroot.'/local/phishing/action/user_group_delete.php',
                array('id' => $usergroupdata->id)), 'Delete',array('class' =>'btn btn-danger btn-xs' ))
    );
       $i++;
   }
   echo html_writer::table($table);
}
}else{
    echo html_writer::div(
        get_string('cap', 'local_phish'),'alert alert-danger'
    );
}



echo $OUTPUT->footer();

?>
<script type="text/javascript">
    $(function(){
        $('.generaltable').dataTable();
    })
</script>
