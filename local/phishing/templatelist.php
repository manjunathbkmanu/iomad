<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phish
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
//defined('MOODLE_INTERNAL') || die();

require('../../config.php');
require_login(0 , FALSE);
global $CFG,$DB,$USER;
$context = context_system::instance();
$overall = has_capability('local/phishing:overall',$context);
$createtemp = has_capability('local/phishing:createtemp',$context);
$PAGE->set_context(context_system::instance());
$title = get_string('listoftemp', 'local_phishing');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/templatelist.php');
//api key initialization
//Manju: changed the plugin name from local_gophish to local_phishing.08/02/2021.
$companyid = $USER->company->id;
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$PAGE->navbar->ignore_active();
$PAGE->requires->jquery();
$PAGE->requires->css(new 
    moodle_url($CFG->wwwroot.'/local/course_report/css/dataTables.bootstrap4.min.css'));
$PAGE->requires->css(new 
    moodle_url($CFG->wwwroot.'/local/course_report/css/buttons.bootstrap4.min.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/newjs/jquery.dataTables.min.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/newjs/dataTables.bootstrap4.min.js'), true);
$previewnode = $PAGE->navbar->add(get_string('pluginname','local_phishing'),'');
$previewnode = $previewnode->add(get_string('listoftemp','local_phishing'),$CFG->wwwroot.'/local/phishing/templatelist.php');
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback
echo '<h2>'.get_string('listoftemp','local_phishing').'</h2>';
echo '<br>';
//api curl initialization
if($overall || $createtemp){
    echo html_writer::link(
        new moodle_url(
            $CFG->wwwroot.'/local/phishing/action/template_create.php'
        ),
        get_string('newtemp','local_phishing'),
        array(
          'class' => 'btn btn-primary'
      )
    );
    echo '<br><br>';  

    $curl_handle = curl_init();
    $url = $protocol.$urlip.'/api/templates/?api_key='.$apikey;
    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
// Set the curl URL option
    curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl_handle, CURLOPT_URL, $url);

// This option will return data as a string instead of direct output
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

// Execute curl & store data in a variable
    $curl_data = curl_exec($curl_handle);

    if (!curl_exec($curl_handle)) {
        die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
    }

    curl_close($curl_handle);
// Decode JSON into PHP array
    $template_datas = json_decode($curl_data);
    $table = new html_table();
    $table->head = (array) get_strings(array('name', 'mdate','action'), 'local_phishing');
    $i=1;
    if(!empty($template_datas)){
    foreach ($template_datas as $key => $template_data) {      # code...
        $date = new DateTime(substr($template_data->modified_date, 0, 19) . 'Z');
        // $modifiedDate = $date->format('F jS Y H:i:s a');
        $modifiedDate = strtotime(substr($template_data->modified_date, 0, 19) . 'Z');
       $table->data[] = array(
        $template_data->name,
        $modifiedDate,
        html_writer::link(
            new moodle_url(
                $CFG->wwwroot.'/local/phishing/action/template_create.php',
                array(
                    'id' => $template_data->id,
                    'edit'=>1
                )
            ),
            'Edit',
            array(
                'class' => 'btn btn-small btn-primary'
            )
        ).' '.html_writer::link(
                        new moodle_url(
                            $CFG->wwwroot.'/local/phishing/action/template_copy.php',
                            array('id' => $template_data->id)),'',array('class' =>'btn btn-campaign btn-xs', 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => 'Copy Template')).' '.html_writer::link(
            new moodle_url(
                $CFG->wwwroot.'/local/phishing/action/template_delete.php',
                array('id' => $template_data->id)), 'Delete',array('class' =>'btn btn-danger btn-xs' ))
    );
       $i++;
   }
   echo html_writer::table($table);
}
}else{
    echo html_writer::div(
        get_string('cap', 'local_phishing'),'alert alert-danger'
    );
}



echo $OUTPUT->footer();

?>
<style type="text/css">
    .path-local-phishing .grade-contents .btn-campaign {
        font-size: 15px;
        padding: 2px 5px;
    }
    .path-local-phishing .grade-contents .btn-campaign:before{
        content: "\f0c5";
        font-family: 'FontAwesome';
        font-size: 14px;
        font-weight: 400;
        margin-right:5px;
    }
    a.btn-campaign, .btn-campaign {
        background: #ff56da;
        border: 2px solid #ff56da;
        color: #fff;
    }
    a.btn-campaign:hover{
        background: #220087;
        border: 2px solid #220087;
        color: #fff;
        outline: none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
    }
</style>
<script type="text/javascript">
    $(function(){
        $('.generaltable').dataTable({
            "order": [[ 1, "asc" ]],
            "columnDefs" : [
                {
                "render": function ( data, type, row ) {
                    const d = new Date( parseInt(row[1])*1000 );
                    var date = getDateFormat(d);
                    return '<span style="display:none;">'+(parseInt(row[1]))+'</span>'+ date;
                },
                "targets": 1
            },
            { "visible": true,  "targets": [ 1 ] }
            ],
        });
    })
    function getDateFormat(dt){
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        var hours = dt.getHours();
      var minutes = dt.getMinutes();
      var sec = dt.getSeconds();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      sec = sec < 10 ? '0'+sec : sec;
      var strTime = hours + ':' + minutes + ':'+sec+' ' + ampm;

        return month[dt.getMonth()] +' '+dt.getDate()+(dt.getDate() % 10 == 1 && dt.getDate() != 11 ? 'st' : (dt.getDate() % 10 == 2 && dt.getDate() != 12 ? 'nd' : (dt.getDate() % 10 == 3 && dt.getDate() != 13 ? 'rd' : 'th')))+ ' '+dt.getFullYear()+', '+strTime;
    }
</script>
