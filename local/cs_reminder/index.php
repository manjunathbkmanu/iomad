<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_cs_reminder
 * @copyright  Manjunath B K<manjunathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/select_course.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
global $DB, $CFG, $USER,$PAGE,$SESSION;  
require_login(true);

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/cs_reminder/index.php');
$title = get_string('pluginame', 'local_cs_reminder');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
$mform = new local_course_form();
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl='javascript:void(0)';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback


$html .='<div class="container-fluid pt-2 mb-3 formcontainer">
<div class="row"><div class="col-md-12">';
$html .= '<h3>'.get_string('sendreminders', 'local_course_management').'</h3>'; 
$html .='<hr>';
$html .= $mform->render(); 
$html .='</div></div></div>';
echo $html;

if ($mform->is_cancelled()) {
   
} else if ($datafromform = $mform->get_data() || $_POST['courseid'] || $_POST['departmentId']) {
	$courseid = $_POST['courseid'];
	$departmentId = $_POST['departmentId'];
	$selecteddepartment = get_department($departmentId);
	$datafromform->courseid = $courseid;
	$coursename = $DB->get_field('course', 'fullname',array('id'=>$courseid));
	$coursename_as = $coursename;
	$departmentId = $_POST['departmentId'];
	// $enrolledusers = get_enrolled_users_in_course($courseid,$departmentId);
	/** Created by Dilip */
	// In order to filter with department ,pass department
	if($courseid != ""){
		
		$enrolledusers = get_users_enrolled_in_courses($courseid);
		$coursestats = get_course_statistics($enrolledusers, $courseid, $selecteddepartment);
		if(current_language() == 'ar'){
			$coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));

			$coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$courseid));
			$coursename = $coursenamearabic;
			if(empty($coursename)){
				$coursename = $coursename_as;
			}

		}
	}
	// echo '<pre>';print_r($departmentId);
	// // die;
	$html ='';
	$html .= html_writer::start_div('container-fluid');
	$html .= html_writer::start_div('card');
	$html .= html_writer::start_div('card-header');
	$html .= html_writer::start_div('row');
	$html .= html_writer::start_div('col-md-12');
	$html .= get_string('course', 'local_cs_reminder').': '.$coursename;
	$html .= html_writer::start_tag('a',array('class'=>'btn btn-primary text-white float-right','href'=>"".$CFG->wwwroot.'/local/cs_reminder/sentmail.php?id='.$courseid.""));
	$html .= get_string('previousmailsent', 'local_cs_reminder');
	$html .= html_writer::end_tag('a');
	$html .= html_writer::end_div();//col-12 end
	$html .= html_writer::end_div();//row ends
	$html .= html_writer::end_div();//card header ends
	$html .= html_writer::start_tag('form', array('action'=>'send_mail.php','method'=>'GET',));
	$html .= html_writer::start_div('card-body');
	$html .= html_writer::start_div('row');
	$html .= html_writer::start_div('col-md-4');
	$html .= html_writer::start_tag('label');
	//hidden input field for course id
	$html .= html_writer::start_tag('input',array('type'=>'hidden','value'=>$courseid,'name'=>'courseid'));
	$html .= html_writer::start_tag('input',array('type'=>'hidden','value'=>json_encode($selecteddepartment),'name'=>'department[]'));
	$html .= html_writer::start_tag('input',array('type'=>'radio','value'=>'1','name'=>'status', 'data-user' => $coursestats['1']));
	$html .= ' '.get_string('inprogress', 'local_cs_reminder').': '.$coursestats['1'];
	$html .= html_writer::end_tag('label');
	$html .= html_writer::end_div();
	$html .= html_writer::start_div('col-md-4');
	$html .= html_writer::start_tag('label');
	$html .= html_writer::start_tag('input',array('type'=>'radio','value'=>'2','name'=>'status', 'data-user' => $coursestats['2']));
	$html .= ' '.get_string('notstarted', 'local_cs_reminder').': '.$coursestats['2'];
	$html .= html_writer::end_tag('label');
	$html .= html_writer::end_div();
	$html .= html_writer::start_div('col-md-4');
	$html .= html_writer::start_tag('button',array('class'=>'btn btn-primary float-right send-mail'));
	$html .= get_string('sendmail', 'local_cs_reminder');
	$html .= html_writer::end_tag('button');
	$html .= html_writer::end_div();
	$html .= html_writer::end_div();//row ends
	$html .= html_writer::end_div();
	$html .= html_writer::end_tag('form');
	$html .= html_writer::end_div();//card end
	$html .= html_writer::end_div();
	echo $html;
}


echo $OUTPUT->footer();

?>
<link rel="stylesheet" href="./css/bootstrap-multiselect.css" />
<style>
button.multiselect.dropdown-toggle.btn.btn-default {
    color: #e2e2e2;
    background-color: white;
    border: 1px solid #e2e2e2;
}

.form-inline .multiselect-container label.checkbox, .form-inline .multiselect-container label.radio{
	justify-content:inherit !important;
}
.multiselect-container>li>a>label {
    padding: 3px 20px 3px 40px!important;
}

.mform .form-inline .form-control, .mform .form-inline .custom-select{
	width: 302px;
}

.btn-group, .btn-group-vertical{
	width: 302px;
}

span.multiselect-selected-text{
	text-align: left;
}

.col-md-3{
	display:flex;
}

.col-form-label{
	margin-top: auto;
    padding: 9px 0;
}

button.multiselect.dropdown-toggle.btn.btn-default{
    color: #4f5256;
	font-size:14px;
	width:230px !important;
    font-weight:normal;
	display:flex;
	justify-content:space-between;
}

button.multiselect.dropdown-toggle.dropdown-toggle::after{
	margin-left: 26px;
    border-right: .3em solid transparent !important;
    margin-bottom: 12px;
    position: absolute;
    right: <?php if(current_language() == 'ar') { ?> 20px <?php }else{ ?> 25px <?php }?>;
    top: 15px;

}

.form-autocomplete-downarrow{
	color: #e2e2e2;
    top: .5rem;
    right: .5rem;
    cursor: pointer;
    font-size: 10px;
}

select#course_selection_auto{
    color: #4f5256;
    background-image: linear-gradient(45deg, transparent 50%, #4f5256 60%), linear-gradient(135deg, #4f5256 40%, transparent 50%) !important;
    background-position: calc(100% - 30px) 14px, calc(100% - 20px) 14px, 100% 0;
    background-size: 4px 4px, 10px 4px;
    background-repeat: no-repeat;
    -webkit-appearance: none;
    -moz-appearance: none;
    font-size: 14px;
}

.caret{ color: red; }

select#course_selection_auto option{
    color: #000000;
}


.form-inline .multiselect-container li a label.checkbox input[type=checkbox], .form-inline .multiselect-container li a label.radio input[type=radio]{
margin-left: 10px !important;
}
.custom-select{
    overflow: hidden !important;
    text-overflow: ellipsis;
    white-space: nowrap;
    padding-right: 40px;
}

.send-mail{
	display: none;
}

</style>
<script type="text/javascript" src="./js/bootstrap-multiselect.min.js"></script>
<script>
	<?php 

	$config1 = get_config('coursemanagement');
	$departmentsarray = $config->userdepartment;
	$exploadedarray = explode(",", $departmentsarray);
	$allcategoryarray = [];
	foreach ($exploadedarray as $value)
	{
	    if($value != ""){
	        $allcategoryarray[$value] = $value;
	    }
	}
	$checkenrollments = check_availability_enrolled_user_courses();
	if($checkenrollments > 0){
	    $allcategoryarray["N/A"] = "N/A";
	}
?>
var countDepatment = "<?php echo count($allcategoryarray); ?>";
$(document).ready(function (){
	var courseid = "<?=$_POST['courseid']?>";		
	$('#course_selection_auto option[value="'+courseid+'"]').prop('selected', true);



	$('#department_selection_cs_reminder').multiselect({
      allSelectedText: 'All',
      maxHeight: 200,
	  enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
	  selectAllText:'<?php echo get_string('alldepartments', 'local_compliance_dashboard'); ?>',
	  nonSelectedText:'<?php echo get_string('selectdepartment', 'local_compliance_dashboard'); ?>',
      includeSelectAllOption: true,
      filterPlaceholder: '<?php echo get_string('searchinput', 'local_compliance_dashboard'); ?>'
    })
	.multiselect('updateButtonText');
	if("<?php echo is_array(json_decode(json_encode($selecteddepartment))); ?>"){
		$("#department_selection_cs_reminder").val(<?php echo json_encode($selecteddepartment); ?>);
		$('#department_selection_cs_reminder').multiselect('selectAll', true)
	}
else{
		$('#department_selection_cs_reminder').multiselect('selectAll', false)
	
	}
	$("#department_selection_cs_reminder").multiselect("refresh");
	$('button.btn.btn-default.multiselect-clear-filter').remove();	
				
	var departments = $('#department_selection_cs_reminder').val();
	selectAllDepartments(departments);
});


function selectAllDepartments(departments){
    if(departments.length == 1 && countDepatment == 1){
        if(departments.includes("N/A")){
            $('button.multiselect').find('span').html('<?php echo get_string('alldepartments', 'local_compliance_dashboard'); ?>');
        }
    }
}

$('.go-back-btn').click(function(e){
    e.preventDefault();
    window.history.back();
});

$('input[name="status"]').change(function(e){
	e.preventDefault();
	var user = $(this).attr('data-user');
	if(parseInt(user) > 0){
		$('.send-mail').show();
	}else{
		$('.send-mail').hide();
	}
})
</script>