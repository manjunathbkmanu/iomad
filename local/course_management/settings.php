<?php

defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {

    $moderator = get_admin();
    $site = get_site();

    $settings = new admin_settingpage('coursemanagement', get_string('coursemanagementsettings','local_course_management'));
    $ADMIN->add('localplugins', $settings);

    $name = 'coursemanagement/userdepartment';
    $title = get_string('userdepartment', 'local_course_management');
    $description = get_string('userdepartmentdescription', 'local_course_management');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);    
    $settings->add($setting);

   } 