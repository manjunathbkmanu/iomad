<?php


defined('MOODLE_INTERNAL') || die();
include_once('licensecheck.php');
$bodyattributes = $OUTPUT->body_attributes([]);

$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'bodyattributes' => $bodyattributes,
    'gridicon' => $OUTPUT->image_url('grid-icon-inverse', 'theme'),
];

$PAGE->requires->jquery ();
$PAGE->requires->js('/theme/maker/plugins/back-to-top.js');
$PAGE->requires->js('/theme/maker/plugins/drop-hover.js');

echo $OUTPUT->render_from_template('theme_maker/columns1', $templatecontext);

