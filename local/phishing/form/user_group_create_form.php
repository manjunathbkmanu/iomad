<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phishing
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
require_once($CFG->libdir.'/formslib.php');
require_once('../lib.php');
class user_group_create_form extends moodleform {
	function definition() {
		global $CFG,$USER;
		$id = optional_param('id',null, PARAM_INT);
		$edit = optional_param('edit',null, PARAM_INT);
		$mform = $this->_form; 
		//orgaziation name field
		$context = context_system::instance();
		if($edit==1){
			$mform->addElement('static','usergrouphrd',get_string('editusergroup','local_phishing'),'');
		}else{
			$mform->addElement('static','usergrouphrd',get_string('usergroup','local_phishing'),'');
		}
		$mform->addElement('html', '<hr>');
		// Add some extra hidden fields.
		$mform->addElement('hidden', 'id',$id);
        //$mform->setType('id', core_user::get_property_type('id'));
		$mform->setType('id', PARAM_INT);

		$mform->addElement('hidden', 'edit',$edit);
        //$mform->setType('status', core_user::get_property_type('status'));
		$mform->setType('edit', PARAM_INT);
		
		//name of the group
		$mform->addElement('text', 'name',
         get_string('temp_name','local_phishing')); // Add elements to your form
		$mform->addRule('name', get_string('required'), 'required', null, 'client');
		// $mform->addElement('static','name1','',get_string('name_help','local_phishing'),'');
		$mform->setType('name', PARAM_TEXT);
		
		//add bulk user csv form here
		
		
		//user information heading 
		if($edit==1){
			$mform->addElement('static','userinfohrd',get_string('editusergroup','local_phishing'),'');
		}else{
			$mform->addElement('static','userinfohrd',get_string('adduser','local_phishing'),'');
		}
		$mform->addElement('html', '<hr>');
		//add moodle users 
		$users = get_moodle_users();
		$select = $mform->addElement('searchableselector', 'targets',
			get_string('targets','local_phishing'),$users);
		$select->setMultiple(true);
		
		
		// $mform->addElement('static','useremailhrd',get_string('note','local_phishing'),get_string('textnote','local_phishing'));
		$mform->addElement('html', '<hr>');
		//add bulk user here
		$mform->addElement('textarea', 'emailarea',
         get_string('emailtext','local_phishing'),'wrap="virtual" rows="10" cols="50"'); // Add elements to your form
		$mform->setType('emailarea', PARAM_RAW);	
		

         $this->add_action_buttons();
     }
 }
