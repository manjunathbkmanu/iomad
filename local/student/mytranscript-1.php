<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php'); 
require_once('user_ajax.php'); 
// require_once($CFG->dirroot.'/local/student/jslinks.php');
global $DB, $CFG, $USER,$PAGE;

require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/student/mytranscript.php');
$title = get_string('transcript', 'local_student');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading('');
$PAGE->requires->jquery();
echo $OUTPUT->header(); 
echo '<h3>'.$title .' </h3>';  
$courses = enrol_get_my_courses();
    $dateformat = '%d-%b-%Y';
	$duration = ''; 
	$passfail = ' ';
    //print_object($courses);
	//'lcourse'
    $table = new html_table();
    //$table->head = (array) get_strings(array( 'cname','enrolleddate','enddate','duration','vgrade','status'), 'block_enrolled_courses');
    $table->head = (array) get_strings(array( 'cname','enddate','duration','vgrade','passfail','status'), 'local_student');
    if(!empty($courses)){
        foreach($courses as $course) {
			
			$cminfo = get_fast_modinfo($course, $USER->id);
                foreach($cminfo->get_cms() as $cm) {
					if($cm->modname == 'scorm')
					 {
						 $scormlink = get_scorm_status_ebdaa_transcript($cm->instance,$course);
						 $passfail = get_scorm_status_ebdaa_passfail($cm->instance,$course);
					 }
				}
            $instance1 = $DB->get_record('enrol', array('courseid' => $course->id, 'enrol' => 'manual'), '*', MUST_EXIST);
            $enrolid = $DB->get_record('user_enrolments',array('enrolid'=>$instance1->id,'userid'=>$USER->id));
            if(!empty($enrolid)){
                $enroldate = userdate($enrolid->timecreated,$dateformat);
            }else{
                $enroldate = '-';
			}
			
            $completecdetails = $DB->get_record('course', array('id'=>$course->id));
			$enddate = $completecdetails->enddate;
			
			if(!empty($enddate)){
                $enddate = userdate($enddate,$dateformat);
            }else{
                $enddate = '-';
            }
			
			//now get duration from custom field table
			$fieldidsql = $DB->get_record('customfield_field', array('shortname'=>'courseduration'));
			if(!empty($fieldidsql))
			{
				$fieldid = $fieldidsql->id;
				//get value
				$fielddurationsql = $DB->get_record('customfield_data', array('fieldid'=>$fieldid, 'instanceid' =>$course->id));
				if(!empty($fielddurationsql))
				{
					$duration = $fielddurationsql->value;
				}
			}
			
            $grades = grade_get_course_grades($course->id, $USER->id);
            $grademax = $grades->grademax;

            $gradeValue =(((int) $grades->grades[$USER->id]->str_grade));
            if(!$gradeValue==null){
                $gradeRoundvalue = (($gradeValue * 100)/$grademax);
            }else{
                $gradeRoundvalue='-';
            }
            //print_object($gradeRoundvalue);
			$linkcourse = html_writer::link(
                    new moodle_url(
                        $CFG->wwwroot.'/course/view.php?id='.$course->id,array()
                        ),'<i style="font-size:24px" class="fa">&#xf090;</i>',array('class'=>'glyphicon glyphicon-log-in')
                    );
            $table->data[] = array(
                $course->fullname,
               // $enroldate,
				$enddate,
				$duration,
                $gradeRoundvalue,
				$passfail,
                $scormlink

                );
        }
    }
    $tabeled = html_writer::table($table);
	
		/**
 *
 * @param type $scormid
 * @param type $userid
 */
function get_scorm_status_ebdaa_transcript($scormid,$course) {
    global $DB, $USER,$CFG;
    $scormlink = '-';
	$userid = $USER->id;
    $sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
    $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
    if (!empty($lastattempt)) {
        $sql = "SELECT value
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
        $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

		if ($status == 'complete' OR $status == 'passed' OR $status == 'failed'){
			$scormstatus = strtoupper('complete');
			$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
                    <button class="btn btn-sm bg-green w-125" style="cursor:pointer;">'.$scormstatus.'</button>
                </a>';
				
		} else if ($status == 'incomplete') {
			$scormstatus = strtoupper('in progress');
			$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
                    <button class="btn btn-sm bg-yellow w-125" style="cursor:pointer;">'.$scormstatus.'</button>
                </a>';
		}
    } else {
		$scormstatus = strtoupper('not started');
			$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
                    <button class="btn btn-sm bg-red w-125" style="cursor:pointer;">'.$scormstatus.'</button>
                </a>';
	}
    return $scormlink;
}

		/**
 *
 * @param type $scormid
 * @param type $userid
 */
function get_scorm_status_ebdaa_passfail($scormid,$course) {
    global $DB, $USER,$CFG;
    $scormstatus = '-';
	$userid = $USER->id;
    $sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
    $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
    if (!empty($lastattempt)) {
        $sql = "SELECT value
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
        $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

		if ($status == 'complete' OR $status == 'passed' OR $status == 'failed'){
			if($status == 'passed') { $stringshow = 'pass';}
			if($status == 'failed') { $stringshow = 'fail';}
			if($status == 'complete') { $stringshow = 'pass';}
			$scormstatus = strtoupper($stringshow);
				
		} else if ($status == 'incomplete') {
			$scormstatus = '-'; 
		}
    } else {
		$scormstatus = '-';
	}
    return $scormstatus;
}

	
echo $tabeled;
echo $OUTPUT->footer();