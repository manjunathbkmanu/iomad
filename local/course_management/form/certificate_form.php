<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * You may have settings in your plugin
 *
 * @package    local_course_management
 * @copyright  Manjunath  B K<manjunathbk@edzlearn10.com>
 * @license    http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
global $OUTPUT, $CFG;
require_once($CFG->libdir.'/formslib.php');

require_login();
class certificate_setting extends moodleform {
    function definition() {
    	global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        // IOMAD
        require_once($CFG->dirroot . '/local/iomad/lib/company.php');
        $companyid = iomad::get_my_companyid(context_system::instance(), false);
        $id = $USER->id;
        $mform =& $this->_form;
        //header
        $mform->addElement('header','certificatesetting',get_string('certificatesetting','local_course_management'));
        //Hidden Company id.
        $mform->addElement('hidden', 'companyid', $companyid);
        //image for category.
        $mform->addElement('filepicker', 'arabicback', get_string('backgroundarabic', 'local_course_management'), null,
            array('subdirs' => 0, 'areamaxbytes' => 10485760, 'maxfiles' => 1));
        //image for category.
        $mform->addElement('filepicker', 'englishback', get_string('backgroundenglish', 'local_course_management'), null,
            array('subdirs' => 0, 'areamaxbytes' => 10485760, 'maxfiles' => 1));
        $mform->addElement('submit', 'submitbutton', get_string('submit', 'local_course_management'));
    }
}