<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_ebdaa_license
 * @copyright  Manjunath B K<manjunath@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('popup');
$PAGE->set_url($CFG->wwwroot . '/local/local_ebdaa_license/redirect.php');

$licenindex = $CFG->wwwroot . '/local/local_ebdaa_license/index.php';
echo $OUTPUT->header();
$imageurl = $CFG->wwwroot.'/local/ebdaa_license/pix/Ebdaa-Logo.png';
$html ='';
$html .= html_writer::start_div('container');
$html .= html_writer::start_div('row');
$html .= html_writer::start_div('col-md-2');
$html .= html_writer::end_div();
$html .= html_writer::start_div('col-md-8 border text-center redirect-message');
$html .= '<img width="40%" src="'.$imageurl.'">';
$html .= '<h1 class="p-3">'.get_string('invalidmessage', 'local_ebdaa_license').'</h1>';
if (is_siteadmin()) {
$html .= '<a href="'.$licenindex.'" class=" btn btn-primary p-3">'.get_string('updatelicensehere', 'local_ebdaa_license').'</a>';
}
$html .= html_writer::end_div();
$html .= html_writer::start_div('col-md-2');
$html .= html_writer::end_div();
$html .= html_writer::end_div();
$html .= html_writer::end_div();
echo $html;


