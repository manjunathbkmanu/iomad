<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phishing
 * @copyright  Manjunath B K
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}

require_once($CFG->libdir.'/formslib.php');
class phishing_settings extends moodleform {
    function definition() {
        $mform = $this->_form;
        //API key
        $mform->addElement('text', 'api',get_string('api','local_phishing'));
        $mform->addRule('api', get_string('required'), 'required', null, 'client');

        //Username
        $mform->addElement('text', 'username',get_string('username','local_phishing'));
        $mform->addRule('username', get_string('required'), 'required', null, 'client');

        //IP
        $mform->addElement('text', 'ip',get_string('urlip','local_phishing'));
        $mform->addRule('ip', get_string('required'), 'required', null, 'client');

        //Landing Page URL
        $mform->addElement('text', 'landingpageurl',get_string('landingpageurl','local_phishing'));
        $mform->addRule('landingpageurl', get_string('required'), 'required', null, 'client');

        //Protocol
        $choices = array(
            'http://' =>get_string('prohttp','local_phishing'),
            'https://' =>get_string('prohttps','local_phishing')
        );
        $mform->addElement('select', 'protocol', get_string('url', 'local_phishing'), $choices);

        $this->add_action_buttons();


    }
}