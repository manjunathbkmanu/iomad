<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_accesscohort
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../../config.php');
require_once('../form/template_copy_form.php');
require_once($CFG->libdir . '/formslib.php');
global $DB,$USER;
require_login(0,false);
//edit params
$companyid = $USER->company->id;
$eid = optional_param('id',null, PARAM_INT);
//api key initialization
//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$context = context_system::instance();
$contextid = $context->contextlevel;
$overall = has_capability('local/phishing:overall',$context);
//$createtemp = has_capability('local/phishing:createtemp',$context);
//$createorgcap = has_capability('local/accesscohort:addorganization',$context);
//curl initialization
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/phishing/js/import.js'), true);
$curl_handle = curl_init();
$PAGE->set_context(context_system::instance());
$title = get_string('templatehrd', 'local_phishing');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/action/template_copy.php?id='.$eid);
require_login();
$previewnode = $PAGE->navigation->add(get_string('pluginname','local_phishing'), new moodle_url($CFG->wwwroot.'/local/phishing/templatelist.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title, new moodle_url($CFG->wwwroot.'/local/phishing/action/template_copy.php'));
$thingnode->make_active();
$mform = new template_copy_form();
$temp = false;
if ($mform->is_cancelled()){
	redirect(new moodle_url('/local/phishing/templatelist.php', array()));
} else if ($data = $mform->get_data()) {	
	//save file
	file_save_draft_area_files($data->attachments,$contextid,'local_phishing','phishattachments',$data->attachments,array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 50));
	$sql = "SELECT * FROM {files}
	where component='local_phishing' AND itemid = $data->attachments AND filename IS NOT NULL AND mimetype IS NOT NULL ";
	$sql_val = $DB->get_records_sql($sql);
	//print_object($sql_val);
	$array=[];	
	if(!empty($sql_val)){
		$i=1;
		foreach ($sql_val as $key => $value) {	
			$fs = get_file_storage();		
			$file = $fs->get_file($value->contextid, $value->component, $value->filearea,
				$value->itemid, $value->filepath, $value->filename);	
			if($file){
				$contents = base64_encode($file->get_content());
				$array[] = array('id'=>$i,'name'=>$value->filename,'content'=>$contents,'type'=>$value->mimetype);
			}			
			$i++;
		}		
	}
	$url = $protocol.$urlip.'/api/templates/?api_key='.$apikey;
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// Set the curl URL option
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl_handle, CURLOPT_URL, $url);
	// This option will return data as a string instead of direct output
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	// Execute curl & store data in a variable
	$curl_data = curl_exec($curl_handle);
	if (!curl_exec($curl_handle)) {
		die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
	}
	$template_datas = json_decode($curl_data);
	$id =0;
	if(!empty($template_datas)){
		foreach ($template_datas as $key => $template_data) {
			$id = $template_data->id;
		}
	}
	$dataarray = array('id'=>$id,'name'=>$data->name,'subject'=>$data->subject,'html'=>$data->editorvalue,'attachments'=>$array);
	$insert = new stdClass();
	$insert->file_itemid = $data->attachments;
	$insert->api_id = $id;
	$insert->api_type = 'template';
	$insert->timecreated =time();
	$insert->timemodified=time();
	$DB->insert_record('local_phishing_files',$insert);	
	$data_json = json_encode($dataarray);
	if(!empty($data_json)){		
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($curl_handle);
		$msg = get_string('temppostmsg','local_phishing');
			//
			//display message code 
		if($response) {
			redirect(new moodle_url($CFG->wwwroot.'/local/phishing/templatelist.php'),$msg);
		} else {
			echo "error";
		}			
		if (!curl_exec($curl_handle)) {
			die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
		}
	}
	//insert files here 
	curl_close($curl_handle);
}else{
	$url = $protocol.$urlip.'/api/templates/'.$eid.'?api_key='.$apikey;
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		// Set the curl URL option
			//curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl_handle, CURLOPT_URL, $url);
		// This option will return data as a string instead of direct output
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		// Execute curl & store data in a variable
	$curl_data = json_decode(curl_exec($curl_handle));

}
echo $OUTPUT->header();
if($overall){
	$mform->display();
}else{
	echo html_writer::div(
		get_string('cap', 'local_phishing'),'alert alert-danger'
	);
}

echo $OUTPUT->footer();
?>