<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
* Handles uploading files
*
* @package    local_course_report
* @copyright  Manjunath B K<manjunathbk@elearn10.com>
* @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
* @license    http://www.lmsofindia.com 2017 or later
*/
require_once('../../config.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
require_once($CFG->dirroot.'/local/course_report/csslinks.php');
global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_report/team_report.php');
$title = get_string('userreport', 'local_course_report');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();		
include_once('jslink.php');	
echo $OUTPUT->header();
$user = $USER;
$useremail = $DB->get_field('user', 'email', array('id'=>$user->id));
$table  = new \html_table();
$table->id = 'usertable';
$table->head = array(get_string('fullname', 'local_course_report'),
	get_string('email', 'local_course_report'),
	get_string('enrolled', 'local_course_report'),
	get_string('complete', 'local_course_report'),
	get_string('inprogress', 'local_course_report'),
	get_string('notstarted', 'local_course_report'),
	get_string('completioninpercentage', 'local_course_report'),
	get_string('action', 'local_course_report')
);
$getallusersid = $DB->get_records('user',array('deleted'=>0,'suspended'=>0,'alternatename'=>$useremail));
//counts for pie chart.

if(!empty($getallusersid)){
	$enrollcount=0;
	$completedcount=0;
	$inprogressccount=0;
	$notstartedcount=0;
	$teamcount=0;
	$completionperc=0;
	foreach ($getallusersid as $values) {
		if(!empty($values->id) && $values->id != 1)
		{
			$alluserdetails = track_course_status($values->id);
			$enrolledcourses=enrol_get_all_users_courses($values->id);
			$teamcount++;
			$enrollcount=$enrollcount+count($enrolledcourses);
			$completionperc=$completionperc+$alluserdetails['completionpercentage'];

			$link = new moodle_url($CFG->wwwroot.'/local/course_report/user_details.php?id='.$alluserdetails['uid']);
			$remlink = new moodle_url($CFG->wwwroot.'/local/cs_reminder/send_mail.php?userid='.$alluserdetails['uid']);
			$multienroll = new moodle_url($CFG->wwwroot.'/blocks/enrollment/enrollment.php?userid='.$user->id);
			$username = '<a href="'.$link.'">'.$alluserdetails['username'].'</a>';
			if(($alluserdetails['inprogress'] + $alluserdetails['notcompleted']) > 0){
				$coursereminder='<a class="mr-5" href="'.$remlink.'"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>';
				$courseenroll='<a  href="'.$multienroll.'"><i class="fa fa-user-plus" aria-hidden="true"></i></a>';
			}else{
				$coursereminder='-';
				$courseenroll='<a class="ml-5"  href="'.$multienroll.'"><i class="fa fa-user-plus" aria-hidden="true"></i></a>';
			}
			$completedcount=$completedcount+$alluserdetails['completed'];
			$inprogressccount=$inprogressccount+$alluserdetails['inprogress'];
			$notstartedcount=$notstartedcount+$alluserdetails['notcompleted'];

			$table->data[] = array($username,$alluserdetails['email'],count($enrolledcourses), $alluserdetails['completed'], $alluserdetails['inprogress'],$alluserdetails['notcompleted'],$alluserdetails['completionpercentage'],$coursereminder.$courseenroll);
		}
	}
	$chartarray=array(get_string('complete', 'local_course_report')=>$completedcount,
		get_string('inprogress', 'local_course_report')=>$inprogressccount,
		get_string('notstarted', 'local_course_report')=>$notstartedcount);
//chart and stats cards section.
	$data = html_writer::start_div('container-fluid');
	$data .= html_writer::start_div('row pb-3');

	$data .= html_writer::start_div('col-md-6 col-sm-12 col-xs-12 py-2');
	$data .= html_writer::start_div('card h-100 border border-secondary');
	$data .= html_writer::start_div('card-header text-center');
	$data .= get_string('teamstatus', 'local_course_report');
	$data .= html_writer::end_div();//card header end
	$data .= html_writer::start_div('card-body');
	$data .= html_writer::start_tag('canvas', array('id'=>'myChart'));
	$data .= html_writer::end_tag('canvas');
	$data .= html_writer::end_div();//card body end
	$data .= html_writer::end_div();//card end
	$data .= html_writer::end_div();	
	$data .= html_writer::start_div('col-md-6 col-sm-12 col-xs-12 py-2');
	$data .= html_writer::start_div('card h-100 border border-secondary');
	$data .= html_writer::start_div('card-header text-center');
	$data .= get_string('teamreport', 'local_course_report');
	$data .= html_writer::end_div();//card header end
	$data .= html_writer::start_div('card-body text-center p-5');

	$data .='<h3>'.get_string('enrolcount','local_course_report').$enrollcount.'</h3>';
	$data .='<h3>'.get_string('teamsize','local_course_report').$teamcount.'</h3>';
	$data .='<h3>'.get_string('completecount','local_course_report').$completedcount.'</h3>';
	$data .='<h3>'.get_string('inprogresccount','local_course_report').$inprogressccount.'</h3>';
	$data .='<h3>'.get_string('notstartedcount','local_course_report').$notstartedcount.'</h3>';
	$percount=$completionperc/$teamcount;
	$data .='<h3>'.get_string('completionperc','local_course_report').$percount.'</h3>';

	$data .= html_writer::end_div();//card body end
	$data .= html_writer::end_div();//card end
	$data .= html_writer::end_div();
	$data .= html_writer::end_div();
	$data .= html_writer::start_div('container-fluid');
	$data .= html_writer::start_div('row pb-3');
	$data .= html_writer::start_div('col-md-12');
	$data .= html_writer::table($table);
	$data .= html_writer::end_div();
	$data .= html_writer::end_div();

	$data .= html_writer::end_div();
	$data .= html_writer::end_div();
	echo $data;
}else{
	echo "No Users found";
}
echo $OUTPUT->footer();
?>
<script>
	var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'pie',
		data: {
			labels: [<?php if(!empty($chartarray)){
				$i=0;
				foreach($chartarray as$k => $c){
					echo "'" .$k."',";
					$i++;
				}
			}?>],
			datasets: [{
				backgroundColor: [
				"#23712f",
				"#f69e3e",
				"#e74b3c"
				],
				data: [<?php if(!empty($chartarray)){
					$i=0;
					foreach($chartarray as$k => $c){
						echo "'" .$c."',";
						$i++;
					}
				}?>]
			}]
		}
	});
</script>

