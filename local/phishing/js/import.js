function importEmail() {
	var raw = $("#id_email_content").val();
	var checkBox = document.getElementById("id_convert_links_checkbox");
	var x = false;
	if (checkBox.checked == true){
		x = true;
	} 
	var sending = JSON.stringify({ content: raw,convert_links:x});
	//Manju: Made api key and phishing url dynamic. 08/02/2021.
	var apikey = $("#phishingapikey").val();
	var phishingurl = $("#ipaddress").val();
	var protocol = $("#protocol").val();
	$.ajax({
		url: protocol+phishingurl+"/api/import/email?api_key="+apikey,
		method: "POST",
		data: sending,
		dataType: "json",
		contentType: "application/json",
		success: function( data ) {
			document.getElementById("id_htmleditable").innerHTML = data.html;
			document.getElementById("editorvalueid").value = data.html;
		},
	});
}

function importSite() {
	var raw = $("#id_url").val();
	var sending = JSON.stringify({ url: raw,include_resources:false});
	//Manju: Made api key and phishing url dynamic. 08/02/2021.
	var apikey = $("#phishingapikey").val();
	var phishingurl = $("#ipaddress").val();
	var protocol = $("#protocol").val();
	$.ajax({
		url: protocol+phishingurl+"/api/import/site?api_key="+apikey,
		method: "POST",
		data: sending,
		dataType: "json",
		contentType: "application/json",
		success: function( data ) {
			document.getElementById("id_htmleditable").innerHTML = data.html;
			document.getElementById("editorvalueid").value = data.html;
		},
	});
}
//check box for landing page 
function clickMe(){
	if ($('input[name=capture_credentials]:checked').length > 0) {
		document.getElementById("qheader").style.display = 'block';
	}
	if ($('input[name=capture_credentials]:checked').length == 0) {
		document.getElementById("qheader").style.display = 'none';
	}	
}