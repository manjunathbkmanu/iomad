<?php
	

defined('MOODLE_INTERNAL') || die();
include_once('licensecheck.php');
user_preference_allow_ajax_update('drawer-open-nav', PARAM_ALPHA);
require_once($CFG->libdir . '/behat/lib.php');


if ( isloggedin()&&(!isguestuser()) ) {
	
    //$navdraweropen = (get_user_preferences('drawer-open-nav', 'true') == 'true');
    $navdraweropen = false;
    
    
    $extraclasses = [];
	if ($navdraweropen) {
	    $extraclasses[] = 'drawer-open-left';
	}
	$bodyattributes = $OUTPUT->body_attributes($extraclasses);
	
	$blockshtml = $OUTPUT->blocks('side-pre');
	$blockshtmlcenterpre =  $OUTPUT->blocks('center-pre');
	$blockshtmlcenterpost =  $OUTPUT->blocks('center-post');
	
	$hasblocks = strpos($blockshtml, 'data-block=') !== false;
	$hasblockscenterpre = strpos($blockshtmlcenterpre, 'data-block=') !== false;
	$hasblockscenterpost = strpos($blockshtmlcenterpost, 'data-block=') !== false;
	
	$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();
	$templatecontext = [
	    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
	    'output' => $OUTPUT,
	    
	    'sidepreblocks' => $blockshtml,
		'centerpreblocks' => $blockshtmlcenterpre,
		'centerpostblocks' => $blockshtmlcenterpost,
		
		'hasblocks' => $hasblocks,
		'hasblockscenterpre' => $hasblockscenterpre,
		'hasblockscenterpost' => $hasblockscenterpost,
	    
	    
	    'bodyattributes' => $bodyattributes,
	    'navdraweropen' => $navdraweropen,
	    'regionmainsettingsmenu' => $regionmainsettingsmenu,
	    'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu),
	    
	];
	
	$PAGE->requires->jquery ();

	// $PAGE->requires->js('/theme/maker/plugins/flexslider/jquery.flexslider-min.js',true); 
	// $PAGE->requires->js('/theme/maker/plugins/drop-hover.js',true);
	// $PAGE->requires->css('/theme/maker/plugins/animate.css');
	
	$PAGE->requires->js('/theme/maker/plugins/back-to-top.js');

	// $PAGE->requires->js('/theme/maker/plugins/owlcarousel/owl.carousel.min.js');
	
	
	//$PAGE->requires->js('/theme/maker/javascript/home.js');
	$templatecontext['flatnavigation'] = $PAGE->flatnav;
	echo $OUTPUT->render_from_template('theme_maker/frontpage', $templatecontext);

    
} else {
    $navdraweropen = false;
    
    $extraclasses = [];
	if ($navdraweropen) {
	    $extraclasses[] = 'drawer-open-left';
	}
	$bodyattributes = $OUTPUT->body_attributes($extraclasses);
	
	$blockshtml = $OUTPUT->blocks('side-pre');
	$blockshtmlcenterpre =  $OUTPUT->blocks('center-pre');
	$blockshtmlcenterpost =  $OUTPUT->blocks('center-post');
	
	$hasblocks = strpos($blockshtml, 'data-block=') !== false;
	$hasblockscenterpre = strpos($blockshtmlcenterpre, 'data-block=') !== false;
	$hasblockscenterpost = strpos($blockshtmlcenterpost, 'data-block=') !== false;
	
	$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();
	$templatecontext = [
	    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
	    'output' => $OUTPUT,

		'sidepreblocks' => $blockshtml,
		'centerpreblocks' => $blockshtmlcenterpre,
		'centerpostblocks' => $blockshtmlcenterpost,
		
		'hasblocks' => $hasblocks,
		'hasblockscenterpre' => $hasblockscenterpre,
		'hasblockscenterpost' => $hasblockscenterpost,
    
	    'bodyattributes' => $bodyattributes,
	    'navdraweropen' => $navdraweropen,
	    'regionmainsettingsmenu' => $regionmainsettingsmenu,
	    'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu),
	    
	    'gridicon' => $OUTPUT->image_url('grid-icon-inverse', 'theme'),
	    
	];
	
	$PAGE->requires->jquery ();
	// $PAGE->requires->js('/theme/maker/plugins/flexslider/jquery.flexslider-min.js');
	// $PAGE->requires->js('/theme/maker/plugins/owlcarousel/owl.carousel.min.js');
	
	// $PAGE->requires->js('/theme/maker/plugins/drop-hover.js');
	// $PAGE->requires->css('/theme/maker/plugins/animate.css');
	$PAGE->requires->js('/theme/maker/plugins/back-to-top.js');
	//$PAGE->requires->js('/theme/maker/javascript/home.js'); 
	

	$templatecontext['flatnavigation'] = $PAGE->flatnav;
	echo $OUTPUT->render_from_template('theme_maker/frontpage_guest', $templatecontext);
    
    
    
}


