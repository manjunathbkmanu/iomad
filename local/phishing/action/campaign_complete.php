<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_accesscohort
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../../config.php');
global $DB,$USER;
require_login(0,false);
$id = required_param('id', PARAM_INT);
$confirm = optional_param('confirm', 0, PARAM_BOOL);
$flag = 0;
//Manju: changing the plugin name.09/02/2021.
$companyid = $USER->company->id;
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$context = context_system::instance();
$overall = has_capability('local/phishing:overall',$context);
$campaign = has_capability('local/phishing:campaigns',$context);
$PAGE->set_context(context_system::instance());
$title = get_string('cocampaign', 'local_phishing');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/action/campaign_complete.php?id='.$id);
require_login();
$previewnode = $PAGE->navigation->add(get_string('pluginname','local_phishing'), new moodle_url($CFG->wwwroot.'/'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title, new moodle_url($CFG->wwwroot.'/local/phishing/action/campaign_complete.php?id='.$id));
$thingnode->make_active();
if($overall || $campaign){
	if($confirm){
		$curl_handle = curl_init();
		$url = $protocol.$urlip.'/api/campaigns/'.$id.'/complete?api_key='.$apikey;		
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		// Set the curl URL option
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		// This option will return data as a string instead of direct output
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		// Execute curl & store data in a variable
		$curl_data = curl_exec($curl_handle);
		$decodedResponse = json_decode($curl_data);
		if(!$decodedResponse->success){
			redirect(new moodle_url($CFG->wwwroot.'/local/phishing/campaignslist.php'),$decodedResponse->message, 0, 'error');
		}else{
			redirect(new moodle_url($CFG->wwwroot.'/local/phishing/action/campaignsinglelist.php?id='.$id),$decodedResponse->message, 0, 'success');
		}
	} 
	echo $OUTPUT->header();
	$returnurl = $CFG->wwwroot.'/local/phishing/campaignsumlist.php';
	$message = get_string('sure_complete','local_phishing');
	if($flag==1){
		echo html_writer::div(
			'Success','alert alert-success'
		);
		echo '<br>';	
		echo html_writer::link( new moodle_url($CFG->wwwroot.'/local/phishing/campaignsumlist.php'),get_string('list1','local_phishing'),array('class'=>'btn btn-small btn-primary')
	);
	}
	$formcontinue = new single_button(new moodle_url($CFG->wwwroot.'/local/phishing/action/campaign_complete.php', array('confirm' => 1,'id'=>$id,'action'=>'delete')), get_string('yes'));
	$formcancel = new single_button(new moodle_url('/local/phishing/action/campaignsinglelist.php?id='.$id), get_string('no'), 'get');

	if($flag!=1){
		echo $OUTPUT->confirm($message, $formcontinue, $formcancel);
	}
}else{
	echo html_writer::div(
		get_string('cap', 'local_phishing'),'alert alert-danger'
	);
}


echo $OUTPUT->footer();
?>
<style type="text/css">
	.path-local-phishing .grade-contents .btn-primary {
     	font-size: 16px !important; 
    	padding: 8px 20px !important;
	}
</style>






