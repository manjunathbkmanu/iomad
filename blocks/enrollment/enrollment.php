<?php
require_once("../../config.php");
require_once("lib.php");
require_once('form/form.php');
require_once($CFG->dirroot . '/enrol/manual/locallib.php');

$user = optional_param('users', null, PARAM_INT);
$role = optional_param('roles', null, PARAM_INT);
$courses = optional_param_array('courses', null, PARAM_INT);
$datestart = optional_param('datestart', null, PARAM_RAW);
$dateend = optional_param('dateend', null, PARAM_RAW);
$userid = optional_param('userid', null, PARAM_INT);
$context = context_system::instance();

$url = new moodle_url('/blocks/enrollment/enrollment.php');

require_login();

$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');
$PAGE->set_url($url);
$PAGE->set_title(get_string('pluginname', 'block_enrollment'));
$PAGE->set_heading(get_string('pluginname', 'block_enrollment'));

if (!empty($datestart)) {
    $datestart = strtotime($datestart);
}
if (!empty($dateend)) {
    $dateend = strtotime($dateend);
}
if (!empty($datestart) && !empty($dateend) && ($dateend < $datestart)) {
    print_error('La date de fin doit etre supérieure à la date de début', null, $CFG->wwwroot . '/blocks/enrollment/enrollment.php');
}

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');
$PAGE->requires->js("/blocks/enrollment/js/json.js");
$PAGE->requires->js("/blocks/enrollment/js/custom.js");
$PAGE->requires->js_init_call('M.block_enrollment.init');
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/blocks/enrollment/js/bootstrap-multiselect.js'),true);
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/blocks/enrollment/js/bootstrap-multiselect.css'), true);

$mform = new Enrollment_form();
js_reset_all_caches();
echo $OUTPUT->header();

enrollment_display_roles();

if (!empty($user) && !empty($courses)) {
    for ($i = 0; $i < count($courses); $i++) {
        $course = $DB->get_record('course', array('id' => $courses[$i]));
        enrollment_enrol_user($user, $courses[$i], $role, $datestart, $dateend);
    }
} ?>
<span id="success" class="text-success text-center"></span>
<span id="error" class="text-danger text-center"></span>
<?php


//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot.'/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback


$html .='<div class="container-fluid pt-2 mb-3 formcontainer">
<div class="row"><div class="col-md-12">';
$html .= '<h3>'.get_string('enrolmultiplecourse', 'local_course_management').'</h3>'; 
$html .='<hr>';
$html .= $mform->render(); 
$html .='</div></div></div>';
echo $html;
echo $OUTPUT->footer();
?>
<style>
    .input-group-btn{
        display: none;
    }
button.multiselect.dropdown-toggle.btn.btn-default {
    color: #e2e2e2;
    background-color: white;
    border: 1px solid #e2e2e2;
}

.form-inline .multiselect-container label.checkbox, .form-inline .multiselect-container label.radio{
    justify-content:inherit !important;
}
.multiselect-container>li>a>label {
    padding: 3px 20px 3px 40px!important;
}

.mform .form-inline .form-control, .mform .form-inline .custom-select{
    width: 302px;
}

.btn-group, .btn-group-vertical{
    width: 302px;
}

span.multiselect-selected-text{
    text-align: left;
}

.col-md-3{
    display:flex;
}

.col-form-label{
    margin-top: auto;
    padding: 9px 0;
}

button.multiselect.dropdown-toggle.btn.btn-default{
    color: #4f5256;
    font-size:14px;
    width:230px !important;
    font-weight:normal;
    display:flex;
    justify-content:space-between;
}

button.multiselect.dropdown-toggle.dropdown-toggle::after{
    margin-left: 26px;
    border-right: .3em solid transparent !important;
    margin-bottom: 12px;
    position: absolute;
    right: <?php if(current_language() == 'ar') { ?> 20px <?php }else{ ?> 25px <?php }?>;
    top: 15px;

}

.form-autocomplete-downarrow{
    color: #e2e2e2;
    top: .5rem;
    right: .5rem;
    cursor: pointer;
    font-size: 10px;
}

select#user_auto_selection{
    color: #4f5256;
    background-image: linear-gradient(45deg, transparent 50%, #4f5256 60%), linear-gradient(135deg, #4f5256 40%, transparent 50%) !important;
    background-position: calc(100% - 30px) 14px, calc(100% - 20px) 14px, 100% 0;
    background-size: 4px 4px, 10px 4px;
    background-repeat: no-repeat;
    -webkit-appearance: none;
    -moz-appearance: none;
    font-size: 14px;
}

.caret{ color: red; }

select#user_auto_selection option{
    color: #000000;
}


.form-inline .multiselect-container li a label.checkbox input[type=checkbox], .form-inline .multiselect-container li a label.radio input[type=radio]{
margin-left: 10px !important;
}
.custom-select{
    overflow: hidden !important;
    text-overflow: ellipsis;
    white-space: nowrap;
    padding-right: 40px;
}
</style>

