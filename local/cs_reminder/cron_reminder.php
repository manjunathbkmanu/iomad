 <?php
define('CLI_SCRIPT', true);

require(__DIR__.'/../../config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->libdir.'/cronlib.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
global $DB, $CFG, $USER;  

$allmusers=$DB->get_records('user',array('deleted'=>0));
foreach($allmusers as $uid){
	send_mail_to_reminder_cron($uid->id);
	send_mail_to_reminder_cron_one_day($uid->id);
} 


