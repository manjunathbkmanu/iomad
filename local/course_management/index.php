<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php');  
require_login(true);
$companyid = optional_param('companyid','',PARAM_RAW);
global $CFG,$USER;
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_management/index.php');
$title = get_string('coursemanagement', 'local_course_management');
//checking for iomad manager.
$managerroleid = $DB->get_field('role','id',array('shortname'=>'companymanager'));
$check = $DB->get_records('role_assignments',array('roleid'=>$managerroleid,'userid'=>$USER->id));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
if(is_siteadmin()){
}else{
	$companyid = $USER->company->id;
}
echo $OUTPUT->header();
if (is_siteadmin() || !empty($check)){
	$html ='';
	//Courses Section starts here.
	$html .='<div class="container-fluid">
	<div class="row">
	<div class="col-md-12 mb-3 mt-5 text-center">
	<h3>'.get_string('courses', 'local_course_management').'</h3>
	</div>
	</div>
	<div class="row">';
	//1.Progress Dashboard.
	$html.=setting_card($CFG->wwwroot. "/local/compliance_dashboard/index.php?companyid=".$companyid,'<i class="fa fa-line-chart" aria-hidden="true"></i>','progress_dashboard');
	//2. User Enrollments.
	$html.=setting_card($CFG->wwwroot. "/local/course_report/enroll.php?companyid=".$companyid,'<i class="fa fa-tachometer" aria-hidden="true"></i>','coursedashboard');
	//3. Create Course.	
	$html.=setting_card($CFG->wwwroot. "/blocks/iomad_company_admin/company_course_create_form.php?companyid=".$companyid,'<i class="fa fa-plus-circle" aria-hidden="true"></i>','createcourse');

	$html.='</div></div>';
 	//Courses Section ends here.

	//User section Starts here.
	$html .='<div class="container-fluid">
	<div class="row">
	<div class="col-md-12 mb-3 mt-3 text-center">
	<h3>'.get_string('users', 'local_course_management').'</h3>
	</div>
	</div>

	<div class="row">';

	//1. Create Users.
	$html.=setting_card($CFG->wwwroot. "/blocks/iomad_company_admin/company_user_create_form.php",'<i class="fa fa-plus-circle" aria-hidden="true"></i>','adduser');
	//2. Upload users.
	$html.=setting_card($CFG->wwwroot. "/blocks/iomad_company_admin/uploaduser.php",'<i class="fa fa-user" aria-hidden="true"></i>','uploaduser');
	//3. Assign to company.
	$html.=setting_card($CFG->wwwroot. "/blocks/iomad_company_admin/company_users_form.php",'<i class="fa fa-key" aria-hidden="true"></i>','assigntocompany');
	//4. Edit user.
	$html.=setting_card($CFG->wwwroot. "/blocks/iomad_company_admin/editusers.php",'<i class="fa fa-pencil" aria-hidden="true"></i>','browseusers');

	//5. Department Users Manager
	$html.=setting_card($CFG->wwwroot. "/local/settings/department.php",'<i class="fa fa-wrench" aria-hidden="true"></i>','departmentusersmanager');

	//6. Enrol users in multiple courses
	$html.=setting_card($CFG->wwwroot. "/blocks/enrollment/enrollment.php",'<i class="fa fa-th-large" aria-hidden="true"></i>','enrolmultiplecourse');

	$html.='</div></div>';

	//User section ends here.

	//Reports and Reminders section starts here.
	$html .='<div class="container-fluid"> 
	<div class="row">
	<div class="col-md-12 mt-3 mb-3 text-center">
	<h3>'.get_string('reportsandreminders', 'local_course_management').'</h3>
	</div>
	</div>
	<div class="row">';

	//1. User report.
	$html.=setting_card($CFG->wwwroot. "/local/course_report/user_report.php",'<i class="fa fa-address-card" aria-hidden="true"></i>','userreport');

	//2. Send reminder.
	$html.=setting_card($CFG->wwwroot. "/local/cs_reminder/index.php?companyid=".$companyid,'<i class="fa fa-address-card" aria-hidden="true"></i>','sendreminders');

	//3. Course report.
	$html.=setting_card($CFG->wwwroot. "/local/course_report/index.php?companyid=".$companyid,'<i class="fa fa-line-chart" aria-hidden="true"></i>','coursereport');



	$html.='</div></div>';

	//Reports and Reminders section ends here.

	//Other settings section starts here.
	$html .='<div class="container-fluid"> 
	<div class="row">
	<div class="col-md-12 mt-3 mb-3 text-center">
	<h3>'.get_string('othersettings', 'local_course_management').'</h3>
	</div>
	</div>
	<div class="row">';

	//1. SMTP Settings.
	$html.=setting_card($CFG->wwwroot. "/admin/settings.php?section=outgoingmailconfig",'<i class="fa fa-envelope" aria-hidden="true"></i>','SMTP_Settings');

	//2. LDAP Settings.
	$html.=setting_card($CFG->wwwroot. "/admin/settings.php?section=authsettingldap",'<i class="fa fa-server" aria-hidden="true"></i>','LDAP_Settings');

	//3. SAML Settings.
	$html.=setting_card($CFG->wwwroot. "/admin/settings.php?section=authsettingsaml2",'<i class="fa fa-server" aria-hidden="true"></i>','SAML2_Settings');

	//4. Reminder email Settings.
	$html.=setting_card($CFG->wwwroot. "/local/cs_reminder/reminder_settings.php",'<i class="fa fa-paper-plane" aria-hidden="true"></i>','reminder_Settings');

	//5. OpenID Connect Settings.
	$html.=setting_card($CFG->wwwroot. "/admin/settings.php?section=authsettingiomadoidc",'<i class="fa fa-plug" aria-hidden="true"></i>','openidconnect_settings');

	//5. Branding Settings.
	$html.=setting_card($CFG->wwwroot. "/blocks/iomad_company_admin/company_edit_form.php?action=color",'<i class="fa fa-eyedropper" aria-hidden="true"></i>','brandingsettings');
	
	//7. Autoenrollment.
	$html.=setting_card($CFG->wwwroot. "/local/course_management/autoenrollment.php",'<i class="fa fa-puzzle-piece" aria-hidden="true"></i>','addautoenrollmentrule');

	//8. Certificate Settings.
	$html.=setting_card($CFG->wwwroot. "/local/course_management/certificatesetting.php",'<i class="fa fa-certificate" aria-hidden="true"></i>','certificatesetting');

	//9. Phishing Settings.
	$html.=setting_card($CFG->wwwroot. "/local/phishing/phishing_config.php",'<i class="fa fa-user-secret" aria-hidden="true"></i>','phishing');

	$html.='</div></div>';

	//Other settings sections ends here.
	echo $html;
}else{
	echo get_string('noaccess','local_course_management');
}

echo $OUTPUT->footer();