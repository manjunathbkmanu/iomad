<?php
// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_student
 * @copyright  Manjunath B K<manjunathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once ('../../config.php');
require_once ('dash_ajax.php');
require_once ($CFG->dirroot . '/local/ebdaa_functions/lib.php');
global $DB, $CFG, $USER, $PAGE;
//company id.
$companyid = optional_param('companyid','',PARAM_INT);

$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/compliance_dashboard/js/custom.js'));
require_login(true);
$filter_department = 'false';
$filter_department_selected = 'false';
if($_POST){
    if ((isset($_POST['submit']) && $_POST['submit'] == 'true') && (isset($_POST['department']) && count($_POST['department']) > 0))
    {
        $filter_department = implode("','", $_POST['department']);
        unset($_SESSION['department_filter']);
        $_SESSION['department_filter'] = $_POST['department'];
        $filter_department_selected = $_SESSION['department_filter'];
    }
    else if (isset($_POST['clear']) && $_POST['clear'] != '')
    {
        unset($_SESSION['department_filter']);
    
    }else{

        $filter_department_selected = 'true';
    }
}

//var_dump($_SESSION['department_filter']);die;
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/compliance_dashboard/index.php');
$title = get_string('pagetitle', 'local_compliance_dashboard');
$PAGE
    ->navbar
    ->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE
    ->requires
    ->jquery();
$PAGE
    ->requires
    ->js(new moodle_url($CFG->wwwroot . '/local/course_report/js/chart.js') , true);
if (current_language() == 'ar')
{
    $PAGE
        ->requires
        ->css(new moodle_url($CFG->wwwroot . '/local/compliance_dashboard/arabic.css') , true);
}
$PAGE
    ->requires
    ->css(new moodle_url($CFG->wwwroot . '/local/compliance_dashboard/styles.css') , true);

echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback

$getallcategories = $DB->get_records('course_categories');
$config = get_config('coursemanagement');
$departmentsarray = $config->userdepartment;
$exploadedarray = explode(",", $departmentsarray);
$allcategoryarray = [];
foreach ($exploadedarray as $value)
{
    if($value != ""){
        $allcategoryarray[$value] = $value;
    }
}
$checkenrollments = check_availability_enrolled_user_courses();
if($checkenrollments > 0){
    $allcategoryarray["N/A"] = "N/A";
}
$html = html_writer::start_div('container-fluid');
$html .= html_writer::start_div('row mb-3 mt-3');
$html .= html_writer::start_div('col-md-4');
$html .= html_writer::end_div(); //col-6 ends


$html .= html_writer::start_div('col-md-8');
$html .= '<form method="POST">';
$html .= html_writer::start_div('button-groups pull-right');
$html .= html_writer::start_tag('select', array(
    'id' => 'department',
    'name' => 'department[]',
    'class' => 'department',
    'multiple' => "multiple"
));
foreach ($allcategoryarray as $catid => $catname)
{
    if ($filter_department_selected != 'false' && in_array($catname, $filter_department_selected))
    {
        $html .= html_writer::start_tag('option', array(
                'value' => $catid,
                'selected' => 'selected'
        ));

    }else{
        if($filter_department_selected == 'false'){
            $html .= html_writer::start_tag('option', array(
                    'value' => $catid,
                    'selected' => 'selected'
            ));
        }else{
            $html .= html_writer::start_tag('option', array(
                    'value' => $catid
            ));
        }
    }
    $html .= $catname;
    $html .= html_writer::end_tag('option');
}
$html .= html_writer::end_tag('select');
$html .= '<input type="submit" id="cd_clear" class="btn btn-primary" name="clear" value="' . get_string('clear', 'local_compliance_dashboard') . '"></form>';
$html .= '<button class="btn btn-primary" type="submit" value="true" name="submit"   id="exportSelected">' . get_string('submit', 'local_compliance_dashboard') . '</button>';
$html .= html_writer::end_div(); //button groups
$html .= html_writer::end_div(); //col-2 ends
// $html .= html_writer::start_div('col-md-2');
// $html .= '<button class="btn btn-primary" type="button" value="true" name="export" id="chart_export">' . get_string('export', 'local_compliance_dashboard') . '</button>';
// $html .= html_writer::end_div(); //col-2 ends
$html .= html_writer::end_div(); //row ends
$html .= html_writer::end_div(); //container end
echo $html;

if(!is_siteadmin()){
    $coursesql ="SELECT c.* FROM {course} AS c
    JOIN {company_course} AS cc ON cc.courseid = c.id WHERE c.visible = 1 AND c.id != 1 AND cc.companyid =$companyid ORDER BY c.fullname ASC";
}else{
    $coursesql = 'SELECT * FROM {course} WHERE visible = 1 AND id !=1';
}

$allcourses = $DB->get_records_sql($coursesql);

$udata .= html_writer::start_div('row'); //container \
$udata .= html_writer::start_div('my_cls2', array(
    'style' => "padding-bottom: 8%;border: 1px solid #ece9e9;"
)); //container end
$udata .= '<div class="row2 text-center"><span style="font-weight: 600;">' . get_string('allcourses', 'local_compliance_dashboard') . '</span><span  style="font-weight: 600;"><label>'.get_string('duedate', 'local_compliance_dashboard').': <lable> --</span></div>';
$udata .= '<canvas id="doughnutChart"></canvas>';
$udata .= html_writer::end_div(); //container end
foreach ($allcourses as $course)
{
    $due_date = '--';
    $coursename = $course->fullname;
    if (current_language() == 'ar')
    {
        $coursenamecustom = $DB->get_field('customfield_field', 'id', array(
            'shortname' => 'coursenamearabic'
        ));

        $coursenamearabic = $DB->get_field('customfield_data', 'value', array(
            'fieldid' => $coursenamecustom,
            'instanceid' => $course->id
        ));
        $coursename = $coursenamearabic;
        if (empty($coursename))
        {
            $coursename = $course->fullname;
        }

    }
    $due_date = $course->enddate == 0 ? "--" : date('Y-m-d H:i:s', $course->enddate);
    $udata .= html_writer::start_div('my_cls2', array(
        'style' => "padding-bottom: 8%;border: 1px solid #ece9e9;"
    )); //container end
    $udata .= '<div class="row2 text-center"><span style="font-weight: 600;">' . add3dots($coursename, ' ...', 25) . '<div style="display: inline"; class="item-action dropdown" data-display="static">
                  <a href="#" data-toggle="dropdown" data-display="static" class="icon" aria-expanded="false">
                <img src="' . $CFG->wwwroot . '/local/compliance_dashboard/pix/3dot.png" style="width: 20px;float: right;">   </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-md-left">
                  <a href="javascript:void(0)" onclick="course_report(' . $course->id . ')" class="dropdown-item font_change_for_all">'.get_string('details', 'local_compliance_dashboard').'
                  </a>
                    <a href="javascript:void(0)" onclick="cs_reminder(' . $course->id . ')" class="dropdown-item font_change_for_all">
                  </i>'.get_string('send_reminders', 'local_compliance_dashboard').'
                  </a>
                                    </div>
                  </div></span><span  style="font-weight: 600;"><label>'.get_string('duedate', 'local_compliance_dashboard') .': <lable>' . $due_date . '</span></div>';
    $udata .= '<canvas id="doughnutChart' . $course->id . '"></canvas>';
    $udata .= html_writer::end_div(); //container end
    
}

$udata .= html_writer::end_div(); //container end
echo $udata;
echo $OUTPUT->footer();
?>
<form id="course_report_form" action="<?php echo $CFG->wwwroot . '/local/course_report/index.php'; ?>" method="POST">
    <input type="hidden" value="0" name="courseid" id="courseid" />
    <input type="hidden" value= "0" name="departmentId" id="course_report_department" />
     <input type="submit" style="display:none;">
</form>
<form id="cs_reminder_form" action="<?php echo $CFG->wwwroot . '/local/cs_reminder/index.php'; ?>" method="POST">
    <input type="hidden" value="0" name="courseid" id="courseid_cs" />
    <input type="hidden" value= "0" name="departmentId" id="cs_reminder_department" />
     <input type="submit" name="submitbutton" value="Submit" style="display:none;">
</form>

<link rel="stylesheet" href="./css/bootstrap-multiselect.css" />
<script type="text/javascript" src="./js/bootstrap-multiselect.min.js"></script>
<script type="text/javascript">
var departments;
var countDepatment = "<?php echo count($allcategoryarray); ?>";
$(function() {
  $('.department')
    .multiselect({
      includeSelectAllOption: true,
      allSelectedText: 'All',
      maxHeight: 200,
      enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
      selectAllText:'<?php echo get_string('alldepartments', 'local_compliance_dashboard'); ?>',
      nonSelectedText:'<?php echo get_string('selectdepartment', 'local_compliance_dashboard'); ?>',
      filterPlaceholder: '<?php echo get_string('searchinput', 'local_compliance_dashboard'); ?>'
    });

    departments = $('.department').val();
    selectAllDepartments(departments);
});

function selectAllDepartments(departments){
    if(departments.length == 1 && countDepatment == 1){
        if(departments.includes("N/A")){
            $('button.multiselect').find('span').html('<?php echo get_string('alldepartments', 'local_compliance_dashboard'); ?>');
        }
    }
}
function course_report(id){
    $('#courseid').val(id);
    $('#course_report_department').val(departments)
    $('#course_report_form').submit();
}
function cs_reminder(id){
    $('#courseid_cs').val(id);
    $('#cs_reminder_department').val(departments)
    $('#cs_reminder_form').submit();
}
</script>

<script>



<?php
$notcompleted = get_string('notcompleted', 'local_course_report');
$complete = get_string('complete', 'local_course_report');
$overdue = get_string('overdue', 'local_course_report');
$nouser = get_string('nouser', 'local_course_report');

$backgroundColor = [];
$hoverBackgroundColor = [];
$all_sum_chart = [];
$all_sum_complete = 0;
$all_sum_notcompleted = 0;
$all_sum_overdue = 0;
$nouserBool = true;
foreach ($allcourses as $course)
{
    $coursestatistics = '';
    $chartdata = [];
    // $allusersofcourse = get_enrolled_users_in_course($course->id);
    /** Created by Dilip */
    // In order to get all user's Id lists enrolled in courses of object $course
    $allusersofcourse = get_users_enrolled_in_courses($course->id);
    if (!empty($allusersofcourse))
    {
        $totalusers = count($allusersofcourse);

        if (!empty($allusersofcourse))
        {

            $coursestatistics = get_course_statistics_c_dashboard($allusersofcourse, $course->id, $filter_department_selected);
        }
        $chartdata = [];

        if (!empty($coursestatistics) && count($coursestatistics) > 0)
        {
            if($coursestatistics[0] > 0 || $coursestatistics[1] > 0 || $coursestatistics[2] > 0){
                $chartdata[$complete] = $coursestatistics[0];
                $chartdata[$notcompleted] = $coursestatistics[1];
                $chartdata[$overdue] = $coursestatistics[2];
                $percentagenotstarted = ($coursestatistics[2] * 100) / $totalusers;
                $percentagecompleted = ($coursestatistics[0] * 100) / $totalusers;
                $percentageinprogress = ($coursestatistics[1] * 100) / $totalusers;

                /* all course data */
                $all_sum_complete += (int)$coursestatistics[0];
                $all_sum_notcompleted += (int)$coursestatistics[1];
                $all_sum_overdue += (int)$coursestatistics[2];
                /* all course data */
                $nouserBool = false;
                $backgroundColor = ["#28a944", "#FDB45C","#F7464A"];
                $hoverBackgroundColor = ["#3be25f", "#FFC870","#FF5A5E"];
            }else{
                $nouserBool = true;
                $backgroundColor = ["#2c82df"];
                $hoverBackgroundColor = ["#7daef1"];

                $chartdata[$nouser] = 1;
            }
        }else{
            $nouserBool = true;
        $backgroundColor = ["#2c82df"];
        $hoverBackgroundColor = ["#7daef1"];

            $chartdata[$nouser] = 1;
        }
    }
    else
    {

        $nouserBool = true;
        $backgroundColor = ["#2c82df"];
        $hoverBackgroundColor = ["#7daef1"];

        $chartdata[$nouser] = 1;

    }
?>
<?php if (!empty($chartdata) && !$nouserBool){ ?>
Chart.Legend.prototype.afterFit = function() {
    this.height = this.height + 15;
};
var ctxD = document.getElementById("doughnutChart<?php echo $course->id; ?>").getContext('2d');
var myLineChart = new Chart(ctxD, {
type: 'doughnut',
data: {
labels: [<?php if (!empty($chartdata))
    {
        $i = 0;
        foreach ($chartdata as $k => $c)
        {
            echo "'" . $k . "',";
            $i++;
        }
    } ?>],
datasets: [{
data: [<?php if (!empty($chartdata))
    {
        $i = 0;
        foreach ($chartdata as $k => $c)
        {
            echo "'" . $c . "',";
            $i++;
        }
    } ?>],
backgroundColor: <?php echo json_encode($backgroundColor); ?>,
hoverBackgroundColor: <?php echo json_encode($hoverBackgroundColor); ?>
}]
},
options: {
    responsive: true,
    legend: {
        labels: {
            fontSize: 14
        }
    },
}
});
<?php }else {?>
    var noUserString = "<?php echo $nouser; ?>";
    var ctxD = document.getElementById("doughnutChart<?php echo $course->id; ?>").getContext('2d');
    var myLineChart = new Chart(ctxD, {
    type: 'doughnut',
    data: {
        labels: [<?php if (!empty($chartdata))
            {
                $i = 0;
                foreach ($chartdata as $k => $c)
                {
                    echo "'" . $k . "',";
                    $i++;
                }
            } ?>],
        datasets: [{
        data: [<?php if (!empty($chartdata))
            {
                $i = 0;
                foreach ($chartdata as $k => $c)
                {
                    echo "'" . $c . "',";
                    $i++;
                }
            } ?>],
        backgroundColor: <?php echo json_encode($backgroundColor); ?>,
        hoverBackgroundColor: <?php echo json_encode($hoverBackgroundColor); ?>
        }]
    },
    options: {
        responsive: true,
        legend: {
            labels: {
                fontSize: 14
            }
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              return noUserString+': 0';
            }
          },
        },
    }
    });
<?php } ?>
<?php
}

$all_sum_chart[$complete] = $all_sum_complete;
$all_sum_chart[$notcompleted] = $all_sum_notcompleted;
$all_sum_chart[$overdue] = $all_sum_overdue;
//print_r($all_sum_chart);die;
if($all_sum_complete > 0 || $all_sum_notcompleted > 0 || $all_sum_overdue > 0){
?>

    //doughnut
var ctxD = document.getElementById("doughnutChart").getContext('2d');
var myLineChart = new Chart(ctxD, {
type: 'doughnut',
data: {
    labels: ["<?=$complete?>", "<?=$notcompleted?>","<?=$overdue?>"],
    datasets: [{
    data: [<?php if (!empty($all_sum_chart))
    {
        $i = 0;
        foreach ($all_sum_chart as $k => $c)
        {
            echo "'" . $c . "',";
            $i++;
        }
    } ?>],
    backgroundColor: ["#28a944", "#FDB45C","#F7464A"],
    hoverBackgroundColor: ["#3be25f", "#FFC870","#FF5A5E"]
    }]
},
options: {
    responsive: true,
    legend: {
        labels: {
            fontSize: 14
        }
    }
}
});
<?php }else{ ?>
var nouserstring = "<?php echo $nouser; ?>";
var ctxD = document.getElementById("doughnutChart").getContext('2d');
var myLineChart = new Chart(ctxD, {
type: 'doughnut',
data: {
    labels: ["<?=$nouser?>"],
    datasets: [{
        data: [1],
        backgroundColor: ["#2c82df"],
        hoverBackgroundColor: ["#7daef1"]
    }]
},
options: {
    responsive: true,
    legend: {
        labels: {
            fontSize: 14
        }
    },tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              return nouserstring+': 0';
            }
          },
        },
}
});
<?php } ?>
//doughnut
</script>
