<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * You may have settings in your plugin
 *
 * @package    local_course_management
 * @copyright  Manjunath  B K<manjunathbk@edzlearn10.com>
 * @license    http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
global $OUTPUT, $CFG;
require_once($CFG->libdir.'/formslib.php');
require_login();
class default_settings_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        $id = $USER->id;
        $mform =& $this->_form;
        //Default language setting.
        $defaultlanguage = array('en'=>'English &lrm;(en)&lrm;',
          'ar'=>'عربي &lrm;(ar)&lrm;');
        $mform->addElement('select', 'defaultlanguage', get_string('seldefaultlanguage', 'local_course_management'), $defaultlanguage);
        $mform->setType('defaultlanguage', PARAM_TEXT);

        //Default home page setting.
        $defaulthome = array('2'=>get_string('defaultcase', 'local_course_management'),
                             '0'=>get_string('lmshomepage', 'local_course_management'),
                            '1'=>get_string('lmsonly', 'local_course_management')
                            );
        $mform->addElement('select', 'defaulthome', get_string('selectdefaulthome', 'local_course_management'), $defaulthome);
        $mform->setType('defaulthome', PARAM_INT);

        $mform->addElement('submit', 'submitbutton', get_string('submit', 'local_course_management'),array('onclick'=>'selectDlang()'));
    }

}