<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_student
 * @copyright  Manjunath B K<manjunathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('user_ajax.php'); 
global $DB, $CFG, $USER,$PAGE;
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/student/js/custom.js'));
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/student/user_index.php');
$title = get_string('pagetitle', 'local_student');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
echo $OUTPUT->header(); 

//get all categories
$getallcategories = $DB->get_records('course_categories');
$allcategoryarray = [];
$selectuser = get_string('all', 'local_student');
$allcategoryarray['']=$selectuser;
foreach ($getallcategories as $value) {
	$allcategoryarray[$value->id] = $value->name;
}
$html  = html_writer::start_div('container');
$html .= html_writer::start_div('row mb-3 mt-3');
$html .= html_writer::start_div('col-md-6');
$html .= html_writer::end_div();//col-6 ends

$html .= html_writer::start_div('col-md-2');
$html .= html_writer::tag('input','',array('type'=>'text','id'=>'myInput','placeholder'=> get_string('searchinput', 'local_compliance_dashboard'),'class'=>'float-right'));
$html .= html_writer::end_div();//col-2 ends

$html .= html_writer::start_div('col-md-2');
$html .= html_writer::start_tag('select',array('id'=>'category'));
foreach ($allcategoryarray as $catid => $catname) {
    $html .= html_writer::start_tag('option',array('value'=>$catid));
    $html .= $catname;
    $html .= html_writer::end_tag('option');
}
$html .= html_writer::end_tag('select');
$html .= html_writer::end_div();//col-2 ends
$html .= html_writer::start_div('col-md-2');
$html .= html_writer::start_tag('select',array('id'=>'sort'));
$html .= html_writer::start_tag('option',array('value'=>'ASC'));
$html .= get_string('atoz','local_student');
$html .= html_writer::end_tag('option');
$html .= html_writer::start_tag('option',array('value'=>'DESC'));
$html .= get_string('ztoa','local_student');
$html .= html_writer::end_tag('option');
$html .= html_writer::end_tag('select');

$html .= html_writer::end_div();//col-2 ends
$html .= html_writer::end_div();//row ends
$html .= html_writer::end_div();//container end
echo $html;
$data = html_writer::start_div('firsttable');
$data .= html_writer::end_div();
echo $data;
echo $OUTPUT->footer();


