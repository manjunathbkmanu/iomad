<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local plugin "bulkenrol" - Language pack
 *
 * @package   local_bulkenrol
 * @copyright 2017 Soon Systems GmbH on behalf of Alexander Bias, Ulm University <alexander.bias@uni-ulm.de>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['bulkenrol:enrolusers'] = 'تسجيل عدد كبير';
$string['bulkenrol_form_intro'] = 'هنا ، يمكنك تسجيل المستخدمين بكميات كبيرة في الدورة التدريبية الخاصة بك. يتم تعريف المستخدم الذي يتم تسجيله بواسطة عنوان بريده الإلكتروني المخزن في حساب نظام إدارة التعليم الخاص به.';
$string['enrol_users_successful'] = 'تم تسجيل المستخدمين بنجاح';
$string['enrol_users'] = 'تسجيل المستخدمين';
$string['enrolplugin'] = 'مكون التسجيل';
$string['enrolplugin_desc'] = 'يتم استخدامها في تسجيل المستخدمين بكميات كبيرة. إذا كانت طريقة التسجيل المكونة غير نشطة / مضافة في الدورة التدريبية عندما يكون المستخدمون مسجلين جماعيًا ، تتم إضافتها / تنشيطها تلقائيًا.';
$string['error_enrol_users'] = 'كانت هناك مشكلة عند تسجيل المستخدمين في الدورة التدريبية.';
$string['error_enrol_user'] = 'كان هناك مشكلة في تسجيل المستخدم باستخدام البريد الإلكتروني <em>{$a->email}</em> إلى الدورة.';
$string['error_exception_info'] = 'معلومات الإستثناء';
$string['error_getting_user_for_email'] = 'حدثت مشكلة عند الحصول على سجل المستخدم لعنوان البريد الإلكتروني <em>{$a}</em> من قاعدة البيانات';
$string['error_group_add_members'] = 'حدثت مشكلة عند إضافة المستخدمين إلى مجموعة (مجموعات) الدورة التدريبية.';
$string['error_group_add_member'] = 'حدثت مشكلة عند إضافة المستخدم <em>{$a->email}</em> إلى مجموعة الدورة التدريبية <em>{$a->group}</em>.';
$string['error_invalid_email'] = 'تم وجود بريد إلكتروني غير صالح. {$a->row} (<em>{$a->email}</em>). سيتم تجاهله.';
$string['error_more_than_one_record_for_email'] = 'تم وجود أكثر من مستخدم يستخدمون البريد الإلكتروني <em>{$a}</em>em>. سيتم تجاهله، ولن يتم تسجيل أي مستخدمين في نظام إدارة التعليم.';
$string['error_no_email'] = 'لم يتم العثير على بريد إلكتروني {$a->line} (<em>{$a->content}</em>). سيتم تجاهله';
$string['error_no_record_found_for_email'] = 'لا يوجد حساب مرتبط بالبريد الإلكتروني <em>{$a}</em>.<br />سيتم تجاهله ولن يتم إنشاء حساب في نظام إدارة التعليم.';
$string['error_usermails_empty'] = 'قائم البريد الإلكتروني فارغة. الرجاء إضافة على الأقل بريد إلكتروني واحد.';
$string['error_check_is_already_member'] = 'هناك خطأ في التحقق من وجود المستخدم (<em>{$a->email}</em>) في مجموعة (<em>{$a->groupname}</em>). {$a->error}';
$string['pluginname'] = 'تسجيل المستخدمين بكمية كبيرة';
$string['privacy:metadata'] = 'يعمل المكون الإضافي للتسجيل الجماعي للمستخدم كأداة لتسجيل المستخدمين في الدورات التدريبية ، لكنه لا يخزن أي بيانات شخصية.';
$string['hints'] = 'إشارات';
$string['row'] = 'صف';
$string['usermails'] = 'قائمة البريد الإلكتروني';
$string['usermails_help'] = 'يل مستخدم LMS حالي في هذه الدورة التدريبية ، أضف عنوان بريده الإلكتروني إلى هذا النموذج ، عنوان مستخدم / بريد إلكتروني واحد في كل سطر.<br /><br />مثال:<br />alice@example.com<br />bob@example.com<br /><br />يمكنك إنشاء مجموعات وإضافة المستخدمين المسجلين إلى المجموعات. كل ما عليك القيام به هو إضافة سطر عنوان مع علامة التجزئة واسم المجموعة ، مع فصل قائمة المستخدمين.<br /><br />مثال:<br /># Group 1<br />alice@example.com<br />bob@example.com<br /># Group 2<br />carol@example.com<br />dave@example.com';
$string['users_to_enrol_in_course'] = 'يتم تسجيل المستخدمين في الدورة';
$string['user_enroled'] = 'تسجيل المستخدم';
$string['user_enroled_yes'] = 'سيتم تسجيل المستخدم';
$string['user_enroled_already'] = 'تم تسجيل المستخدم';
$string['user_groups'] = 'عضوية في المجموعة';
$string['user_groups_yes'] = 'تم إضافة المستخدم إلى المجموعة';
$string['user_groups_already'] = 'المستخدم تم تسجيله مسبقاً إلى المجموعة';
$string['parameter_empty'] = 'المقياس فارغ';
