<?php
require_once('../../config.php');
global $DB;
$keeyword = optional_param('query','',PARAM_RAW);
$course = optional_param('course','',PARAM_RAW);
if(!empty($keeyword)){
	$courses=$DB->get_records_sql('SELECT * FROM {course} WHERE fullname LIKE "%'.$keeyword.'%"');
	if(!empty($courses)){
		$coursearray=[];
		foreach ($courses as $course) {
			$coursearray[$course->id]=$course->fullname;
		}
		echo json_encode($coursearray);
	}
}
if(!empty($course)){
			//get self enrollment status.
	$courseself=$DB->get_field('enrol', 'status', array('courseid'=>$course,'enrol'=>'self'));
	$manualenroll = "SELECT id FROM {enrol} 
	WHERE enrol ='manual'
	AND courseid =".$course."";
	$enrollmentid = $DB->get_record_sql($manualenroll);


	$return_arr = array('selfenrol'=>$courseself,'enrollmentid'=>$enrollmentid->id,'courseid'=>$course);
	echo json_encode($return_arr);
}
