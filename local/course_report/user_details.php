<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once('form/user_form.php');
require_once($CFG->libdir . '/formslib.php'); 
require_once($CFG->dirroot.'/local/course_report/csslinks.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
global $DB,$PAGE,$USER,$CFG; 
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_report/user_report.php');

$userid = $_GET['id'];
$username='';
if(!empty($userid)){
$userobject = $DB->get_record('user',array('id'=>$userid));
$username = fullname($userobject);
}
$title = $username;
//$PAGE->navbar->ignore_active();
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();		
include_once('jslink.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/chart.js'), true);
echo $OUTPUT->header();
if(!empty($userid)){
	$userenrolledcourses = enrol_get_all_users_courses($userid);
	$table  = new \html_table();
	$table->id = 'usertable';
	$table->head = array(get_string('course', 'local_course_report'),
		get_string('status', 'local_course_report'),
		get_string('score', 'local_course_report')
	);
	foreach ($userenrolledcourses as $course) {
		$coursename = $course->fullname;
		$coursescore = get_user_course_score($userid,$course->id);
		$coursestatus = course_status_check($userid,$course->id);
		$table->data[] = array($coursename, $coursestatus, $coursescore);
	}
	echo html_writer::table($table);
}

echo $OUTPUT->footer();
?>
