<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * You may have settings in your plugin
 *
 * @package    local_ebdaa_license
 * @copyright  Manjunath B K
 * @license    http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); // It must be included from a Moodle page
}
global $OUTPUT, $CFG;
require_once($CFG->libdir.'/formslib.php');
require_login();
class local_update_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        $mform =& $this->_form;
        
        //Updater Hidden Fields
        $mform->addElement('hidden', 'update_id', $this->_customdata['update_id']);
        $mform->setType('update_id', PARAM_TEXT);

        $mform->addElement('hidden', 'has_sql', $this->_customdata['has_sql']);
        $mform->setType('has_sql', PARAM_TEXT);

        $mform->addElement('hidden', 'version', $this->_customdata['version']);
        $mform->setType('version', PARAM_TEXT);

        $mform->addElement('submit', 'submit', get_string('downloadupdate', 'local_ebdaa_update'));
    }

}