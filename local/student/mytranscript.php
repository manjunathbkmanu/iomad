<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php'); 
require_once('user_ajax.php'); 
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
global $DB, $CFG, $USER,$PAGE;

require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/student/mytranscript.php');
$title = get_string('transcript', 'local_student');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading('');
$PAGE->requires->jquery();
echo $OUTPUT->header(); 
echo '<h3>'.$title .' </h3>';
if(is_siteadmin()){
	$courses = $DB->get_records_sql('SELECT * FROM {course} WHERE id !=1 AND visible=1');
}else{
	$courses = enrol_get_my_courses();
}

$dateformat = '%d-%b-%Y';
$duration = ''; 
$passfail = ' ';
$table = new html_table();
    //$table->head = (array) get_strings(array( 'cname','enrolleddate','enddate','duration','vgrade','status'), 'block_enrolled_courses');
$table->head = (array) get_strings(array( 'cname','enddate','completiondate','duration','vgrade','passfail','status','certificates'), 'local_student');
if(!empty($courses)){
	foreach($courses as $course) {
		$cminfo = get_fast_modinfo($course, $USER->id);
		foreach($cminfo->get_cms() as $cm) {
			if($cm->modname == 'scorm')
			{
				$scormlink = get_scorm_status_ebdaa_transcript($cm->instance,$course);
				$passfail = get_scorm_status_ebdaa_passfail($cm->instance,$course);
			}
		}
		$instance1 = $DB->get_record('enrol', array('courseid' => $course->id, 'enrol' => 'manual'), '*', MUST_EXIST);
		$enrolid = $DB->get_record('user_enrolments',array('enrolid'=>$instance1->id,'userid'=>$USER->id));
		if(!empty($enrolid)){
			$enroldate = userdate($enrolid->timecreated,$dateformat);
		}else{
			$enroldate = '-';
		}

		$completecdetails = $DB->get_record('course', array('id'=>$course->id));
		$enddate = $completecdetails->enddate;

		if(!empty($enddate)){
			// $enddate = userdate($enddate,$dateformat);
			$enddate = date('d-m-Y',$enddate);
		}else{
			$enddate = '-';
		}

			//now get duration from custom field table
		$fieldidsql = $DB->get_record('customfield_field', array('shortname'=>'courseduration'));
		if(!empty($fieldidsql))
		{
			$fieldid = $fieldidsql->id;
				//get value
			$fielddurationsql = $DB->get_record('customfield_data', array('fieldid'=>$fieldid, 'instanceid' =>$course->id));
			if(!empty($fielddurationsql))
			{
				$duration = $fielddurationsql->value;
			}
		}
			// if pass/fail returns pass then show default score as 100% 9 Jan 2020
		if ($passfail == 'PASS' || $passfail == 'pass') {

			$gradeRoundvalue= '100';
		} else{
			$grades = grade_get_course_grades($course->id, $USER->id);
			$grademax = $grades->grademax;

			$gradeValue =(((int) $grades->grades[$USER->id]->str_grade));
			if(!$gradeValue==null){
				$gradeRoundvalue = (($gradeValue * 100)/$grademax);
			}else{
				$gradeRoundvalue='-';
			}
			if($gradeRoundvalue == 100){
				$passfail='PASS';
			}


        } // end of if clause of PASS/FAIL

            //print_object($gradeRoundvalue);
        $linkcourse = html_writer::link(
        	new moodle_url(
        		$CFG->wwwroot.'/course/view.php?id='.$course->id,array()
        	),'<i style="font-size:24px" class="fa">&#xf090;</i>',array('class'=>'glyphicon glyphicon-log-in')
        );
//Manju:for arabic language corrections.
			//Manju:16/03/2020. 
        $coursename =$course->fullname;
        $coursedurationar = $duration;        
        if(current_language() == 'ar'){
        	$coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));

        	$coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$course->id));
        	$coursename = $coursenamearabic;
        	if(empty($coursename)){
        		$coursename =$course->fullname;
        	}

        	$courseduration = $DB->get_field('customfield_field', 'id', array('shortname'=>'durationinarabic'));

        	$coursedurationar = $DB->get_field('customfield_data', 'value', array('fieldid'=>$courseduration,'instanceid'=>$course->id));
        	if(empty($coursedurationar)){
        		$coursedurationar = $duration;
        	}
        }
        //get course completion date.
       $complt=course_completion_date($USER->id,$course->id);
        if(!empty($complt)){
        	$completiondate=date('d-m-Y',$complt);
        }else{
        	$completiondate='-';
        }
        $username = fullname($USER);
        $downlink='-';
        if(!empty($complt)){
        	$link1 = $CFG->wwwroot.'/local/course_management/download.php?username='.$USER->id.'&coursename='.$course->id.'&complete='.$completiondate.'&lg=en';
        	$downloadcert1 = '<a href="'.$link1.'"><img src="pix/en_tr.png" width="15%"></a>';
        	$link2 = $CFG->wwwroot.'/local/course_management/download.php?username='.$USER->id.'&coursename='.$course->id.'&complete='.$completiondate.'&lg=ar';
        	$downloadcert2 = '<a href="'.$link2.'"><img src="pix/ar_tr.png" width="15%"></a>';
        	$downlink=$downloadcert1.' | '.$downloadcert2;
        }


        $table->data[] = array(
        	$coursename,
               // $enroldate,
        	$enddate,
        	$completiondate,
        	$coursedurationar,
        	$gradeRoundvalue,
        	$passfail,
        	$scormlink,
        	$downlink
        	
        );
    }
}
$tabeled = html_writer::table($table);

		/**
 *
 * @param type $scormid
 * @param type $userid
 */
		function get_scorm_status_ebdaa_transcript_1($scormid,$course) {
			global $DB, $USER,$CFG;
			$scormlink = '-';
			$userid = $USER->id;
	//SELECT article, dealer, price FROM   shop WHERE  price=(SELECT MAX(price) FROM shop)

			$sql = "SELECT attempt
			FROM {scorm_scoes_track}
			WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
			";

			$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));

			if (!empty($lastattempt)) {
		//now find the status
				$sql = "SELECT value
				FROM {scorm_scoes_track}
				WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
				$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

				if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
					$scormstatus = strtoupper('completed');
					$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
					<button class="btn btn-sm bg-green w-125" style="cursor:pointer;margin-top: auto;">'.$scormstatus.'</button>
					</a>';

				} else if ($status == 'incomplete') {
					$scormstatus = strtoupper('in progress');
					$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
					<button class="btn btn-sm bg-yellow w-125" style="cursor:pointer;margin-top: auto;">'.$scormstatus.'</button>
					</a>';
				}
			} else {
				$scormstatus = strtoupper('not started');
				$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
				<button class="btn btn-sm bg-red w-125" style="cursor:pointer;margin-top: auto;">'.$scormstatus.'</button>
				</a>';
			}
			return $scormlink;
		}

	/**
 *
 * @param type $scormid
 * @param type $userid
 */
	function get_scorm_status_ebdaa_transcript($scormid,$course) {
		global $DB, $USER,$CFG;
		$status = '-';
		$userid = $USER->id;
    /* $sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
               $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid)); */

               $sql = "SELECT attempt
               FROM {scorm_scoes_track}
               WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
               ";

               $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));

               if (!empty($lastattempt)) {
               	$sql = "SELECT value
               	FROM {scorm_scoes_track}
               	WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
               	$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

               	if(!empty($status)) {
               		if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
               			$scormstatus = get_string('complete', 'local_student');
               			$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
               			<button class="btn btn-sm bg-green w-125" style="cursor:pointer;margin-top: auto;">'.$scormstatus.'</button>
               			</a>';

               		} else if ($status == 'incomplete') {
               			$scormstatus = get_string('inprogress', 'local_student');
               			$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
               			<button class="btn btn-sm bg-yellow w-125" style="cursor:pointer;margin-top: auto;">'.$scormstatus.'</button>
               			</a>';
               		}
               	}
               } else {

               	$sql = "SELECT MAX(attempt)
               	FROM {scorm_scoes_track}
               	WHERE userid = ? AND scormid = ?
               	";
               	$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
               	if (!empty($lastattempt)) {
               		$sql = "SELECT value
               		FROM {scorm_scoes_track}
               		WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
               		$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

               		if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
               			$scormstatus = get_string('complete', 'local_student');
               			$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
               			<button class="btn btn-sm bg-green w-125" style="cursor:pointer;margin-top: auto;">'.$scormstatus.'</button>
               			</a>';

               		} else if ($status == 'incomplete') {
               			$scormstatus = get_string('inprogress', 'local_student');
               			$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
               			<button class="btn btn-sm bg-yellow w-125" style="cursor:pointer;margin-top: auto;">'.$scormstatus.'</button>
               			</a>';
               		}
               	} else {
               		$scormstatus = get_string('notstarted', 'local_student');
               		$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
               		<button class="btn btn-sm bg-red w-125" style="cursor:pointer;margin-top: auto;">'.$scormstatus.'</button>
               		</a>';
               	}
		//$scormstatus = strtoupper('not started');
		/* 	$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
                    <button class="btn btn-sm bg-red w-125" style="cursor:pointer;">'.$scormstatus.'</button>
                    </a>'; */
                }
                return $scormlink;
            }
		/**
 *
 * @param type $scormid
 * @param type $userid
 */
		function get_scorm_status_ebdaa_passfail($scormid,$course) {
			global $DB, $USER,$CFG;
			$scormstatus = '-';
			$userid = $USER->id;
    /*
	$sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
	$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
*/
	$sql = "SELECT attempt
	FROM {scorm_scoes_track}
	WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
	";

	$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));
	if (!empty($lastattempt)) {
		$sql = "SELECT value
		FROM {scorm_scoes_track}
		WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
		$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

		if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
			if($status == 'passed') { $stringshow = 'pass';}
			if($status == 'failed') { $stringshow = 'fail';}
			if($status == 'complete') { $stringshow = 'pass';}
			if($status == 'completed') { $stringshow = 'pass';}
			$scormstatus = strtoupper($stringshow);

		} else if ($status == 'incomplete') {
			$scormstatus = '-'; 
		}
	} else {

    	//non scored scorm only returns lesson status
		$sql = "SELECT MAX(attempt)
		FROM {scorm_scoes_track}
		WHERE userid = ? AND scormid = ?
		";
		$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
		if (!empty($lastattempt)) {
			$sql = "SELECT value
			FROM {scorm_scoes_track}
			WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
			$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

			if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
				if($status == 'passed') { $stringshow = 'pass';}
				if($status == 'failed') { $stringshow = 'fail';}
				if($status == 'complete') { $stringshow = 'pass';}
				if($status == 'completed') { $stringshow = 'pass';}
				$scormstatus = strtoupper($stringshow);

			} else if ($status == 'incomplete') {
				$scormstatus = '-'; 
			}
		} else {
			$scormstatus = '-';
		}

	}
	return $scormstatus;
}


echo $tabeled;
echo $OUTPUT->footer();