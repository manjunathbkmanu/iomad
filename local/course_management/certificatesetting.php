<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once('form/certificate_form.php');
require_once('lib.php');  
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_management/certificatesetting.php');
$title = get_string('certificatesetting', 'local_course_management');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
//getting form data here.
$mform = new certificate_setting();
//Form processing and displaying is done here
if ($mform->is_cancelled()) {

} else if ($fromform = $mform->get_data()) {
	$companyid = $fromform->companyid;
	$fdata = $DB->get_record('certificate_settings',array('companyid'=>$fromform->companyid));

	if(empty($fdata)){
		$insert  = new stdClass();
		// $context = context_coursecat::instance($companyid);
		// $contextid = $context->id;

		$insert->arabicback = $fromform->arabicback;
		$imgitemid1 = $fromform->arabicback;
		file_save_draft_area_files($imgitemid1,
			$context->id,
			'theme_iomad',
			'arabicback',
			$companyid,
			array('subdirs' => 0, 'maxbytes' => 150 * 1024, 'maxfiles' => 1));

		$insert->englishback=$fromform->englishback;
		$imgitemid2 = $fromform->englishback;
		file_save_draft_area_files($imgitemid2,
			$context->id,
			'theme_iomad',
			'englishback',
			$companyid,
			array('subdirs' => 0, 'maxbytes' => 150 * 1024, 'maxfiles' => 1));

		$insert->companyid = $fromform->companyid;
		$res = $DB->insert_record('certificate_settings',$insert);
		$redirecturl = $CFG->wwwroot.'/local/course_management/certificatesetting.php';
		if($res){
			redirect($redirecturl,get_string('certificatesadded','local_course_management'));
		}

	}else{
		$update  = new stdClass();
		$update->id = $fdata->id;
		
		// $context = context_coursecat::instance($companyid);
		// $contextid = $context->id;

		$update->arabicback=$fromform->arabicback;
		$imgitemid1 = $fromform->arabicback;

		file_save_draft_area_files($imgitemid1,
			$context->id,
			'theme_iomad',
			'arabicback',
			$companyid,
			array('subdirs' => 0, 'maxbytes' => 150 * 1024, 'maxfiles' => 1));
		$update->englishback=$fromform->englishback;
		$imgitemid2 = $fromform->englishback;
		file_save_draft_area_files($imgitemid2,
			$context->id,
			'theme_iomad',
			'englishback',
			$companyid,
			array('subdirs' => 0, 'maxbytes' => 150 * 1024, 'maxfiles' => 1));

		$update->companyid = $fromform->companyid;
		$res = $DB->update_record('certificate_settings', $update);
		$redirecturl = $CFG->wwwroot.'/local/course_management/certificatesetting.php';
		if($res){
			redirect($redirecturl,get_string('certificatesupdated','local_course_management'));
		}
	}
}else{
	//set the form data.
	$fdata = $DB->get_record('certificate_settings',array('id'=>1));
	if(!empty($fdata)){
		$data = new \stdClass();
		$data->id = $fdata->id;
		$data->arabicback = $fdata->arabicback;
		$data->englishback = $fdata->englishback;
		$mform->set_data($data);
	}
}
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/course_management/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback
$mform->display();
echo $OUTPUT->footer();