<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_accesscohort
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../../config.php');
global $DB,$USER;
require_login(0,false);
$companyid = $USER->company->id;
$id = required_param('id', PARAM_INT);
$confirm = optional_param('confirm', 0, PARAM_BOOL);
$flag = 0;
//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');
$context = context_system::instance();
$overall = has_capability('local/phishing:overall',$context);
$createtemp = has_capability('local/phishing:createtemp',$context);
$PAGE->set_context(context_system::instance());
$title = get_string('delete_temp', 'local_phishing');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/action/template_delete.php?id='.$id);
require_login();
$previewnode = $PAGE->navigation->add(get_string('pluginname','local_phishing'), new moodle_url($CFG->wwwroot.'/'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title, new moodle_url($CFG->wwwroot.'/local/phishing/action/template_delete.php'));
$thingnode->make_active();
if($overall || $createtemp){
	if($confirm){
		$curl_handle = curl_init();
		$url = $protocol.$urlip.'/api/templates/'.$id.'?api_key='.$apikey;
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		$curl_data = json_encode($curl_handle);
		if(!empty($id)){
			$flag =1;
			curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST,"DELETE");
			curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $curl_data);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
			$response  = curl_exec($curl_handle);
			curl_close($curl_handle);
		}
		$msg = get_string('deletemsg','local_phishing');
		$decodedResponse = json_decode($curl_data);
		redirect(new moodle_url($CFG->wwwroot.'/local/phishing/templatelist.php'),$msg);
	}
	echo $OUTPUT->header();
	$returnurl = $CFG->wwwroot.'/local/phishing/templatelist.php';
	$message = get_string('sure','local_phishing');
	if($flag==1){
		echo html_writer::div(
			'Success','alert alert-success'
		);
		echo '<br>';	
		echo html_writer::link( new moodle_url($CFG->wwwroot.'/local/phishing/templatelist.php'),get_string('list1','local_phishing'),array('class'=>'btn btn-small btn-primary')
	);
	}
	$formcontinue = new single_button(new moodle_url($CFG->wwwroot.'/local/phishing/action/template_delete.php', array('confirm' => 1,'id'=>$id,'action'=>'delete',)), get_string('yes'));
	$formcancel = new single_button(new moodle_url('/local/phishing/templatelist.php'), get_string('no'), 'get');
	
	if($flag!=1){
		echo $OUTPUT->confirm($message, $formcontinue, $formcancel);
	}
}else{
	echo html_writer::div(
		get_string('cap', 'local_phishing'),'alert alert-danger'
	);
}


echo $OUTPUT->footer();
?>

<style type="text/css">
	.path-local-phishing .grade-contents .btn-primary {
     	font-size: 16px !important; 
    	padding: 8px 20px !important;
	}
</style>






