<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_ebdaa_functions
 * @copyright  Manjunath B K<manjunathbk@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */

global $DB,$CFG,$OUTPUT,$USER;
require_once("$CFG->libdir/gradelib.php");
require_once("$CFG->dirroot/lib/completionlib.php");
require_once("$CFG->dirroot/grade/querylib.php");
require_once($CFG->libdir . '/enrollib.php');
require_once($CFG->dirroot . '/lib/enrollib.php');

use core_completion\progress;
//Manju: This function will returns an array containing all user's id enrolled in a course.[17/02/2020]
function get_enrolled_users_in_course($courseid,$departmentId = false)
{
	global $DB;
	/* Created by Manish */
	// In order to filter with department ,pass department key as departmentId
	$allenrolleduser = enrol_get_course_users($courseid,false,array(),array(),$departmentId);
	$listofusers =[];
	foreach ($allenrolleduser as $user) {
		if($user->deleted == 0){
			$listofusers[] = $user->id;
		}
	}
	return  $listofusers;
}

//Dilip: This function will returns an array containing all user's id enrolled in a course.[25/09/2020]
function get_users_enrolled_in_courses($course_id){
	global $DB;
	$allenrolleduser = enrol_get_course_users($course_id,false,array(),array());
	$listofusers =[];
	foreach ($allenrolleduser as $user) {
		if($user->deleted == 0){
			$listofusers[] = $user->id;
		}
	}
	return  $listofusers;
}

// Manju: This function will returns the course completion, in-progress and not started users count in a course.[17/02/2020]
// Dilip: Passing $selecteddepartment to filter users attending course and department
function get_course_statistics($allusersid, $courseid, $selecteddepartment)
{
	global $DB;
	$coursecompltedusers = 0;
	$coursenotstartedusers = 0;
	$courseinprogressusers = 0;
	$statusprogress = array();
	$statusnotcompleted = array();
	$statuscompleted = array();
	$courseobject = $DB->get_record('course',array('id'=>$courseid));
	$notstarted = get_string('notstarted', 'local_course_report');
	$complete = get_string('complete', 'local_course_report');
	$inprogress = get_string('inprogress', 'local_course_report');
	foreach ($allusersid as $userid) {
		$userobject = $DB->get_record('user', array('id' => $userid));
		if($userobject){
			if ($selecteddepartment !='false' && count($selecteddepartment) > 0) {
				$depart = (strlen(trim($userobject->department))>0) ? $userobject->department : 'N/A';
			    if(in_array($depart, $selecteddepartment)){
			    	$cstatus=course_status_check($userid, $courseobject->id);
					if($cstatus == $notstarted){
						$coursenotstartedusers++;
					}
					elseif ($cstatus == $inprogress) {
						$courseinprogressusers++;
					}
					elseif ($cstatus == $complete) {
						$coursecompltedusers++;
					}
			    }
			}
		}
	}
	$enrollmentstats = array($coursecompltedusers,$courseinprogressusers,$coursenotstartedusers);
	return $enrollmentstats;
}
function get_scorm_status_ebdaa($scormid,$course,$userid) {
	global $DB, $CFG;
	$scormstatus = '';
	$status = '-';
	$userid = $userid;	
	$sql = "SELECT attempt
	FROM {scorm_scoes_track}
	WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
	";

	$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));
	if (!empty($lastattempt)) {
		$sql = "SELECT value
		FROM {scorm_scoes_track}
		WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
		$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));
		if(!empty($status)) {
			if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
				$scormstatus = 'completed';
			} else if ($status == 'incomplete') {
				$scormstatus = 'inprogress';
			}
		}
	} else {
		$sql = "SELECT MAX(attempt)
		FROM {scorm_scoes_track}
		WHERE userid = ? AND scormid = ?
		";
		$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
		if (!empty($lastattempt)) {
			$sql = "SELECT value
			FROM {scorm_scoes_track}
			WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
			$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));
			if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
				$scormstatus = 'completed';

			} else if ($status == 'incomplete') {
				$scormstatus = 'inprogress';
			}
		} else {
			$scormstatus = 'notstarted';
		}
	}
	return $scormstatus;
}

/**
*Manju: This function will give average users score in a particular course.
*@param $allusersid have all the users id's enrolled in course.
*@param $courseid is unique course id.
*@return $averagescore give average users score in a course.
*/
function average_userscore($allusersid,$courseid)
{
	GLOBAL $DB;
	$allusersscore =[];
	foreach($allusersid as $userid){
       $courseobj = $DB->get_record('course',array('id'=>$courseid));
                    
            $cminfo = get_fast_modinfo($courseobj, $userid);
                foreach($cminfo->get_cms() as $cm) {
                    if($cm->modname == 'scorm')
                     {
					    $passfail = get_scorm_status_ebdaa_passfail_blockscoremyreport($cm->instance,0,$userid);
                     }
                 }

                //Mihir for score as 100 for completed or pass
// if pass/fail returns pass then show default score as 100% 9 Jan 2020
    if ($passfail == 'PASS' or $passfail == 'pass') {

        $gradeRoundvalue= '100';
    } else {

			$grades = \grade_get_course_grades($courseobj->id, $userid);
            $grademax = $grades->grademax;

            $gradeValue =(((int) $grades->grades[$userid]->str_grade));
            if(!$gradeValue==null){
                $gradeRoundvalue = (($gradeValue * 100)/$grademax);
            }else{
                $gradeRoundvalue=0;
            }
    }

		if(!empty($gradeRoundvalue)){
			$allusersscore[] = $gradeRoundvalue;
		}
	}
	if(count($allusersscore)>0){
		$allusersscore = array_filter($allusersscore);
		$averagescore = array_sum($allusersscore)/count($allusersscore);
	}	
	return $averagescore;
}
function get_scorm_status_ebdaa_passfail_blockscoremyreport($scormid,$course,$userid) {
    global $DB, $USER,$CFG;
    $scormstatus = '-';
//    $userid = $USER->id;
    /*
    $sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
    $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
*/
    $sql = "SELECT attempt
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
               ";
    
    $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));
    if (!empty($lastattempt)) {
        $sql = "SELECT value
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
        $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

        if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
            if($status == 'passed') { $stringshow = 'pass';}
            if($status == 'failed') { $stringshow = 'fail';}
            if($status == 'complete') { $stringshow = 'pass';}
            if($status == 'completed') { $stringshow = 'pass';}
            $scormstatus = strtoupper($stringshow);
                
        } else if ($status == 'incomplete') {
            $scormstatus = '-'; 
        }
    } else {

        //non scored scorm only returns lesson status
        $sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
            $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
            if (!empty($lastattempt)) {
                $sql = "SELECT value
                      FROM {scorm_scoes_track}
                     WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
                $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

                if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
                    if($status == 'passed') { $stringshow = 'pass';}
                    if($status == 'failed') { $stringshow = 'fail';}
                    if($status == 'complete') { $stringshow = 'pass';}
                    if($status == 'completed') { $stringshow = 'pass';}
                    $scormstatus = strtoupper($stringshow);
                        
                } else if ($status == 'incomplete') {
                    $scormstatus = '-'; 
                }
            } else {
                $scormstatus = '-';
            }

    }
    return $scormstatus;
}

/**
*Manju: This function will returns the status of user in particular course.
*@param $userid is the unique user id.
*@param $cid is unique course id.
*@return $coursestatus will return the status of particular user in course.
*/
function course_status_check($userid, $cid){
	global $DB,$CFG;
	$coursestatus = '';
	$statusprogress = array();
	$statusnotcompleted = array();
	$statuscompleted = array();
	$courseid = $DB->get_record('course',array('id'=>$cid));
	$cminfo = get_fast_modinfo($courseid, $userid);
	foreach($cminfo->get_cms() as $cm) {
		if($cm->modname == 'scorm')
		{
			$coursestatustemp = get_scorm_status_ebdaa($cm->instance,$courseid,$userid);

			if ($coursestatustemp == 'completed' OR $coursestatustemp == 'complete' OR $coursestatustemp == 'passed' OR $coursestatustemp == 'failed'){
				$statuscompleted[] = $courseid->id;
			} else if ($coursestatustemp == 'inprogress') {
				$statusprogress[] = $courseid->id;
			}else if ($coursestatustemp == 'notstarted') {
				$statusnotcompleted[] =$courseid->id; // this is actually not started
			}
		}

	} 

	if(!empty($statusprogress)){
		$coursestatus = get_string('inprogress', 'local_course_report');
	}
	elseif (!empty($statusnotcompleted)) {
		$coursestatus = get_string('notstarted', 'local_course_report');
	}
	elseif (!empty($statuscompleted)) {
		$coursestatus = get_string('complete', 'local_course_report');
	}
	return $coursestatus;

}

/**
*Manju: This function will returns the name, email ,score and status of an user in *particular course.[17/02/2020]
*@param $allusersofcourse have all the users id's enrolled in course.
*@param $courseid is unique course id.
*@return $userdetailsarray will return name, email ,score and status of an user.
*/
// Dilip: Added array() $selecteddepartment to filters users attending coarses in selecteddepartment
function enrolled_users_tabledata($allusersofcourse,$courseid,$selecteddepartment)
{
	global $DB;
	$userdetailsarray = [];
	foreach($allusersofcourse as  $userid){
		$userdetails = $DB->get_record('user', array('id' => $userid));
		if($userdetails){
			if ($selecteddepartment !='false' && count($selecteddepartment) > 0) {
				$depart = (strlen(trim($userdetails->department))>0) ? $userdetails->department : 'N/A';
			    if(in_array($depart, $selecteddepartment)){
					$fullname ='';
					$email ='';
					if(!empty($userdetails)){
						$fullname = $userdetails->firstname.' '.$userdetails->lastname;
						$email = $userdetails->email;
					}

					if(!empty($userid)){
						 $courseobj = $DB->get_record('course',array('id'=>$courseid));
										
								$cminfo = get_fast_modinfo($courseobj, $userid);
									foreach($cminfo->get_cms() as $cm) {
										if($cm->modname == 'scorm')
										 {
											$passfail = get_scorm_status_ebdaa_passfail_blockscoremyreport($cm->instance,0,$userid);
										 }
									 }

									//Mihir for score as 100 for completed or pass
					// if pass/fail returns pass then show default score as 100% 9 Jan 2020
						if ($passfail == 'PASS' or $passfail == 'pass') {

							$gradeRoundvalue= '100';
						} else {

								$grades = \grade_get_course_grades($courseobj->id, $userid);
								$grademax = $grades->grademax;

								$gradeValue =(((int) $grades->grades[$userid]->str_grade));
								if(!$gradeValue==null){
									$gradeRoundvalue = (($gradeValue * 100)/$grademax);
								}else{
									$gradeRoundvalue=0;
								}
						}		
					}
					$userstatus = course_status_check($userid,$courseid);

					$userdetailsarray[] = array(
						"id" => $userdetails->id,
						"fullname" => $fullname,
						"email" => $email,
						"department" => $userdetails->department,
						"score" => $gradeRoundvalue,
						"status"=> $userstatus
					);
			     }
			}
		}
	}
	return $userdetailsarray;
}


function track_course_status($userid){
	global $DB,$CFG;
	$completionpercentage = '';
	$returningdata =[]; 
	$statusprogress = [];
	$statusnotcompleted = [];
	$statuscompleted =[];
	$userobject = $DB->get_record('user',array('id'=>$userid));
	$userenrolledcourses = enrol_get_all_users_courses($userid);
	foreach ($userenrolledcourses as $courseid) {
		$cminfo = get_fast_modinfo($courseid, $userid);
		foreach($cminfo->get_cms() as $cm) {
			if($cm->modname == 'scorm')
			{
				$coursestatustemp = get_scorm_status_ebdaa($cm->instance,$courseid,$userid);
				if ($coursestatustemp == 'completed' OR $coursestatustemp == 'complete' OR $coursestatustemp == 'passed' OR $coursestatustemp == 'failed'){
					$statuscompleted[] = $courseid->id;
				} else if ($coursestatustemp == 'inprogress') {
					$statusprogress[] = $courseid->id;
				}else if ($coursestatustemp == 'notstarted') {
								$statusnotcompleted[] =$courseid->id; // this is actually not started
							}
						}

					} 
				}
				$notcompletedcount = count($statusnotcompleted);
				$inprogresscount = count($statusprogress);
				$completedcount = count($statuscompleted);

				if(!empty($completedcount)){
					$completionpercentage = ($completedcount * 100) /($notcompletedcount + $inprogresscount + $completedcount);
				}
				return $returningdata[] =[
					'uid'=>$userobject->id,
					'email'=>$userobject->email,
					'username'=>$userobject->firstname.' '.$userobject->lastname,
					'notcompleted'=>$notcompletedcount,
					'inprogress'=>$inprogresscount,
					'completed'=>$completedcount,
					'completionpercentage'=>round($completionpercentage, 2)];

				}
	/**
*Manju: This function will returns score an user in particular course.[17/02/2020]
*@param $userid is unique user id.
*@param $courseid is unique course id.
*@return $usercoursescore will return score of an user in course.
*/
function get_user_course_score($userid,$courseid)
{
	$grades = grade_get_course_grades($courseid, $userid);
	$grademax = $grades->grademax;

	$gradeValue =(((int) $grades->grades[$userid]->str_grade));
	if(!$gradeValue==null){
		$gradeRoundvalue = (($gradeValue * 100)/$grademax);
	}else{
		$gradeRoundvalue=' ';
	}
	if(!empty($gradeRoundvalue)){
		$usercoursescore = $gradeRoundvalue;
	}
	return $usercoursescore;
}

/*
*@param $status, 1 = In Progress, 2 = Not Started.
*@param $courseid is course id.
*@param $selecteddepartments is for filter user basis department
*/

function send_mail_to_users($courseid,$status,$selecteddepartments){
	global $DB;
	//get all enrolled users for this course
	$enrolledusers = get_enrolled_users_in_course($courseid);
	$users = completion_status_reminder_mail($enrolledusers, $courseid, $status);
	//Manju; 1/12/2020. changing sender as supporting user from admin.
	// $sender = get_admin();
	$sender = \core_user::get_support_user();
	$currentdate = time();
	foreach ($users as $userid) {
		$user = \core_user::get_user($userid);
		$depart = (strlen(trim($user->department))>0) ? $user->department : 'N/A';
	    if(in_array($depart, $selecteddepartments)){
	    	$config = get_config('emailcontent');
			$subject = $config->subjectforuserremindermail;
			$body = $config->userremindermailbody;
			$welcome = new \local_cs_reminder\message($courseid);
			$messageuser = $welcome->replace_values($user, $body);
			$messageusersubject = $welcome->replace_values($user, $subject);
	            // Email send to user 
			$mailstatus = email_to_user($user, $sender, $messageusersubject, html_to_text($messageuser),$messageuser);
			if($mailstatus){
				$insert  = new stdClass();
				$insert->userid = $user->id;
				$insert->created_date = $currentdate;
				$insert->courseid = $courseid;
				$insert->type = "courseremindermail";
				$DB->insert_record('local_cs_reminder',$insert);
			}
	    }
	}
}

//manjunath: course completion, in-progress and not started statistics.
function completion_status_reminder_mail($allusersid, $courseid, $status)
{
	global $DB;
	$completedusers = array();
	$notstartedusers = array();
	$inprogressusers = array();
	$coursecompltedusers = 0;
	$coursenotstartedusers = 0;
	$courseinprogressusers = 0;
	$statusprogress = array();
	$statusnotcompleted = array();
	$statuscompleted = array();
	$courseobject = $DB->get_record('course',array('id'=>$courseid));
	foreach ($allusersid as $userid) {
		$cminfo = get_fast_modinfo($courseobject, $userid);
		foreach($cminfo->get_cms() as $cm) {
			if($cm->modname == 'scorm')
			{
				$statusprogress = array();
				$statusnotcompleted = array();
				$statuscompleted = array();

				$coursestatustemp = get_scorm_status_ebdaa($cm->instance,$courseobject,$userid);

				if ($coursestatustemp == 'completed') {
					$statuscompleted[] =$courseobject->id;
					
				} else if ($coursestatustemp == 'inprogress') {
					$statusprogress[] = $courseobject->id;
					
				} else {
					$statusnotcompleted[] = $courseobject->id;
					
				}
			}
		} 
		if(!empty($statusprogress)){
			$inprogressusers[] = $userid;
		}
		elseif (!empty($statusnotcompleted)) {
			$notstartedusers[] = $userid;
		}
		elseif (!empty($statuscompleted)) {
			$completedusers[] = $userid;
		}
	}
	if($status == 1){
		$enrollmentstats = $inprogressusers;

	}elseif($status == 2){
		$enrollmentstats = $notstartedusers;

	}
	return $enrollmentstats;
}

function send_mail_to_teammember($userid){
	global $DB,$CFG;
	require_once($CFG->dirroot.'/enrol/externallib.php');

	//getting the users enrolled courses.
	$userscourses = enrol_get_users_courses($userid);
	//create an array of courses if the user has more courses.
	$usercoursearray=[];
	foreach ($userscourses as $course) {
		$usercoursearray[course_status_check($userid, $course->id)] = $course->id;
	}
	//$usercoursearray is having status of the enrolled users courses.
	//Manju; 12/12/2020. changing sender as supporting user from admin.
	// $sender = get_admin();
	$sender = \core_user::get_support_user();
	$currentdate = time();
	foreach ($usercoursearray as $coursestatus => $courseid) {
		if($coursestatus ==='In Progress' || $coursestatus ==='Not Started' ){
			$user = \core_user::get_user($userid);
			$config = get_config('emailcontent');
			$subject = $config->subjectforuserremindermail;
			$body = $config->userremindermailbody;
			$welcome = new \local_cs_reminder\message($courseid);
			$messageuser = $welcome->replace_values($user, $body);
			$messageusersubject = $welcome->replace_values($user, $subject);
            // Email send to user 
			$mailstatus = email_to_user($user, $sender, $messageusersubject, html_to_text($messageuser),$messageuser);
			if($mailstatus){
				$insert  = new stdClass();
				$insert->userid = $user->id;
				$insert->created_date = $currentdate;
				$insert->courseid = $courseid;
				$insert->type = "courseremindermail";
				$DB->insert_record('local_cs_reminder',$insert);
			}
		}
	}
}

function send_mail_to_reminder_cron($userid){

	global $DB,$CFG;
	$ndate = new DateTime("+7 day", new DateTimeZone($CFG->forcetimezone));
	require_once($CFG->dirroot.'/enrol/externallib.php');
	//getting the users enrolled courses.
	$userscourses = enrol_get_users_courses($userid);
	
	//create an array of courses if the user has more courses.
	$usercoursearray=[];
	foreach ($userscourses as $course) {
		$usercoursearray[$course->id] = course_status_check($userid, $course->id);
	}
	//$usercoursearray is having status of the enrolled users courses.
	//Manju: 12/12/2020. changed supporting user as sender from admin.
	// $sender = get_admin();
	$sender = \core_user::get_support_user();
	$currentdate = time();
		foreach ($usercoursearray as $courseid => $coursestatus) {
	if($coursestatus ==='In Progress' || $coursestatus ==='Not Started'){
	$due_check = $DB->get_record('course',array('id'=>$courseid));
	$new_set_time_zone_val = date('Y-m-d H:i',$due_check->enddate);
	$due_date_time = date('Y-m-d H:i', strtotime("$new_set_time_zone_val"));
	$curr_date_time = $ndate->format('Y-m-d H:i');
	if($curr_date_time == $due_date_time){
		
		$user = \core_user::get_user($userid);
			$config = get_config('emailcontent');
			$subject = $config->subjectforuserremindermailwithcron;
			$body = $config->userremindermailbodywithcron;
			$welcome = new \local_cs_reminder\message($courseid);
			$messageuser = $welcome->replace_values($user, $body);
			$messageusersubject = $welcome->replace_values($user, $subject);
            // Email send to user 
//	echo $userid.'</br>';
//echo 'course id ='.$courseid.'===user id ='.$userid.' = course status ='.$coursestatus.'</br>';

echo html_to_text($messageuser);
echo '</br>';
echo '============';
echo '</br>';
			$mailstatus = email_to_user($user, $sender, $messageusersubject, html_to_text($messageuser),$messageuser);
	
		
		if($mailstatus){
				$insert  = new stdClass();
				$insert->userid = $user->id;
				$insert->created_date = $currentdate;
				$insert->courseid = $courseid;
				$insert->type = "courseremindermail";
				$DB->insert_record('local_cs_reminder',$insert);
			}
		}
	}
	}
}

function send_mail_to_reminder_cron_one_day($userid){
	global $DB,$CFG;
	$ndate = new DateTime("+1 day", new DateTimeZone($CFG->forcetimezone));
	require_once($CFG->dirroot.'/enrol/externallib.php');
	//getting the users enrolled courses.
	$userscourses = enrol_get_users_courses($userid);
	
	//create an array of courses if the user has more courses.
	$usercoursearray=[];
	foreach ($userscourses as $course) {
		$usercoursearray[$course->id] = course_status_check($userid, $course->id);
	}
	//$usercoursearray is having status of the enrolled users courses.
	//Manju: 12/12/2020. changing support user as sender from admin.
	// $sender = get_admin();
	$sender = \core_user::get_support_user();
	$currentdate = time();
		foreach ($usercoursearray as $courseid => $coursestatus) {
	if($coursestatus ==='In Progress' || $coursestatus ==='Not Started'){
		$due_check = $DB->get_record('course',array('id'=>$courseid));
	$new_set_time_zone_val = date('Y-m-d H:i',$due_check->enddate);
	$due_date_time = date('Y-m-d H:i', strtotime("$new_set_time_zone_val"));
	$curr_date_time = $ndate->format('Y-m-d H:i');
		if($curr_date_time == $due_date_time){
			$user = \core_user::get_user($userid);
			$config = get_config('emailcontent');
			$subject = $config->subjectforuserremindermailwithcrononeday;
			$body = $config->userremindermailbodywithcrononeday;
			$welcome = new \local_cs_reminder\message($courseid);
			$messageuser = $welcome->replace_values($user, $body);
			$messageusersubject = $welcome->replace_values($user, $subject);
            // Email send to user 
//	echo $userid.'</br>';
//echo 'course id ='.$courseid.'===user id ='.$userid.' = course status ='.$coursestatus.'</br>';

echo html_to_text($messageuser);
echo '</br>';
echo '============';
echo '</br>';
			$mailstatus = email_to_user($user, $sender, $messageusersubject, html_to_text($messageuser),$messageuser);
	
		
		if($mailstatus){
				$insert  = new stdClass();
				$insert->userid = $user->id;
				$insert->created_date = $currentdate;
				$insert->courseid = $courseid;
				$insert->type = "courseremindermail";
				$DB->insert_record('local_cs_reminder',$insert);
			}
		}
	}
	}
}

//manju: function to save new rule for autocourse enrollment.[20/02/2020].
function save_new_rule($data){
	global $DB,$CFG,$USER;
	$insert  = new stdClass();
	$insert->rulename = $data->name;
	$insert->ruledescription = $data->ruledescription;
	$insert->rulekey = $data->rulekey;
	$insert->rulevalue = $data->rulevalue_as == 'select'? '' : $data->rulevalue_as;
 	
	if($data->selectall == 0){
		$insert->courses = implode(",",$data->courseid);
	}else{
		$allcoursearray=[];
		$sql= "SELECT * FROM {course} WHERE visible = 1 AND id !=1 GROUP BY fullname ASC";
		$allcourses = $DB->get_records_sql($sql);
		foreach ($allcourses as $course) {
			$allcoursearray[]=$course->id;
		}
		$insert->courses = implode(",",$allcoursearray);
	}
	$insert->companyid = $USER->company->id;

	
	$inserteddata = $DB->insert_record('local_course_management',$insert);

	if($inserteddata){
	//enroll users based on this rule.
		//get the newly added rule.
		$newrulesql="SELECT * FROM {local_course_management} ORDER BY id DESC LIMIT 1";
		$newrule=$DB->get_record_sql($newrulesql);
		//check for department or manager based rule. 0-Department, 1 - Manager.
		if($newrule->rulekey == 0){
			//get all users having this department.
			$alldepartments = get_alldepartments_ebdaa();
			$allusers=$DB->get_records('user',array('department'=>$alldepartments[$newrule->rulevalue],'deleted'=>0));
			//get all courses to be enrol in this rule.
			if(!empty($allusers)){
				$allcourse = explode(",", $newrule->courses);
				foreach ($allusers as $induser) {
					if(!empty($allcourse)){
						foreach ($allcourse as $crsid) {
							$enroll=all_user_enroll_into_course($induser,$crsid);
						}
					}
				}
			}
		}elseif($newrule->rulekey == 1){
			//get all the users having this manager.
			$managermail=$DB->get_field('user', 'email', array('id'=>$newrule->rulevalue));
			//get all the user having this manager.
			$allmusers=$DB->get_records('user',array('alternatename'=>$managermail,'deleted'=>0));
			if(!empty($allmusers)){
				$allcourse = explode(",", $newrule->courses);
				foreach ($allmusers as $induser) {
					if(!empty($allcourse)){
						foreach ($allcourse as $crsid) {
							$enroll=all_user_enroll_into_course($induser,$crsid);
						}
					}
				}
			}

		}
		elseif($newrule->rulekey == 2){
			//get all the users having this All User.
			$allmusers=$DB->get_records('user',array('deleted'=>0));
			if(!empty($allmusers)){
				$allcourse = explode(",", $newrule->courses);
				foreach ($allmusers as $induser) {
					if(!empty($allcourse)){
						foreach ($allcourse as $crsid) {
							$enroll=all_user_enroll_into_course($induser,$crsid);
						}
					}
				}
				
			}

		
		return true;
	}else{
		return false;
	}
}
}
function get_alldepartments_ebdaa($arg=false){
	global $DB,$CFG;
	$departmentoptions=[];
	$config = get_config('coursemanagement');
	$departmentsarray=$config->userdepartment;
	$exploadedarray = explode(",",$departmentsarray);
	foreach ($exploadedarray as $arraykey => $arrayvalue) {
		if($arrayvalue != ""){
			if($arg){
				/* Created by Manish */
				//assign value as key
				// Does the user and department is one to one?
				$departmentoptions[$arrayvalue]=$arrayvalue;
			}else{
				$departmentoptions[]=$arrayvalue;
			}
		}
	}
	return $departmentoptions;
}

function get_all_managers_ebdaa(){
	global $DB,$CFG;
	$managersarray =[];
	$managerquery="SELECT id, alternatename as managermail FROM {user} WHERE alternatename != 0 OR alternatename IS NOT NULL";
	$managersmail=$DB->get_records_sql($managerquery);
	foreach ($managersmail as $mail) {
		if(!empty($mail->managermail)){
			$manager=$DB->get_record('user',array('email'=>$mail->managermail,'deleted'=>0));
			if(!empty($manager)){
				$managersarray[$manager->id] = fullname($manager);
			}
			
		}
	}
	return $managersarray;
}
function get_all_users_ebdaa(){
	global $DB,$CFG;
	$managersarray =[];
	     	$all_users=$DB->get_records('user',array('deleted'=>0));
			foreach($all_users as $data_all){
				$managersarray[$data_all->id] = fullname($data_all);
		}
	return $managersarray;
}


//this function is used to check user is enroll or not if not enroll then enroll user into that course.
function all_user_enroll_into_course($userdetails,$cid){
	global $DB,$CFG;
	$contextid = context_course::instance($cid);
	//if(!is_enrolled($contextid,$userdetails->id)){
		$enrolplugin = enrol_get_plugin('manual');
		$instances = enrol_get_instances($cid, true);
		foreach ($instances as $instance) {
			if ($instance->enrol === 'manual') {
				break;
			}
		}
		if ($instance->enrol !== 'manual') {
			//throw new coding_exception('No manual enrol plugin in course');
		}
		$role = $DB->get_record('role', array('shortname' => 'student'), '*', MUST_EXIST);
		$enrolplugin->enrol_user($instance, $userdetails->id, $role->id);  
	
//	} 

	
}

	/**
 *
 * @param type $scormid
 * @param type $userid
 */
	function get_scorm_status_ebdaa_blocktable_link($scormid,$course) {
		global $DB, $USER,$CFG;
		$status = '-';
		$userid = $USER->id;
		$sql = "SELECT attempt
		FROM {scorm_scoes_track}
		WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
		";

		$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));

		if (!empty($lastattempt)) {
			$sql = "SELECT value
			FROM {scorm_scoes_track}
			WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
			$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

			if(!empty($status)) {
				if ($status == 'completed' OR $status == 'complete' OR $status == 'passed' OR $status == 'failed'){
					$scormstatus = strtoupper('complete');
					$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
					<button class="btn btn-sm bg-green w-125" style="cursor:pointer;">'.$scormstatus.'</button>
					</a>';
					
				} else if ($status == 'incomplete') {
					$scormstatus = strtoupper('in progress');
					$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
					<button class="btn btn-sm bg-yellow w-125" style="cursor:pointer;">'.$scormstatus.'</button>
					</a>';
				}
			}
		} else {
			$sql = "SELECT MAX(attempt)
			FROM {scorm_scoes_track}
			WHERE userid = ? AND scormid = ?
			";
			$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
			if (!empty($lastattempt)) {
				$sql = "SELECT value
				FROM {scorm_scoes_track}
				WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
				$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));
				if ($status == 'completed' OR $status == 'complete' OR $status == 'passed' OR $status == 'failed'){
					$scormstatus = strtoupper('complete');
					$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
					<button class="btn btn-sm bg-green w-125" style="cursor:pointer;">'.$scormstatus.'</button>
					</a>';

				} else if ($status == 'incomplete') {
					$scormstatus = strtoupper('in progress');
					$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
					<button class="btn btn-sm bg-yellow w-125" style="cursor:pointer;">'.$scormstatus.'</button>
					</a>';
				}
			 else {
				$scormstatus = strtoupper('not started');
				$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
				<button class="btn btn-sm bg-red w-125" style="cursor:pointer;">'.$scormstatus.'</button>
				</a>';
				}
			}
		else{
			$scormstatus = strtoupper('not started');
			$scormlink = '<a href='.$CFG->wwwroot.'/course/view.php?id='.$course->id.'>
			<button class="btn btn-sm bg-red w-125" style="cursor:pointer;">'.$scormstatus.'</button>
			</a>';
				
			}
		}
		return $scormlink;
	}

//Manju: this function will delete enrolled users.[25/02/2020]
	function unenroll_users_enrolled_in_rule($ruleid){
		global $DB,$CFG;
	//rulekey 0 => Department and 1=> Manager.
	//get the data from the autoenrollment table.
		$autoenrolldata = $DB->get_record('local_course_management',array('id'=>$ruleid));
		$courses = explode(",", $autoenrolldata->courses);
		if($autoenrolldata->rulekey == 0){
			$alldepartments = get_alldepartments_ebdaa();
			if (array_key_exists($autoenrolldata->rulevalue, $alldepartments)) {
				$department = $alldepartments[$autoenrolldata->rulevalue];
				$getallusers = $DB->get_records('user',array('department'=>$department,'deleted'=>0));
				foreach ($getallusers as  $singleuser) {
					foreach ($courses as $ckey => $cvalue) {
						$instance = $DB->get_record('enrol', array('courseid' => $cvalue, 'enrol' => 'manual'));
						$plugin = enrol_get_plugin($instance->enrol);
						$return = $plugin->unenrol_user($instance, $singleuser->id);
					}
				}
			}
		}
		if($autoenrolldata->rulekey == 1){
			$allmanagers = get_all_managers_ebdaa();
			if (array_key_exists($autoenrolldata->rulevalue, $allmanagers)) {
				$manager = $allmanagers[$autoenrolldata->rulevalue];
				$managermail = $DB->get_field('user', 'email',array('id'=>$autoenrolldata->rulevalue));
				$getallusers = $DB->get_records('user',array('alternatename'=>$managermail,'deleted'=>0));
				foreach ($getallusers as  $singleuser) {
					foreach ($courses as $ckey => $cvalue) {
						$instance = $DB->get_record('enrol', array('courseid' => $cvalue, 'enrol' => 'manual'));
						$plugin = enrol_get_plugin($instance->enrol);
						$return = $plugin->unenrol_user($instance, $singleuser->id);
					}
				}
			}

		}
		
		
	}


//Manju: This function will return an array containing all certificates data.
	function get_allcertificates($userid=null){
		global $DB,$CFG;
		//get all the enrolled courses of an user.
		$allcourses=enrol_get_all_users_courses($userid);
		$retarray=[];
		foreach ($allcourses as $courses) {
			//coursename.
			$coursename=$courses->fullname;
			//course completion date.
			$courseid=$courses->id;

			$cminfo = get_fast_modinfo($courseid, $userid);
			foreach($cminfo->get_cms() as $cm) {
				if($cm->modname == 'scorm')
				{
					$scormid=$cm->instance;
					$sql = "SELECT attempt
					FROM {scorm_scoes_track}
					WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
					";
					$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));
					if(empty($lastattempt)){
						$sql = "SELECT MAX(attempt)
						FROM {scorm_scoes_track}
						WHERE userid = ? AND scormid = ?
						";
						$lastattempt2 = $DB->get_field_sql($sql, array($userid, $scormid));
						if (!empty($lastattempt2)) {
							$sql = "SELECT timemodified
							FROM {scorm_scoes_track}
							WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status' AND value ='completed'";
							$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt2));
						}
					}
				}
			}
			if(!empty($status)){
				$retarray[]=array('coursename'=>$coursename,
					'courseid'=>$courseid,
					'completion'=>$status);
			}
		}
		return $retarray;
	}

//course completion date for single course.
	function course_completion_date($userid,$courseid){
		global $DB,$CFG;
		$status='';
		$cminfo = get_fast_modinfo($courseid, $userid);
		foreach($cminfo->get_cms() as $cm) {
			if($cm->modname == 'scorm')
			{
				$scormid=$cm->instance;
				$sql = "SELECT attempt
				FROM {scorm_scoes_track}
				WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
				";
				$lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));
				if(empty($lastattempt)){
					$sql = "SELECT MAX(attempt)
					FROM {scorm_scoes_track}
					WHERE userid = ? AND scormid = ?
					";
					$lastattempt2 = $DB->get_field_sql($sql, array($userid, $scormid));
					if (!empty($lastattempt2)) {
						$sql = "SELECT timemodified
						FROM {scorm_scoes_track}
						WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status' AND value ='completed'";
						$status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt2));
					}
				}else{
					$sql1 = "SELECT timemodified
					FROM {scorm_scoes_track}
					WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
					";
					$result = $DB->get_record_sql($sql1,array($userid, $scormid,$userid, $scormid));
					if(!empty($result)){
						$status = $result->timemodified;
					}else{
						return false;
					}

				}
			}
		}
		return $status;
	}
	/* new dashboard start */
	function add3dots($string, $repl, $limit) 
	{
		global $DB;
		if(mb_strlen($string) > $limit) 
		{
			return mb_substr($string, 0, $limit) . $repl;
		}
		else 
		{
			return $string;
		}
	}

// Dilip: Added array() $selecteddepartment to filters users attending coarses in selecteddepartment
function get_course_statistics_c_dashboard($allusersid,$courseid,$selecteddepartment)
{
	global $DB;
		$coursecompltedusers = 0;
		$coursenotstartedusers = 0;
		$courseinprogressusers = 0;
		$statusprogress = array();
		$statusnotcompleted = array();
		$statuscompleted = array();
		$courseobject = $DB->get_record('course',array('id'=>$courseid));
		$notcompleted = get_string('notcompleted', 'local_course_report');
		$complete = get_string('complete', 'local_course_report');
		$inprogress = get_string('inprogress', 'local_course_report');
		foreach ($allusersid as $userid) {
			$userobject = $DB->get_record('user', array('id' => $userid));
			if($userobject){
				$depart = (strlen(trim($userobject->department))>0) ? $userobject->department : 'N/A';
				if ($selecteddepartment != 'false') {
				    if(in_array($depart, $selecteddepartment)){
						$cstatus=course_status_check_with_overdue($userobject->id, $courseobject->id);
						$course_overdue_check = 'false';
						if($courseobject->enddate != 0){
							$curr_date = date('Y-m-d h:i:s');
							$c_enddate = date('Y-m-d h:i:s',$courseobject->enddate);
							$course_overdue_check = $curr_date > $c_enddate ? 'true':'false';
						}
						if($cstatus == $complete){
							$coursecompltedusers++;
						}
						
						elseif ($cstatus == $notcompleted && $course_overdue_check == 'false') {
							$courseinprogressusers++;
						}
						elseif ($cstatus == $notcompleted && $course_overdue_check == 'true') {
							$coursenotstartedusers++;
						}
				    }
				}else{
			        $departments = get_alldepartments_ebdaa(true);
			        $departments["N/A"] = "N/A";
		         	if(in_array($depart, $departments)){
				    	$cstatus=course_status_check_with_overdue($userobject->id, $courseobject->id);
						$course_overdue_check = 'false';
						if($courseobject->enddate != 0){
							$curr_date = date('Y-m-d h:i:s');
							$c_enddate = date('Y-m-d h:i:s',$courseobject->enddate);
							$course_overdue_check = $curr_date > $c_enddate ? 'true':'false';
						}
						if($cstatus == $complete){
							$coursecompltedusers++;
						}
						
						elseif ($cstatus == $notcompleted && $course_overdue_check == 'false') {
							$courseinprogressusers++;
						}
						elseif ($cstatus == $notcompleted && $course_overdue_check == 'true') {
							$coursenotstartedusers++;
						}
			         }
			    }
			}
		}
		$enrollmentstats = array($coursecompltedusers,$courseinprogressusers,$coursenotstartedusers);
		return $enrollmentstats;
}
function course_status_check_with_overdue($userid, $cid){
	global $DB,$CFG;
	$coursestatus = '';
	$statusprogress = array();
	$statusnotcompleted = array();
	$statuscompleted = array();
	$courseid = $DB->get_record('course',array('id'=>$cid));
	$cminfo = get_fast_modinfo($courseid, $userid);
	foreach($cminfo->get_cms() as $cm) {
		if($cm->modname == 'scorm')
		{
			$coursestatustemp = get_scorm_status_ebdaa($cm->instance,$courseid,$userid);

			if ($coursestatustemp == 'completed' OR $coursestatustemp == 'complete' OR $coursestatustemp == 'passed' OR $coursestatustemp == 'failed'){
				$statuscompleted[] = $courseid->id;
			} else if ($coursestatustemp == 'inprogress') {
				$statusprogress[] = $courseid->id;
			}else if ($coursestatustemp == 'notstarted') {
								$statusnotcompleted[] = $courseid->id; // this is actually not started
							}
						}

					} 

					if(!empty($statusprogress) || !empty($statusnotcompleted)){
						$coursestatus = get_string('notcompleted', 'local_course_report');
					}
					elseif (!empty($statuscompleted)) {
						$coursestatus = get_string('complete', 'local_course_report');
					}
					return $coursestatus;
				
				}

function get_department($departmentId){
	if(is_array($departmentId)){
		$output = [];
		foreach ($departmentId as $key => $value) {
			if($value){
				foreach ($value as $k => $v){
					array_push($output, $v);
				}
			}
	
		}
		$selecteddepartment = $output;
	}else{
		$selecteddepartment =  explode(",",$departmentId);
	}
	return $selecteddepartment;
}

function check_availability_enrolled_user_courses(){
	global $DB;
	$availability = 0;
	$departments = get_alldepartments_ebdaa(true);
    $departments["N/A"] = "N/A";
	$sql = "SELECT id,fullname,category,enddate FROM {course}
	WHERE visible = 1
	AND id != 1";
	$allcourses = $DB->get_records_sql($sql);
	if($allcourses){
		foreach ($allcourses as $course){
			$allusersofcourse = get_users_enrolled_in_courses($course->id);
		    if (!empty($allusersofcourse)){
		        $totalusers = count($allusersofcourse);
		        if (!empty($allusersofcourse)){

		            $coursestatistics = get_course_statistics_c_dashboard($allusersofcourse, $course->id, $departments);
		        }
		        $chartdata = [];

		        if (!empty($coursestatistics) && count($coursestatistics) > 0){
		            if($coursestatistics[0] > 0 || $coursestatistics[1] > 0 || $coursestatistics[2] > 0){
		            	$availability++;
		            }
		        }
		    }
		}
	}

	return $availability;
}

