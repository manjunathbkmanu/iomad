<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * You may have settings in your plugin
 *
 * @package    local_course_management
 * @copyright  Manjunath  B K<manjunathbk@edzlearn10.com>
 * @license    http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
global $OUTPUT, $CFG;
require_once($CFG->libdir.'/formslib.php');
require_login();
class local_autoenrollment_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        $id = $USER->id;
        $mform =& $this->_form;
        //header
        $mform->addElement('header','addautoenrollmentrule',get_string('addautoenrollmentrule','local_course_management'));
        //rule name.
        $mform->addElement('text', 'name', get_string('rulename', 'local_course_management'));
        $mform->setType('name', PARAM_TEXT);
        //rule description.
        
        $mform->addElement('hidden', 'rulevalue_as');
        $mform->setType('rulevalue_as', PARAM_TEXT);
        

        $mform->addElement('textarea', 'ruledescription', get_string('ruledescription', 'local_course_management'),'wrap="virtual" rows="10" cols="50"');
        $mform->setType('ruledescription', PARAM_TEXT);
        //rule key
        $rulekeys = array(
            '2'=>get_string('all_users','local_course_management'),
            '0'=>get_string('department','local_course_management'),
            '1'=>get_string('manager','local_course_management'));
        $mform->addElement('select', 'rulekey', get_string('selectkey', 'local_course_management'), $rulekeys,array('onchange'=>'selectRulevalue()'));
        $mform->setType('rulekey', PARAM_TEXT);
        //rule value1
        $departmentoptions=[];
        $config = get_config('coursemanagement');
        $departmentsarray=$config->userdepartment;
        $exploadedarray = explode(",",$departmentsarray);
        foreach ($exploadedarray as $arraykey => $arrayvalue) {
            if($arrayvalue != ""){
                $departmentoptions[]=$arrayvalue;
            }
        }
        //manager code 
        $managersarray =[];
        $managerquery="SELECT id, icq as managermail FROM {user} WHERE icq != 0 OR icq IS NOT NULL";
        $managersmail=$DB->get_records_sql($managerquery);
        foreach ($managersmail as $mail) {
            if(!empty($mail->managermail)){
                $manager=$DB->get_record('user',array('email'=>$mail->managermail));
                if(!empty($manager)){
                    $managersarray[$manager->id] = fullname($manager);
                }
            }
        }
        $addvalue = ($managersarray+$departmentoptions);

        $mform->addElement('select', 'rulevalue', get_string('selectvalue', 'local_course_management'), $addvalue);

        $mform->addElement('advcheckbox', 'selectall', get_string('selectallcourse', 'local_course_management'),null,array('onclick'=>'selectAll();'));

        //helping text to select.
        $mform->addElement('static', 'description', '',get_string('helpdescription', 'local_course_management'));

        // $coursesql = 'SELECT * FROM {course} WHERE visible = 1 AND id !=1 GROUP BY fullname ASC';
        if(!is_siteadmin()){
            $companyid = $DB->get_field('company_users','companyid',array('userid'=>$USER->id));
            $coursesql ="SELECT c.* FROM {course} AS c
            JOIN {company_course} AS cc ON cc.courseid = c.id WHERE c.visible = 1 AND c.id != 1 AND cc.companyid =$companyid ORDER BY c.fullname ASC";
        }else{
            $coursesql = 'SELECT * FROM {course} WHERE visible = 1 AND id !=1';
        }
        $allcourses = $DB->get_records_sql($coursesql);
        $course_options = [];
        foreach($allcourses as $course){
            $course_options[$course->id] = $course->fullname;
        }
        $options = array(                                
            'multiple' => true,                                      
            'noselectionstring' => get_string('selectcourse', 'local_course_management'),
            'placeholder' => get_string('selectcourse', 'local_course_management'),                                                     
        );
        if(!empty($options)) {
            $select = $mform->addElement('select','courseid', get_string('selectcourse','local_course_management'), $course_options,$options);
        }
        $mform->addElement('submit', 'submitbutton', get_string('submit', 'local_course_management'));
    }

}