<?php
// This file is part of the promocode Form plugin for Moodle - http://moodle.org/
//
// promocode Form is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// promocode Form is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with promocode Form.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin for Moodle is used to send emails through a web form.
 *
 * @package    local_phish
 * @copyright  2016-2018 TNG Consulting Inc. - www.tngconsulting.ca
 * @author     Michael Milette
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
global $CFG;
if ( $hassiteconfig ){

	$settings = new admin_settingpage( 'local_phishing', get_string('pluginname','local_phishing') );
	// Create 
	$ADMIN->add( 'localplugins', $settings );
	// Add a setting field to the settings for this page
	$settings->add( new admin_setting_configtext(
		'local_phishing/api',
		get_string(
			'api',
			'local_phishing'
		),
		'',
		'',
		PARAM_TEXT
	));
	$settings->add( new admin_setting_configtext(
		'local_phishing/apiusername',
		get_string(
			'username',
			'local_phishing'
		),
		'',
		'',
		PARAM_TEXT
	));
	$settings->add( new admin_setting_configtext(
		'local_phishing/urlip',
		get_string(
			'urlip',
			'local_phishing'
		),
		'',
		'',
		PARAM_TEXT
	));
// $defaulturl = $CFG->wwwroot;
// 	$settings->add( new admin_setting_configtext(
// 	'local_phishing/landingpageurl',
// 	get_string(
// 		'landingpageurl',
// 		'local_phishing'
// 	),
// 	'',
// 	'MANJU',
// 	PARAM_TEXT
// ));

//manju: starts here.
	$name = 'local_phishing/landingpageurl';
	$title = get_string('landingpageurl', 'local_phishing');
	$description = get_string('landingpageurl', 'local_phishing');
	$default = '';
	$setting = new admin_setting_configtext($name, $title, $description, $default);    
	$settings->add($setting);
//ends
	
	$default = 'http://';
	$choices = array(
		'http://' =>get_string('prohttp','local_phishing'),
		'https://' =>get_string('prohttps','local_phishing')
	);
	$settings->add( new admin_setting_configselect('local_phishing/protocol'.$postfix,get_string(
		'url','local_phishing'),'',$default,$choices));
	//add external page 
	//tempplate page 
	$temp = new admin_externalpage('templatelist', new lang_string('listoftemp', 'local_phish'), $CFG->wwwroot . '/local/phishing/templatelist.php','local/phish:overall'
);
	$ADMIN->add('localplugins', $temp);

    //landing page
	$temp = new admin_externalpage('landingpagelist', new lang_string('listoflandingpage', 'local_phish'), $CFG->wwwroot . '/local/phishing/landingpagelist.php','local/phish:overall'
);
	$ADMIN->add('localplugins', $temp);
   //sending profile
	$temp = new admin_externalpage('sendingprofilelist', new lang_string('sendingprofilelist', 'local_phish'), $CFG->wwwroot . '/local/phishing/sendingprofilelist.php','local/phish:overall'
);
	$ADMIN->add('localplugins', $temp); 
    //campaign
	$temp = new admin_externalpage('recentcamp', new lang_string('recentcamp', 'local_phish'), $CFG->wwwroot . '/local/phishing/campaignsumlist.php','local/phish:overall'
);
	$ADMIN->add('localplugins', $temp);

	$temp = new admin_externalpage('createcamp', new lang_string('createcamp', 'local_phish'), $CFG->wwwroot . '/local/phishing/action/campaign_create.php','local/phish:overall'
);
	$ADMIN->add('localplugins', $temp);
    //user create 
	$temp = new admin_externalpage('listofuser', new lang_string('listofuser', 'local_phish'), $CFG->wwwroot . '/local/phishing/usergrouplist.php','local/phish:overall'
);
	$ADMIN->add('localplugins', $temp);

	
}
