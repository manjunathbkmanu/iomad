<?php 
$string['title'] = 'EBDAA License';
$string['licensesettings'] = 'EBDAA License Settings';
$string['licensecode'] = 'License Key';
$string['clientname'] = 'Client Name';
$string['save'] = 'Save';
$string['licenseandclient'] = 'Please Enter License Key and Client Name';
$string['verify'] = 'Verify License';
$string['verifylicence'] = 'Verify';