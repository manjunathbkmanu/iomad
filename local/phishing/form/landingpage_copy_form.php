<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phishing
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../lib.php');
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
require_once($CFG->libdir.'/formslib.php');
class landingpage_copy_form extends moodleform {
	function definition() {
		global $CFG,$USER;
		$id = optional_param('id',null, PARAM_INT);
		$mform = $this->_form; 
		//orgaziation name field
		$companyid = $USER->company->id;
		//Manju: changed the plugin name from local_gophish to local_phishing.08/02/2021.
		$apikey = get_config('local_phishing'.$companyid, 'api');
		$urlip = get_config('local_phishing'.$companyid, 'urlip');
		$landingpage = get_page_value($urlip,$apikey,$id);	
		$context = context_system::instance();
		$mform->addElement('static','pagehrd',get_string('copyladingpage','local_phishing'),'');
		$mform->addElement('html', '<hr>');
		// Add some extra hidden fields.
		$mform->addElement('hidden', 'id',$id);
        //$mform->setType('id', core_user::get_property_type('id'));
		$mform->setType('id', PARAM_INT);
        //$mform->setType('status', core_user::get_property_type('status'));
		$mform->setType('edit', PARAM_INT);
		//name of the template
		$mform->addElement('text', 'name',
         get_string('name','local_phishing')); // Add elements to your form
		$mform->addRule('name', get_string('required'), 'required', null, 'client');
		// $mform->addElement('static','name1','',get_string('landing_name','local_phishing'));
		$mform->setType('name', PARAM_TEXT);
		$mform->setDefault('name', 'Copy of '.$landingpage->name);

		//landing page url hre 
		$mform->addElement('text', 'url',
         get_string('url','local_phishing')); // Add elements to your form
		//$mform->addRule('name', get_string('required'), 'required', null, 'client');
		$mform->setType('url', PARAM_RAW);
		$mform->addElement('static','url1','',get_string('url_static','local_phishing'));
		$mform->addElement('button', 'url_site', get_string("import",'local_phishing'),array('onclick'=>'importSite()'));

		$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'noclean' => true, 'context' => $context);
		$mform->addElement('editor','html',get_string('html','local_phishing'),null,$editoroptions);
		$mform->setDefault('html', $landingpage->html);
		
		$mform->addElement('static','html1','',get_string('html_help','local_phishing'));

		//new landing page 
		//
		$mform->addElement('advcheckbox', 'capture_credentials', '', get_string('capture_credentials','local_phishing'), array('group' => 1,'onclick'=>'clickMe()'), array(0, 1));
		//
		$mform->addElement('html', '<div id="qheader" style="display:none">');
		// $mform->addElement('advcheckbox', 'capture_passwords', '',  get_string('capture_passwords','local_phishing'), array('group' => 1), array(0, 1));
		//
		$mform->addElement('text', 'redirect_url',
         get_string('redirect_url','local_phishing')); // Add elements to your form
		//$mform->addRule('name', get_string('required'), 'required', null, 'client');
		$mform->setType('redirect_url', PARAM_RAW);
		$mform->addElement('html', '</div>');
		//value can store here editable 
        $mform->addElement('hidden', 'editorvalue','',array('id'=>'editorvalueid'));
		$mform->setType('editorvalue', PARAM_RAW);
		$this->add_action_buttons();
     }
 }
