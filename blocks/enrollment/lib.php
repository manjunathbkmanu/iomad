<?php

function enrollment_display_courses_options($userid) {
    $courses = enrollment_get_courses_list($userid);
    foreach ($courses as $course) {
        $options .= '<option value="' . $course->id . '">' . $course->shortname . '</option>';
    }
    return $options;
}

function enrollment_get_courses_list($userid) {
    global $DB;
    $req = "
    SELECT DISTINCT (c.id), c.shortname
    FROM {course} c
    WHERE
    c.id != 1
    AND
    c.id NOT IN (
        SELECT DISTINCT(c.id)
        FROM {enrol} e, {course} c, {user_enrolments} ue
        WHERE
        ue.userid = " . $userid . "
        AND ue.enrolid = e.id
        AND e.courseid = c.id
        )
        ";
        $courses = array();
        $result = $DB->get_records_sql($req);
        foreach ($result as $course) {
            $courses[$course->id] = $course->shortname;
        }
        return $courses;
    }

    function enrollment_display_users_options($userid) {
        global $DB,$CFG;
        if(!empty($userid)){
            $useremail = $DB->get_field('user', 'email', array('id'=>$userid));
            $users = $DB->get_records('user',array('deleted'=>0,'suspended'=>0,'alternatename'=>$useremail));
        }else{
            $users = enrollment_get_users_list();
        }

        $options = '';
        $options.='<option value="">'.get_string('selectuser', 'block_enrollment').'</option>';
        foreach ($users as $user) {
            $options .= '<option value="' . $user->id . '">' . $user->lastname . ' ' . $user->firstname . '</option>';
        }
        return $options;
    }

    function enrollment_get_users_list() {
        global $DB,$USER;
        if(!is_siteadmin()){
            $companyid = $DB->get_field('company_users','companyid',array('userid'=>$USER->id));
            $usersql ="SELECT u.* FROM {user} AS u
            JOIN {company_users} AS cu ON cu.userid = u.id WHERE u.deleted = 0 AND u.suspended = 0 AND u.id > 2 AND cu.companyid =$companyid ORDER BY u.firstname ASC";
        }else{
            $usersql ="SELECT * FROM {user} WHERE deleted = 0 AND suspended = 0 AND id > 2 ORDER BY firstname ASC";
        }
    return $DB->get_records_sql($usersql);
    }

    function enrollment_enrol_user($userid, $courseid, $role, $timestart, $timeend) {
        global $DB, $CFG;
        $instance = $DB->get_record('enrol', array('enrol' => 'manual', 'courseid' => $courseid));
        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);

        if (!$enrol_manual = enrol_get_plugin('manual')) {
            throw new coding_exception('Can not instantiate enrol_manual');
        }

        if (!empty($timestart) && !empty($timeend) && $timeend < $timestart) {
            print_error('La date de fin doit etre supérieure à la date de début', null, $CFG->wwwroot . '/blocks/enrollment/enrollment.php');
        }
        if (empty($timestart)) {
            $timestart = $course->startdate;
        }
        if (empty($timeend)) {
            $timeend = 0;
        }
        $enrol_manual->enrol_user($instance, $userid, $role, $timestart, $timeend);
    }

    function enrollment_get_role_name($roleid) {
        global $DB;
        $sql = '
        SELECT
            *
        FROM
        {role}
        WHERE
        id= ' . $roleid;
        return $DB->get_record_sql($sql);
    }

    function enrollment_get_roles() {
        $roles = array();
        $rolesContext = get_roles_for_contextlevels(CONTEXT_COURSE);
        foreach ($rolesContext as $roleContext) {
            $role = enrollment_get_role_name($roleContext);
            $roles[] = $role;
        }
        return $roles;
    }

    function enrollment_display_roles() {
        $roles = enrollment_get_roles();
        $options = '';
        foreach ($roles as $role) {
            $role->name = role_get_name($role);
            $selected = $role->id == 5 ? 'selected' : '';
            $options .= '<option value="' . $role->id . '" ' . $selected . '>' . $role->name . '</option>';
        }
        return $options;
    }
