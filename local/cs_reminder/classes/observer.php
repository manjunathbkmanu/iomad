<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin sends users a welcome message after logging in
 * and notify a moderator a new user has been added
 * it has a settings page that allow you to configure the messages
 * send.
 *
 * @package    local
 * @subpackage cs_reminder
 * @copyright  Manjunath
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


namespace local_cs_reminder;
defined('MOODLE_INTERNAL') || die();
class observer {

    public static function send_remainder_email(\core\event\user_created $event) {
        global $CFG, $SITE,$DB,$USER;
        $eventdata = $event->get_data();
        $user = \core_user::get_user($eventdata['objectid']);
        //Manju: 12/12/2020.Changing support user as sender instead of admin.
        // $sender = get_admin();
        $sender = \core_user::get_support_user();
        $currentdate = time();
        if (!empty($user->email)) {
            $companyid = $USER->company->id;
            //get all settings configration value from emailcontent setting.
            $config = get_config('emailcontent'.$companyid);
            $moderator = clone($sender);   
            $messageuser = $config->newusermailbody; 
            $messageusersubject = $config->subjectfornewuseraccountmail;
            $welcome = new \local_cs_reminder\message(null);
            $messageuser = $welcome->replace_values($user, $messageuser);
            $messageusersubject = $welcome->replace_values($user, $messageusersubject);
            // Email sending to user                              
            $mailstatus = email_to_user($user, $sender, $messageusersubject, 
            html_to_text($messageuser),$messageuser);
            if($mailstatus){
            $sql='UPDATE {user} SET address="" WHERE id='.$user->id.'';
            $DB->execute($sql);
            $insert  = new \stdClass();
            $insert->userid = $user->id;
            $insert->created_date = $currentdate;
            $insert->courseid = 1;
            $insert->type = "newaccountmail";
            // $insert->type = get_string('newaccountmail', 'local_cs_reminder');
            $DB->insert_record('local_cs_reminder',$insert);
        }

        }    
    }

//sending enrollment mail
    public static function send_enrolment_email(\core\event\user_enrolment_created $event){
        global $CFG,$SITE,$DB,$USER;
        $eventdata = $event->get_data();;
        $courseid = $eventdata['courseid'];
        $user = \core_user::get_user($eventdata['relateduserid']);
        //Manju: 12/12/2020.Changing support user as sender instead of admin.
        //$sender = get_admin();
        $sender = \core_user::get_support_user();

        $currentdate = time();
        if (!empty($user->email)) {
            $companyid = $USER->company->id;
            $config = get_config('emailcontent'.$companyid);
            $moderator = clone($sender);           
            $messageuser = $config->userenrollmentmailbody; 
            $messageusersubject = $config->subjectfornewuserenrollment;
            $welcome = new \local_cs_reminder\message($courseid);
            $messageuser = $welcome->replace_values($user, $messageuser);
            $messageusersubject = $welcome->replace_values($user, $messageusersubject);
            // Email sending to user                              
            $mailstatus = email_to_user($user, $sender, $messageusersubject, html_to_text($messageuser), $messageuser);

            if($mailstatus){
            $insert  = new \stdClass();
            $insert->userid = $user->id;
            $insert->created_date = $currentdate;
            $insert->courseid = $courseid;
            $insert->type = "enrollmentmail";
            // $insert->type = get_string('enrollmentmail', 'local_cs_reminder');
            $DB->insert_record('local_cs_reminder',$insert);
        }


        }    
    }

}       