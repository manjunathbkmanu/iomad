<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * You may have settings in your plugin
 *
 * @package    local_hpanalytics
 * @copyright  2014 Daniel Neis
 * @license    http://www.gnu.org/copyleft/gpl.html gnu gpl v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
global $OUTPUT, $CFG;
require_once($CFG->libdir.'/formslib.php');
require_login();
class local_enroll_form extends moodleform {
    function definition() {
        global $CFG,$DB,$USER,$PAGE,$OUTPUT;
        $mform =& $this->_form;
        $mform->setType('id',PARAM_INT);
        ///manju: creating course dropdown with suggestion.30/06/2020.
        if(!is_siteadmin()){
            $companyid = $DB->get_field('company_users','companyid',array('userid'=>$USER->id));
            $coursesql ="SELECT c.* FROM {course} AS c
            JOIN {company_course} AS cc ON cc.courseid = c.id WHERE c.visible = 1 AND c.id != 1 AND cc.companyid =$companyid ORDER BY c.fullname ASC";
        }else{
            $coursesql = 'SELECT * FROM {course} WHERE visible = 1 AND id !=1 ORDER BY fullname ASC';
        }
        $allcourses = $DB->get_records_sql($coursesql);
        $course_options = [];
        $selectcourse = get_string('pleaseselectcourse', 'local_course_report');
        $course_options[]=$selectcourse;
        foreach($allcourses as $course){
           $coursename=$course->fullname;
           if(current_language() == 'ar'){
               $coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));

               $coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$course->id));
               $coursename = $coursenamearabic;
               if(empty($coursename)){
                $coursename =$course->fullname;
            }

        }
        $course_options[$course->id] = $coursename;

    }
    $options = array(                                                                                                           
        'multiple' => false,                                                                                                     
        'noselectionstring' => get_string('selectcourse', 'local_course_report'),
        'placeholder' => get_string('selectcourse', 'local_course_report'),
        'onchange'=>'courseSelected();',                                                           
    );
    if(!empty($options)) {
        $select = $mform->addElement('select','courseid', get_string('selectcourse','local_course_report'), $course_options,$options);
    } 
}

        /**
     * Extend the form definition after data has been parsed.
     */
        public function definition_after_data() {
         global $USER, $CFG, $DB, $OUTPUT;
         $user = $USER;
         $userid = $USER->id;
         $mform = $this->_form;        
         profile_definition_after_data($mform, $userid); 

     }
 }
