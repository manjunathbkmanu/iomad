<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
require_once($CFG->libdir . '/formslib.php');
require_once('lib.php');
global $DB, $CFG, $USER;  
require_login(true);
$courseid = optional_param('id','',PARAM_INT);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/cs_reminder/index.php');
$title = get_string('pluginame', 'local_cs_reminder');
$pagename = get_string('mailreport', 'local_cs_reminder');
$PAGE->navbar->add($title);
$PAGE->navbar->add($pagename);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$PAGE->requires->jquery();
$PAGE->requires->css(new 
	moodle_url($CFG->wwwroot.'/local/course_report/css/dataTables.bootstrap4.min.css'));
$PAGE->requires->css(new 
	moodle_url($CFG->wwwroot.'/local/course_report/css/buttons.bootstrap4.min.css'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/newjs/jquery.dataTables.min.js'), true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/course_report/js/newjs/dataTables.bootstrap4.min.js'), true);
// $PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/cs_reminder/js/date-eu.js'),true);
js_reset_all_caches();
echo $OUTPUT->header();

//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/cs_reminder/index.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback

if(!empty($courseid)){
	$sql = "
	    SELECT *
	    FROM {local_cs_reminder}
	    WHERE courseid=$courseid
	   ORDER BY created_date DESC
	";
$tabledata = $DB->get_records_sql($sql);
// $tabledata = $DB->get_records('local_cs_reminder', array('courseid'=>$courseid));
}else{
// $tabledata = $DB->get_records('local_cs_reminder');
	$sql = "
	    SELECT *
	    FROM {local_cs_reminder}
	   ORDER BY created_date DESC
	";
	$tabledata = $DB->get_records_sql($sql);
}

//sent mail details table
	$sentmail  = new \html_table();
	$sentmail->head = array(get_string('username', 'local_cs_reminder'),
		get_string('sentdate', 'local_cs_reminder'),
		get_string('coursename', 'local_cs_reminder'),
		get_string('mailtype', 'local_cs_reminder')
);
	foreach ($tabledata as $key => $value) {
		if($value->type == get_string('newaccountmail', 'local_cs_reminder')){
			$updateData = new stdClass();
		  	$updateData->id = $value->id;
		  	$updateData->type = "newaccountmail";
		   
		  	$DB->update_record('local_cs_reminder',$updateData); 
		  }elseif($value->type == get_string('courseremindermail', 'local_cs_reminder')){
			$updateData = new stdClass();
		  	$updateData->id = $value->id;
		  	$updateData->type = "courseremindermail";
		   
		  	$DB->update_record('local_cs_reminder',$updateData); 

		  }elseif($value->type == get_string('enrollmentmail', 'local_cs_reminder')){
			$updateData = new stdClass();
		  	$updateData->id = $value->id;
		  	$updateData->type = "enrollmentmail";
		   
		  	$DB->update_record('local_cs_reminder',$updateData); 

		  }
		if($value->courseid > 1){
			$courseobject = $DB->get_record('course',array('id'=>$value->courseid));
			$coursename = $courseobject->fullname;
				if(current_language() == 'ar'){
			$coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));

			$coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$value->courseid));
			$coursename = $coursenamearabic;
			if(empty($coursename)){
				$coursename =$courseobject->fullname;
			}

		}
		}else{
			$coursename ='';
		}
		
		// $sentdate = gmdate("d-m-Y H:i:s", $value->created_date);
		$sentdate = $value->created_date;

		$userobject = $DB->get_record('user',array('id'=>$value->userid));
		$username = $userobject->firstname.' '.$userobject->lastname;
		// $sentmail->data[] = array($username, $sentdate, $coursename, $value->type);
		$sentmail->data[] = array($username, $sentdate, $coursename, get_string($value->type, 'local_cs_reminder'));
	}
$html ='';
$html .= html_writer::start_div('container-fluid');
$html .= html_writer::start_div('row');
$html .= html_writer::start_div('col-md-12');
$html .= html_writer::table($sentmail);
$html .= html_writer::end_div();
$html .= html_writer::end_div();
$html .= html_writer::end_div();

echo $html;

echo $OUTPUT->footer();
?>
<script type="text/javascript">
	$(function(){
		$('.generaltable').dataTable({
			"dom": 'rtip',
			"order": [[ 1, "desc" ]],
    		"columnDefs" : [
    			{
                "render": function ( data, type, row ) {
                	const d = new Date( parseInt(row[1])*1000 );
					var date = d.toLocaleString("en-US", {  
									day : 'numeric',
									month : 'short',
									year : 'numeric',
									hour: '2-digit',
									minute: '2-digit',
								});
                    return date;
                },
                "targets": 1
            },
            { "visible": true,  "targets": [ 1 ] }
    		],	
		    "bLengthChange" : false,
		    "bInfo":false,
		});
	})
</script>
