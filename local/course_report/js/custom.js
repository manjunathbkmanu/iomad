    // $(document).ready(function () {
    //     $('#courseselector').typeahead({
    //         source: function (query, result) {
    //             $.ajax({
    //                 url: M.cfg.wwwroot+"/local/course_report/ajax.php",
				// 	data: 'query=' + query,            
    //                 dataType: "json",
    //                 type: "POST",
    //                 success: function (data) {
				// 		result($.map(data, function (item) {
				// 			return item;
    //                     }));
    //                 }
    //             });
    //         }
    //     });
    // });

    function courseSelected(){
        $('#bulkenrollink').addClass("disabled");
        $('#manualenrollink').addClass("disabled");
    	var course = document.getElementById("id_courseid").value;
        $.ajax({
            url: M.cfg.wwwroot+"/local/course_report/ajax.php",
            data: 'course=' + course,            
            type: "POST",
            success: function (data) {
                var obj = JSON.parse(data);
                var selfenrol = obj.selfenrol;
                $("#id_userid").val(selfenrol);
                var enrollmentid = obj.enrollmentid;
                var courseid = obj.courseid;                
                $('#bulkenrollink').attr("href", M.cfg.wwwroot+"/local/bulkenrol/index.php?id="+courseid); // Set herf value
                $('#manualenrollink').attr("href", M.cfg.wwwroot+"/enrol/manual/manage.php?id="+courseid+"&enrolid="+enrollmentid); // Set herf value
                $('#bulkenrollink').removeClass("disabled");
                $('#manualenrollink').removeClass("disabled");
        }
    });
    }
