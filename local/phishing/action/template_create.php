<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_accesscohort
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../../config.php');
require_once('../form/template_create_form.php');
require_once($CFG->libdir . '/formslib.php');
global $DB,$USER;
require_login(0,false);
//edit params
$companyid = $USER->company->id;
$eid = optional_param('id',null, PARAM_INT);
$edit = optional_param('edit',null, PARAM_INT);
//api key initialization
//Manju: changing plugin name local_gophish to local_phishing. 08/02/2021.
$protocol = get_config('local_phishing'.$companyid, 'protocol');
$apikey = get_config('local_phishing'.$companyid, 'api');
$urlip = get_config('local_phishing'.$companyid, 'urlip');

$context = context_system::instance();
$contextid = $context->contextlevel;
$overall = has_capability('local/gophish:overall',$context);
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/local/phishing/js/import.js'), true);
$curl_handle = curl_init();
$PAGE->set_context(context_system::instance());
if($edit==1){
	$title = get_string('templatehrdedit', 'local_phishing');
}else{
	$title = get_string('templatehrd', 'local_phishing');
}
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_url('/local/phishing/action/template_create.php');
require_login();
$previewnode = $PAGE->navigation->add(get_string('pluginname','local_phishing'), new moodle_url($CFG->wwwroot.'/local/phishing/templatelist.php'), navigation_node::TYPE_CONTAINER);
$thingnode = $previewnode->add($title, new moodle_url($CFG->wwwroot.'/local/phishing/action/template_create.php'));
$thingnode->make_active();
$mform = new template_create_form();
$temp = false;
if ($mform->is_cancelled()){
	redirect(new moodle_url('/local/phishing/templatelist.php', array()));
} else if ($data = $mform->get_data()) {
	
	if($data->checkboxtext == "1"){
		if($data->editorvalue == ''){
			$data->editorvalue .= '<html><head><title></title></head><body><p>'.$data->html['text'].'</p>{{.Tracker}}</body></html>';
		}else{
			$htmlData = $data->editorvalue;
			$htmlData .= '<p>'.$data->html['text'].'</p>{{.Tracker}}';
			$data->editorvalue = $htmlData;
		}
	}else{
		if($edit =1  and !empty($eid)){
			$templateValue = get_template_value($urlip,$apikey,$eid);
			if (strpos($templateValue->html, '{{.Tracker}}') == true) {
				$string = str_replace('{{.Tracker}}', '', $data->html['text']);
				$data->editorvalue = $string;
			}else{
				$data->editorvalue = $data->html['text'];
			}
		}else{
			$data->editorvalue = $data->html['text'];
		}
	}

	//save file
	file_save_draft_area_files($data->attachments,$contextid,'local_gophish','gophishattachments',$data->attachments,array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 50));
	$sql = "SELECT * FROM {files}
	where component='local_gophish' AND itemid = $data->attachments AND filename IS NOT NULL AND mimetype IS NOT NULL ";
	$sql_val = $DB->get_records_sql($sql);
	$array=[];	
	if(!empty($sql_val)){
		$i=1;
		foreach ($sql_val as $key => $value) {	
			$fs = get_file_storage();		
			$file = $fs->get_file($value->contextid, $value->component, $value->filearea,
				$value->itemid, $value->filepath, $value->filename);	
			if($file){
				$contents = base64_encode($file->get_content());
				$array[] = array('id'=>$i,'name'=>$value->filename,'content'=>$contents,'type'=>$value->mimetype);
			}			
			$i++;
		}		
	}
	$url = $protocol.$urlip.'/api/templates/?api_key='.$apikey;
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	// Set the curl URL option
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl_handle, CURLOPT_URL, $url);
	// This option will return data as a string instead of direct output
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	// Execute curl & store data in a variable
	$curl_data = curl_exec($curl_handle);
	if (!curl_exec($curl_handle)) {
		die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
	}
	$template_datas = json_decode($curl_data);
	$id =0;
	if(!empty($template_datas)){
		foreach ($template_datas as $key => $template_data) {
			$id = $template_data->id;
		}
	}
	$id = $id+1;		
	//create array for creating template
	if($edit==1 and !empty($eid)){
		$dataarray = array('id'=>$eid,'name'=>$data->name,'subject'=>$data->subject,'html'=>$data->editorvalue,'attachments'=>$array);
		$record = $DB->get_record('local_gophish_files',array('api_id'=>$eid,'api_type'=>'template'));
		if(!empty($record)){
			$updaterecord = new stdClass();
			$updaterecord->id = $record->id;
			$updaterecord->file_itemid = $data->attachments;
			$updaterecord->api_id = $eid;
			$updaterecord->api_type = 'template';
			$updaterecord->timecreated =time();
			$updaterecord->timemodified=time();
			$DB->update_record('local_gophish_files',$updaterecord);
		}

	}else{
		$dataarray = array('id'=>$id,'name'=>$data->name,'subject'=>$data->subject,'html'=>$data->editorvalue,'attachments'=>$array);
		$insert = new stdClass();
		$insert->file_itemid = $data->attachments;
		$insert->api_id = $id;
		$insert->api_type = 'template';
		$insert->timecreated =time();
		$insert->timemodified=time();
		$DB->insert_record('local_gophish_files',$insert);
	}		
	$data_json = json_encode($dataarray);
	if(!empty($data_json)){		
		if($edit==1 and !empty($eid)){
			$putUrl = $protocol.$urlip.'/api/templates/'.$eid.'?api_key='.$apikey;
			curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($curl_handle, CURLOPT_URL, $putUrl);	
			curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'PUT');
			curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data_json);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
			$response  = curl_exec($curl_handle);
			$msg = get_string('tempeditmsg','local_phishing');
			$redirectUrl = $CFG->wwwroot.'/local/phishing/action/template_create.php?id='.$eid.'&edit=1';
		}else{
			
			curl_setopt($curl_handle, CURLOPT_POSTFIELDS,$data_json);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
			$response  = curl_exec($curl_handle);
			$msg = get_string('temppostmsg','local_phishing');
			$redirectUrl = $CFG->wwwroot.'/local/phishing/action/template_create.php';
		}
			//
			//display message code 
		if($response) {
			$decodedResponse = json_decode($response);
			if(isset($decodedResponse->id) && $decodedResponse->id != ''){
				redirect(new moodle_url($CFG->wwwroot.'/local/phishing/templatelist.php'),$msg);
			}else{
				redirect(new moodle_url($redirectUrl),$decodedResponse->message, 0, 'error');
			}
		} else {
			echo "error";
		}			
		if (!curl_exec($curl_handle)) {
			die('Error: "' . curl_error($curl_handle) . '" - Code: ' . curl_errno($curl_handle));
		}
	}
	//insert files here 
	curl_close($curl_handle);
}else{
	$url = $protocol.$urlip.'/api/templates/'.$eid.'?api_key='.$apikey;
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
		// Set the curl URL option
			//curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($curl_handle, CURLOPT_URL, $url);
		// This option will return data as a string instead of direct output
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		// Execute curl & store data in a variable
	$curl_data = json_decode(curl_exec($curl_handle));	
	if($edit==1 and !empty($eid)){
		$setdata = new stdClass();
		$setdata->name = $curl_data->name;
		$setdata->subject = $curl_data->subject;
		//$setdata->text = $curl_data->text;
		//$setdata->text =array('text'=>$curl_data->html);
		$setdata->html =array('text'=>$curl_data->html);
		$context = context_system::instance();
		$contextid = $context->contextlevel;
		$draftitemid = file_get_submitted_draft_itemid('gophishattachments');
		$valu_item = $DB->get_record('local_gophish_files',array('api_id'=>$curl_data->id,'api_type'=>'template'));
		if(!empty($valu_item)){
			$val = $valu_item->file_itemid;
		}else{
			$val =0;
		}		
		file_prepare_draft_area($draftitemid, $contextid, 'local_gophish_files', 'gogophishattachments', $val, array('subdirs' => 0, 'maxbytes' => '*', 'maxfiles' => 50));		
		$setdata->attachments = $draftitemid;
		$mform->set_data($setdata);
	}

}
echo $OUTPUT->header();
//Dilip ebdaa go back button
$retuurnurl= $CFG->wwwroot . '/local/phishing/templatelist.php';
$mhtml='';
$mhtml.=html_writer::start_div('float-right');
$mhtml.=html_writer::start_tag('a',array('href'=>$retuurnurl, 'class' => 'go-back-btn'));
$mhtml.='<i class="fa fa-arrow-left" aria-hidden="true"></i>
';
$mhtml.=get_string('back_button','local_compliance_dashboard');
$mhtml.=html_writer::end_tag('a');
$mhtml.=html_writer::end_div('');
echo $mhtml;
// end of goback
if($overall){
	$mform->display();
}else{
	echo html_writer::div(
		get_string('cap', 'local_phishing'),'alert alert-danger'
	);
}

echo $OUTPUT->footer();
?>






