<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_learningreport
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../../config.php');
global $CFG,$USER,$DB;
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
require_once($CFG->libdir . '/formslib.php');
require_login(true);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_url($CFG->wwwroot . '/local/course_management/certificates.php');
$title = get_string('certificates', 'local_course_management');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->jquery();
echo $OUTPUT->header();
$table  = new \html_table();
$table->id = 'certificate';
//Manju: check for admin.
if (is_siteadmin()){
	$table->head = array(get_string('username', 'local_course_management'),
		get_string('coursename', 'local_course_management'),
		get_string('downloadcertificate', 'local_course_management'));
	//get all the active users on moodle.
	$userssql="SELECT * FROM {user} WHERE deleted != 1 AND suspended != 1";
	$allusers=$DB->get_records_sql($userssql);
	$allusersdata=[];
	foreach ($allusers as $user) {
		//Manju:get all the completed courses of all active users.
		$certificatedata=get_allcertificates($user->id);
		if(!empty($certificatedata)){
			$allusersdata[$user->id]=$certificatedata;
		}
	}
	foreach ($allusersdata as $userid => $usercourses) {
		$userobject=$DB->get_record('user',array('id'=>$userid));
		$username=fullname($userobject);
		foreach ($usercourses as $ucourse) {
			$coursename=$ucourse['coursename'];
			$coursecompletion=date('d-m-Y',$ucourse['completion']);
			$link=$CFG->wwwroot.'/local/course_management/download.php?username='.$username.'&coursename='.$coursename.'&complete='.$coursecompletion;
			$downloadcert='<a href="'.$link.'">'.get_string('downloadcert', 'local_course_management').'</a>';

			$table->data[] = array(fullname($userobject),$ucourse['coursename'],$downloadcert);
		}
	}
}else{
	//for other users.
	$username=fullname($USER);
	$certificatedata=get_allcertificates($USER->id);
	$table->head = array(get_string('coursename', 'local_course_management'),
		get_string('downloadcertificate', 'local_course_management'));
	foreach ($certificatedata as $cvalue) {
		$coursename=$cvalue['coursename'];
		$coursecompletion= date('d-m-Y',$cvalue['completion']);


		$link=$CFG->wwwroot.'/local/course_management/download.php?username='.$username.'&coursename='.$coursename.'&complete='.$coursecompletion;
		$downloadcert='<a href="'.$link.'">'.get_string('downloadcert', 'local_course_management').'</a>';
		$table->data[] = array($cvalue['coursename'],$downloadcert);
	}
}
echo html_writer::table($table);
echo $OUTPUT->footer();