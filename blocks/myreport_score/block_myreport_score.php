<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the Activity modules block.
 *
 * @package    block_activity_modules
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->libdir . '/filelib.php');
require_once("$CFG->libdir/gradelib.php");
require_once("$CFG->dirroot/lib/completionlib.php");
require_once("$CFG->dirroot/grade/querylib.php");
use core_completion\progress;

class block_myreport_score extends block_base {

   function init() {
    $this->title = get_string('pluginname', 'block_myreport_score');
}

function get_content() {
    global $DB, $OUTPUT,$USER;


    if($this->content !== NULL) {
        return $this->content;
    }

    $this->content = new stdClass;
    $this->content->items = array();
    $this->content->icons = array();
	$this->content->text = '';
    $sortorder = 'visible DESC, sortorder ASC';
    $courses = enrol_get_my_courses();
    $dateformat = '%d-%b-%Y';
    //print_object($courses);
    $coursesprogress = [];
    $statuscompleted = [];
    $statusnotcompleted = [];
    $statusprogress = [];
    if(!empty($courses)){
        foreach($courses as $course) {
            $courseobj = $DB->get_record('course',array('id'=>$course->id));
                    
            $cminfo = get_fast_modinfo($course, $USER->id);
                foreach($cminfo->get_cms() as $cm) {
                    if($cm->modname == 'scorm')
                     {
                         $passfail = $this->get_scorm_status_ebdaa_passfail_blockscoremyreport($cm->instance,$course);
                     }
                 }

            $info = new completion_info($courseobj);
			

                //Mihir for score as 100 for completed or pass
// if pass/fail returns pass then show default score as 100% 9 Jan 2020
    if ($passfail == 'PASS' or $passfail == 'pass') {

        $gradeRoundvalue= '100';
    } else {

			$grades = \grade_get_course_grades($courseobj->id, $USER->id);
            $grademax = $grades->grademax;

            $gradeValue =(((int) $grades->grades[$USER->id]->str_grade));
            if(!$gradeValue==null){
                $gradeRoundvalue = (($gradeValue * 100)/$grademax);
            }else{
                $gradeRoundvalue=0;
            }
    }

            $gradeforchart[] = $gradeRoundvalue;
            
            if(current_language() == 'ar'){
                $coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));

                $coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$courseobj->id));
                if(empty($coursenamearabic)){
                    $gradeforchartname[] =$courseobj->fullname;
                }else{
                    $gradeforchartname[] =$coursenamearabic;
                }
            }else{
                $gradeforchartname[] = $courseobj->fullname;
            }
			
			/*
             // Check this user is enroled.
            if ($info->is_tracked_user($USER->id)) {
                $completion = new \completion_info($courseobj);
                $percentage = progress::get_course_progress_percentage($courseobj,$USER->id);
                if (!is_null($percentage)) {
                    $percentage = floor($percentage);
                }
                $params = array(
                    'userid'    => $USER->id,
                    'course'  => $courseobj->id
                    );
                $ccompletion = new completion_completion($params);

                $coursesprogress[$courseobj->id]['completed'] = 
                $completion->is_course_complete($USER->id);
                $coursesprogress[$courseobj->id]['progress'] = $percentage;
                $completiondate = '';
                if ($coursesprogress[$courseobj->id]['completed'] == false) {
                    if ($coursesprogress[$courseobj->id]['progress'] > 0 ) {
                        $statusprogress[] = $courseobj->id;
                    } else {
                        $statusnotcompleted[] =$courseobj->id;
                    }

                } else {
                    $statuscompleted[] =$courseobj->id;
                }
                
            }
			
			*/
        }
    
    // $cm = count($statuscompleted);
    // $nt = count($statusnotcompleted);
    // $prg = count($statusprogress);

    // $sales = array($cm,$nt,$prg);
    // $labels = array(get_string('completed','block_myreport_score'),get_string('notstarted','block_myreport_score'),get_string('inprogress','block_myreport_score'));   
    //$chart = new core\chart_bar();
    // $chart = new core\chart_pie();
    // $chart->set_doughnut(true); // Calling set_doughnut(true) we display the chart as a doughnut.
    // $series1 = new \core\chart_series('Status', $sales);
    // $chart->add_series($series1);
    //$chart->add_series($sales);    
    // $chart->set_labels($labels);
   // for y axis 

    //$xx =  $OUTPUT->render($chart); //not needed
	
	
//green - #28a944
//red - #dc3546
//yellow - #ffc107
	
    $coursescore = $gradeforchart;
    $scorelabels = $gradeforchartname;
    //$chart = new core\chart_bar();
    $chartscore = new core\chart_bar();
    
    $scoreseries1 = new \core\chart_series(get_string('score', 'block_myreport_score'), $coursescore);
    $chartscore->add_series($scoreseries1);
    //$chart->add_series($sales);    
    $chartscore->set_labels($scorelabels);
    global $CFG;
    // $CFG->chart_colorset = ['#ff0000', '#66ff33', '#ff9933', '#85144b', '#B10DC9'];
     $CFG->chart_colorset = ['#28a944'];
	$yy =  $OUTPUT->render($chartscore);
	
    $this->content->text .= $yy;
	
	}
    $this->content->footer = '';
    return $this->content;
}


/**
 *
 * @param type $scormid
 * @param type $userid
 */
function get_scorm_status_ebdaa_passfail_blockscoremyreport($scormid,$course) {
    global $DB, $USER,$CFG;
    $scormstatus = '-';
    $userid = $USER->id;
    /*
    $sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
    $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
*/
    $sql = "SELECT attempt
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' AND value = ( SELECT max(value) FROM {scorm_scoes_track} WHERE userid = ? AND scormid = ? AND element = 'cmi.core.score.raw' )
               ";
    
    $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid,$userid, $scormid));
    if (!empty($lastattempt)) {
        $sql = "SELECT value
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
        $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

        if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
            if($status == 'passed') { $stringshow = 'pass';}
            if($status == 'failed') { $stringshow = 'fail';}
            if($status == 'complete') { $stringshow = 'pass';}
            if($status == 'completed') { $stringshow = 'pass';}
            $scormstatus = strtoupper($stringshow);
                
        } else if ($status == 'incomplete') {
            $scormstatus = '-'; 
        }
    } else {

        //non scored scorm only returns lesson status
        $sql = "SELECT MAX(attempt)
              FROM {scorm_scoes_track}
             WHERE userid = ? AND scormid = ?
               ";
            $lastattempt = $DB->get_field_sql($sql, array($userid, $scormid));
            if (!empty($lastattempt)) {
                $sql = "SELECT value
                      FROM {scorm_scoes_track}
                     WHERE userid = ? AND scormid = ? AND attempt =? AND element ='cmi.core.lesson_status'";
                $status = $DB->get_field_sql($sql, array($userid, $scormid, $lastattempt));

                if ($status == 'complete' OR $status == 'completed' OR $status == 'passed' OR $status == 'failed'){
                    if($status == 'passed') { $stringshow = 'pass';}
                    if($status == 'failed') { $stringshow = 'fail';}
                    if($status == 'complete') { $stringshow = 'pass';}
                    if($status == 'completed') { $stringshow = 'pass';}
                    $scormstatus = strtoupper($stringshow);
                        
                } else if ($status == 'incomplete') {
                    $scormstatus = '-'; 
                }
            } else {
                $scormstatus = '-';
            }

    }
    return $scormstatus;
}



}


