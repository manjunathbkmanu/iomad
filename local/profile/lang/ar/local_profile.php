<?php 
$string['title'] = 'الملف الشخصي';
$string['firstname'] = 'الاسم الاول';
$string['lastname'] = 'اسم العائلة';
$string['email'] = 'البريد الإلكتروني';
$string['employeeid'] = 'هوية الموظف';
$string['department'] = 'القسم';
$string['search'] = 'بحث';
$string['firstnameerror'] = 'الرجاء إدخال الاسم الأول';
$string['lastnameerror'] = 'الرجاء ادخال اسم العائلة';
$string['insert_file'] = 'الصورة الشخصية';
$string['submit'] = 'الموافقة';
$string['changepassword'] = 'تغيير كلمة السر';
$string['alternatename'] = 'مدير';
$string['lang'] = 'لغة';
