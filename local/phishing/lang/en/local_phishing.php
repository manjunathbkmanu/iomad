<?php
$string['pluginname'] = 'Phishing';
$string['settings'] = 'Phishing genaral Settings.';
$string['enable'] = 'Select Checkbox for enaled phish settings.';
$string['api'] = 'API Key';
$string['username'] = 'Username';
$string['templatehrd'] = '<h3>Creating Template</h3>';
$string['templatecopy'] = '<h3>Copy Template</h3>';
$string['templatehrdedit'] = '<h3>Edit Template</h3>';
$string['temp_name'] = 'Campaign Name: ';
$string['temp_name_help'] = 'Enter Name: ';
$string['subname'] = 'Email Subject: ';
$string['text'] = 'Raw text of email sent to users: ';
$string['html'] = 'Add html code';
$string['attachments'] = 'Add Files.';
$string['modified_date'] = 'Modified Date.';
$string['checkboxtext'] = 'Add Tracking Image';
//templates list 
$string['listoftemp'] = 'List of Templates';
$string['sno'] = 'SNO';
$string['name'] = 'Name';
$string['mdate'] = 'Modified Date';
$string['action'] = 'Action';
$string['newtemp'] ='New Template';
$string['tempnotfound'] = 'Template not found';
//delete template 
$string['delete_temp'] = 'Delete Template';
$string['temppostmsg'] = 'Template Created Successfully';
$string['tempeditmsg'] = 'Template Record is Updated Successfully';
$string['deletemsg'] = 'Template Deleted Successfully';
$string['cap'] = 'Access is denied';
//help button 
$string['tempname_name'] = 'Enter Template Name';
$string['subject_name'] = 'Enter Subjct Name';
$string['text_help']= 'Enter Normal Text';
$string['html_help'] = 'Enter Html Code';
$string['addfile_help'] = 'Add file here';

//Landing Pages
$string['createladingpage'] = '<h3>Create Landing Page</h3>';
$string['copyladingpage'] = '<h3>Copy Landing Page</h3>';
$string['editladingpage'] = '<h3>Edit Landing Page</h3>';
$string['listoflandingpage'] = 'List of Landing Pages';
$string['newlandingpage'] ='New Landing Page';
$string['update'] ='Record is updated Successfully!.';
$string['success'] ='Record is created Successfully!.';
$string['delete_page'] ='Delete Landing Page.';
$string['deletepage'] = 'Page Deleted Successfully';

//landing page help
$string['landing_name'] = 'Enter Landing Page Name';


//campaigns
$string['dashboard'] = 'Dashboard';
$string['campaigns'] = 'Campaign Lists';
// $string['emailsent'] = 'Email Sent';
$string['emailsent'] = '<i class="fa fa-envelope-o emailsent"></i>';
// $string['emailopen'] = 'Email Opened';
$string['emailopen'] = '<i class="fa fa-envelope-open-o emailopen"></i>';
// $string['linkclick'] = 'Clicked Link';
$string['linkclick'] = '<i class="fa fa-mouse-pointer linkclick"></i>';
// $string['submitteddata'] = 'Submitted Data';
$string['submitteddata'] = '<i class="fa fa-exclamation-circle submitteddata"></i>';
// $string['emailreport'] = 'Email Report';
$string['emailreport'] = '<i class="fa fa-bullhorn emailreport"></i>';
$string['recentcamp'] = 'Recent Campaigns';
$string['cdate'] = 'Created Date';
$string['status'] = 'Status';
$string['urlip'] = 'IP';
$string['ccampaign'] = '<h3>Create Campaign</h3>';
$string['ecampaign'] = '<h3>Edit Campaign</h3>';
$string['dcampaign'] = 'Delete Campaign';
$string['cocampaign'] = 'Complete Campaign';
$string['details'] = 'Details';
// $string['copy'] ='Results for Copy of ';
//Manjunath: changed the text. 09/06/2021.
$string['copy'] ='Results for ';
$string['rcamp'] ='Recent Campaigns';
$string['newcamp'] = 'New Campaign is Created Successfully!.';
$string['deletecamp'] = 'Campaign is Deleted Successfully!.';
//create camp
$string['createcamp']= 'New Campaign';
$string['copycampaign']= 'Copy Campaign';
$string['editcamp']= 'Edit Campaign';
$string['emailtemp'] = 'Email Template';
$string['landingpage'] = 'Landing Page';
$string['url'] = 'Protocol';
$string['all'] = 'All';
$string['launchdate'] = 'Launch Date';
$string['sendemail'] = 'Send Emails By (Optional)';
$string['sendprofile'] = 'Sending Profile';
$string['groups'] = 'Groups ';
//static words here 
$string['cam_help'] = 'Enter Campaign Name';
$string['temp_help'] = 'Select Template';
$string['land_help'] = 'Select Landing Page';
$string['url_help'] = 'Enter Url';
$string['launch_help'] = 'Enter Launch Date';
$string['email_help'] = 'Enter Send by Email Date';
$string['send_help'] = 'Select Sending Profile';
$string['group_help'] = 'By Selecting Multiple Groups Users Added into Groups.';

//sending profile langfile 
$string['sendingprofile'] = '<h3>Sending Profiles</h3>';
$string['name_help'] = 'Enter Sending Profile Name.';
$string['sendingprofilelist'] = 'List of Sending Profiles';
$string['newsendingprofile'] = 'Create Sending Profile';
$string['editsendingprofile'] = 'Edit Sending Profiles';
$string['interface_type'] = 'Interface Type ';
$string['from'] = 'From ';
$string['host'] = 'Host ';
$string['host_help'] = 'Host help ';
$string['username'] = 'Username ';
$string['password'] = 'Password ';
$string['emailhdr'] = '<h3>Email Headers</h3>';
$string['xcheader'] = 'x-custom header';
$string['urlphish'] = '{{Url}}-phish';
$string['ignorecer'] = 'Ignore Certificate Errors';
$string['delete_sendp'] = 'Delete Sending Profile';
$string['cresend'] = 'Sending Profile is Created Successfully!.';
$string['editresend'] = 'Sending Profile is Updated Successfully!.';
$string['deletesend'] = 'Sending Profile is Deleted Successfully!.';
//static words here
$string['interface_help'] = 'Enter Interface Type : Example : interface_type: SMTP';
$string['from_address_help'] = 'Enter From Address : Example : from_address: John Doe <john@example.com>';
$string['host_help'] = 'Enter Host Name : Example : smtp.example.com:25 ';
$string['username_help'] = 'Enter User Name : {Optional}';
$string['password_help'] = 'Enter Password : {Optional}';
$string['header_help'] = 'Enter Key  : Example : Key : X-Header';
$string['header1_help'] = 'Enter Value : Example : Value : Foo Bar';


//user and grops
$string['usergroup'] ='<h3>Users & Groups</h3>';
$string['delusergroup'] ='Delete Users & Groups';
$string['newusergroup'] ='New Users & Groups';
$string['editusergroup'] ='<h3>Edit Users & Groups</h3>';
$string['groupcreate'] = 'New Users and Group is Created Successfully!.';
$string['groupedit'] = 'Users and Group Updated Successfully!.';
$string['listofuser'] = 'List of Users and Groups';
$string['noofmembes'] = '# of Members';
$string['position'] = 'Position';
$string['add'] = 'Add';
$string['bulk user'] = 'Bulk Import Users';
$string['adduser'] = '<h3>Add User</h3>';
$string['edituser'] = '<h3>Edit User</h3>';
$string['targets'] = 'Select Users';
$string['emailtext'] ='Enter Email Adress with Sapareted by Comma';
$string['textnote'] = '<h3>Select Dropdown List or Else Enter User Email Address with Saparted by Comma.</h3>';
$string['note'] = '<h3>Note </h3>';
$string['emailnot'] = 'These Email Eddress Are not Present in Lms. So Please Create User First then Create Group';

$string['import'] = 'Import';
$string['import_content'] = 'Import Email Content';
$string['checkboxtemp'] = 'Change Links to Point to Landing Page';
$string['url_static'] = 'Specify the URL to automatically import a website.';
$string['capture_credentials'] = 'Capture Submitted Data';
$string['capture_passwords'] = 'Capture Passwords';
$string['redirect_url'] = 'Redirect to';
$string['passnote'] = ' Warning: Credentials are currently not encrypted. This means that captured passwords are stored in the database as cleartext. Be careful with this! ';
$string['sure'] = 'Are you sure want to delete?';
$string['sure_complete'] = 'Are you sure want to complete?';
$string['list1'] = 'Back to Listing Page';
$string['imagedesc'] = 'The recommended image size for image is 800 by 800 pixels. File type must be .png and .jpg';
$string['fname'] = 'First Name';
$string['lname'] = 'Last Name';
$string['email'] = 'Email';
$string['reported'] = 'Reported';
$string['launch_date'] = 'Launch Date';
$string['activecampaign'] = 'Active Campaigns';
$string['archivecampaign'] = 'Archived Campaigns';
$string['newcampaign'] = 'New Campaign';

$string['prohttps'] = 'HTTPS';
$string['prohttp'] = 'HTTP';
$string['landingpageurl'] = 'Landing Page URL';













