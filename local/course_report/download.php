<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Simple Certificate block outline view.
 *
 * @package ohio_transcript
 * @author Sangita Kumari
 * @copyright Mihir jana (lmsofindia.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or late
 */
require_once('../../config.php');
require_once($CFG->dirroot.'/local/ebdaa_functions/lib.php');
require_once($CFG->libdir.'/pdflib.php');
global $DB, $COURSE, $OUTPUT, $PAGE, $USER,$CFG;
$courseid = required_param('courseid',PARAM_INT);
$coursename = $DB->get_field("course","fullname", array('id'=>$courseid));
$coursenamecustom1 = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));

$coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom1,'instanceid'=>$courseid));
if(empty($coursenamearabic)){
	$coursenamearabic=$coursename;
}
$allusersofcourse = \get_enrolled_users_in_course($courseid);
if(!empty($allusersofcourse)){
	$totalusers = count($allusersofcourse);
	$coursestatistics = get_course_statistics($allusersofcourse,$courseid);
}
//labels and data for pie chart
$notstarted = get_string('notstarted_ar', 'local_course_report');
$complete = get_string('complete_ar', 'local_course_report');
$inprogress = get_string('inprogress_ar', 'local_course_report');
if(!empty($coursestatistics)){
	$percentagenotstarted = ($coursestatistics[2] * 100)/$totalusers;
	$percentagecompleted = ($coursestatistics[0] * 100)/$totalusers;
	$percentageinprogress = ($coursestatistics[1] * 100)/$totalusers;
}
if(!empty($allusersofcourse)){
	$averagescore = average_userscore($allusersofcourse,$courseid);
	$calculatedscore = 100 - $averagescore;
	$percentagegrade = round($averagescore, 2);
}
$chart='';
$chart.='<table border="1" cellpadding="4" cellspacing="1">
<tr style="background-color:#2a80b9;color:#ffffff;">
<th>'.get_string('course_ar', 'local_course_report').'</th>
<th>'.$notstarted.' %'.'</th>
<th>'.$inprogress.' %'.'</th>
<th>'.$complete.' %'.'</th>
<th>'.get_string('averageassessmentscore_ar', 'local_course_report').'</th>
</tr>
<tr>
<td>'.$coursenamearabic.'</td>
<td>'.$coursestatistics[2].'</td>
<td>'.$coursestatistics[1].'</td>
<td>'.$coursestatistics[0].'</td>
<td>'.$percentagegrade.'</td>
</tr>
</table>';

$charttable1='';
$charttable1.='<table border="1" cellpadding="4" cellspacing="1">
<tr style="background-color:#2a80b9;color:#ffffff;">
<th>'.get_string('course_ar', 'local_course_report').'</th>
<th>'.$notstarted.' %'.'</th>
<th>'.$inprogress.' %'.'</th>
<th>'.$complete.' %'.'</th>
<th>'.get_string('totalscount_ar', 'local_course_report').'</th>
</tr>
<tr>
<td>'.$coursenamearabic.'</td>
<td>'.round($percentagenotstarted, 2).'</td>
<td>'.round($percentageinprogress, 2).'</td>
<td>'.round($percentagecompleted, 2).'</td>
<td>'.$totalusers.'</td>
</tr>
</table>';

$usersdata = enrolled_users_tabledata($allusersofcourse,$courseid);
$usertable1='';
$usertable1.='<table class="table table-striped" border="1" cellpadding="4" cellspacing="1">
<tr style="background-color:#2a80b9;color:#ffffff;">
<th>'.get_string('fullname_ar', 'local_course_report').'</th>
<th>'.get_string('email_ar', 'local_course_report').'</th>
<th>'.get_string('score_ar', 'local_course_report').'</th>
<th>'.get_string('status_ar', 'local_course_report').'</th>
</tr>
';
foreach ($usersdata as $key => $value) {
	if($value['status']=='Not Started' ||$value['status']=='لم يبدأ' ){
		$crsstatus=get_string('notstarted_ar', 'local_course_report');
	}elseif($value['status']=='Completed' ||$value['status']=='منجز'){
		$crsstatus=get_string('complete_ar', 'local_course_report');
	}elseif($value['status']=='In Progress' || $value['status']=='في تقدم'){
		$crsstatus=get_string('inprogress_ar', 'local_course_report');
	}else{
		$crsstatus='';
	}

	$usertable1.='<tr>
	<td>'.$value['fullname'].'</td>
	<td>'.$value['email'].'</td>
	<td>'.$value['score'].'</td>
	<td>'.$crsstatus.'</td>
	</tr>';

}
$usertable1.='</table>';

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
    //Page header
	public function Header() {
		global $CFG,$DB;
		$courseid = required_param('courseid',PARAM_INT);
		$coursename = $DB->get_field("course","fullname", array('id'=>$courseid));
    	        //Manju:get the course name in arabic.
		$coursenamecustom = $DB->get_field('customfield_field', 'id', array('shortname'=>'coursenamearabic'));
		$coursenamearabic = $DB->get_field('customfield_data', 'value', array('fieldid'=>$coursenamecustom,'instanceid'=>$courseid));
		if(empty($coursenamearabic)){
			$coursenamearabic=$coursename;
		}
		$currentdate = date("d/m/Y");
		$coursereport='تقرير الدورة';

        // Logo
		$image_file = K_PATH_IMAGES.'logo_example.jpg';
		$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
		$this->SetFont('dejavusans', '', 18);
        // Title
		$this->Cell(0, 15, $coursereport.' : '.$coursenamearabic, 0, false, 'C', 0, '', 0, false, 'M', 'M');
		
		$this->SetFont('dejavusans', 'B', 10);
		$this->SetY(12);
		$this->Cell(0, 25, $currentdate, 0, false, 'C', 0, '', 0, false, 'M', 'M');

	}

    // Page footer
	public function Footer() {
		global $CFG;
        // Position at 15 mm from bottom
		$this->SetY(-15);
        // Set font
		$this->SetFont('dejavusans', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		// $string = $CFG->wwwroot."/local/course_report/pix/logo.jpg";
		// $logoWidth = 15;
		// $logoX = 10;
		// $logo = $this->Image($string, $logoX, $this->GetY(), $logoWidth);
		// $this->Cell(0,10, $logo, '', '', 'L'); 
	}
}
// create new PDF document
$pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set some language dependent data:
$lg = Array();
$lg['a_meta_charset'] = 'UTF-8';
$lg['a_meta_dir'] = 'rtl';
$lg['a_meta_language'] = 'fa';
$lg['w_page'] = 'page';

// set some language-dependent strings (optional)
$pdf->setLanguageArray($lg);
$pdf->AddPage();
// set font
$pdf->SetFont('dejavusans', '', 12);
$pdf->WriteHTML($chart, true, 0, true, 0);
$pdf->WriteHTML($charttable1, true, 0, true, 0);
// print newline
$pdf->Ln();
// print newline
$pdf->Ln();
// set font
$pdf->SetFont('dejavusans', '', 12);
$pdf->WriteHTML($usertable1, true, 0, true, 0);
// set LTR direction for english translation
$pdf->setRTL(false);
// Close and output PDF document
$pdf->Output('arabic.pdf', 'D');