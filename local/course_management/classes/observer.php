<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin sends users a welcome message after logging in
 * and notify a moderator a new user has been added
 * it has a settings page that allow you to configure the messages
 * send.
 *
 * @package    local
 * @subpackage course_management
 * @copyright  Manjunath B K
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_course_management;
defined('MOODLE_INTERNAL') || die();
require_once ($CFG->dirroot . '/local/ebdaa_functions/lib.php');
class observer
{

    public static function autoenroll_users(\core\event\user_loggedin $event)
    {
        global $CFG, $SITE, $DB, $USER;
        $eventdata = $event->get_data();
        $user = \core_user::get_user($eventdata['objectid']);
        $userid = $user->id;
        /* for all new user enrollment start*/
        $managerruledata_as = $DB->get_records('local_course_management', array(
            'rulekey' => 2
        ));
        if (count($managerruledata_as) > 0)
        {
            foreach ($managerruledata_as as $managerruledata_as_val)
            {
                $allcourse = explode(",", $managerruledata_as_val->courses);
                foreach ($allcourse as $courseid)
                {
                    $enroll = all_user_enroll_into_course($user, $courseid);
                }
            }

        }

        /* for all new user enrollment end */
        if (!empty(trim($user->department)))
        {
            //$rulekey= 0 for department
            $alldepartments = get_alldepartments_ebdaa();
            $departmenval = array_search($user->department, $alldepartments);
            if (trim($departmenval) != '')
            {
                $ruledata = $DB->get_records('local_course_management', array(
                    'rulekey' => 0,
                    'rulevalue' => $departmenval
                ));

                if (count($ruledata) > 0)
                {
                    foreach ($ruledata as $rda)
                    {
                        $allcourses = explode(",", $rda->courses);
                        if (!empty($allcourses))
                        {
                            foreach ($allcourses as $courseid)
                            {
                                $enroll = all_user_enroll_into_course($user, $courseid);
                            }
                        }
                    }
                }
            }
        }

        if (!empty(trim($user->alternatename)))
        {
            //$rulekey= 1 for manager
            $allmanagers = get_all_managers_ebdaa();
            $managermail = $DB->get_record('user', array(
                'email' => $user->alternatename
            ));
            if (!empty($managermail))
            {
                $managerval = array_search(fullname($managermail) , $allmanagers);
                $managerruledata = $DB->get_records('local_course_management', array(
                    'rulekey' => 1,
                    'rulevalue' => $managerval
                ));
                if (count($managerruledata) > 0)
                {
                    foreach ($managerruledata as $dda)
                    {
                        $allcourse = explode(",", $dda->courses);
                        foreach ($allcourse as $courseid)
                        {
                            $enroll = all_user_enroll_into_course($user, $courseid);
                        }
                    }
                }
            }
        }
    }
    //Manju: enable self enrolment when course updated.
    public static function update_self_enrolment(\core\event\course_updated $event)
    {
        global $CFG, $SITE, $DB, $USER;

        $eventdata = $event->get_data();
        $courseid = $eventdata['courseid'];
        //yes=0, No=1
        if (!empty($eventdata['other']['updatedfields']))
        {
            $selfenrol = $eventdata['other']['updatedfields']['customfield_self_enrolment'];
            if (!empty($selfenrol))
            {
                $sql = "UPDATE {enrol} SET status = " . $selfenrol . " WHERE enrol = 'self' AND courseid = " . $courseid . "";
                $result = $DB->execute($sql);
                if ($result)
                {
                    return true;
                }
            }
        }
    }
    //Manju: enable self enrolment when course created.
    public static function create_self_enrolment(\core\event\course_created $event)
    {
        global $CFG, $SITE, $DB, $USER;
        $eventdata = $event->get_data();
        $courseid = $eventdata['courseid'];
        //check whether self enrolment field is present.
        $selffieldid = $DB->get_field('customfield_field', 'id', array(
            'shortname' => 'self_enrolment'
        ));
        if ($selffieldid)
        {
            $fieldval = $DB->get_field('customfield_data', 'value', array(
                'instanceid' => $courseid,
                'fieldid' => $selffieldid
            ));
            $sql = "UPDATE {enrol} SET status = " . $fieldval . " WHERE enrol = 'self' AND courseid = " . $courseid . "";
            $result = $DB->execute($sql);
            if ($result)
            {
                return true;
            }
        }
    }
}

