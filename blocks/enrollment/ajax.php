<?php
require_once("../../config.php");
require_once("lib.php");
$context = context_system::instance();
$PAGE->set_context($context);
$user = optional_param('user','',PARAM_INT);
$uarray = optional_param('sendarray','',PARAM_RAW);
$courses = explode(',', $uarray);
$role=5;
array_pop($courses);
if (!empty($user) && !empty($courses)) {
	for ($i = 0; $i < count($courses); $i++) {
		if(is_numeric($courses[$i])){
			$course = $DB->get_record('course', array('id' => $courses[$i]));
			$result=enrollment_enrol_user($user, $course->id, $role, null, null);
		}
	}
	echo "<i class='fa fa-check-circle-o' aria-hidden='true' style='color:green;'></i>&nbsp;Enrolment Successful";
}else{
	echo "<p style='color:red;'>Unable to enroll. Please check</p>";
}
