<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles uploading files
 *
 * @package    local_phishing
 * @copyright  Prashant Yallatti<prashant@elearn10.com>
 * @copyright  Dhruv Infoline Pvt Ltd <lmsofindia.com>
 * @license    http://www.lmsofindia.com 2017 or later
 */
require_once('../lib.php');
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
require_once($CFG->libdir.'/formslib.php');
class template_copy_form extends moodleform {
	function definition() {
		global $CFG,$USER;
		$id = optional_param('id',null, PARAM_INT);
		$edit = optional_param('edit',null, PARAM_INT);
		//manju: changing plugin name local_gophish to local_phishing. 09/02/2021.
		$companyid = $USER->company->id;
		$apikey = get_config('local_phishing'.$companyid, 'api');
		$urlip = get_config('local_phishing'.$companyid, 'urlip');
		$templateValue = get_template_value($urlip,$apikey,$id);

		// print_object($templateValue);	
		$mform = $this->_form; 
		//orgaziation name field	
		$context = context_system::instance();
		$mform->addElement('static','templatehrd',get_string('templatecopy','local_phishing'),'');
		$mform->addElement('html', '<hr>');
		// Add some extra hidden fields.
		$mform->addElement('hidden', 'id',$id);
        //$mform->setType('id', core_user::get_property_type('id'));
		$mform->setType('id', PARAM_INT);






		//name of the template
		$mform->addElement('text', 'name',
         get_string('temp_name','local_phishing')); // Add elements to your form
		$mform->addRule('name', get_string('required'), 'required', null, 'client');
		// $mform->addElement('static','name1','',get_string('tempname_name','local_phishing'));
		$mform->setType('name', PARAM_TEXT);
		$mform->setDefault('name', 'Copy of '.$templateValue->name);

		//subject name
		$mform->addElement('text', 'subject',
         get_string('subname','local_phishing')); // Add elements to your form
		$mform->addRule('subject', get_string('required'), 'required', null, 'client');
		// $mform->addElement('static','subject_name','',get_string('subject_name','local_phishing'));
		$mform->setType('subject', PARAM_TEXT);
		$mform->setDefault('subject', $templateValue->subject);
		//html editor

		/*//subject text
		$mform->addElement('textarea', 'text', get_string("text", "local_phishing"), 'wrap="virtual" rows="10" cols="50"');
		$mform->addElement('static','text_help','',get_string('text_help','local_phishing'));

		$mform->setType('text', PARAM_TEXT);*/
		$mform->addElement('textarea', 'email_content',
         get_string('import_content','local_phishing'),'wrap="virtual" rows="20" cols="50"'); // Add elements to your form
		//$mform->addRule('subject', get_string('required'), 'required', null, 'client');
		$mform->setType('url', PARAM_TEXT);
		
		$mform->addElement('advcheckbox', 'convert_links_checkbox', '', get_string('checkboxtemp','local_phishing'), array('group' => 1), array(0, 1));
		$mform->addElement('button', 'url_insert', get_string("import",'local_phishing'),array('onclick'=>'importEmail()'));


		$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'noclean' => true, 'context' => $context);
		$mform->addElement('editor','html',get_string('html','local_phishing'),null,$editoroptions)->setValue(['text' => $templateValue->html , 'format' => FORMAT_HTML]);
		// $mform->addElement('static','html_help','',get_string('html_help','local_phishing'));
		
		$mform->addElement('checkbox', 'checkboxtext',
         get_string('checkboxtext','local_phishing')); // Add elements to your form
		if (strpos($templateValue->html, '{{.Tracker}}') !== false) {
			$mform->setDefault('checkboxtext', 1);
		}
         $mform->setType('checkboxtext', PARAM_TEXT);
         $mform->addElement('static','','',get_string('imagedesc','local_phishing'));
         $mform->addElement('filemanager', 'attachments', get_string('attachments', 'local_phishing'), null,
         	array('subdirs' => 0, 'maxbytes' => '0','maxfiles' => 50,'context'=>$context,'accepted_types'=>array('.png','.jpg')));
			// $mform->addElement('static','addfile_help','',get_string('addfile_help','local_phishing'));

		//value can store here editable 
        $mform->addElement('hidden', 'editorvalue','',array('id'=>'editorvalueid'));
		$mform->setType('editorvalue', PARAM_RAW);

         $this->add_action_buttons();
     }
 }
